<div id="item_one">
    <div class="row mb-3">
        <div class="col mx-auto">
            <label for="" class="line w-100 lead font-weight-bold text-dark">Datos personales</label>
        </div>
    </div>
    <div class="row">
        <div class="col col_form_input">
            <label for="document_type_id" class="font-weight-bold h4">
                Tipo de identifcación <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span>
            </label>
            <div class="form-group has-default mb-5">
                <div class="input-group">
                    <select required name="document_type_id" id="document_type_id"
                        @class([
                            'form-control',
                            'w-100',
                            'is-invalid' => $errors->has('document_type_id'),
                        ])>
                        <option value="" @selected(empty(old('document_type_id'))) disabled>Seleccione una opción...</option>
                        @foreach ($documentTypes as $document)
                            <option value="{{ $document->id }}" @selected(old('document_type_id') == $document->id)>{{ $document->name }}</option>
                        @endforeach
                    </select>
                    {{-- Validador con JS --}}
                    <div class="invalid-feedback font-weight-bold position-absolute" style="margin-top: 7%" id="document_type_id_validate" style="display: none"></div>
                    {{-- FIN Validador con JS --}}
                    @error('document_type_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col col_form_input">
            <label for="document_number" class="font-weight-bold h4">
                Número de identificación <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span>
            </label>
            <div class="form-group has-default">
                <div class="input-group">
                    <input type="text" name="document_number" id="document_number" required value="{{ old('document_number') }}"
                        @class([
                            'form-control',
                            'is-invalid' => $errors->has('document_number'),
                        ])>
                    @error('document_number')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <span id="text_alert">Digite su número de identificación sin puntos ni comas.</span>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col col_form_input">
            <label for="name" class="font-weight-bold h4">
                Nombre completo <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span>
            </label>
            <div class="form-group has-default">
                <div class="input-group">
                    <input type="text" name="name" id="name" required value=" {{ old('name') }}" @class(['form-control', 'is-invalid' => $errors->has('name')])>
                    {{-- Validador con JS --}}
                    <div class="invalid-feedback font-weight-bold" id="name_validate" style="display: none"></div>
                    {{-- FIN Validador con JS --}}
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col col_form_input">
            <label for="birth_date" class="font-weight-bold h4">
                Fecha de nacimiento <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span>
            </label>
            <div class="form-group has-default">
                <div class="input-group">
                    <input type="date" name="birth_date" id="birth_date" onchange="validate_date(event);" required @class(['form-control', 'is-invalid' => $errors->has('birth_date')]) value="{{ old('birth_date') }}">
                    {{-- Validador con JS --}}
                    <div class="invalid-feedback font-weight-bold" id="birth_date_validate"style="display: none"></div>
                    {{-- FIN Validador con JS --}}
                    @error('birth_date')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col col_form_input">
            <label for="age" class="font-weight-bold h4">Edad</label>
            <div class="form-group has-default">
                <div class="input-group">
                    <input type="number" name="age" id="age" class="form-control" placeholder="Edad del paciente" required disabled value="{{ old('age') }}">
                    <div class="invalid-feedback"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col col_form_input">
            <label for="gender" class="font-weight-bold h4">
                Sexo <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span>
            </label>
            <div class="form-group has-default">
                <div class="input-group">
                    <select required name="gender" id="gender"
                        @class([
                            'form-control',
                            'w-100',
                            'is-invalid' => $errors->has('gender'),
                        ])>
                        <option @selected(empty(old('gender'))) disabled>Seleccione una opción...</option>
                        <option @selected(old('gender') == 'Male') value="Male">Hombre</option>
                        <option @selected(old('gender') == 'Female') value="Female">Mujer</option>
                    </select>
                    {{-- Validador con JS --}}
                    <div class="invalid-feedback font-weight-bold" id="gender_validate" style="margin-top: 7%; display: none"></div>
                    {{-- FIN Validador con JS --}}
                    @error('gender')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col col_form_input">
            <label for="city_id" class="font-weight-bold h4">
                Ciudad <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span>
            </label>
            <div class="form-group has-default  mb-5">
                <div class="input-group">
                    <select name="city_id" id="city" required
                        @class([
                            'form-control',
                            'w-100',
                            'is-invalid' => $errors->has('city_id'),
                        ])>
                        <option value="" @selected(empty(old('city_id'))) disabled>Seleccione una ciudad...</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}" @selected(old('city_id') == $city->id)>{{ $city->name }}</option>
                        @endforeach
                    </select>
                    {{-- Validador con JS --}}
                    <div class="invalid-feedback font-weight-bold" id="city_validate" style="margin-top: 7%; display: none"></div>
                    {{-- FIN Validador con JS --}}
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col col_form_input">
            <label for="email" class="font-weight-bold h4">Correo electrónico</label>
            <div class="form-group has-default">
                <div class="input-group">
                    <input type="email" name="email" id="email" required value="{{ old('email') }}" @class(['form-control', 'is-invalid' => $errors->has('email')])>
                    {{-- Validador con JS --}}
                    <div class="invalid-feedback font-weight-bold" id="email_validate" style="display: none"></div>
                    {{-- FIN Validador con JS --}}
                    @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col col_form_input">
            <label for="phone" class="font-weight-bold h4">
                Número de contacto <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span>
            </label>
            <div class="form-group has-default">
                <div class="input-group">
                    <input type="number" name="phone" id="phone" required value="{{ old('phone') }}" @class(['form-control', 'is-invalid' => $errors->has('phone')])>
                    {{-- Validador con JS --}}
                    <div class="invalid-feedback font-weight-bold" id="phone_validate" style="display: none"></div>
                    {{-- FIN Validador con JS --}}
                    @error('phone')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
    </div>

    <div class="row my-3">
        <div class="col col_form_input d-flex justify-content-center">
            <a href="#" id="bottom_one" class="btn btn-white"
                style="border: solid #000 2px; width: 210px; height: auto; font-weight: 700; font-size: 14px; color: #000">CONTINUAR</a>
        </div>
    </div>
</div>
