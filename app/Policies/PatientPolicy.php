<?php

namespace App\Policies;

use App\Enums\Patient\PatientStatus;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PatientPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Patient  $patient
     * @return bool|void
     */
    public function update(User $user, Patient $patient)
    {
        if ($patient->patient_status == PatientStatus::OutsideOfProgram || $patient->deleted_at !== null) {
            return false;
        }

        if ($user->hasPermissionTo('actualizar pacientes')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Patient  $patient
     * @return bool|void
     */
    public function delete(User $user, Patient $patient)
    {
        if ($patient->patient_status == PatientStatus::OutsideOfProgram || $patient->deleted_at !== null) {
            return false;
        }

        if ($user->hasPermissionTo('eliminar pacientes')) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Patient  $patient
     * @return bool|void
     */
    public function restore(User $user, Patient $patient)
    {
        if ($patient->patient_status == PatientStatus::OutsideOfProgram || $patient->deleted_at === null) {
            return false;
        }

        if ($user->hasPermissionTo('restaurar pacientes')) {
            return true;
        }
    }
}
