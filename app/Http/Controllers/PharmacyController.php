<?php

namespace App\Http\Controllers;

use App\Http\Requests\Pharmacy\StoreRequest;
use App\Http\Requests\Pharmacy\UpdateRequest;
use App\Http\Resources\PharmacyResource;
use App\Models\Country;
use App\Models\Pharmacy;
use App\Models\PharmacyChain;
use App\Services\DocumentType\DocumentTypeService;
use App\Services\Pharmacy\PharmacyService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PharmacyController extends Controller
{
    use ApiResponses;

    /** @var PharmacyService */
    private $pharmacyService;

    /** @var DocumentTypeService */
    private $documentTypeService;

    public function __construct(PharmacyService $pharmacyService, DocumentTypeService $documentTypeService)
    {
        $this->pharmacyService = $pharmacyService;
        $this->documentTypeService = $documentTypeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Pharmacy::class);

        $countries = Country::get();
        $documentTypes = $this->documentTypeService->list();
        $pharmacyChains = PharmacyChain::get();

        $pharmacies = $this->pharmacyService->list(false);

        return view('supervisor.pharmacy.list', compact('pharmacies', 'countries', 'documentTypes', 'pharmacyChains'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Pharmacy::class);
            $pharmacy = $this->pharmacyService->store($request->validated());

            return $this->resourceResponse(
                message: 'Farmacia creada correctamente.',
                data: new PharmacyResource($pharmacy),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Pharmacy  $pharmacy
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Pharmacy $pharmacy)
    {
        if ($request->ajax()) {
            $this->authorize('update', $pharmacy);

            $pharmacy->load('city');

            return $this->resourceResponse(
                message: '',
                data: new PharmacyResource($pharmacy)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Pharmacy  $pharmacy
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Pharmacy $pharmacy)
    {
        if ($request->ajax()) {
            $this->authorize('update', $pharmacy);
            $this->pharmacyService->update($pharmacy, $request->validated());
            $pharmacy->refresh();

            return $this->resourceResponse(
                message: 'Farmacia actualizada correctamente.',
                data: new PharmacyResource($pharmacy)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
