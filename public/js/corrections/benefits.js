axios.defaults.headers.common = {
    "Accepts": "Application/json",
    "X-Requested-With": "XMLHttpRequest",
};

document.addEventListener("DOMContentLoaded", () => {
    const benefitType = document.getElementById("benefit_type");
    /**
     * @type {HTMLSelectElement}
     */
    const benefitStatus = document.getElementById("status");
    const authorizationLetterStatus = document.getElementById("autorized");
    const acceptedSpecialDiscountStatus = document.getElementById("is_accepted");
    const authorizationLetterRejectionReason = document.getElementById("causal_types");
    const invoiceNumberUnits = document.getElementById("number_units");
    const invoiceFarmacy = document.getElementById("pharmacy_id");
    const invoiceDiscount = document.getElementById("discount");
    const invoiceTransactionDate = document.getElementById("transaction_date");
    const invoiceNumber = document.getElementById("invoice_number");
    const invoiceFile = document.getElementById("factura");
    const closingReason = document.getElementById("closing_reason");

    const formulario = document.getElementById("formulario");

    benefitType.addEventListener("change", validateBenefitType);
    validateBenefitType();

    benefitStatus.addEventListener("change", validateStatusFlow);
    validateStatusFlow();

    authorizationLetterStatus.addEventListener("change", validateAuthorizationLetterStatus);
    validateAuthorizationLetterStatus();

    formulario.addEventListener("submit", submitForm);

    function validateStatusFlow() {
        const currentStatus = parseInt(benefitStatus.value, 10);

        switch (currentStatus) {
            case 0: // Cerrado
                if (benefitType.value == "Copago") {
                    showAuthorizationLetterFields();
                } else {
                    hideAuthorizationLetterFields();
                }

                showInvoiceFields();
                showClosedBenefitField();
                break;
            case 1: // Carta pendiente
                hideAuthorizationLetterFields();
                authorizationLetterStatus.value = "Pendiente";
                hideInvoiceFields();
                hideClosedBenefitField();
                break;
            case 2: // Descuento especial rechazado
                showAuthorizationLetterFields();
                authorizationLetterStatus.value = "Rechazado";
                authorizationLetterStatus.disabled = true;
                authorizationLetterStatus.required = false;
                authorizationLetterStatus.closest("li").classList.add("inactive");
                authorizationLetterStatus.closest("li").classList.remove("active");
                acceptedSpecialDiscountStatus.value = "0";
                acceptedSpecialDiscountStatus.disabled = true;
                acceptedSpecialDiscountStatus.required = false;
                acceptedSpecialDiscountStatus.closest("li").classList.add("inactive");
                acceptedSpecialDiscountStatus.closest("li").classList.remove("active");
                hideInvoiceFields();
                hideClosedBenefitField();
                break;
            case 3: // Factura pendiente
                hideAuthorizationLetterFields();
                if (benefitType.value == "Copago") {
                    authorizationLetterStatus.disabled = false;
                    authorizationLetterStatus.required = true;
                    authorizationLetterStatus.closest("li").classList.remove("inactive");
                    authorizationLetterStatus.closest("li").classList.add("active");
                }
                hideInvoiceFields();
                hideClosedBenefitField();
                break;
            case 4: // Factura cargada
                hideAuthorizationLetterFields();
                if (benefitType.value == "Copago") {
                    authorizationLetterStatus.value = "Aprobado";
                }
                showInvoiceFields();
                hideClosedBenefitField();
                break;
            default: // Proceso observado
                hideAuthorizationLetterFields();
                authorizationLetterStatus.value = "Observada";
                hideInvoiceFields();
                hideClosedBenefitField();
                break;
        }
    };

    function validateBenefitType() {
        Array.from(benefitStatus.options).forEach(option => {
            if (["1", "2", "5"].includes(option.value)) {
                option.disabled = benefitType.value == "OOP";
                if (benefitType.value == "OOP") {
                    if (!option.selected) {
                        option.style.display = "none";
                    }
                } else {
                    option.style.removeProperty("display");
                }
            }
        });
        validateStatusFlow();
        validateAuthorizationLetterStatus();
    };

    function validateAuthorizationLetterStatus() {
        if (benefitType.value == "OOP") {
            hideAuthorizationLetterFields();
        } else {
            switch (authorizationLetterStatus.value) {
                case "Rechazado":
                    if (benefitStatus.value == "3") {// Factura pendiente
                        acceptedSpecialDiscountStatus.value = 1;
                        acceptedSpecialDiscountStatus.disabled = true;
                        acceptedSpecialDiscountStatus.required = false;
                        authorizationLetterRejectionReason.disabled = false;
                        authorizationLetterRejectionReason.required = true;
                        authorizationLetterRejectionReason.closest("li").classList.remove("inactive");
                        authorizationLetterRejectionReason.closest("li").classList.add("active");
                    } else {
                        acceptedSpecialDiscountStatus.disabled = false;
                        acceptedSpecialDiscountStatus.required = true;
                        authorizationLetterRejectionReason.disabled = false;
                        authorizationLetterRejectionReason.required = true;
                        authorizationLetterRejectionReason.closest("li").classList.remove("inactive");
                        authorizationLetterRejectionReason.closest("li").classList.add("active");
                    }
                    break;
                default:
                    acceptedSpecialDiscountStatus.disabled = true;
                    acceptedSpecialDiscountStatus.required = false;
                    acceptedSpecialDiscountStatus.value = "";
                    authorizationLetterRejectionReason.disabled = true;
                    authorizationLetterRejectionReason.required = false;
                    authorizationLetterRejectionReason.closest("li").classList.add("inactive");
                    authorizationLetterRejectionReason.closest("li").classList.remove("active");
                    authorizationLetterRejectionReason.value = "";
                    break;
            }
        }
    }

    function showAuthorizationLetterFields() {
        authorizationLetterStatus.disabled = false;
        authorizationLetterStatus.required = true;
        authorizationLetterStatus.closest("li").classList.remove("inactive");
        authorizationLetterStatus.closest("li").classList.add("active");
        acceptedSpecialDiscountStatus.disabled = false;
        acceptedSpecialDiscountStatus.required = true;
        acceptedSpecialDiscountStatus.closest("li").classList.remove("inactive");
        acceptedSpecialDiscountStatus.closest("li").classList.add("active");
        authorizationLetterRejectionReason.disabled = false;
        authorizationLetterRejectionReason.required = true;
        authorizationLetterRejectionReason.closest("li").classList.remove("inactive");
        authorizationLetterRejectionReason.closest("li").classList.add("active");
    }
    function hideAuthorizationLetterFields() {
        authorizationLetterStatus.disabled = true;
        authorizationLetterStatus.required = false;
        authorizationLetterStatus.closest("li").classList.add("inactive");
        authorizationLetterStatus.closest("li").classList.remove("active");
        authorizationLetterStatus.value = "";
        acceptedSpecialDiscountStatus.disabled = true;
        acceptedSpecialDiscountStatus.required = false;
        acceptedSpecialDiscountStatus.closest("li").classList.add("inactive");
        acceptedSpecialDiscountStatus.closest("li").classList.remove("active");
        acceptedSpecialDiscountStatus.value = "";
        authorizationLetterRejectionReason.disabled = true;
        authorizationLetterRejectionReason.required = false;
        authorizationLetterRejectionReason.closest("li").classList.add("inactive");
        authorizationLetterRejectionReason.closest("li").classList.remove("active");
        authorizationLetterRejectionReason.value = "";
    }
    function showInvoiceFields() {
        invoiceNumberUnits.disabled = false;
        invoiceNumberUnits.required = true;
        invoiceNumberUnits.closest("li").classList.remove("inactive");
        invoiceNumberUnits.closest("li").classList.add("active");
        invoiceFarmacy.disabled = false;
        invoiceFarmacy.required = true;
        invoiceFarmacy.closest("li").classList.remove("inactive");
        invoiceFarmacy.closest("li").classList.add("active");
        invoiceDiscount.disabled = false;
        invoiceDiscount.required = true;
        invoiceDiscount.closest("li").classList.remove("inactive");
        invoiceDiscount.closest("li").classList.add("active");
        invoiceTransactionDate.disabled = false;
        invoiceTransactionDate.required = true;
        invoiceTransactionDate.closest("li").classList.remove("inactive");
        invoiceTransactionDate.closest("li").classList.add("active");
        invoiceNumber.disabled = false;
        invoiceNumber.required = true;
        invoiceNumber.closest("li").classList.remove("inactive");
        invoiceNumber.closest("li").classList.add("active");
        invoiceFile.disabled = false;
        invoiceFile.required = true;
        invoiceFile.closest("li").classList.remove("inactive");
        invoiceFile.closest("li").classList.add("active");
    }
    function hideInvoiceFields() {
        invoiceNumberUnits.disabled = true;
        invoiceNumberUnits.required = false;
        invoiceNumberUnits.value = "";
        invoiceNumberUnits.closest("li").classList.add("inactive");
        invoiceNumberUnits.closest("li").classList.remove("active");
        invoiceFarmacy.disabled = true;
        invoiceFarmacy.required = false;
        invoiceFarmacy.value = "";
        invoiceFarmacy.closest("li").classList.add("inactive");
        invoiceFarmacy.closest("li").classList.remove("active");
        invoiceDiscount.disabled = true;
        invoiceDiscount.required = false;
        invoiceDiscount.value = "";
        invoiceDiscount.closest("li").classList.add("inactive");
        invoiceDiscount.closest("li").classList.remove("active");
        invoiceTransactionDate.disabled = true;
        invoiceTransactionDate.required = false;
        invoiceTransactionDate.value = "";
        invoiceTransactionDate.closest("li").classList.add("inactive");
        invoiceTransactionDate.closest("li").classList.remove("active");
        invoiceNumber.disabled = true;
        invoiceNumber.required = false;
        invoiceNumber.value = "";
        invoiceNumber.closest("li").classList.add("inactive");
        invoiceNumber.closest("li").classList.remove("active");
        invoiceFile.disabled = true;
        invoiceFile.required = false;
        invoiceFile.value = "";
        invoiceFile.closest("li").classList.add("inactive");
        invoiceFile.closest("li").classList.remove("active");
    }
    function showClosedBenefitField() {
        closingReason.disabled = false;
        closingReason.required = true;
        closingReason.closest("li").classList.remove("inactive");
        closingReason.closest("li").classList.add("active");
    }
    function hideClosedBenefitField() {
        closingReason.disabled = true;
        closingReason.required = false;
        closingReason.value = "";
        closingReason.closest("li").classList.add("inactive");
        closingReason.closest("li").classList.remove("active");
    }

    function submitForm(e) {
        e.preventDefault();

        Swal.fire({
            title: "¿Desea corregir este beneficio?",
            text: "Los cambios que está a punto de guardar pueden generar inconsistencias y/o errores en los reportes y beneficios actuales.",
            icon: "warning",
            showDenyButton: true,
            confirmButtonText: 'Comprendo los riesgos y deseo continuar',
            denyButtonText: "Cancelar",
        })
            .then(response => {
                if (response.isConfirmed) {
                    const data = new FormData();

                    data.append("benefit_type", benefitType.value);
                    data.append("status", benefitStatus.value);
                    data.append("autorized", authorizationLetterStatus.value);
                    data.append("is_accepted", acceptedSpecialDiscountStatus.value);
                    data.append("causal_types", authorizationLetterRejectionReason.value);
                    data.append("number_units", invoiceNumberUnits.value);
                    data.append("pharmacy_id", invoiceFarmacy.value);
                    data.append("discount", invoiceDiscount.value);
                    data.append("transaction_date", invoiceTransactionDate.value);
                    data.append("invoice_number", invoiceNumber.value);
                    data.append("factura", invoiceFile.files[0]);
                    data.append("closing_reason", closingReason.value);

                    Array.from(formulario.elements).forEach(input => {
                        if (input.classList.contains('form-control') && input.nextElementSibling && input.nextElementSibling.classList.contains("invalid-feedback")) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    axios.post(location.pathname, data)
                        .then(response => {
                            Swal.fire({
                                title: response.data.message,
                                icon: 'success'
                            })
                                .then(() => {
                                    location.replace("/corrections/benefits");
                                });
                        })
                        .catch(error => {
                            Swal.fire({
                                title: "El beneficio no pudo ser actualizado",
                                text: error.response.data.message,
                                icon: 'error'
                            });

                            if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                                for (const key in error.response.data.errors) {
                                    if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                        const input = formulario.elements[key];
                                        input.classList.add('is-invalid');
                                        input.nextElementSibling.textContent = error.response.data.errors[key][0];
                                    }
                                }
                            }
                        });
                }
            });
    }
});
