<?php

namespace App\Services\Pathology;

use App\Models\Pathology;
use Illuminate\Support\Facades\DB;

class PathologyService
{
    /**
     * Returns a list of pathologies.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Pathology>
     */
    public function list($onlyActive = true)
    {
        $pathologies = Pathology::query();

        if ($onlyActive) {
            $pathologies->active();
        }

        return $pathologies->get();
    }

    /**
     * Create a Patology.
     *
     * @param  array  $input
     * @return Pathology
     */
    public function store(array $input): Pathology
    {
        /** @var Pathology|null */
        $pathology = null;

        DB::transaction(function () use (&$pathology, $input) {
            $pathology = Pathology::create($input);
            $pathology->medicineDoses()->attach($input['doses']);
        });

        /** @var Pathology $pathology */
        return $pathology;
    }

    public function update(Pathology $pathology, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use (&$updated, $pathology, $input) {
            $updated = $pathology->update($input);
            $pathology->medicineDoses()->sync($input['doses']);
        });

        return $updated;
        // return $pathology->update($input);
    }
}
