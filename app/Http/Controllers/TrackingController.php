<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tracking\UpdateRequest;
use App\Models\Country;
use App\Models\DocumentType;
use App\Models\InsuranceCarrier;
use App\Models\Patient;
use App\Models\Tracking;
use App\Services\Tracking\TrackingService;

class TrackingController extends Controller
{
    /** @var TrackingService */
    private $trackingService;

    public function __construct(TrackingService $trackingService)
    {
        $this->trackingService = $trackingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $trackings = $this->trackingService->list();

        return view('tracking.tracking_management');
    }

    /**
     * Returns the list of patients.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function patients()
    {
        $patients = Patient::get();

        return view('tracking.patients', compact('patients'));
    }

    /**
     * Shows the relationships for the specified resource.
     *
     * @param  Tracking  $tracking
     * @return \Illuminate\Contracts\View\View
     */
    public function managementTracking(Tracking $tracking)
    {
        $documentTypes = DocumentType::select('id', 'name')->get();
        $countries = Country::select('id', 'name')->get();
        $insurance_carriers = InsuranceCarrier::select('id', 'name')->get();
        $trackings = Tracking::where('patient_id', $tracking->patient->id)->get();
        $tracking->load([
            'patient.city.country',
            'patient.documentType',
            'patient.lastBenefit',
            'patient.treatment.currentFormulation.pathologyHasMedicineDose.medicineDose.medicine',
            'patient.treatment.currentFormulation.pathologyHasMedicineDose.medicineDose.dose',
            'patient.treatment.currentFormulation.pathologyHasMedicineDose.pathology',
            'patient.treatment.doctor',
        ])->get();
        // $cities = City::where('country_id', $tracking->patient->city->country_id)->select('id', 'name')->get();
        return view('tracking.tracking_management', compact('trackings', 'tracking', 'documentTypes', 'countries', 'insurance_carriers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Patient  $patient
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Patient $patient)
    {
        $inputTracking = [
            'user_id' => auth()->id(),
            'patient_id' => $patient->id,
        ];

        $tracking = TrackingService::store($inputTracking);

        return redirect()->route('tracking.management', $tracking->id);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show()
    {
        $tracking = Tracking::all();

        return view('tracking.show_tracking', compact('tracking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Tracking  $tracking
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Tracking $tracking)
    {
        return view('tracking.edit_tracking', compact('tracking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Tracking  $tracking
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Tracking $tracking)
    {
        $tracking->fill($request->validated())->save();

        return $request->wantsJson() ?
        response()->json([
            'message' => 'Seguimiento modificado correctamente',
            'data' => $tracking,
        ]) :
        redirect()->back()->with('edit', 'Seguimiento modificado correctamente');
    }
}
