@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-danger h2">Medicinas</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse">
        <div class="col d-flex justify-content-end mt-3">
            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            @can('crear medicinas', \App\Models\Admin\Medicine::class)
                <button type="button" class="btn bg-danger d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearMedicina">
                    <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>Crear medicina
                </button>
            @endcan
        </div>
        <div class="col">

            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-danger">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre de la medicina</th>
                        <th scope="col">Presentaciones</th>
                        <th scope="col">Estado</th>
                        @can('actualizar medicinas')
                            <th scope="col">Acciones</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach ($medicines as $key => $medicine)
                        <tr>
                            <td align="center" style="width: 50px">{{ $key + 1 }}</td>
                            <td>{{ $medicine->name }}</td>
                            <td>{{ $medicine->doses->pluck('presentation')->join(', ', ' y ') }}</td>
                            @if ($medicine->is_active == 0)
                                <td>Inactivo</td>
                            @elseif ($medicine->is_active == 1)
                                <td>Activo</td>
                            @endif
                            @can('actualizar medicinas', $medicine)
                                <td>
                                    <div class="col d-flex justify-content-center" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-secondary bg-danger edit-button" type="button" style="color: white" href="{{ route('admin.medicines.update', $medicine) }}" data-id="{{ $medicine->id }}">
                                            <span class="iconify" data-icon="bxs:edit"></span>&nbsp;
                                            Editar
                                        </a>
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Modal crear medicinas --}}
    <div class="modal fade" id="crearMedicina" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear medicina</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('admin.medicines.store') }}" method="POST" id="create-medicine">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombre de la medicina" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="" class="lead text-danger font-weight-bold">Presentaciones</label>
                                            </div>
                                            <div class="row">
                                                @foreach ($doses->chunk(2) as $item)
                                                    <div class="col">
                                                        <ul class="list-group">
                                                            @foreach ($item as $dose)
                                                                <li class="list-group-item">
                                                                    <input class="switch" type="checkbox" name="doses" id="doses" value="{{ $dose->id }}" autocomplete="off">
                                                                    {{ $dose->presentation }}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endforeach
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal editar medicinas --}}
    <div class="modal fade" id="updateMedicine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Editar medicina</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="#" method="POST" id="update-medicine">
                                @csrf
                                @method('PUT')
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name2" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name2" id="name2" class="form-control" placeholder="Nombre de la medicina" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-2">
                                                <label for="is_active2" class="lead text-danger font-weight-bold">Estado</label>
                                            </div>
                                            <div class="col-6">
                                                <select name="is_active2" id="is_active2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    <option value="0">Inactivo</option>
                                                    <option value="1">Activo</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-2">
                                                <label for="dose_id2" class="lead text-danger font-weight-bold">Presentaciones</label>
                                            </div>
                                            <div class="row">
                                                @foreach ($doses->chunk(2) as $item)
                                                    <div class="col">
                                                        <ul class="list-group">
                                                            @foreach ($item as $dose)
                                                                <li class="list-group-item">
                                                                    <input class="switch" type="checkbox" name="doses2" id="dose-{{ $dose->id }}" value="{{ $dose->id }}" autocomplete="off">
                                                                    {{ $dose->presentation }}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endforeach
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };


        //Funcion modal create
        const form = document.getElementById('create-medicine');

        const create = e => {
            e.preventDefault();
            const data = {
                name: document.getElementById('name').value,
                doses: Array.from(document.querySelectorAll('[name=doses]:checked')).map(el => el.value),
            }

            axios.post('/admin/management/medicines', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        confirmButtonColor: '#3085d6',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }

        form.addEventListener('submit', create);
        //Fin función modal create


        // Función modal update
        const updateMedicineForm = document.getElementById('update-medicine');

        const editMedicine = e => {
            e.preventDefault();

            axios.get(`/admin/management/medicines/${e.target.closest('.edit-button').dataset.id}`)
                .then((response) => {
                    const medicine = response.data.data;

                    updateMedicineForm.dataset.id = medicine.id;
                    updateMedicineForm.elements['name2'].value = medicine.name;
                    medicine.doses.forEach(dose => {
                        document.getElementById(`dose-${dose.id}`).checked = true;
                    });
                    updateMedicineForm.elements['is_active2'].value = medicine.is_active;

                    Array.from(updateMedicineForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    $('#updateMedicine').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#3085d6',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editMedicine);
        });

        const updateMedicine = e => {
            e.preventDefault();

            const updateMedicineData = {
                name: document.getElementById('name2').value,
                doses: Array.from(document.querySelectorAll('[name=doses2]:checked')).map(el => el.value),
                is_active: document.getElementById('is_active2').value,
            }

            axios.put(
                    `/admin/management/medicines/${updateMedicineForm.dataset.id}`,
                    updateMedicineData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updatePathology').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                        confirmButtonColor: '#3085d6',
                    });

                    Array.from(updateMedicineForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updateMedicineForm.elements[`${key}2`];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updateMedicineForm.addEventListener('submit', updateMedicine);
        // Fin función modal update
    </script>
    <style>
        .switch {
            --inactive-bg: #cfd8dc;
            --active-bg: #00e676;
            --size: 20px;
            appearance: none;
            width: calc(var(--size) * 2.2);
            height: var(--size);
            display: inline-block;
            border-radius: calc(var(--size) / 2);
            cursor: pointer;
            background-color: var(--inactive-bg);
            background-image: radial-gradient(circle calc(var(--size) / 2.1),
                    #fff 100%,
                    #0000 0),
                radial-gradient(circle calc(var(--size) / 1.0), #0003 0%, #0000 100%);
            background-repeat: no-repeat;
            background-position: calc(var(--size) / -1.75) 0;
            transition: background 0.2s ease-out;
        }

        .switch:checked {
            background-color: var(--active-bg);
            background-position: calc(var(--size) / 1.75) 0;
        }
    </style>
@endsection
