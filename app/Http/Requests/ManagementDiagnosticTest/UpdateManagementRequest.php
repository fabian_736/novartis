<?php

namespace App\Http\Requests\ManagementDiagnosticTest;

use Illuminate\Foundation\Http\FormRequest;

class UpdateManagementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'management_date' => ['required', 'date_format:Y-m-d', 'before_or_equal:today', 'after_or_equal:'.$this->management_diagnostic_test->created_at->toDateString()],
            'diagnostic_test_id' => ['required', 'exists:diagnostic_tests,id'],
            'laboratory_id' => ['required', 'exists:laboratories,id'],
        ];
    }

    public function attributes()
    {
        return [
            'management_date' => 'Fecha de coordinación',
            'diagnostic_test_id' => 'Prueba a realizar',
            'laboratory_id' => 'Laboratorio',
        ];
    }
}
