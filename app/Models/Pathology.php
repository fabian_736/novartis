<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperPathology
 */
class Pathology extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'name',
        'is_active',
        'medicine_id',
    ];

    /**
     * Scope a query to only include active pathologies.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Obtiene a los Tratamientos asociados a la Patología.
     *
     * @return HasMany<Treatment>
     */
    public function treatments(): HasMany
    {
        return $this->hasMany(Treatment::class);
    }

    /**
     * Retrieves the medicine.
     *
     * @return BelongsTo<Medicine, Pathology>
     */
    public function medicine(): BelongsTo
    {
        return $this->belongsTo(Medicine::class);
    }

    /**
     * Retrieves the medicine doses.
     *
     * @return BelongsToMany<MedicineDose>
     */
    public function medicineDoses(): BelongsToMany
    {
        return $this->belongsToMany(MedicineDose::class, 'pathology_has_medicine_doses', 'pathology_id', 'medicine_dose_id')
            ->using(PathologyHasMedicineDose::class)
            ->withPivot(['id'])
            ->withTimestamps();
    }
}
