<?php

namespace App\Http\Controllers;

use App\Http\Requests\Laboratory\StoreRequest;
use App\Http\Requests\Laboratory\UpdateRequest;
use App\Http\Resources\Laboratory\LaboratoryResource;
use App\Models\Country;
use App\Models\Laboratory;
use App\Services\DocumentType\DocumentTypeService;
use App\Services\Laboratory\LaboratoryService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LaboratoryController extends Controller
{
    use ApiResponses;

    /** @var LaboratoryService */
    private $laboratoryService;

    /** @var DocumentTypeService */
    private $documentTypeService;

    public function __construct(LaboratoryService $laboratoryService, DocumentTypeService $documentTypeService)
    {
        $this->laboratoryService = $laboratoryService;
        $this->documentTypeService = $documentTypeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Laboratory::class);

        $countries = Country::get();
        $documentTypes = $this->documentTypeService->list();
        $laboratories = $this->laboratoryService->list(false);

        return view('supervisor.laboratory.list', compact('laboratories', 'countries', 'documentTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Laboratory::class);
            $laboratory = $this->laboratoryService->store($request->validated());

            return $this->resourceResponse(
                message: 'Laboratorio creado correctamente.',
                data: new LaboratoryResource($laboratory),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Laboratory  $laboratory
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Laboratory $laboratory)
    {
        if ($request->ajax()) {
            $this->authorize('update', $laboratory);

            $laboratory->load('city');

            return $this->resourceResponse(
                message: '',
                data: new LaboratoryResource($laboratory)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Laboratory  $laboratory
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Laboratory $laboratory)
    {
        if ($request->ajax()) {
            $this->authorize('update', $laboratory);
            $this->laboratoryService->update($laboratory, $request->validated());
            $laboratory->refresh();

            return $this->resourceResponse(
                message: 'Laboratorio actualizado correctamente.',
                data: new LaboratoryResource($laboratory)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
