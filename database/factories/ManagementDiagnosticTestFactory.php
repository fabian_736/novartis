<?php

namespace Database\Factories;

use App\Models\DiagnosticTest;
use App\Models\Laboratory;
use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ManagementDiagnosticTest>
 */
class ManagementDiagnosticTestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'patient_id' => Patient::factory(),
            'status' => 1,
        ];
    }

    /**
     * Indicate that the model's email address should be managed.
     *
     * @return static
     */
    public function managed()
    {
        return $this->state(function (array $attributes) {
            return [
                'management_date' => now(),
                'diagnostic_test_id' => DiagnosticTest::factory(),
                'laboratory_id' => Laboratory::factory(),
                'status' => 2,
            ];
        });
    }

    /**
     * Indicate that the model's email address should be executed.
     *
     * @return static
     */
    public function executed()
    {
        return $this->managed()->state(function (array $attributes) {
            return [
                'test_date' => now(),
                'invoice_number' => $this->faker->regexify('[A-Z0-9]{5-20}'),
                'test_price' => $this->faker->randomNumber(),
                'status' => 0,
            ];
        });
    }

    /**
     * Indicate that the model's email address should not be executed.
     *
     * @return static
     */
    public function notExecuted()
    {
        return $this->managed()->state(function (array $attributes) {
            return [
                'test_date' => null,
                'invoice_number' => null,
                'test_price' => null,
                'observations' => $this->faker->sentence(10),
                'status' => 3,
            ];
        });
    }
}
