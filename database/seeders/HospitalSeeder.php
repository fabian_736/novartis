<?php

namespace Database\Seeders;

use App\Models\Hospital;
use Illuminate\Database\Seeder;

class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hospital::insert([
            ['id' => 1, 'name' => 'CENTRO MEDICO SANNA SUR -ARQ', 'country_id' => 1],
            ['id' => 2, 'name' => 'CLINICA ANGLOAMERICANA', 'country_id' => 1],
            ['id' => 3, 'name' => 'CLINICA JOCKEY SALUD', 'country_id' => 1],
            ['id' => 4, 'name' => 'CLINICA INTERNACIONAL', 'country_id' => 1],
            ['id' => 5, 'name' => 'CLINICA SANTA ISABEL', 'country_id' => 1],
            ['id' => 6, 'name' => 'INSTITUTO MEDICO DE MIRAFLORES', 'country_id' => 1],
            ['id' => 7, 'name' => 'CLINICA RICARDO PALMA', 'country_id' => 1],
            ['id' => 8, 'name' => 'CLINICA SAN FELIPE', 'country_id' => 1],
            ['id' => 9, 'name' => 'CLINICA SAN GABRIEL', 'country_id' => 1],
            ['id' => 10, 'name' => 'CLINICA SAN PABLO', 'country_id' => 1],

            ['id' => 11, 'name' => 'CLINICA AREQUIPA', 'country_id' => 1],
            ['id' => 12, 'name' => 'CLINICA CAYETANO HEREDIA', 'country_id' => 1],
            ['id' => 13, 'name' => 'CLINICA DELGADO', 'country_id' => 1],
            ['id' => 14, 'name' => 'CLINICA EL GOLF', 'country_id' => 1],
            ['id' => 15, 'name' => 'CLINICA SAN BORJA', 'country_id' => 1],
            ['id' => 16, 'name' => 'CLINICA VESALIO', 'country_id' => 1],
            ['id' => 17, 'name' => 'CLINICA ADVENTISTA ANA STAHL - IQUITOS', 'country_id' => 1],
            ['id' => 18, 'name' => 'PETROLEOS DEL PERU', 'country_id' => 1],
            ['id' => 19, 'name' => 'SANNA SANCHEZ FERRER', 'country_id' => 1],
            ['id' => 20, 'name' => 'CL SAN JUAN DE DIOS-ARQ', 'country_id' => 1],

            ['id' => 21, 'name' => 'CLINICA MONTEFIORI', 'country_id' => 1],
            ['id' => 22, 'name' => 'CONSULTORIO PARTICULAR GENERICO', 'country_id' => 1],
            ['id' => 23, 'name' => 'MONTE CARMELO-ARQ', 'country_id' => 1],
            ['id' => 24, 'name' => 'CLINICA PERUANO AMERICANA ', 'country_id' => 1],
            ['id' => 25, 'name' => 'CLINICA GOOD HOPE', 'country_id' => 1],
            ['id' => 26, 'name' => 'CLINICA DEL SUR', 'country_id' => 1],
            ['id' => 27, 'name' => 'CLINICA TEZZA', 'country_id' => 1],
            ['id' => 28, 'name' => 'CLINICA SOLDERMA', 'country_id' => 1],
            ['id' => 29, 'name' => 'CONSULTORIO PRIVADA MIRAFLORES', 'country_id' => 1],
            ['id' => 30, 'name' => 'CLINICA PERUANA AMERICANA', 'country_id' => 1],

            ['id' => 31, 'name' => 'ESSALUD TRUJILLO', 'country_id' => 1],
            ['id' => 32, 'name' => 'HOSPITAL SABOGAL', 'country_id' => 1],
            ['id' => 33, 'name' => 'HOSPITAL ALMENARA', 'country_id' => 1],
            ['id' => 34, 'name' => 'HOSPITAL REBAGLIATI ', 'country_id' => 1],
            ['id' => 35, 'name' => 'ESSALUD AREQUIPA', 'country_id' => 1],
            ['id' => 36, 'name' => 'HOSPITAL MILITAR', 'country_id' => 1],
            ['id' => 37, 'name' => 'FAP', 'country_id' => 1],
            ['id' => 38, 'name' => 'HOSPITAL DEL NI¥O', 'country_id' => 1],
            ['id' => 39, 'name' => 'ESSALUD CUSCO', 'country_id' => 1],
            ['id' => 40, 'name' => 'CLINICA SAN JUDAS TADEO', 'country_id' => 1],

            ['id' => 41, 'name' => 'CLINICA INTERNACIONAL SAN BORJA', 'country_id' => 1],
            ['id' => 42, 'name' => 'CLINICA BELEN', 'country_id' => 1],
            ['id' => 43, 'name' => 'CLINICA LIMATAMBO', 'country_id' => 1],
            ['id' => 44, 'name' => 'CLINICA SANTA MARTHA DEL SUR', 'country_id' => 1],
            ['id' => 45, 'name' => 'CENTRO DE EXCELENCIA', 'country_id' => 1],
            ['id' => 46, 'name' => 'CLINICA MEDEX', 'country_id' => 1],
            ['id' => 47, 'name' => 'AUNA', 'country_id' => 1],
            ['id' => 48, 'name' => 'PARTICULAR', 'country_id' => 1],
            ['id' => 49, 'name' => 'CLINICA SANNA TRUJILLO', 'country_id' => 1],
            ['id' => 50, 'name' => 'JOCKEY SALUD', 'country_id' => 1],

            ['id' => 51, 'name' => 'SAN FELIPE', 'country_id' => 1],
            ['id' => 52, 'name' => 'CLINICA SANTA MARIA DEL SUR', 'country_id' => 1],
            ['id' => 53, 'name' => 'CLINICA JAVIER PRADO', 'country_id' => 1],
            ['id' => 54, 'name' => 'HOSPITAL DE LA SOLIDARIDAD', 'country_id' => 1],
            ['id' => 55, 'name' => 'CLINICA LASER', 'country_id' => 1],
            ['id' => 56, 'name' => 'CLINICA AUNA', 'country_id' => 1],
            ['id' => 57, 'name' => 'INDEFINIDO PERU', 'country_id' => 1],
            ['id' => 58, 'name' => 'INDEFINIDO ECUADOR', 'country_id' => 2],
        ]);
    }
}
