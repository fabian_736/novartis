<?php

namespace App\Http\Controllers;

use App\Http\Requests\ManagementDiagnosticTest\NewManagementDiagnosticTestStoreRequest;
use App\Http\Requests\ManagementDiagnosticTest\UpdateManagementRequest;
use App\Http\Requests\ManagementDiagnosticTest\UpdateTestRequest;
use App\Http\Resources\ManagementDiagnosticTest\ManagementDiagnosticTestResource;
use App\Models\Benefit;
use App\Models\ManagementDiagnosticTest;
use App\Models\Patient;
use App\Services\DiagnosticTest\DiagnosticTestService;
use App\Services\Laboratory\LaboratoryService;
use App\Services\ManagementDiagnosticTest\ManagementDiagnosticTestService;
use App\Services\Medicine\MedicineService;
use App\Services\Patient\PatientService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ManagementDiagnosticTestController extends Controller
{
    use ApiResponses;

    /** @var ManagementDiagnosticTestService */
    private $managementDiagnosticTestService;

    /** @var LaboratoryService */
    private $laboratoryService;

    /** @var MedicineService */
    private $medicineService;

    /** @var DiagnosticTestService */
    private $diagnosticTestService;

    /** @var PatientService */
    private $patientService;

    public function __construct(ManagementDiagnosticTestService $managementDiagnosticTestService, LaboratoryService $laboratoryService, MedicineService $medicineService, DiagnosticTestService $diagnosticTestService, PatientService $patientService)
    {
        $this->managementDiagnosticTestService = $managementDiagnosticTestService;
        $this->laboratoryService = $laboratoryService;
        $this->medicineService = $medicineService;
        $this->diagnosticTestService = $diagnosticTestService;
        $this->patientService = $patientService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $this->authorizeAny(['create', 'update'], ManagementDiagnosticTest::class);

        $managementDiagnosticTests = $this->managementDiagnosticTestService->list();
        $patients = $this->patientService->list();

        return view('coordinator.diagnostic.list', compact('managementDiagnosticTests', 'patients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NewManagementDiagnosticTestStoreRequest  $request
     * @param  ManagementDiagnosticTest  $managementDiagnosticTest
     * @return \Illuminate\Http\JsonResponse
     */
    public function newManagementDiagnosticTestStore(NewManagementDiagnosticTestStoreRequest $request, ManagementDiagnosticTest $managementDiagnosticTest)
    {
        $this->authorize('create', ManagementDiagnosticTest::class);

        if ($request->ajax()) {
            $patient = Patient::whereId($request->safe()->patient_id)->firstOrFail();
            $managementDiagnosticTest = $this->managementDiagnosticTestService->store($patient, $request->safe()->file);

            return $this->resourceResponse(
                message: 'Prueba diagnóstica creada correctamente.',
                data: new ManagementDiagnosticTestResource($managementDiagnosticTest)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Benefit  $benefit
     * @param  ManagementDiagnosticTest  $managementDiagnosticTest
     * @return \Illuminate\Contracts\View\View
     */
    public function management_diagnostic_test(Benefit $benefit, ManagementDiagnosticTest $managementDiagnosticTest)
    {
        $this->authorize('create', ManagementDiagnosticTest::class);

        $laboratories = $this->laboratoryService->list();
        $medicines = $this->medicineService->list();
        $diagnosticTests = $this->diagnosticTestService->list();

        $managementDiagnosticTest->load([
            'patient.treatment.pathology.medicine',
            'patient',
        ]);

        if ($managementDiagnosticTest->status == 0 || $managementDiagnosticTest->status == 3) {
            //$managementDiagnosticTest == 0 | Ejecutada o No ejecutada.
            // Abrirá el formulario cuándo se ha ejecutado la prueba y poder consultar datos
            return view('coordinator.diagnostic.form_test_finish', compact('laboratories', 'medicines', 'diagnosticTests', 'managementDiagnosticTest'));
        } elseif ($managementDiagnosticTest->status == 1) {
            //$managementDiagnosticTest == 1 | Por Gestionar.
            // Abrirá el formulario cuándo no se ha gestionado la prueba
            return view('coordinator.diagnostic.form_management', compact('laboratories', 'medicines', 'diagnosticTests', 'managementDiagnosticTest'));
        } else {
            // Abrirá el formulario cuándo no se ha realizado la prueba
            return view('coordinator.diagnostic.form_test', compact('laboratories', 'medicines', 'diagnosticTests', 'managementDiagnosticTest'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateManagementRequest  $request
     * @param  ManagementDiagnosticTest  $managementDiagnosticTest
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function updateManagement(UpdateManagementRequest $request, ManagementDiagnosticTest $managementDiagnosticTest)
    {
        $this->authorize('create', ManagementDiagnosticTest::class);

        $managementDiagnosticTest->fill($request->validated());

        $managementDiagnosticTest->status = 2;

        $managementDiagnosticTest->save();

        return $request->wantsJson() ?
            response()->json([
                'message' => 'Prueba diagnóstica modificada correctamente',
                'data' => $managementDiagnosticTest,
            ]) :
            redirect()->route('management-diagnostic-tests.index')->with('message', 'Prueba diagnóstica modificada correctamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTestRequest  $request
     * @param  ManagementDiagnosticTest  $managementDiagnosticTest
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function updateTest(UpdateTestRequest $request, ManagementDiagnosticTest $managementDiagnosticTest)
    {
        $this->authorize('create', ManagementDiagnosticTest::class);

        if ($request->input('status') == 0) {
            $managementDiagnosticTest->fill($request->validated())->save();
        } else {
            $managementDiagnosticTest->update([
                'invoice_number' => null,
                'test_date' => null,
                'test_price' => null,
                'status' => ($request->input('status')),
            ]);
        }

        return $request->wantsJson() ?
            response()->json([
                'message' => 'Prueba diagnóstica modificada correctamente',
                'data' => $managementDiagnosticTest,
            ]) :
            redirect()->route('management-diagnostic-tests.index')->with('message', 'Prueba diagnóstica modificada correctamente');
    }
}
