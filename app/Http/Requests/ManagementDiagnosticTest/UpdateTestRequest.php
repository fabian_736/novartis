<?php

namespace App\Http\Requests\ManagementDiagnosticTest;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validations = [
            'status' => 'required|in:0,3',
        ];

        if ($this->input('status') == 0) {
            $validations['invoice_number'] = ['required', 'alpha_num'];
            $validations['test_date'] = ['required', 'date_format:Y-m-d', 'before_or_equal:today', 'after_or_equal:'.$this->management_diagnostic_test->created_at->toDateString()];
            $validations['test_price'] = ['required', 'numeric', 'min:30'];
        } else {
            $validations['invoice_number'] = 'exclude';
            $validations['test_date'] = 'exclude';
            $validations['test_price'] = 'exclude';
        }

        if ($this->input('status') == 3) {
            $validations['observations'] = ['required', 'string', 'min:5'];
        } else {
            $validations['observations'] = 'exclude';
        }

        return $validations;
    }

    public function attributes()
    {
        return [
            'status' => 'Estado de la prueba',
            'invoice_number' => 'Número de factura',
            'test_date' => 'Fecha de la prueba',
            'test_price' => 'Valor de la prueba',
            'observations' => 'Observaciones',
        ];
    }
}
