<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperPharmacyChain
 */
class PharmacyChain extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'name',
        'country_id',
        'is_active',
    ];

    /**
     * Scope a query to only include active pharmacy chains.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Obtiene a las Farmacias asociadas a la cadena Farmacéutica.
     *
     * @return HasMany<Pharmacy>
     */
    public function pharmacies(): HasMany
    {
        return $this->hasMany(Pharmacy::class);
    }

    /**
     * Retrieves the country.
     *
     * @return BelongsTo<Country, PharmacyChain>
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }
}
