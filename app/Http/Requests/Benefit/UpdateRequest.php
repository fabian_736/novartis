<?php

namespace App\Http\Requests\Benefit;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'autorized' => 'required',
            'causal_types' => 'required',
            'status' => 'required',
            'discount' => 'required',
            'closing_reason' => 'required',
            'benefit_type_status' => 'required',
            'benefit_type' => 'required',
            'patient_id' => 'required',
        ];
    }
}
