<?php

namespace Database\Seeders;

use App\Models\Doctor;
use Illuminate\Database\Seeder;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment('local')) {
            Doctor::create([
                'name' => 'Dr Jhon Patrick',
                'specialty_id' => 1,
                'country_id' => 1,
            ]);

            Doctor::create([
                'name' => 'Dr Noah Jonas',
                'specialty_id' => 2,
                'country_id' => 2,
            ]);
        }
    }
}
