<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('benefits', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('formulations', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('management_diagnostic_tests', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('trackings', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('treatments', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('benefits', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('formulations', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('management_diagnostic_tests', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('trackings', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('treatments', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
};
