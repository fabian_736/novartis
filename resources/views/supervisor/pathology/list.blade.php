@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-danger h2">Patologías</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse">
        <div class="col d-flex justify-content-end mt-3">
            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            @can('crear patologias', \App\Models\Admin\Pathology::class)
                <button type="button" class="btn bg-danger d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearPatologia">
                    <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>Crear patología
                </button>
            @endcan
        </div>
        <div class="col">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-danger">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre de la patología</th>
                        <th scope="col">Medicina</th>
                        <th scope="col">Estado</th>
                        @can('actualizar patologias')
                            <th scope="col">Acciones</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pathologies as $key => $pathology)
                        <tr>
                            <td align="center" style="width: 50px">{{ $key + 1 }}</td>
                            <td>{{ $pathology->name }}</td>
                            <td>{{ $pathology->medicine->name }}</td>
                            @if ($pathology->is_active == 0)
                                <td>Inactivo</td>
                            @elseif ($pathology->is_active == 1)
                                <td>Activo</td>
                            @endif
                            @can('actualizar patologias', $pathology)
                                <td>
                                    <div class="col d-flex justify-content-center" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-secondary bg-danger edit-button" type="button" style="color: white" href="{{ route('admin.pathologies.update', $pathology) }}" data-id="{{ $pathology->id }}">
                                            <span class="iconify" data-icon="bxs:edit"></span>&nbsp;
                                            Editar
                                        </a>
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Modal crear patologías --}}
    <div class="modal fade" id="crearPatologia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear patología</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('admin.pathologies.store') }}" method="POST" id="create-pathology">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombre de la patología" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="medicine_id" class="lead text-danger font-weight-bold">Medicina</label>
                                            </div>
                                            <div class="col">
                                                <select name="medicine_id" id="medicine_id" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($medicines as $medicine)
                                                        @if ($medicine->is_active == 1)
                                                            <option value="{{ $medicine->id }}">{{ $medicine->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="" class="lead text-danger font-weight-bold">Presentaciones</label>
                                            </div>
                                            <div class="row">
                                                <div class="row" id="doses-container">
                                                    <ul class="list-group"></ul>
                                                </div>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row my-5">
                            <div class="col d-flex justify-content-center">
                                <button class="btn btn-dark w-25" type="submit">
                                    <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                    Guardar
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>

    {{-- Modal editar patologías --}}
    <div class="modal fade" id="updatePathology" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Editar patología</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="#" method="POST" id="update-pathology">
                                @csrf
                                @method('PUT')
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name2" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name2" id="name2" class="form-control" placeholder="Nombre de la patología" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="medicine_id2" class="lead text-danger font-weight-bold">Medicina</label>
                                            </div>
                                            <div class="col">
                                                <select name="medicine_id2" id="medicine_id2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($medicines as $medicine)
                                                        <option value="{{ $medicine->id }}" data-is-active="{{ $medicine->is_active }}">{{ $medicine->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="is_active2" class="lead text-danger font-weight-bold">Estado</label>
                                            </div>
                                            <div class="col">
                                                <select name="is_active2" id="is_active2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    <option value="0">Inactivo</option>
                                                    <option value="1">Activo</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="" class="lead text-danger font-weight-bold">Presentaciones</label>
                                            </div>
                                            <div class="row" id="doses-container2">
                                                <ul class="list-group"></ul>
                                            </div>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <template id="checkbox-template">
        <li class="list-group-item">
            <input type="checkbox" name="doses" />
            <label class="text-dark"></label>
        </li>
    </template>

    <template id="checkbox-template2">
        <li class="list-group-item">
            <input type="checkbox" name="doses2" id="" value="" />
            <label></label>
        </li>
    </template>

    <script src="{{ url('js/landing/select.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };

        //Funcion modal create
        const form = document.getElementById('create-pathology');

        const create = e => {
            e.preventDefault();
            const data = {
                name: document.getElementById('name').value,
                medicine_id: document.getElementById('medicine_id').value,
                doses: Array.from(document.querySelectorAll('[id^=dose1]:checked')).map(el => el.value),
            }

            axios.post('/admin/management/pathologies', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        confirmButtonColor: '#3085d6',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        form.addEventListener('submit', create);
        //Fin función modal create


        // Función modal update
        const updatePathologyForm = document.getElementById('update-pathology');

        const editPathology = e => {
            e.preventDefault();

            axios.get(`/admin/management/pathologies/${e.target.closest('.edit-button').dataset.id}`)
                .then(async (response) => {
                    const pathology = response.data.data;

                    updatePathologyForm.dataset.id = pathology.id;
                    updatePathologyForm.elements['name2'].value = pathology.name;
                    updatePathologyForm.elements['medicine_id2'].value = pathology.medicine_id;
                    updatePathologyForm.elements['is_active2'].value = pathology.is_active;
                    await onSelectMedicinesChange(pathology.medicine_id, document.querySelector("#doses-container2 ul"), "dose2");
                    pathology.medicine_doses.forEach(medicineDose => {
                        document.getElementById(`dose2-${medicineDose.id}`).checked = true;
                    })

                    Array.from(document.getElementById("medicine_id2").options).forEach(option => {
                        if (option.dataset.isActive == "0" && option.value != pathology.medicine_id) {
                            option.setAttribute("hidden", "");
                            option.setAttribute("disabled", "");
                        } else {
                            option.removeAttribute("hidden");
                            option.removeAttribute("disabled");
                        }
                    })

                    Array.from(updatePathologyForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    $('#updatePathology').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#3085d6',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editPathology);
        });

        const updatePathology = e => {
            e.preventDefault();

            const updatePathologyData = {
                name: document.getElementById('name2').value,
                medicine_id: document.getElementById('medicine_id2').value,
                doses: Array.from(document.querySelectorAll('[id^=dose2]:checked')).map(el => el.value),
                is_active: document.getElementById('is_active2').value,
            }

            axios.put(
                    `/admin/management/pathologies/${updatePathologyForm.dataset.id}`,
                    updatePathologyData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updatePathology').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                        confirmButtonColor: '#3085d6',
                    });

                    Array.from(updatePathologyForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updatePathologyForm.elements[`${key}2`];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updatePathologyForm.addEventListener('submit', updatePathology);
        // Fin función modal update

        document.getElementById("medicine_id").addEventListener("change", e => {
            onSelectMedicinesChange(e.target.value, document.querySelector("#doses-container ul"), "dose1")
        });
        document.getElementById("medicine_id2").addEventListener("change", e => {
            onSelectMedicinesChange(e.target.value, document.querySelector("#doses-container2 ul"), "dose2")
        });
        //Cargar dosis
        async function onSelectMedicinesChange(medicine, presentationElement, idPrefix) {
            await $.get(`/api/medicines/${medicine}/doses`, function(data) {
                const template = document.getElementById("checkbox-template");
                const ul = document.createElement("ul");
                ul.classList.add("list-group");
                data.forEach(dose => {
                    const li = template.content.cloneNode(true);
                    const input = li.querySelector("input");
                    const label = li.querySelector("label");

                    input.id = `${idPrefix}-${dose.pivot.id}`;
                    input.value = dose.pivot.id;

                    label.for = `${idPrefix}-${dose.pivot.id}`;
                    label.textContent = dose.presentation;

                    ul.appendChild(li);
                });

                presentationElement.replaceWith(ul);
            });
        }
    </script>
@endsection
