//Valida los campos de la primera seccion
$("#bottom_one").click(function () {
    var document_type_id = $("#document_type_id").val();
    var document_number = document.getElementById("document_number").value;
    var name = document.getElementById("name").value;
    var birth_date = document.getElementById("birth_date").value;
    var age = document.getElementById("age").value;
    var gender = $("#gender").val();
    var phone = document.getElementById("phone").value;
    var city_id = document.getElementById("city").value;

    if (document_type_id <= 0 || document_type_id == "") {
        $("#document_type_id_validate").show();
        $("#document_type_id_validate").html(
            "<i class='fas fa-times mr-2 '></i>Por favor seleccione una opcion"
        );
        $("#document_type_id").addClass("is-invalid").removeClass("is-valid");

    } else {
        $("#document_type_id").addClass("is-valid").removeClass("is-invalid");
        $("#document_type_id_validate").hide();
    }

    if (document_number <= 0 || document_number == "") {
        $("#document_number").addClass("is-invalid").removeClass("is-valid");
        $('#text_alert').addClass("text-danger font-weight-bold");
    } else {
        $("#document_number").addClass("is-valid").removeClass("is-invalid");
        $('#text_alert').addClass("text-dark").removeClass('text-danger font-weight-bold');
    }

    if (name <= 0 || name == "") {
        $("#name_validate").show();
        $("#name_validate").html(
            "<i class='fas fa-times mr-2 '></i>Por favor escribe tu nombre completo"
        );
        $("#name").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#name").addClass("is-valid").removeClass("is-invalid");
        $("#name_validate").hide();
    }

    if (birth_date <= 0 || birth_date == "") {
        $("#birth_date_validate").show();
        $("#birth_date_validate").html(
            "<i class='fas fa-times mr-2 '></i>Por favor digite una fecha de nacimiento"
        );
        $("#birth_date").val("");
        $("#birth_date").addClass("is-invalid").removeClass("is-valid");
        $("#age").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#birth_date").addClass("is-valid").removeClass("is-invalid");
        $("#age").addClass("is-valid").removeClass("is-invalid");
        $("#birth_date_validate").hide();
    }

    if (gender <= 0 || gender == "") {
        $("#gender_validate").show();
        $("#gender_validate").html(
            "<i class='fas fa-times mr-2 '></i>Por favor selecciona una opcion"
        );
        $("#gender").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#gender_validate").hide();
        $("#gender").addClass("is-valid").removeClass("is-invalid");
    }

    if (city_id <= 0 || city_id == "") {
        $("#city_validate").show();
        $("#city_validate").html(
            "<i class='fas fa-times mr-2 '></i>Por favor selecciona una opcion"
        );
        $("#city").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#city_validate").hide();
        $("#city").addClass("is-valid").removeClass("is-invalid");
    }

    if (phone <= 0 || phone == "") {
        $("#phone_validate").show();
        $("#phone_validate").html(
            "<i class='fas fa-times mr-2 '></i>Por favor escribe tu numero telefonico"
        );
        $("#phone").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#phone_validate").hide();
        $("#phone").addClass("is-valid").removeClass("is-invalid");
    }

    if (
        document_type_id == "" ||
        document_number == "" ||
        name == "" ||
        birth_date == "" ||
        age == "" ||
        gender == "" ||
        phone == "" ||
        city_id == ""
    ) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "¡Completa todos los campos!",
            showConfirmButton: false,
            timer: 1500,
        });
    } else {

        $("#1").click(function () {


            $("#item_one").show("slow");
            $("#item_two").hide("slow");
            $("#item_three").hide("slow");

            $("#1").css("background-color", "#221F1F");
            $("#2").css("background-color", "#C4C4C4");
            $("#3").css("background-color", "#C4C4C4");
        });

        $("#2").click(function () {
            $("#item_one").hide("slow");
            $("#item_two").show("slow");
            $("#item_three").hide("slow");

            $("#1").css("background-color", "#221F1F");
            $("#2").css("background-color", "#221F1F");
            $("#3").css("background-color", "#C4C4C4");
        });



        $("#item_one").hide("slow");
        $("#item_two").show("slow");
        $("#item_three").hide("slow");

        $("#1").css("background-color", "#221F1F");
        $("#2").css("background-color", "#221F1F");
        $("#3").css("background-color", "#C4C4C4");
    }
});
