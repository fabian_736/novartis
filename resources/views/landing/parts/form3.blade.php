<div id="item_three" style="display: none">
    <div class="row my-3">
        <div class="col col_form_input mx-auto">
            <label for="" class="line w-100 lead font-weight-bold text-dark">Persona responsable del paciente</label>
            <style>
                .line {
                    border-bottom: solid #000000 2.5px;
                }
            </style>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-6 col_form_input">
            <label for="" class="font-weight-bold h4">Posee responsable del paciente</label>
            <div class="form-group has-default">
                <div class="input-group">
                    <div class="row">
                        <div class="col-1">
                            <input type="checkbox" name="has_carer" id="button3" value="1" onchange="javascript:showContent_cuidador1()">
                            <label for="button3" class="fas"></label>
                        </div>
                        <div class="col ml-3">
                            <span class="h5">Si</span>
                        </div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-1">
                            <input type="checkbox" name="has_carer" id="button4" value="0" onchange="javascript:showContent_cuidador2()">
                            <label for="button4" class="fas"></label>
                        </div>
                        <div class="col ml-3">
                            <span class="h5">No</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6" id="cuidadoroculto2">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4">Vínculo con el
                    paciente <span style="color: #EF5630; font-size: 11px">*Campo
                        obligatorio</span></label>
                <div class="form-group has-default mb-5">
                    <div class="input-group">
                        <select name="relationship_patient" id="relationship_patient" class="form-control w-100 @error('relationship_patient') is-invalid @enderror">
                            <option value="" selected disabled>Seleccione una opción...</option>
                            <option @if (old('relationship_patient')=='Madre' ) {{ 'selected' }} @endif value="Madre">Madre</option>
                            <option @if (old('relationship_patient')=='Padre' ) {{ 'selected' }} @endif value="Padre">Padre</option>
                            <option @if (old('relationship_patient')=='Hijo/a' ) {{ 'selected' }} @endif value="Hermano/a">Hermano/a</option>
                            <option @if (old('relationship_patient')=='Tio/a' ) {{ 'selected' }} @endif value="Enfermero/a">Enfermero/a</option>
                            <option @if (old('relationship_patient')=='Otro' ) {{ 'selected' }} @endif value="Otro/a">Otro/a</option>
                        </select>
                        {{-- Validador con JS --}}
                        <div class="invalid-feedback font-weight-bold position-absolute" id="relationship_patient_validate" style="margin-top: 7%; display: none">

                        </div>
                        {{-- FIN Validador con JS --}}
                        @error('gender')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row " id="cuidadoroculto">
        <div class="row">
            <div class="col-sm-6">
                <div class="col col_form_input">
                    <label for="" class="font-weight-bold h4">Tipo de
                        identificación <span style="color: #EF5630; font-size: 11px">*Campo
                            obligatorio</span></label>
                    <div class="form-group has-default mb-5">
                        <div class="input-group">
                            <select name="carer_document_type_id" id="carer_document_type_id" class="form-control w-100 @error('carer_document_type_id') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                @foreach ($documentTypes as $document)
                                <option @if (old('carer_document_type_id')==$document->id) {{ 'selected' }} @endif
                                    value="{{ $document->id }}">
                                    {{ $document->name }}
                                </option>
                                @endforeach
                            </select>
                            {{-- Validador con JS --}}
                            <div class="invalid-feedback font-weight-bold position-absolute" id="carer_document_type_id_validate" style="margin-top: 7%; display: none">

                            </div>
                            {{-- FIN Validador con JS --}}
                            @error('carer_document_type')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col col_form_input">
                    <label for="" class="font-weight-bold h4">Número de
                        identificación <span style="color: #EF5630; font-size: 11px">*Campo
                            obligatorio</span></label>
                    <div class="form-group has-default">
                        <div class="input-group">
                            <input value="{{ old('carer_document_number') }}" name="carer_document_number" id="carer_document_number" type="text" class="form-control @error('carer_document_numbers') is-invalid @enderror">
                            @error('carer_document_number')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <span>Digite su número de identificación sin puntos ni comas.</span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="col col_form_input">
                    <label for="" class="font-weight-bold h4">Nombre completo <span style="color: #EF5630; font-size: 11px">*Campo
                            obligatorio</span></label>
                    <div class="form-group has-default">
                        <div class="input-group">
                            <input value="{{ old('carer_name') }}" name="carer_name" id="carer_name" type="text" class="form-control @error('carer_name') is-invalid @enderror">
                            @error('carer_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            {{-- Validador con JS --}}
                            <div class="invalid-feedback font-weight-bold " id="carer_name_validate" style="display: none">

                            </div>
                            {{-- FIN Validador con JS --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col col_form_input">
                    <label for="" class="font-weight-bold h4">Fecha de nacimiento </label>
                    <div class="form-group has-default">
                        <div class="input-group">
                            <input value="{{ old('carer_birth_date') }}" name="carer_birth_date" id="carer_birth_date" type="date" class="form-control @error('carer_birth_date') is-invalid @enderror">
                            @error('carer_birth_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <div class="col col_form_input">
                    <label for="" class="font-weight-bold h4">Edad </label>
                    <div class="form-group has-default">
                        <div class="input-group">
                            <input placeholder="Edad" disabled value="{{ old('carer_age') }}" name="carer_age" id="carer_age" type="number" class="form-control @error('carer_age') is-invalid @enderror">
                            @error('carer_age')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col col_form_input">
                    <label for="" class="font-weight-bold h4">Número de contacto <span style="color: #EF5630; font-size: 11px">*Campo
                            obligatorio</span></label>
                    <div class="form-group has-default">
                        <div class="input-group">
                            <input value="{{ old('carer_phone') }}" name="carer_phone" id="carer_phone" type="number" class="form-control @error('carer_phone') is-invalid @enderror">
                            @error('carer_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            {{-- Validador con JS --}}
                            <div class="invalid-feedback font-weight-bold " id="carer_phone_validate" style=" display: none">

                            </div>
                            {{-- FIN Validador con JS --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col col_form_input">
                    <label for="" class="font-weight-bold h4">Correo electrónico</label>
                    <div class="form-group has-default">
                        <div class="input-group">
                            <input value="{{ old('carer_email') }}" name="carer_email" id="carer_email" type="email" class="form-control @error('carer_email') is-invalid @enderror">
                            @error('carer_email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            {{-- Validador con JS --}}
                            <div class="invalid-feedback font-weight-bold " id="carer_email_validate" style=" display: none">

                            </div>
                            {{-- FIN Validador con JS --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="firm" id="firm">
        </div>
    </div>

    <div class="row">
        <div class="col col_form_input mt-3">
            <p class="h4"><span class="font-weight-bold">Firma el
                    consentimiento
                    informado</span> para acceder a su beneficio. <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span> <br>
                Haga clic en el campo para firmar con el mouse o con el dedo.</p>
        </div>
    </div>

    <div class="row ">
        <div class="col col_form_input py-3">
            <div class="row">
                <div class="col col_form_input" id="image_print" style="display: none">
                    <img id="sig-image" src="" alt="Your signature will go here!" />
                </div>
            </div>
            <div class="row-reverse">
                <div class="col m-0 p-0">
                    <textarea class="form-control p-3 " id="before_sign" cols="5" rows="5" style="cursor: pointer; border-style: solid; border-width: 2px; border-radius: 20px; border-color: gray" id="exampleFormControlTextarea1" rows="3" data-toggle="modal" data-target="#exampleModalCenter1"></textarea>
                </div>
                <div class="col m-0 p-0 mt-3">
                    <p class="h4"><span>Con esta firma, acepto los términos y
                            condiciones del programa, así como mi participación dentro del
                            mismo. </p>
                </div>
            </div>
        </div>



    </div>
    <div class="row">
        <div class="col-3 d-flex justify-content-end offset-md-6 ">
            <a href="#" class="btn btn-white btn_sm back_firma" style="border: solid #000 2px; font-weight: 700; font-size: 12px; color: #000; display: none">CORREGIR</a>
        </div>
        <div class="col-3 ">
            <div class="col col_form_input d-flex justify-content-center">
                <input type="button" class="btn btn-md" value="FIRMAR E INSCRIBIRSE" id="submit_end" style="background: #221F1F">
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="loading_spinner" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body ">
                <div class="image d-flex justify-content-center">
                    <img src="{{asset('img/2.png')}}" alt="">
                </div>
                <div class="d-flex justify-content-center mt-5">
                    <div class=".div " id="container">
                        <svg viewBox="0 0 100 100">
                            <defs>
                                <filter id="shadow">
                                    <feDropShadow dx="0" dy="0" stdDeviation="1.5" flood-color="#00278d" />
                                </filter>
                            </defs>
                            <circle id="spinner" style="fill:transparent;stroke:#dd2476;stroke-width: 7px;stroke-linecap: round;filter: invert(12%) sepia(81%) saturate(6962%) hue-rotate(222deg) brightness(50%) contrast(70%);" cx="50" cy="50" r="45" />
                        </svg>
                    </div>
                </div>
                <h2 class="text-center">Cargando...</h2>
            </div>
        </div>
    </div>
</div>
<style>
    .div {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #container {
        width: 50px;
        height: 50px;
    }

    @keyframes animation {
        0% {
            stroke-dasharray: 1 98;
            stroke-dashoffset: -105;
        }

        50% {
            stroke-dasharray: 80 10;
            stroke-dashoffset: -160;
        }

        100% {
            stroke-dasharray: 1 98;
            stroke-dashoffset: -300;
        }
    }

    #spinner {
        transform-origin: center;
        animation-name: animation;
        animation-duration: 1.2s;
        animation-timing-function: cubic-bezier;
        animation-iteration-count: infinite;
    }
</style>
