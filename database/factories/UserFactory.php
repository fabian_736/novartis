<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\DocumentType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'document_number' => $this->faker->isbn10(),
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => Hash::make('password'),
            'address' => $this->faker->address(),
            'city_id' => City::factory(),
            'phone' => $this->faker->phoneNumber(),
            'is_admin' => false,
            'document_type_id' => DocumentType::factory(),
        ];
    }

    /**
     * Indicate that the model's email address should be administrator.
     *
     * @return static
     */
    public function asAdmin()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_admin' => true,
            ];
        });
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
