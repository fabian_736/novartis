<?php

namespace App\Policies;

use App\Models\ManagementDiagnosticTest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ManagementDiagnosticTestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear pruebas', 'actualizar pruebas'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear pruebas')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ManagementDiagnosticTest  $managementDiagnosticTest
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, ManagementDiagnosticTest $managementDiagnosticTest)
    {
        if ($user->hasPermissionTo('actualizar pruebas')) {
            return true;
        }
    }
}
