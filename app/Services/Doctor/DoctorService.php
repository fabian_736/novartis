<?php

namespace App\Services\Doctor;

use App\Models\Doctor;

class DoctorService
{
    /**
     * Returns a list of doctors.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Doctor>
     */
    public function list($onlyActive = true)
    {
        $doctors = Doctor::query();

        if ($onlyActive) {
            $doctors->active();
        }

        return $doctors->get();
    }

    /**
     * Create a Doctor.
     *
     * @param  array  $input
     * @return Doctor
     */
    public function store(array $input): Doctor
    {
        return Doctor::create($input);
    }

    public function update(Doctor $doctor, array $input): bool
    {
        return $doctor->update($input);
    }
}
