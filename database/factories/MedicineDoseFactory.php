<?php

namespace Database\Factories;

use App\Models\Dose;
use App\Models\Medicine;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MedicineDose>
 */
class MedicineDoseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'medicine_id' => Medicine::factory(),
            'dose_id' => Dose::factory(),
        ];
    }
}
