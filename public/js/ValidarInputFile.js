$(document).on('change', '.input-excel', function () {
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 70 * 1024 * 1024) {
        Swal.fire(
            'Error',
            'El archivo no debe superar los 70MB',
            'error'
        )
        this.value = '';
        this.files[0].name = '';
    } else {
        // recuperamos la extensión del archivo
        var ext = fileName.split('.').pop();
        ext = ext.toLowerCase();

        switch (ext) {
            case 'xlsx':
            case 'csv':
                break;
            default:
                Swal.fire(
                    'Error',
                    'El archivo no tiene la extensión adecuada',
                    'error'
                )
                this.value = ''; // reset del valor
                this.files[0].name = '';
        }
    }

    Swal.fire(
        'Buen trabajo',
        'Los datos se cargaron correctamente',
        'success'

    )
    document.getElementById('archivo').innerHTML = document.getElementById('file-1').files[0].name;
    document.getElementById('archivo').innerHTML = document.getElementsByName('upload_hospital').files[0].name;
});

$(document).on('change', '.consentimiento', function () {
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 70 * 1024 * 1024) {
        Swal.fire(
            'Error',
            'El archivo no debe superar los 70MB',
            'error'
        )
        this.value = '';
        this.files[0].name = '';
    } else {
        // recuperamos la extensión del archivo
        var ext = fileName.split('.').pop();
        ext = ext.toLowerCase();

        switch (ext) {
            case 'pdf':
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'bmp':
                break;
            default:
                Swal.fire(
                    'Error',
                    'El archivo no tiene la extensión adecuada',
                    'error'
                )
                this.value = ''; // reset del valor
                this.files[0].name = '';
        }
    }

    Swal.fire(
        'Buen trabajo',
        'El archivo se cargó correctamente',
        'success'

    )
    document.getElementById('archivo').innerHTML = document.getElementById('file-1').files[0].name;
    document.getElementById('archivo').innerHTML = document.getElementsByName('upload_hospital').files[0].name;
});
