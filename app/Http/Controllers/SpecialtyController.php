<?php

namespace App\Http\Controllers;

use App\Http\Requests\Specialty\StoreRequest;
use App\Http\Requests\Specialty\UpdateRequest;
use App\Http\Resources\Specialty\SpecialtyResource;
use App\Models\Specialty;
use App\Services\Specialty\SpecialtyService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SpecialtyController extends Controller
{
    use ApiResponses;

    /** @var SpecialtyService */
    private $service;

    public function __construct(SpecialtyService $specialtyService)
    {
        $this->service = $specialtyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Specialty::class);

        $specialties = $this->service->list(false);

        return view('supervisor.specialty.list', compact('specialties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Specialty::class);
            $specialty = $this->service->store($request->validated());

            return $this->resourceResponse(
                message: 'Especialidad creada correctamente.',
                data: new SpecialtyResource($specialty),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Specialty  $specialty
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Specialty $specialty)
    {
        if ($request->ajax()) {
            $this->authorize('update', $specialty);

            return $this->resourceResponse(
                message: '',
                data: new SpecialtyResource($specialty)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Specialty  $specialty
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Specialty $specialty)
    {
        if ($request->ajax()) {
            $this->authorize('update', $specialty);
            $this->service->update($specialty, $request->validated());
            $specialty->refresh();

            return $this->resourceResponse(
                message: 'Especialidad actualizada correctamente.',
                data: new SpecialtyResource($specialty)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
