@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-danger h2">Laboratorios</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse">
        <div class="col d-flex justify-content-end mt-3">
            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            @can('crear laboratorios', \App\Models\Laboratory::class)
                <button type="button" class="btn bg-danger d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearLaboratorio">
                    <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>Crear laboratorio
                </button>
            @endcan
        </div>

        <div class="col">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-danger">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Ciudad</th>
                        <th scope="col">Identificación / Nit</th>
                        <th scope="col">Estado</th>
                        @can('actualizar laboratorios')
                            <th scope="col">Acción</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach ($laboratories as $key => $laboratory)
                        <tr>
                            <td align="center" style="width: 50px">{{ $key + 1 }}</td>
                            <td>{{ $laboratory->name }}</td>
                            <td>{{ $laboratory->city->name }}</td>
                            <td>{{ $laboratory->document_number }}</td>
                            @if ($laboratory->is_active == 0)
                                <td>Inactivo</td>
                            @elseif($laboratory->is_active == 1)
                                <td>Activo</td>
                            @endif
                            @can('actualizar laboratorios', $laboratory)
                                <td>
                                    <div class="col d-flex justify-content-center" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-secondary bg-danger edit-button" type="button" style="color: white" href="{{ route('admin.laboratories.update', $laboratory) }}" data-id="{{ $laboratory->id }}">
                                            <span class="iconify" data-icon="bxs:edit"></span>&nbsp;
                                            Editar
                                        </a>
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Modal crear laboratorio --}}
    <div class="modal fade" id="crearLaboratorio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear laboratorio</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('admin.laboratories.store') }}" method="POST" id="create-laboratory">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_type_id" class="lead text-danger font-weight-bold">Tipo de documento</label>
                                            </div>
                                            <div class="col">
                                                <select name="document_type_id" id="document_type_id" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($documentTypes as $documentType)
                                                        <option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_number" class="lead text-danger font-weight-bold">Número de documento</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="document_number" id="document_number" class="form-control" placeholder="Número de documento" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombre" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country" class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country" id="country" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        @if ($country->is_active == 1)
                                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="city_id" class="lead text-danger font-weight-bold">Ciudad</label>
                                            </div>
                                            <div class="col">
                                                <select name="city_id" id="city" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="address" class="lead text-danger font-weight-bold">Dirección</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="address" id="address" class="form-control" placeholder="Dirección" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal editar laboratorio --}}
    <div class="modal fade" id="updateLaboratory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Editar laboratorio</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="#" method="POST" id="update-laboratory">
                                @csrf
                                @method('PUT')
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_type_id" class="lead text-danger font-weight-bold">Tipo de documento</label>
                                            </div>
                                            <div class="col">
                                                <select name="document_type_id" id="document_type_id2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($documentTypes as $documentType)
                                                        <option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_number" class="lead text-danger font-weight-bold">Número de documento</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="document_number" id="document_number2" class="form-control" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name2" class="form-control" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country" class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country" id="country2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country->id }}" data-is-active="{{ $country->is_active }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="city_id" class="lead text-danger font-weight-bold">Ciudad</label>
                                            </div>
                                            <div class="col">
                                                <select name="city_id" id="city2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="address" class="lead text-danger font-weight-bold">Dirección</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="address" id="address2" class="form-control" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="is_active" class="lead text-danger font-weight-bold">Estado</label>
                                            </div>
                                            <div class="col">
                                                <select name="is_active" id="is_active2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    <option value="0">Inactivo</option>
                                                    <option value="1">Activo</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ url('js/landing/select.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };


        //Funcion modal create
        const form = document.getElementById('create-laboratory');

        const create = e => {
            e.preventDefault();
            const data = {
                document_type_id: document.getElementById('document_type_id').value,
                document_number: document.getElementById('document_number').value,
                name: document.getElementById('name').value,
                country: document.getElementById('country').value,
                city_id: document.getElementById('city').value,
                address: document.getElementById('address').value,
            }
            axios.post('/admin/management/laboratories', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }

        form.addEventListener('submit', create);
        //Fin función modal create


        // Función modal update
        const updateLaboratoryForm = document.getElementById('update-laboratory');

        const editLaboratory = e => {
            e.preventDefault();

            axios.get(`/admin/management/laboratories/${e.target.closest('.edit-button').dataset.id}`)
                .then(async (response) => {
                    const laboratory = response.data.data;

                    updateLaboratoryForm.dataset.id = laboratory.id;
                    updateLaboratoryForm.elements['document_type_id2'].value = laboratory.document_type_id;
                    updateLaboratoryForm.elements['document_number2'].value = laboratory.document_number;
                    updateLaboratoryForm.elements['name2'].value = laboratory.name;
                    updateLaboratoryForm.elements['address2'].value = laboratory.address;
                    updateLaboratoryForm.elements['country'].value = laboratory.city.country_id;
                    await onSelectCountryChange(updateLaboratoryForm.elements['country'], "#city2");
                    updateLaboratoryForm.elements['city2'].value = laboratory.city_id;
                    updateLaboratoryForm.elements['is_active2'].value = laboratory.is_active;

                    Array.from(document.getElementById("country2").options).forEach(option => {
                        if (option.dataset.isActive == "0" && option.value != laboratory.city.country_id) {
                            option.setAttribute("hidden", "");
                            option.setAttribute("disabled", "");
                        } else {
                            option.removeAttribute("hidden");
                            option.removeAttribute("disabled");
                        }
                    })

                    Array.from(updateLaboratoryForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    $('#updateLaboratory').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#221F1F',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editLaboratory);
        });

        const updateLaboratory = e => {
            e.preventDefault();

            const updateLaboratoryData = {
                document_type_id: document.getElementById('document_type_id2').value,
                document_number: document.getElementById('document_number2').value,
                name: document.getElementById('name2').value,
                address: document.getElementById('address2').value,
                country: document.getElementById('country2').value,
                city_id: document.getElementById('city2').value,
                is_active: document.getElementById('is_active2').value,
            }
            axios.put(
                    `/admin/management/laboratories/${updateLaboratoryForm.dataset.id}`,
                    updateLaboratoryData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updateLaboratory').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(updateLaboratoryForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updateLaboratoryForm.elements[`${key}2`];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updateLaboratoryForm.addEventListener('submit', updateLaboratory);
        // Fin función modal update

        $(function() {
            $('#country').on('change', e => {
                onSelectCountryChange(e.target, "#city");
            });
            $('#country2').on('change', e => {
                onSelectCountryChange(e.target, "#city2");
            });
        })
    </script>
@endsection
