<?php

namespace App\Services\PathologyHasMedicineDose;

use App\Models\PathologyHasMedicineDose;

class PathologyHasMedicineDoseService
{
    /**
     * Returns a list of pathology has medicine dose.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, PathologyHasMedicineDose>
     */
    public static function list()
    {
        return PathologyHasMedicineDose::get();
    }

    /**
     * Create a pathology has medicine dose.
     *
     * @param  array  $input
     * @return PathologyHasMedicineDose
     */
    public static function store(array $input): PathologyHasMedicineDose
    {
        return PathologyHasMedicineDose::create($input);
    }

    public static function update(PathologyHasMedicineDose $pathologyHasMedicineDose, array $input): bool
    {
        return $pathologyHasMedicineDose->update($input);
    }
}
