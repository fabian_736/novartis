<?php

namespace App\Http\Requests\Patient;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTreatmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_time_using_biological' => 'required',
            'pathology_id' => ['required', 'exists:pathologies,id'],
            'medicine_id' => ['required', 'exists:medicines,id'],
            'doctor_id' => ['required', 'exists:doctors,id'],
            'frequency' => 'nullable',
            'pathology_has_medicine_dose_id' => ['required', 'exists:pathology_has_medicine_doses,id'],
            'observations' => 'nullable|string|between:3,200',
        ];
    }

    public function attributes()
    {
        return [
            'first_time_using_biological' => 'Primera vez usando biológicos',
            'pathology_id' => 'Patología',
            'medicine_id' => 'Producto',
            'doctor_id' => 'Doctor',
            'frequency' => 'Frecuencia',
            'observations' => 'Observaciones',
            'pathology_has_medicine_dose_id' => 'Dosis',
        ];
    }
}
