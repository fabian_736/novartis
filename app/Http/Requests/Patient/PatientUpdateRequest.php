<?php

namespace App\Http\Requests\Patient;

use App\Enums\Gender;
use App\Enums\Patient\PatientStatus;
use App\Enums\Patient\PatientTypes;
use App\Enums\Patient\SuspensionReasons;
use App\Enums\RelationshipPatient;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $minDate = now()->subCentury()->toDateString();
        $fecha = now()->subYears(18)->toDateString();

        $rules = [
            'document_type_id' => 'required|exists:document_types,id',
            'document_number' => [
                'alpha_num',
                'required',
                Rule::unique('patients')
                    ->where('document_type_id', $this->input('document_type_id'))
                    ->ignore($this->patient),
            ],
            'name' => ['required', 'regex:/^[a-záéíóúüñ ]+/i'],
            'patient_type' => ['required', new Enum(PatientTypes::class)],
            'birth_date' => "required|date|after_or_equal:$minDate|before_or_equal:today",
            'gender' => ['required', new Enum(Gender::class)],
            'email' => [
                'nullable',
                'email:rfc,dns',
                Rule::unique('patients', 'email')->ignore($this->patient),
            ],
            'phone' => 'required|numeric|digits_between:7,9',
            'country_id' => 'required|exists:countries,id',
            'city_id' => [
                'required',
                Rule::exists('cities', 'id')
                    ->where(fn ($query) => $query->where('country_id', $this->input('country_id'))),
            ],
            'has_carer' => 'required|boolean',
            'patient_status' => ['required', new Enum(PatientStatus::class)],
            'date_admission' => 'required|date|before_or_equal:today',
        ];

        if ($this->patient_type == PatientTypes::NotInsured->value) {
            $rules['hospital_id'] = ['required', 'exists:hospitals,id'];
            $rules['insurance_carrier_id'] = 'exclude';
        } else {
            $rules['insurance_carrier_id'] = ['required', 'exists:insurance_carriers,id'];
            $rules['hospital_id'] = 'exclude';
        }

        if ($this->has_carer == 1) {
            $rules['carer_document_type_id'] = 'required|exists:document_types,id';
            $rules['carer_document_number'] = 'required|alpha_num';
            $rules['carer_name'] = ['required', 'regex:/^[a-záéíóúüñ ]+/i'];
            $rules['carer_birth_date'] = "required|date|after_or_equal:$minDate|before_or_equal:$fecha";
            $rules['relationship_patient'] = ['required', new Enum(RelationshipPatient::class)];
            $rules['carer_email'] = 'nullable|email:rfc,dns';
            $rules['carer_phone'] = 'required|numeric|digits_between:7,9';
        } else {
            $rules['relationship_patient'] = 'exclude';
            $rules['carer_document_type_id'] = 'exclude';
            $rules['carer_document_number'] = 'exclude';
            $rules['carer_name'] = 'exclude';
            $rules['carer_birth_date'] = 'exclude';
            $rules['carer_email'] = 'exclude';
            $rules['carer_phone'] = 'exclude';
        }

        switch ($this->patient_status) {
            case PatientStatus::InAccess->value:
            case PatientStatus::Active->value:
                $rules['is_reactivated'] = 'nullable|boolean';
                $rules['suspension_reason'] = 'exclude';
                break;
            case PatientStatus::Suspended->value:
                $rules['is_reactivated'] = 'exclude';
                $rules['suspension_reason'] = ['required', new Enum(SuspensionReasons::class)];
                break;
            default:
                $rules['is_reactivated'] = 'exclude';
                $rules['suspension_reason'] = 'exclude';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'document_type_id' => 'tipo de documento',
            'document_number' => 'número de identificación',
            'name' => 'nombre',
            'birth_date' => 'fecha de nacimiento',
            'gender' => 'genero',
            'email' => 'correo electrónico',
            'phone' => 'número de contacto',
            'country_id' => 'país',
            'city_id' => 'ciudad',
            'has_carer' => 'posee cuidador',
            'hospital_id' => 'hospital',
            'insurance_carrier_id' => 'aseguradora',
            'date_admission' => 'fecha de inscripción',
            'patient_status' => 'estado',
            'patient_type' => 'tipo de paciente',
            'carer_document_type_id' => 'tipo de identificación del paciente',
            'carer_document_number' => 'número de identificación del paciente',
            'carer_name' => 'nombre del paciente',
            'carer_birth_date' => 'fecha de nacimiento del paciente',
            'carer_email' => 'correo electrónico del paciente',
            'carer_phone' => 'número de contacto del paciente',
            'is_reactivated' => 'reactivado',
            'suspension_reason' => 'motivo de suspensión',
        ];
    }

    public function messages()
    {
        return [
            'document_number.unique' => 'Ya existe un paciente con el tipo de documento y :attribute indicados',
            'carer_birth_date.before_or_equal' => 'El cuidador debe ser mayor de edad',
            'carer_birth_date.after_or_equal' => 'El cuidador debe ser menor de 100 años',
            'birth_date.after_or_equal' => 'El usuario debe ser menor de 100 años',
            'birth_date.before_or_equal' => 'La fecha no es valida',
            'date_admission.before_or_equal' => 'La fecha de inscripción no es valida, la fecha debe ser anterior o igual a la fecha actual',
            'city_id.exists' => 'La :attribute seleccionada no existe en el país seleccionado.',
        ];
    }
}
