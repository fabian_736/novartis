<?php

namespace App\Models;

use App\Traits\SoftDeletesParent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperBenefit
 */
class Benefit extends Model implements AuditableContract, HasMedia
{
    use AuditableTrait, HasFactory, InteractsWithMedia, SoftDeletes, SoftDeletesParent;

    protected $fillable = [
        'delivery_place',
        'transaction_date',
        'invoice_number',
        'autorized',
        'causal_types',
        'status',
        'discount',
        'closing_reason',
        'patient_id',
        'city_id',
        'benefit_type',
        'is_accepted',
        'number_units',
        'pharmacy_id',
        'closed_at',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('invoice')
            ->singleFile();
    }

    /**
     * Retrieves the pharmacy.
     *
     * @return BelongsTo<Pharmacy, Benefit>
     */
    public function pharmacy(): BelongsTo
    {
        return $this->belongsTo(Pharmacy::class);
    }

    /**
     * Retrieves the patient.
     *
     * @return BelongsTo<Patient, Benefit>
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * Retrieves the city.
     *
     * @return BelongsTo<City, Benefit>
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Benefit status is coded.
     *
     * @return string
     */
    public function getStatusDecodeAttribute()
    {
        switch ($this->status) {
            case 0:
                return 'Cerrado';
            case 1:
                return 'Carta pendiente';
            case 2:
                return 'Descuento especial rechazado';
            case 3:
                return 'Factura pendiente';
            case 4:
                return 'Factura cargada';
            case 5:
                return 'Proceso observado';
            default:
                return 'Indefinido';
        }
    }
}
