@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <div class="row mx-auto">
                    <div class="d-flex align-items-center" style="width: 100px">
                        <label for="" class="text-primary font-weight-bold h2">PSP |</label>
                    </div>
                    <div class="col d-flex align-items-center p-0 pt-2">
                        <label for="" class="h5" style="color: #0C68B0">Programa seguimiento <br> de
                            pacientes</label>
                    </div>
                </div>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row py-3" style="overflow-y: auto; max-height: 80vh">

        {{-- Configuración del sistema --}}
        @canany(['asignar permisos a roles', 'crear pacientes', 'actualizar pacientes', 'crear usuarios', 'actualizar usuarios', 'crear permisos', 'actualizar permisos', 'eliminar permisos', 'eliminar permisos permanentemente', 'crear roles', 'actualizar roles', 'eliminar roles', 'eliminar roles permanentemente', 'crear cadenas', 'actualizar cadenas', 'crear farmacias', 'actualizar farmacias', 'crear especialidades', 'actualizar especialidades', 'crear doctores', 'actualizar doctores', 'crear paises', 'actualizar paises', 'crear ciudades', 'actualizar ciudades', 'crear aseguradoras', 'actualizar aseguradoras', 'crear hospitales', 'actualizar hospitales', 'crear medicinas', 'actualizar medicinas', 'crear patologias', 'actualizar patologias'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Administración</label>
                        </div>
                    </div>
                    @canany(['asignar permisos a roles', 'crear usuarios', 'actualizar usuarios', 'crear permisos', 'actualizar permisos', 'eliminar permisos', 'eliminar permisos permanentemente', 'crear roles', 'actualizar roles', 'eliminar roles', 'eliminar roles permanentemente', 'crear cadenas', 'actualizar cadenas', 'crear farmacias', 'actualizar farmacias', 'crear especialidades', 'actualizar especialidades', 'crear doctores', 'actualizar doctores', 'crear paises', 'actualizar paises', 'crear ciudades', 'actualizar ciudades', 'crear aseguradoras', 'actualizar aseguradoras', 'crear hospitales', 'actualizar hospitales', 'crear medicinas', 'actualizar medicinas', 'crear patologias', 'actualizar patologias'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.management.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Administrar sistema</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany

                    @canany(['ver paciente', 'crear pacientes', 'actualizar pacientes'])
                        <div class="col">
                            <a href="{{ route('patients.list_patients') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Administrar pacientes</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                </div>
            </div>
        @endcanany

        {{-- Beneficios --}}
        @canany(['crear beneficios', 'actualizar beneficios', 'cerrar beneficios', 'crear compras', 'actualizar compras', 'crear cartas', 'actualizar cartas', 'corregir beneficios', 'eliminar beneficios', 'restaurar beneficios'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-orange py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Beneficios</label>
                        </div>
                    </div>
                    @canany(['crear beneficios', 'actualizar beneficios', 'cerrar beneficios', 'crear compras', 'actualizar compras', 'crear cartas', 'actualizar cartas'])
                        <div class="col mb-1">
                            <a href="{{ route('benefits.benefits.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #CB5032 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Consultar beneficios</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-orange d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    @canany(['corregir beneficios', 'eliminar beneficios', 'restaurar beneficios'])
                        <div class="col mb-1">
                            <a href="{{ route('corrections.benefits.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #CB5032 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Corrección de beneficios</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-orange d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                </div>
            </div>
        @endcanany

        {{-- Pruebas diagnosticas --}}
        @canany(['crear pruebas', 'actualizar pruebas'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-success py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Pruebas diagnósticas</label>
                        </div>
                    </div>
                    @canany(['crear pruebas', 'actualizar pruebas'])
                        <div class="col mb-1">
                            <a href="{{ route('management-diagnostic-tests.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #4CAF50 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Consultar pruebas diagnósticas</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-success d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    <div class="col">
                        <div class="card-body bg-white shadow p-0" style="border-bottom: solid #4CAF50 1px;">
                            <div class="row mx-auto ">
                                <div class="col-10 d-flex align-items-center">
                                    <div class="col p-0 d-flex justify-content-end">
                                        <div class="bg-white d-flex justify-content-center"
                                            style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endcanany

        {{-- Seguimiento --}}
        @canany(['crear seguimientos', 'actualizar seguimientos'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-orange py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Seguimientos</label>
                        </div>
                    </div>
                    @can('crear seguimientos')
                        <div class="col mb-1">
                            <a href="{{ route('tracking.patients') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #CB5032 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Registrar seguimientos</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-orange d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcan

                    @can('actualizar seguimientos')
                        <div class="col mb-1">
                            <a href="{{ route('tracking.management_show') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #CB5032 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Modificar Seguimientos</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-orange d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcan
                </div>
            </div>
        @endcanany

        {{-- Reportes --}}
        @can('ver reportes')
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-primary py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Reportes</label>
                        </div>
                    </div>
                    @can('ver reportes')
                        <div class="col mb-1">
                            <a href="{{ route('reports.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #0C68B0 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 link_card_danger text-capitalize">Consultar reportes</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-primary d-flex justify-content-center" style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcan
                    <div class="col">
                        <div class="card-body bg-white shadow p-0" style="border-bottom: solid #0C68B0 1px;">
                            <div class="row mx-auto ">
                                <div class="col-10 d-flex align-items-center">
                                    <div class="col p-0 d-flex justify-content-end">
                                        <div class="bg-white d-flex justify-content-center"
                                            style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endcan
    </div>

    <style>
        .title {
            font-size: 1.2em;
        }
    </style>
@endsection
