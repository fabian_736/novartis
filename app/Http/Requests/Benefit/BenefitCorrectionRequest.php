<?php

namespace App\Http\Requests\Benefit;

use App\Enums\Benefit\AuthorizationLetterRejectionReasons;
use App\Enums\Benefit\AuthorizationLetterStatus;
use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Rules\Corrections\Benefits\NewStatusIsValid;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class BenefitCorrectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'benefit_type' => ['required', new Enum(BenefitTypes::class)],
            'status' => ['required', new NewStatusIsValid($this->benefit, $this->input('benefit_type'))],
        ];

        if ($this->input('benefit_type') == BenefitTypes::Copago->value) {
            $rules['autorized'] = ['required', new Enum(AuthorizationLetterStatus::class)];

            if ($this->input('autorized') == AuthorizationLetterStatus::Rechazado->value) {
                $rules['is_accepted'] = 'required|in:0,1';
                $rules['causal_types'] = ['required', new Enum(AuthorizationLetterRejectionReasons::class)];
            }
        }

        if (in_array($this->input('status'), [BenefitStatus::FacturaCargada->value, BenefitStatus::Cerrado->value])) {
            $rules['number_units'] = 'required|numeric|between:1,10';
            $rules['pharmacy_id'] = 'required|exists:pharmacies,id';
            $rules['discount'] = 'required|numeric|min:0.01';
            $rules['transaction_date'] = 'required|date|after_or_equal:2022-01-01|before_or_equal:today';
            $rules['invoice_number'] = ['required', 'regex:/[a-z0-9]/i'];
            $rules['factura'] = 'file|mimes:png,jpg,pdf,jpeg|max:560000';
        }

        if ($this->input('status') == BenefitStatus::Cerrado->value) {
            $rules['closing_reason'] = 'required|string|between:10,300';
        }

        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function attributes()
    {
        return [
            'benefit_type' => 'tipo de beneficio',
            'status' => 'estado del beneficio',
            'autorized' => 'autorización de carta co-pago',
            'is_accepted' => '¿el paciente acepta el descuento especial?',
            'causal_types' => 'motivo de rechazo',
            'number_units' => 'cantidad del producto',
            'pharmacy_id' => 'farmacia',
            'discount' => 'descuento otorgado en valores',
            'transaction_date' => 'fecha de transacción',
            'invoice_number' => 'orden de remisión',
            'factura' => 'fotografía de factura / orden de remisión',
            'closing_reason' => 'motivo de cierre',
        ];
    }
}
