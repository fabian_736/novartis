<?php

namespace App\Policies;

use App\Enums\Patient\PatientTypes;
use App\Models\Benefit;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BenefitPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear beneficios', 'actualizar beneficios'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear beneficios')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, Benefit $benefit)
    {
        if ($benefit->patient->patient_type == PatientTypes::NotInsured) {
            return false;
        }
        if ($user->hasPermissionTo('actualizar beneficios')) {
            return true;
        }
    }

    /**
     * Determine whether the user can amend the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function amend(User $user, Benefit $benefit)
    {
        if ($benefit->deleted_at !== null || $benefit->parent_deleted_at !== null) {
            return false;
        }

        if ($user->hasPermissionTo('corregir beneficios')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function delete(User $user, Benefit $benefit)
    {
        if ($benefit->deleted_at !== null || $benefit->parent_deleted_at !== null) {
            // dd("dates");
            return false;
        }

        if ($user->hasPermissionTo('eliminar beneficios')) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function restore(User $user, Benefit $benefit)
    {
        if ($benefit->deleted_at === null || $benefit->parent_deleted_at !== null) {
            return false;
        }

        if ($user->hasPermissionTo('restaurar beneficios')) {
            return true;
        }
    }
}
