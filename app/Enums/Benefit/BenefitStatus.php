<?php

namespace App\Enums\Benefit;

enum BenefitStatus: int
{
    case Cerrado = 0;
    case CartaPendiente = 1;
    case DescuentoEspecialRechazado = 2;
    case FacturaPendiente = 3;
    case FacturaCargada = 4;
    case ProcesoObservado = 5;
}
