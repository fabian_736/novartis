<?php

namespace App\Services\Dose;

use App\Models\Dose;

class DoseService
{
    /**
     * Returns a list of doses.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Dose>
     */
    public function list($onlyActive = true)
    {
        $doses = Dose::query();

        if ($onlyActive) {
            $doses->active();
        }

        return $doses->get();
    }

    /**
     * Create a Dose.
     *
     * @param  array  $input
     * @return Dose
     */
    public function store(array $input): Dose
    {
        return Dose::create($input);
    }

    public function update(Dose $dose, array $input): bool
    {
        return $dose->update($input);
    }
}
