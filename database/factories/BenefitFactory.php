<?php

namespace Database\Factories;

use App\Enums\Benefit\AuthorizationLetterRejectionReasons;
use App\Enums\Benefit\AuthorizationLetterStatus;
use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Models\Patient;
use App\Models\Pharmacy;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Benefit>
 */
class BenefitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'status' => BenefitStatus::CartaPendiente,
            'benefit_type' => $this->faker->randomElement(BenefitTypes::cases()),
            'benefit_type_status' => $this->faker->boolean(),
            'patient_id' => Patient::factory(),
            'pharmacy_id' => Pharmacy::factory(),
        ];
    }

    /**
     * Indicate that the model's benefit type should be Copago.
     *
     * @return static
     */
    public function copay()
    {
        return $this->state(function (array $attributes) {
            return [
                'benefit_type' => BenefitTypes::Copago,
                'status' => BenefitStatus::FacturaPendiente,
            ];
        });
    }

    /**
     * Indicate that the model's benefit type should be OOP.
     *
     * @return static
     */
    public function OOP()
    {
        return $this->state(function (array $attributes) {
            return [
                'benefit_type' => BenefitTypes::OOP,
            ];
        });
    }

    /**
     * Indicate that the model's authorization letter should be approved.
     *
     * @return static
     */
    public function approvedAuthorizationLetter()
    {
        return $this->state(function (array $attributes) {
            return [
                'autorized' => AuthorizationLetterStatus::Aprobado,
                'status' => BenefitStatus::FacturaPendiente,
            ];
        });
    }

    /**
     * Indicate that the model's authorization letter should be declined and special discount accepted.
     *
     * @return static
     */
    public function acceptedSpecialDiscount()
    {
        return $this->state(function (array $attributes) {
            return [
                'autorized' => AuthorizationLetterStatus::Rechazado,
                'causal_types' => $this->faker->randomElement(AuthorizationLetterRejectionReasons::cases()),
                'status' => BenefitStatus::FacturaPendiente,
                'benefit_type' => BenefitTypes::OOP,
            ];
        });
    }

    /**
     * Indicate that the model's authorization letter and special discount should be declined.
     *
     * @return static
     */
    public function declinedSpecialDiscount()
    {
        return $this->state(function (array $attributes) {
            return [
                'autorized' => AuthorizationLetterStatus::Rechazado,
                'causal_types' => $this->faker->randomElement(AuthorizationLetterRejectionReasons::cases()),
                'status' => BenefitStatus::DescuentoEspecialRechazado,
                'benefit_type' => BenefitTypes::Copago,
            ];
        });
    }

    /**
     * Indicate that the model's authorization letter should be observed.
     *
     * @return static
     */
    public function observed()
    {
        return $this->state(function (array $attributes) {
            return [
                'autorized' => AuthorizationLetterStatus::Aprobado,
                'status' => BenefitStatus::FacturaPendiente,
            ];
        });
    }

    /**
     * Indicate that the model's status should be Factura pendiente.
     *
     * @return static
     */
    public function pendingInvoice()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => BenefitStatus::FacturaPendiente,
            ];
        });
    }

    /**
     * Indicate that the model's status should be Factura cargada.
     *
     * @return static
     */
    public function invoiceUploaded()
    {
        return $this->state(function (array $attributes) {
            return [
                'number_units' => $this->faker->numberBetween(1, 10),
                'pharmacy_id' => Pharmacy::factory()->create(),
                'discount' => $this->faker->numberBetween(0, 100),
                'transaction_date' => now(),
                'invoice_number' => $this->faker->regexify('[a-z0-9]{10,40}'),
                'status' => BenefitStatus::FacturaCargada,
            ];
        });
    }

    /**
     * Indicate that the model's status should be closed
     *
     * @return static
     */
    public function closed()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => BenefitStatus::Cerrado,
                'closing_reason' => $this->faker->words(asText: true),
                'closed_at' => now(),
            ];
        });
    }
}
