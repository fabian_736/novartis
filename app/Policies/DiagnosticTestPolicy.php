<?php

namespace App\Policies;

use App\Models\DiagnosticTest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DiagnosticTestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear pruebas', 'actualizar pruebas'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\DiagnosticTest  $diagnosticTest
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function view(User $user, DiagnosticTest $diagnosticTest)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear pruebas')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\DiagnosticTest  $diagnosticTest
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, DiagnosticTest $diagnosticTest)
    {
        if ($user->hasPermissionTo('actualizar pruebas')) {
            return true;
        }
    }
}
