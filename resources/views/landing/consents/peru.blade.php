@extends('landing.consents.base')
@section('consent')
    <div class="col-11 mx-auto d-flex justify-content-center p-4 my-5" style="border: solid 1px black; ">
        <div class=" perfect-scrollbar-on " style="min-height: 40vh; max-height: 40vh; overflow: auto; ">
            <div class="row-reverse">
                <div class="col">
                    <label for="" class="h4 font-weight-bold">Aviso de privacidad para
                        pacientes</label>
                </div>
                <div class="col">
                    <p class="h5">
                        NOVARTIS BIOCIENCES PERU S.A., con domicilio en Juan de Arona 151 – San Isidro oficina 601 y correo
                        electrónico datospersonales.peru@novartis.com (en adelante “Novartis”) ha generado un programa de
                        apoyo que otorga los siguientes beneficios a los pacientes que hayan sido prescritos por su médico
                        tratante con los medicamentos Cosentyx® o Xolair®
                    </p>
                </div>
                <div class="col">
                    <p class="h5">A través de este programa, el paciente podrá recibir los siguientes beneficios:
                    </p>
                </div>
                <div class="col ">
                    <ul>
                        <li style="list-style: none"><i class="h5">1. Descuento para pacientes que realicen la
                                compra del tratamiento a través de puntos de venta autorizados y no cuenten con seguro
                                privado para Cosentyx® o Xolair®.</i></li><br>
                        <li style="list-style: none"><i class="h5">2. Cobertura del co-pago del tratamiento
                                según póliza de aseguradora para Cosentyx® o Xolair®</i></li><br>
                        <li style="list-style: none"><i class="h5">3. Cobertura de costo de pruebas de
                                diagnóstico y de monitoreo realizadas por terceros y que sean solicitadas por su médico
                                tratante para Cosentyx® o Xolair®</i>
                        </li><br>
                    </ul>
                </div>
                <div class="col">
                    <p class="h5">Es importante recalcar que los beneficios dependerán del producto prescrito y
                        de la región en la cual se lleve a cabo el programa<br><br>

                        Para lo anterior, si usted continúa con su registro en esta página, entenderemos que otorga su
                        consentimiento para el tratamiento de datos personales registrados con base en el Aviso de
                        Privacidad; así como la aceptación de términos y condiciones del programa.<br><br>
                    </p>
                </div>
                <div class="col">

                    <ul>
                        <li style="list-style: none"><i class="h5">1. Nombre completo del paciente</i></li><br>
                        <li style="list-style: none"><i class="h5">2. Fecha de nacimiento</i>
                        </li><br>
                        <li style="list-style: none"><i class="h5">3. Correo electrónico</i>
                        </li><br>
                        <li style="list-style: none"><i class="h5">4. Teléfono</i>
                        </li><br>
                        <li style="list-style: none"><i class="h5">5. Producto prescrito</i>
                        </li><br>
                        <li style="list-style: none"><i class="h5">6. Seleccione patología y dosis</i></li><br>
                    </ul>

                </div>

                <div class="col">
                    <p class="h5">Con base en lo anterior, los datos personales que Novartis recabe podrán ser
                        tratados únicamente para las siguientes finalidades:
                    </p>
                </div>

                <div class="col">
                    <p class="h5">Finalidades primarias, estas son aquellas necesarias para poder concretar la
                        relación con usted y que cumplen el objetivo principal de la recolección, en caso de no estar de
                        acuerdo con estas no podrán realizarse las acciones descritas: (i) Para que usted pueda participar
                        en programas de pacientes y/o de investigación clínica; (ii) Para que usted pueda participar y/o
                        colaborar en diversos eventos o foros relacionados con la salud, ya sea presenciales o digitales
                        (remotos); (iii) Para integrar bases de datos acorde con las otras finalidades aquí descritas; (iv)
                        Para cumplir la legislación aplicable; (v) Para mantener la relación jurídica que pueda generarse;
                        (vi) En algunos casos, Novartis podrán realizar las acciones que considere necesarias, a efecto de
                        comprobar, directamente o a través de terceros, la veracidad de los datos proporcionados y; (vii)
                        Permitirle el acceso a cualquiera de nuestras instalaciones.</p>
                </div>
                <div class="col">
                    <p class="h5">
                        Finalidades secundarias o complementarias, no necesarias para cumplir con el objetivo principal de
                        la recolección: adicionalmente, se podrán utilizar sus datos personales: (i) Para invitarlo a
                        participar en diferentes programas, eventos, foros y conferencias relacionados con la salud ya sea
                        en forma presencial o remota; (ii) Para ofrecerle y/o enviarle información relacionada con la salud
                        y, en algunos casos, muestras médicas; (iii) Para, previa codificación y/o anonimización de los
                        datos, agregarlos a otros y realizar análisis estadísticos, de generación de modelos de información
                        y/o perfiles de comportamiento actual y predictivo y/o investigaciones clínicas o científicas; (iv)
                        Participar en encuestas; (v) Para cumplir con nuestros procesos internos y; (vi) Para dar
                        seguimiento a los eventos adversos que hayan tenido que ser reportados conforme a la legislación
                        aplicable. Adicionalmente, y únicamente para las finalidades descritas más arriba, Novartis podrá
                        compartir sus datos con otras filiales de Novartis o su tercero designado.
                    </p>
                </div>
                <div class="col">
                    <p class="h5">
                        Los datos personales se almacenarán en un banco de datos personales titularidad de Novartis o su
                        tercero designado.<br><br>

                        Se deja constancia de que la entrega de los datos personales es facultativa. [Sin embargo, en caso
                        de que el paciente no desee brindar sus datos, no será posible brindarle los beneficios del
                        Programa].<br><br>

                        Asimismo, se le informa que los datos personales que se recabarán podrían ser remitidos por Novartis
                        a servidores, filiales y/o proveedores que se encuentren fuera de Perú, pero siempre con el único
                        propósito de cumplir con las finalidades descritas más arriba y respetando en todo momento la
                        seguridad y confidencialidad de los datos.<br><br>

                        Con base en lo anterior, Novartis da a conocer a usted el uso de la información para el tratamiento
                        de su datos personales y personales sensibles con base en las finalidades aquí descritas y cumple
                        con la legislación aplicable sobre datos personales y personales sensibles<br><br>

                        Al dar click en el botón “Aceptar y continuar”, yo (nombre y apellido) otorgo mi consentimiento y
                        autorizo a Novartis para el procesamiento, dentro y/o fuera del país, de mis datos personales y
                        personales sensibles que recabe con base el presente documento. <br><br>

                        Manifiesto que mi consentimiento es voluntario y que no es requerimiento para poder adquirir mi
                        medicamento. <br><br>

                        Finalmente reconozco que: (i) El presente consentimiento tendrá la misma vigencia que el Programa o
                        la de mi participación en este, (ii) Tengo el derecho de solicitar acceso, la corrección, o incluso,
                        revocar mi consentimiento en cualquier momento respecto a mis datos personales y (iii) Cualquier
                        solicitud respecto a mi información personal, debe realizarse a través del siguiente correo:
                        datospersonales.peru@novartis.com <br><br>

                        Novartis garantiza que la información personal estará debidamente protegida a través de la
                        implementación de medidas administrativas, técnicas y físicas tendientes a prevenir la pérdida, el
                        uso indebido, el acceso no autorizado, la divulgación o la alteración de sus datos personales. La
                        información podrá ser bloqueada y almacenada por un plazo de 10 años, contados a partir del
                        cumplimiento del objetivo para el que se recabó la misma <br>

                        Para más información sobre nuestro aviso de privacidad puede ingresar al siguiente enlace <a target="_blank" href=" https://www.cac.novartis.com/politica-de-privacidad"> https://www.cac.novartis.com/politica-de-privacidad</a>

                    </p>
                </div>
                <div class="col my-5 d-flex justify-content-center">
                    <img src="{{ url('img/logo_novartis.png') }}" alt="" class="w-25">
                </div>
                <div class="col my-5 d-flex justify-content-center h5">
                    <span>Para mayor información comuníquese a la línea 01-6409653</span>
                </div>
            </div>

        </div>
    </div>
@endsection
