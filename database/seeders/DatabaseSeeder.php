<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CitySeeder::class,
            SpecialtySeeder::class,
            // DoctorSeeder::class,
            HospitalSeeder::class,
            InsuranceCarrierSeeder::class,
            DoseSeeder::class,
            DiagnosticTestSeeder::class,
            DocumentTypeSeeder::class,
            PharmacyChainSeeder::class,
            PermissionsSeeder::class,
            UsersSeeder::class,
            PatientSeeder::class,
        ]);
    }
}
