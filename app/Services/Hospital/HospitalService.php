<?php

namespace App\Services\Hospital;

use App\Models\Hospital;

class HospitalService
{
    /**
     * Returns a list of hospitals.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Hospital>
     */
    public function list($onlyActive = true)
    {
        $hospitals = Hospital::query();

        if ($onlyActive) {
            $hospitals->active();
        }

        return $hospitals->get();
    }

    /**
     * Create a hospital.
     *
     * @param  array  $input
     * @return Hospital
     */
    public function store(array $input): Hospital
    {
        return Hospital::create($input);
    }

    public function update(Hospital $hospital, array $input): bool
    {
        return $hospital->update($input);
    }
}
