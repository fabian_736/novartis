<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/favicon.png') }}">
    <link rel="icon" type="image/png" href="{{ url('img/favicon.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Novartis Landing
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

    <link href="{{ url('css/app.css') }}" rel="stylesheet" />
    <link href="{{ url('css/landing.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script src="{{ url('js/app.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    
</head>



<body class="off-canvas-sidebar body_landing">
    <div class="wrapper wrapper-full-page">

        <div class="container ">


            @yield('content')



        </div>
    </div>






    <style>
        select.form-control:not([size]):not([multiple]) {
            background: #ffffff;
            border-radius: 5px;
            padding-inline: 25px;
            color: rgb(0, 0, 0);
            cursor: pointer;
        }

    </style>


    <style>
        ::placeholder {
            font-weight: bold !important;
        }

        body {
            padding: 0 !important;
            margin: 0 !important;
        }

        .perfect-scrollbar-on::-webkit-scrollbar {
            width: 10px;
            /* Tamaño del scroll en vertical */
            height: 10px;
            /* Tamaño del scroll en horizontal */
            ma
        }

        .perfect-scrollbar-on::-webkit-scrollbar-thumb {
            background: #1D61A6 !important;

        }

        /* Cambiamos el fondo y agregamos una sombra cuando esté en hover */
        .perfect-scrollbar-on::-webkit-scrollbar-thumb:hover {
            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
        }

        /* Cambiamos el fondo cuando esté en active */
        .perfect-scrollbar-on::-webkit-scrollbar-thumb:active {
            background-color: #999999;
        }


        .perfect-scrollbar-on::-webkit-scrollbar-track {
            background: #DDDDDD;
        }

    </style>



</body>

</html>
