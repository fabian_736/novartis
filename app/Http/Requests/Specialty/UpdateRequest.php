<?php

namespace App\Http\Requests\Specialty;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'between:4,100', 'regex:/^[a-záéíóúüñ ]+/i', Rule::unique('specialties', 'name')->ignore($this->specialty)],
            'is_active' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'is_active' => 'Estado',
        ];
    }
}
