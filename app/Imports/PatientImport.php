<?php

namespace App\Imports;

use App\Enums\Gender;
use App\Enums\Patient\PatientStatus;
use App\Enums\Patient\PatientTypes;
use App\Enums\RelationshipPatient;
use App\Models\City;
use App\Models\Doctor;
use App\Models\DocumentType;
use App\Models\Dose;
use App\Models\Hospital;
use App\Models\InsuranceCarrier;
use App\Models\Medicine;
use App\Models\MedicineDose;
use App\Models\Pathology;
use App\Models\PathologyHasMedicineDose;
use App\Models\Patient;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Enum;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Row;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PatientImport implements OnEachRow, WithBatchInserts, WithChunkReading, WithHeadingRow, WithValidation
{
    use Importable;

    /**
     * The cities cache.
     *
     * @var array<string, int|null>
     */
    private $cities = [];

    /**
     * The doctors cache.
     *
     * @var array<string, int|null>
     */
    private $doctors = [];

    /**
     * The document types cache.
     *
     * @var array<string|int, int|null>
     */
    private $documentTypes = [];

    /**
     * The hospitals cache.
     *
     * @var array<string, int|null>
     */
    private $hospitals = [];

    /**
     * The insurances cache.
     *
     * @var array<string, int|null>
     */
    private $insurances = [];

    /**
     * The medicines cache.
     *
     * @var array<string, int|null>
     */
    private $medicines = [];

    /** @var \Illuminate\Database\Eloquent\Collection<int, MedicineDose> */
    private $medicineDoses;

    /**
     * The pathologies cache.
     *
     * @var array<string, int|null>
     */
    private $pathologies = [];

    /** @var \Illuminate\Database\Eloquent\Collection<int, PathologyHasMedicineDose> */
    private $pathologyHasMedicineDoses;

    /**
     * The medicine presentations cache.
     *
     * @var array<string, int|null>
     */
    private $presentations = [];

    /**
     * Creates a new instance for the import.
     */
    public function __construct()
    {
        $this->medicineDoses = MedicineDose::all();
        $this->pathologyHasMedicineDoses = PathologyHasMedicineDose::all();
    }

    /**
     * Undocumented function.
     *
     * @param  Row  $row
     * @return void
     */
    public function onRow(Row $row)
    {
        $rowIndex = $row->getIndex();
        $row = $row->toArray();

        $patient = Patient::create([
            'document_number' => $row['document_number'],
            'document_type_id' => $row['document_type_id'],
            'name' => $row['name'],
            'birth_date' => $row['birth_date'],
            'gender' => $row['gender'],
            'city_id' => $row['city_id'],
            'email' => $row['email'],
            'phone' => $row['phone'],
            'patient_type' => $row['patient_type'],
            'first_time_using_biological' => $row['first_time_using_biological'],
            'hospital_id' => $row['hospital_id'],
            'insurance_carrier_id' => $row['insurance_carrier_id'],
            'has_carer' => $row['has_carer'],
            'relationship_patient' => $row['relationship_patient'],
            'carer_document_type_id' => $row['carer_document_type_id'],
            'carer_document_number' => $row['carer_document_number'],
            'carer_name' => $row['carer_name'],
            'carer_birth_date' => $row['carer_birth_date'],
            'carer_phone' => $row['carer_phone'],
            'carer_email' => $row['carer_email'],
            'patient_status' => $row['patient_status'],
            'date_admission' => $row['date_admission'],
        ]);

        $treatment = $patient->treatment()->create([
            'doctor_id' => $row['doctor_id'],
            'pathology_id' => $row['pathology_id'],
        ]);

        $treatment->formulations()->create([
            'starts_at' => now('America/Lima'),
            'frequency' => $row['frequency'],
            // 'dose' => $row['dose'], // TODO: Definir qué se hará con este campo
            'pathology_has_medicine_dose_id' => $row['pathology_has_medicine_dose_id'],
        ]);
    }

    /**
     * Prepare the import data for validation.
     *
     * @param  mixed  $data
     * @param  mixed  $index
     * @return array
     */
    public function prepareForValidation($data, $index): array
    {
        $newData = [];

        $newData['document_number'] = $data['dni'] ?? null;
        $newData['document_type_id'] = $this->getDocumentTypeId($data['tipo_de_documento']);
        $newData['name'] = $data['nombre_paciente'] ?? null;
        $newData['birth_date'] = $this->transformDate($data['fecha_nacimiento']);
        $newData['gender'] = $this->getNormalizedGender($data['genero']);
        $newData['country_id'] = $data['pais'] ?? null;
        $newData['city_id'] = $this->getCityId($data['pais'], $data['ciudad']);
        $newData['email'] = $data['email'] ?? null;
        $newData['phone'] = $data['numero_celular'] ?? null;
        $newData['patient_type'] = $this->getNormalizedPatientType($data['tipo_paciente']);
        $newData['first_time_using_biological'] = $this->getNormalizedBooleanColumn($data['primera_vez_usando_biologicos']);
        $newData['patient_status'] = $data['estado_paciente'] ?? null;
        $newData['date_admission'] = $this->transformDate($data['fecha_de_ingreso']);

        if ($newData['patient_type'] == PatientTypes::Insured->value) {
            $newData['insurance_carrier_id'] = $this->getInsuranceId($data['aseguradora']);
            $newData['hospital_id'] = null;
        } else {
            $newData['insurance_carrier_id'] = null;
            $newData['hospital_id'] = $this->getHospitalId($data['clinicahospital']);
        }

        $newData['has_carer'] = $this->getNormalizedBooleanColumn($data['tiene_acudiente']);

        if ($newData['has_carer'] == 1) {
            $newData['relationship_patient'] = $data['vinculo_del_acudiente'] ?? null;
            $newData['carer_document_type_id'] = $this->getDocumentTypeId($data['tipo_de_documento_acudiente']);
            $newData['carer_document_number'] = $data['dni_de_acudiente'] ?? null;
            $newData['carer_name'] = $data['nombre_de_acudiente'] ?? null;
            $newData['carer_birth_date'] = $this->transformDate($data['fecha_nacimiento_acudiente']);
            $newData['carer_phone'] = $data['telefono_acudiente'] ?? null;
            $newData['carer_email'] = $data['email_acudiente'] ?? null;
        } else {
            $newData['relationship_patient'] = null;
            $newData['carer_document_type_id'] = null;
            $newData['carer_document_number'] = null;
            $newData['carer_name'] = null;
            $newData['carer_birth_date'] = null;
            $newData['carer_phone'] = null;
            $newData['carer_email'] = null;
        }

        $newData['doctor_id'] = $this->getDoctorId($data['medico']);
        $newData['frequency'] = $data['frecuencia_de_consumo_medicamento_dias_al_mes'] ?? null;
        // $newData['dose'] = $data['dosis'] ?? null; // TODO: Definir qué se hará con este campo
        $newData['pathology_id'] = $this->getPathologyId($data['patologia_enfermedad']);
        $newData['medicine_id'] = $this->getMedicineId($data['medicamento']);
        $newData['pathology_has_medicine_dose_id'] = $this->getPathologyHasMedicineDoseId(
            pathologyName: $data['patologia_enfermedad'],
            medicineName: $data['medicamento'],
            presentationName: $data['presentacion']
        );

        return $newData;
    }

    /**
     * Validates the import data.
     *
     * @return array
     */
    public function rules(): array
    {
        $maxPersonBirthDate = now()->subCentury()->toDateString();
        $minCareerBirthData = now()->subYears(18)->toDateString();

        return [
            'document_type_id' => 'required',
            'document_number' => 'nullable|alpha_num|unique:patients,document_number',
            'name' => ['required', 'regex:/^[a-záéíóúüñ ]+/i'],
            'birth_date' => "nullable|date|after_or_equal:$maxPersonBirthDate|before_or_equal:today",
            'gender' => ['required', new Enum(Gender::class)],
            'country_id' => 'required',
            'city_id' => 'required',
            'email' => 'nullable|email:rfc,dns|unique:patients,email',
            'phone' => 'nullable|numeric|digits_between:7,9',
            'patient_type' => ['required', new Enum(PatientTypes::class)],
            'first_time_using_biological' => 'required|in:1,0',
            'hospital_id' => 'required_if:*.patient_type,0|nullable',
            'insurance_carrier_id' => 'required_if:*.patient_type,1|nullable',
            'has_carer' => 'nullable|in:1,0',
            'relationship_patient' => ['required_if:*.has_carer,1|nullable', new Enum(RelationshipPatient::class)],
            'carer_document_type_id' => 'required_if:*.has_carer,1|nullable',
            'carer_document_number' => 'required_if:*.has_carer,1|nullable|alpha_num',
            'carer_name' => ['required_if:*.has_carer,1|nullable', 'regex:/^[a-záéíóúüñ ]+/i'],
            'carer_birth_date' => "required_if:*.has_carer,1|nullable|date|after_or_equal:$maxPersonBirthDate|before_or_equal:$minCareerBirthData",
            'carer_phone' => 'required_if:*.has_carer,1|nullable|numeric|digits_between:7,9',
            'carer_email' => 'nullable|email:rfc,dns',
            'patient_status' => ['required', new Enum(PatientStatus::class)],
            'date_admission' => 'required|date|before_or_equal:today',
            'doctor_id' => 'required',
            'frequency' => 'required|numeric|integer|min:1',
            //'dose' => , // TODO: Definir qué se va a hacer con este campo
            'pathology_id' => 'required',
            'medicine_id' => 'required',
            'pathology_has_medicine_dose_id' => 'required',
        ];
    }

    /**
     * Show error messages.
     *
     * @return array<string, string>
     */
    public function customValidationMessages()
    {
        return [
            'required' => 'La celda :attribute es requerida o el valor ingresado no existe en nuestros registros.',
        ];
    }

    /**
     * Rename form fields.
     *
     * @return array<string, string>
     */
    public function customValidationAttributes()
    {
        return [
            'document_type_id' => 'TIPO DE DOCUMENTO',
            'document_number' => 'DNI',
            'name' => 'NOMBRE PACIENTE',
            'birth_date' => 'FECHA NACIMIENTO',
            'gender' => 'GENERO',
            'country_id' => 'PAIS',
            'city_id' => 'CIUDAD',
            'email' => 'email',
            'phone' => 'numero celular',
            'patient_type' => 'tipo paciente',
            'first_time_using_biological' => '¿Primera vez usando biológicos?',
            'hospital_id' => 'Clinica/Hospital',
            'insurance_carrier_id' => 'Aseguradora',
            'has_carer' => '¿Tiene acudiente?',
            'relationship_patient' => 'Vínculo del acudiente',
            'carer_document_type_id' => 'TIPO DE DOCUMENTO ACUDIENTE',
            'carer_document_number' => 'DNI DE ACUDIENTE',
            'carer_name' => 'NOMBRE DE ACUDIENTE',
            'carer_birth_date' => 'FECHA NACIMIENTO ACUDIENTE',
            'carer_phone' => 'TELÉFONO ACUDIENTE',
            'carer_email' => 'EMAIL ACUDIENTE',
            'patient_status' => 'ESTADO DEL PACIENTE',
            'date_admission' => 'FECHA DE INGRESO',
            'doctor_id' => 'MEDICO',
            'frequency' => 'Frecuencia de consumo medicamento (días al mes)',
            'dose' => 'Dosis',
            'pathology_id' => 'patología (enfermedad)',
            'medicine_id' => 'Medicamento',
            'pathology_has_medicine_dose_id' => 'Presentación',
        ];
    }

    /**
     * Returns the city id by the given country id and city name.
     *
     * @param  int  $countryId
     * @param  string  $cityName
     * @return int|null
     */
    private function getCityId($countryId, $cityName)
    {
        if (array_key_exists($cityName, $this->cities)) {
            return $this->cities[$cityName];
        }

        $city = City::where('country_id', $countryId)->where('name', $cityName)->first();

        $this->cities[$cityName] = $city?->id ?? null;

        return $this->cities[$cityName];
    }

    /**
     * Returns the doctor id by the given doctor name.
     *
     * @param  string  $doctorName
     * @return int|null
     */
    private function getDoctorId($doctorName)
    {
        if (array_key_exists($doctorName, $this->doctors)) {
            return $this->doctors[$doctorName];
        }

        $doctor = Doctor::where('name', $doctorName)->first();

        $this->doctors[$doctorName] = $doctor?->id ?? null;

        return $this->doctors[$doctorName];
    }

    /**
     * Gets the document type id by the given input.
     *
     * @param  string|int  $documentTypeId
     * @return int|null
     */
    private function getDocumentTypeId($documentTypeId)
    {
        if (array_key_exists($documentTypeId, $this->documentTypes)) {
            return $this->documentTypes[$documentTypeId];
        }

        $documentType = DocumentType::where(function ($query) use ($documentTypeId) {
            return $query->where('id', $documentTypeId)
                ->orWhere('name', $documentTypeId);
        })->first();

        $this->documentTypes[$documentTypeId] = $documentType?->id ?? null;

        return $this->documentTypes[$documentTypeId];
    }

    /**
     * Gets the hospital id by the given hospital name.
     *
     * @param  string  $hospitalName
     * @return int|null
     */
    private function getHospitalId($hospitalName)
    {
        if (array_key_exists($hospitalName, $this->hospitals)) {
            return $this->hospitals[$hospitalName];
        }

        $hospital = Hospital::where('name', $hospitalName)->first();

        $this->hospitals[$hospitalName] = $hospital?->id ?? null;

        return $this->hospitals[$hospitalName];
    }

    /**
     * Gets the insurance id by the given insurance name.
     *
     * @param  string  $insuranceName
     * @return int|null
     */
    private function getInsuranceId($insuranceName)
    {
        if (array_key_exists($insuranceName, $this->insurances)) {
            return $this->insurances[$insuranceName];
        }

        $insurance = InsuranceCarrier::where('name', $insuranceName)->first();

        $this->insurances[$insuranceName] = $insurance?->id ?? null;

        return $this->insurances[$insuranceName];
    }

    /**
     * Gets the medicine id by the given medicine name.
     *
     * @param  string  $medicineName
     * @return int|null
     */
    private function getMedicineId($medicineName)
    {
        if (array_key_exists($medicineName, $this->medicines)) {
            return $this->medicines[$medicineName];
        }

        $medicine = Medicine::where('name', $medicineName)->first();

        $this->medicines[$medicineName] = $medicine?->id ?? null;

        return $this->medicines[$medicineName];
    }

    /**
     * Gets the medicine dose id by the given medicine name and presentation name.
     *
     * @param  string  $medicineName
     * @param  string  $presentationName
     * @return int|null
     */
    private function getMedicineDoseId($medicineName, $presentationName)
    {
        $medicineId = $this->getMedicineId($medicineName);
        $presentationId = $this->getPresentationId($presentationName);

        $medicineDose = $this->medicineDoses
            ->where('medicine_id', $medicineId)
            ->firstWhere('dose_id', $presentationId);

        return $medicineDose?->id;
    }

    /**
     * Normalize the boolean columns with "SI"/"NO" input.
     *
     * @param  string  $input
     * @return int|null
     */
    private function getNormalizedBooleanColumn($input)
    {
        $input = Str::lower($input);

        return match ($input) {
            'si', '1' => 1,
            'no', '0' => 0,
            default => null,
        };
    }

    /**
     * Normalizes the given gender string.
     *
     * @param  string  $gender
     * @return string|null
     */
    private function getNormalizedGender($gender)
    {
        $gender = Str::lower($gender);

        return match ($gender) {
            'male', 'hombre', 'masculino' => Gender::male->value,
            'female', 'mujer', 'femenino' => Gender::Female->value,
            default => null
        };
    }

    /**
     * Normalize the "patient type" input.
     *
     * @param  string  $input
     * @return int|null
     */
    private function getNormalizedPatientType($input)
    {
        $input = Str::lower($input);

        return match ($input) {
            'asegurado', '1' => 1,
            'no asegurado', '0' => 0,
            default => null,
        };
    }

    /**
     * Gets the pathology by the given pathology name.
     *
     * @param  string  $pathologyName
     * @return int|null
     */
    private function getPathologyId($pathologyName)
    {
        if (array_key_exists($pathologyName, $this->pathologies)) {
            return $this->pathologies[$pathologyName];
        }

        $pathology = Pathology::where('name', $pathologyName)->first();

        $this->pathologies[$pathologyName] = $pathology?->id ?? null;

        return $this->pathologies[$pathologyName];
    }

    /**
     * Returns the pathology has medicine dose id by the given pathology name, medicine name and presentation name.
     *
     * @param  string  $pathologyName
     * @param  string  $medicineName
     * @param  string  $presentationName
     * @return int|null
     */
    private function getPathologyHasMedicineDoseId($pathologyName, $medicineName, $presentationName)
    {
        $pathologyId = $this->getPathologyId($pathologyName);
        $medicineDoseId = $this->getMedicineDoseId($medicineName, $presentationName);

        $pathologyHasMedicineDose = $this->pathologyHasMedicineDoses->where('pathology_id', $pathologyId)
            ->where('medicine_dose_id', $medicineDoseId)
            ->first();

        return $pathologyHasMedicineDose?->id;
    }

    /**
     * Gets the medicine presentation by the given medicine presentation name.
     *
     * @param  string  $presentationName
     * @return int|null
     */
    private function getPresentationId($presentationName)
    {
        if (array_key_exists($presentationName, $this->presentations)) {
            return $this->presentations[$presentationName];
        }

        $presentation = Dose::where('presentation', $presentationName)->first();

        $this->presentations[$presentationName] = $presentation?->id ?? null;

        return $this->presentations[$presentationName];
    }

    /**
     * Transform a date value into a date string.
     *
     * @param  string|float|int  $value
     * @param  string  $format
     * @return string|null
     */
    public function transformDate($value, $format = 'Y-m-d'): ?string
    {
        if (empty($value)) {
            return null;
        }

        try {
            /** @var int|float $value */
            $date = Carbon::instance(Date::excelToDateTimeObject($value));
        } catch (\Throwable $e) {
            /** @var string $value */
            $date = Carbon::createFromFormat($format, $value);
        }

        return $date ? $date->toDateString() : null;
    }

    /**
     * {@inheritDoc}
     */
    public function headingRow(): int
    {
        return 2;
    }

    /**
     * {@inheritDoc}
     */
    public function batchSize(): int
    {
        return 200;
    }

    /**
     * {@inheritDoc}
     */
    public function chunkSize(): int
    {
        return 200;
    }
}
