<?php

namespace App\Enums;

enum UserTypes: string
{
    case Administrador = 'Admin';
    case Farmacia = 'Farmacia';
    case Novartis = 'Novartis';
}
