<?php

namespace App\Http\Controllers;

use App\Enums\Patient\PatientStatus;
use App\Enums\Patient\PatientTypes;
use App\Http\Requests\Patient\PatientUpdateRequest;
use App\Http\Requests\Patient\UpdateTreatmentRequest;
use App\Imports\PatientImport;
use App\Models\City;
use App\Models\Country;
use App\Models\Doctor;
use App\Models\DocumentType;
use App\Models\Hospital;
use App\Models\InsuranceCarrier;
use App\Models\Medicine;
use App\Models\Pathology;
use App\Models\Patient;
use App\Services\Formulation\FormulationService;
use App\Services\Patient\PatientService;
use App\Services\Treatment\TreatmentService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PatientController extends Controller
{
    use ApiResponses;

    /**
     * The formulation service instance
     *
     * @var FormulationService
     */
    private FormulationService $formulationService;

    /**
     * The patient service
     *
     * @var PatientService
     */
    private PatientService $patientService;

    /**
     * The treatment service
     *
     * @var TreatmentService
     */
    private TreatmentService $treatmentService;

    public function __construct(PatientService $patientService, FormulationService $formulationService, TreatmentService $treatmentService)
    {
        $this->formulationService = $formulationService;
        $this->patientService = $patientService;
        $this->treatmentService = $treatmentService;
    }

    /**
     * Shows the diagnostic test of the patient.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function patient_diagnostic()
    {
        $patients = $this->patientService->list();

        return view('coordinator.diagnostic.list', compact('patients'));
    }

    /**
     * Shows the list of patients.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function list_patients()
    {
        $query = Patient::with(['lastBenefit', 'city.country'])
            ->withExists('managementDiagnosticTests')->orderby('id', 'asc');

        /** @var \App\Models\User */
        $user = auth()->user();

        if ($user->hasPermissionTo('restaurar pacientes') or $user->is_admin) {
            $query->withTrashed();
        }

        $patients = $query->get();

        return view('coordinator.list_patient', compact('patients'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Patient  $patient
     * @return \Illuminate\Contracts\View\View
     */
    public function personal_information(Patient $patient)
    {
        $this->authorize('update', $patient);

        $documentTypes = DocumentType::all();
        $countries = Country::active()
            ->orWhere('id', $patient->city->country->id)
            ->get();
        $insurance_carriers = InsuranceCarrier::active()
            ->orWhere('id', $patient->insurance_carrier_id)
            ->get();
        $hospitals = Hospital::active()
            ->orWhere('id', $patient->hospital_id)
            ->get();
        $patient->load('city.country', 'documentType')->get();
        $cities = City::where('country_id', $patient->city->country_id)->active()->get();

        return view('coordinator.patient.form', compact('patient', 'documentTypes', 'countries', 'insurance_carriers', 'hospitals', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Patient  $patient
     * @param  PatientUpdateRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_patient(Patient $patient, PatientUpdateRequest $request)
    {
        $this->authorize('update', $patient);

        $attributes = $request->validated();

        if ($attributes['has_carer'] != 1) {
            $attributes['relationship_patient'] = null;
            $attributes['carer_document_type_id'] = null;
            $attributes['carer_document_number'] = null;
            $attributes['carer_name'] = null;
            $attributes['carer_birth_date'] = null;
            $attributes['carer_email'] = null;
            $attributes['carer_phone'] = null;
        }

        if ($attributes['patient_type'] == PatientTypes::NotInsured->value) {
            $attributes['insurance_carrier_id'] = null;
        } else {
            $attributes['hospital_id'] = null;
        }

        switch ($attributes['patient_status']) {
            case PatientStatus::Suspended->value:
                $attributes['is_reactivated'] = null;
                $attributes['inactivated_at'] = null;
                $attributes['suspended_at'] = now();
                $attributes['outside_of_program_since'] = null;
                break;
            case PatientStatus::Inactive->value:
                $attributes['is_reactivated'] = null;
                $attributes['inactivated_at'] = now();
                $attributes['suspended_at'] = null;
                $attributes['outside_of_program_since'] = null;
                $attributes['suspension_reason'] = null;
                break;
            case PatientStatus::OutsideOfProgram->value:
                $attributes['is_reactivated'] = null;
                $attributes['inactivated_at'] = null;
                $attributes['suspended_at'] = null;
                $attributes['outside_of_program_since'] = now();
                $attributes['suspension_reason'] = null;
                break;
            default:
                $attributes['suspension_reason'] = null;
                $attributes['inactivated_at'] = null;
                $attributes['suspended_at'] = null;
                $attributes['outside_of_program_since'] = null;
                break;
        }

        $this->patientService->update($patient, $attributes);
        $this->patientService->uploadConsent($patient, $attributes['file-upload'] ?? null);

        return redirect()->route('patients.list_patients')->with('message', 'Paciente editado');
    }

    /**
     * Shows the relationships for the specified resource.
     *
     * @param  Patient  $patient
     * @return \Illuminate\Contracts\View\View
     */
    public function treatment(Patient $patient)
    {
        $patient->load([
            'treatment.currentFormulation.pathologyHasMedicineDose.medicineDose.medicine',
            'treatment.currentFormulation.pathologyHasMedicineDose.medicineDose.dose',
            'treatment.currentFormulation.pathologyHasMedicineDose.pathology',
            'treatment.doctor',
        ]);

        /** @var \App\Models\Medicine|null */
        $currentPatientMedicine = $patient->treatment?->currentFormulation?->pathologyHasMedicineDose?->medicineDose?->medicine;

        /** @var \App\Models\Pathology|null */
        $patientPathology = $patient->treatment?->currentFormulation?->pathologyHasMedicineDose?->pathology;

        $doctors = Doctor::whereRelation('country', 'id', '=', $patient->city->country_id)
            ->active()
            ->orWhere('id', $patient->treatment?->doctor?->id)
            ->get();
        $medicines = Medicine::active()
            ->orWhere('id', $patient->treatment?->currentFormulation?->pathologyHasMedicineDose?->medicineDose?->medicine?->id)
            ->get();
        $pathologies = Pathology::whereRelation('medicine', 'id', '=', $currentPatientMedicine?->id)
            ->active()
            ->orWhere('id', $patient->treatment?->currentFormulation?->pathologyHasMedicineDose?->pathology?->id)
            ->get();
        $doses = $patientPathology?->medicineDoses;

        return view('coordinator.treatment.form', compact('patient', 'doctors', 'pathologies', 'medicines', 'doses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTreatmentRequest  $request
     * @param  Patient  $patient
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_treatment(UpdateTreatmentRequest $request, Patient $patient)
    {
        DB::transaction(function () use ($request, $patient) {
            // Updating patient attributes
            PatientService::update($patient, ['first_time_using_biological' => $request->safe()->first_time_using_biological]);

            $treatmentAttributes = [
                'patient_id' => $patient->id,
                'doctor_id' => $request->safe()->doctor_id,
                'pathology_id' => $request->safe()->pathology_id,
                'observations' => $request->safe()->observations,
            ];

            // Updates the treatment attributes or creates a new one
            if ($patient->treatment()->exists()) {
                /** @var \App\Models\Treatment */
                $treatment = $patient->treatment;

                $this->treatmentService->update($treatment, $treatmentAttributes);
            } else {
                /** @var \App\Models\Treatment */
                $treatment = $this->treatmentService->store($treatmentAttributes);
            }

            // Finishing the current formulation
            if (! $treatment->wasRecentlyCreated) {
                /** @var \App\Models\Formulation */
                $currentFormulation = $treatment->currentFormulation;
                $this->formulationService->update($currentFormulation, ['ends_at' => now()]);
            }

            // Creating a new formulation
            $formulationAttributes = [
                'frequency' => $request->safe()->frequency,
                'starts_at' => now(),
                'treatment_id' => $treatment->id,
                'pathology_has_medicine_dose_id' => $request->safe()->pathology_has_medicine_dose_id,
            ];
            $this->formulationService->store($formulationAttributes);
        });

        return redirect()->route('patients.list_patients')->with('message', 'Tratamiento editado');
    }

    /**
     * Returns an associated doctor to the patient's city.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function bydoctor($id)
    {
        $doctor = Doctor::with('specialty')->where('id', $id)->first();

        return response()->json($doctor?->specialty);
    }

    /**
     * Imports the patients in the given file.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::transaction(function () use ($request) {
                    /** @var \Illuminate\Http\UploadedFile */
                    $file = $request->file('file');

                    (new PatientImport)->import($file);
                });

                return $this->jsonResponse(message: 'Pacientes cargados correctamente', code: Response::HTTP_CREATED);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                return $this->jsonErrorResponse(
                    message: 'Ocurrió un error al importar el archivo',
                    error: $e->errors(),
                    code: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            } catch (\Throwable $e) {
                return $this->jsonErrorResponse(
                    message: 'El archivo cargado no cumple con el formato establecido. Por favor, utilice la plantilla disponible.',
                    error: $e->getMessage(),
                    code: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Downloads the import template.
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function downloadImportTemplate()
    {
        return Storage::download('import-templates/plantilla-pacientes.xlsx');
    }

    /**
     * Deletes the given user and all related records.
     *
     * @param  Request  $request
     * @param  Patient  $patient
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, Patient $patient)
    {
        $this->authorize('delete', $patient);

        $this->patientService->delete($patient);

        return $this->jsonResponse(message: 'Paciente eliminado correctamente', code: Response::HTTP_NO_CONTENT);
    }

    /**
     * Restores the given patient and all related records.
     *
     * @param  Request  $request
     * @param  Patient  $patient
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(Request $request, Patient $patient)
    {
        $this->authorize('restore', $patient);

        $this->patientService->restore($patient);

        return response()->json(['message' => 'Paciente restaurado correctamente']);
    }
}
