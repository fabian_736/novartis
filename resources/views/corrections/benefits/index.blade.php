@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <span class="text-orange h2">Corrección de beneficios</span>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    <br>
    <div class="col d-flex justify-content-end mt-3">
        <a href="{{ url()->previous() }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
            <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
        </a>
    </div>
    <div class="row-reverse mt-5 table" style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-orange">
                <div class="row">
                    <div class="col-sm-6">
                        <span class="h3 m-0 text-white">Listado de beneficios</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-0">
            <table class="table table-bordered table-striped" id="beneficios">
                <thead class="thead text-white font-weight-bold bg-orange">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Código de paciente</th>
                        <th scope="col">Paciente</th>
                        <th scope="col">Tipo de beneficio</th>
                        <th scope="col">Estado del beneficio</th>
                        <th scope="col">Motivo de cierre</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($benefits ?? [] as $benefit)
                        <tr>
                            <td>{{ $benefit->id }}</td>
                            <td>{{ $benefit->patient->pap }}</td>
                            <td>{{ $benefit->patient->name }}</td>
                            <td>{{ $benefit->benefit_type }}</td>
                            <td>{{ $benefit->status_decode }}</td>
                            <td>{{ $benefit->closing_reason }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn bg-orange dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="iconify" data-icon="fa:eye"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        @can('amend', $benefit)
                                            <li><a class="dropdown-item" href="{{ route('corrections.benefits.edit', $benefit) }}">Corregir</a></li>
                                        @endcan
                                        @can('delete', $benefit)
                                            <li>
                                                <button class="dropdown-item delete-benefit" type="button" data-id="{{ $benefit->id }}">Eliminar</button>
                                            </li>
                                        @endcan
                                        @can('restore', $benefit)
                                            <li>
                                                <button class="dropdown-item restore-benefit" type="button" data-id="{{ $benefit->id }}">Restaurar</button>
                                            </li>
                                        @endcan
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Código de paciente</th>
                        <th scope="col">Paciente</th>
                        <th scope="col">Tipo de beneficio</th>
                        <th scope="col">Estado del beneficio</th>
                        <th scope="col">Motivo de cierre</th>
                        <th scope="col"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    @push('scripts')
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="{{ asset('js/corrections/index.js') }}"></script>
    @endpush
@endsection
