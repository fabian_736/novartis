(function () {
    const patientType = document.getElementById("patient_type");
    const patientStatus = document.getElementById("patient_status");
    const isReactivated = document.getElementById("is_reactivated");
    const suspensionReason = document.getElementById("suspension_reason");

    window.addEventListener('DOMContentLoaded', () => {
        patientType.addEventListener("change", entityOptions);
        patientStatus.addEventListener("change", patientStatusValidation);

        entityOptions();
        patientStatusValidation();
    });

    function entityOptions() {
        if (patientType.value === '1') {
            $('#insuranceCarrier1').show();
            $('#insuranceCarrier_id').disabled = false;
            $('#insuranceCarrier_id').required = true;
            $('#hospital1').hide();
            $('#hospital_id').val('');
            $('#hospital_id').disabled = true;
            $('#hospital_id').required = false;
        } else {
            $('#hospital1').show();
            $('#hospital_id').disabled = false;
            $('#hospital_id').required = true;
            $('#insuranceCarrier1').hide();
            $('#insuranceCarrier_id').val('');
            $('#insuranceCarrier_id').disabled = true;
            $('#insuranceCarrier_id').required = false;
        }
    }

    function patientStatusValidation() {
        switch (patientStatus.value) {
            case "En acceso":
            case "Activo":
                isReactivated.disabled = false;
                isReactivated.closest(".col").style.removeProperty("display");

                suspensionReason.disabled = true;
                suspensionReason.value = "";
                suspensionReason.required = false;
                suspensionReason.closest("li").style.display = "none";
                break;
            case "Suspendido":
                isReactivated.disabled = true;
                isReactivated.checked = false;
                isReactivated.closest(".col").style.display = "none";

                suspensionReason.disabled = false;
                suspensionReason.required = true;
                suspensionReason.closest("li").style.removeProperty("display");
                break;
            default:
                isReactivated.disabled = true;
                isReactivated.checked = false;
                isReactivated.closest(".col").style.display = "none";

                suspensionReason.disabled = true;
                suspensionReason.value = "";
                suspensionReason.required = false;
                suspensionReason.closest("li").style.display = "none";
        }
    }
})();
