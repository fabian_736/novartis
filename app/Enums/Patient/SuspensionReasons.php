<?php

namespace App\Enums\Patient;

enum SuspensionReasons: string
{
    case Pregnancy = 'Paciente en embarazo';
    case Traveling = 'Paciente en viaje';
    case SpecialConditions = 'Paciente con condiciones especiales';
}
