<?php

namespace App\Http\Controllers;

use App\Http\Requests\Pathology\StoreRequest;
use App\Http\Requests\Pathology\UpdateRequest;
use App\Http\Resources\Pathology\PathologyResource;
use App\Models\Medicine;
use App\Models\Pathology;
use App\Services\Pathology\PathologyService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PathologyController extends Controller
{
    use ApiResponses;

    /** @var PathologyService */
    private $pathologyService;

    public function __construct(PathologyService $pathologyService)
    {
        $this->pathologyService = $pathologyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Pathology::class);

        $medicines = Medicine::get();
        $pathologies = $this->pathologyService->list(false);

        return view('supervisor.pathology.list', compact('pathologies', 'medicines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Pathology::class);
            $pathology = $this->pathologyService->store($request->validated());

            return $this->resourceResponse(
                message: 'Patología creada correctamente.',
                data: new PathologyResource($pathology),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Pathology  $pathology
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Pathology $pathology)
    {
        if ($request->ajax()) {
            $this->authorize('update', $pathology);

            $pathology->load('medicineDoses');

            return $this->resourceResponse(
                message: '',
                data: new PathologyResource($pathology)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Pathology  $pathology
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Pathology $pathology)
    {
        if ($request->ajax()) {
            $this->authorize('update', $pathology);
            $this->pathologyService->update($pathology, $request->validated());
            $pathology->refresh();

            return $this->resourceResponse(
                message: 'Patología actualizada correctamente.',
                data: new PathologyResource($pathology)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Returns the doses/medicines associated with a pathology.
     *
     * @param  Pathology  $pathology
     * @param  Medicine  $medicine
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\MedicineDose>
     */
    public function getPathologyDoses(Pathology $pathology, Medicine $medicine)
    {
        $pathology->load('medicineDoses.dose');

        return $pathology->medicineDoses;
    }
}
