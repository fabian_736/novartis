<?php

namespace App\Http\Controllers;

use App\Exceptions\UserCannotBeUpdatedException;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\User\UserResource;
use App\Models\Admin\Role;
use App\Models\Country;
use App\Models\Pharmacy;
use App\Models\User;
use App\Services\DocumentType\DocumentTypeService;
use App\Services\Role\RoleService;
use App\Services\User\UserService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use ApiResponses;

    /** @var UserService */
    private $service;

    /** @var DocumentTypeService */
    private $documentTypeService;

    /** @var RoleService */
    private $roleService;

    public function __construct(UserService $userService, DocumentTypeService $documentTypeService, RoleService $roleService)
    {
        $this->service = $userService;
        $this->documentTypeService = $documentTypeService;
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], User::class);

        $roles = $this->roleService->list();
        $documentTypes = $this->documentTypeService->list();
        $countries = Country::get();
        $pharmacies = Pharmacy::get();

        $users = User::whereRelation('roles', 'is_admin', false)
            ->get();

        return view('supervisor.user.list', compact('users', 'countries', 'documentTypes', 'roles', 'pharmacies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @param  User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request, User $user)
    {
        if ($request->ajax()) {
            $this->authorize('create', User::class);

            $user = null;

            DB::transaction(function () use ($request, &$user) {
                $user = $this->service->store($request->validated());

                $user->assignRole($request->input('role_id'));

                $user->sendWelcomeNotification(now()->addDay());

                // TODO: Determinar si es necesario que los usuarios cargen una imagen
                if ($request->hasFile('image_user')) {
                    $user->addMediaFromRequest('image_user')->toMediaCollection('banner_user');
                }
            });

            return $this->resourceResponse(
                message: 'Usuario creado correctamente.',
                data: new UserResource($user),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function syncRoles(Request $request, User $user)
    // {
    //     // dd($request);
    //     $this->authorize('asignRol', User::class);
    //     $user->syncRoles($request->input('role_id'));

    //     return $request->wantsJson() ?
    //     response()->json([
    //         'message' => 'Rol asignado correctamente',
    //         'data' => $user,
    //     ]) :
    //     redirect()->back()->with('message', 'Rol asignado correctamente');
    // }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, User $user)
    {
        if ($request->ajax()) {
            $this->authorize('update', $user);

            $user->load(['roles', 'city']);

            return $this->resourceResponse(
                message: '',
                data: new UserResource($user)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, User $user)
    {
        if ($request->ajax()) {
            $this->authorize('update', $user);

            if (auth()->id() == $user->id && $user->is_active != $request->safe()->is_active) {
                throw new UserCannotBeUpdatedException('Usted no puede actualizar su estado', 422);
            } else {
                $user->syncRoles($request->input('role_id'));

                $this->service->update($user, $request->validated());

                if ($request->hasFile('image_user')) {
                    $user->addMediaFromRequest('image_user')->toMediaCollection('banner_user');
                }

                $user->refresh();

                return $this->resourceResponse(
                    message: 'Usuario actualizado correctamente.',
                    data: new UserResource($user)
                );
            }
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
