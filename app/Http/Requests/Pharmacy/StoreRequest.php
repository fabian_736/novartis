<?php

namespace App\Http\Requests\Pharmacy;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pharmacy_chain_id' => ['required', 'exists:pharmacy_chains,id'],
            'document_type_id' => ['required', 'exists:document_types,id'],
            'document_number' => ['required', 'unique:pharmacies,document_number', 'digits_between:5,16'],
            'name' => ['required', 'string', 'between:4,100', 'regex:/^[a-záéíóúüñ ]+/i'],
            'country' => ['required', 'exists:countries,id'],
            'city_id' => ['required', Rule::exists('cities', 'id')->where(function ($query) {
                return $query->where('country_id', $this->input('country'));
            })],
            'address' => ['required', 'string', 'between:16,255'],
        ];
    }

    public function attributes()
    {
        return [
            'pharmacy_chain_id' => 'Farmacia',
            'document_type_id' => 'Tipo de documento',
            'document_number' => 'Número de documento',
            'name' => 'Nombre',
            'country' => 'País',
            'city_id' => 'Ciudad',
            'address' => 'Dirección',
        ];
    }
}
