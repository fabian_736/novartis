<?php

namespace App\Enums\Benefit;

enum AuthorizationLetterStatus: string
{
    case Aprobado = 'Aprobado';
    case Pendiente = 'Pendiente';
    case Rechazado = 'Rechazado';
    case Observada = 'Observada';
}
