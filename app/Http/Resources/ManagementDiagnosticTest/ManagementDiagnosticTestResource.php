<?php

namespace App\Http\Resources\ManagementDiagnosticTest;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\ManagementDiagnosticTest
 */
class ManagementDiagnosticTestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>|\Illuminate\Contracts\Support\Arrayable<string, mixed>|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'management_date' => $this->management_date,
            'test_date' => $this->test_date,
            'invoice_number' => $this->invoice_number,
            'test_price' => $this->test_price,
            'status' => $this->status,
            'diagnostic_test_id' => $this->diagnostic_test_id,
            'laboratory_id' => $this->laboratory_id,
            'patient_id' => $this->patient_id,
            'file-upload' => $this->getFirstMediaUrl('file-upload'),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
