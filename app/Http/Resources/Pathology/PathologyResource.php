<?php

namespace App\Http\Resources\Pathology;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Pathology
 */
class PathologyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>|\Illuminate\Contracts\Support\Arrayable<string, mixed>|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_active' => $this->is_active,
            'medicine_id' => $this->medicine_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'medicine_doses' => $this->whenLoaded('medicineDoses'),
        ];
    }
}
