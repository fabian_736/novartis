<?php

namespace App\Http\Resources\InsuranceCarrier;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InsuranceCarrierResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>|\Illuminate\Contracts\Support\Arrayable<string, mixed>|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
