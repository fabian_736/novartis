<?php

namespace App\Policies;

use App\Models\Dose;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DosePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear dosis', 'actualizar dosis'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Dose  $dose
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function view(User $user, Dose $dose)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear dosis')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Dose  $dose
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, Dose $dose)
    {
        if ($user->hasPermissionTo('actualizar dosis')) {
            return true;
        }
    }
}
