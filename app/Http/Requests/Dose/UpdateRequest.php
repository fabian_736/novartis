<?php

namespace App\Http\Requests\Dose;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'presentation' => ['required', 'string', 'between:4,10', Rule::unique('doses', 'presentation')->ignore($this->dose->id)],
            'is_active' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'presentation' => 'Presentación',
            'is_active' => 'Estado',
        ];
    }
}
