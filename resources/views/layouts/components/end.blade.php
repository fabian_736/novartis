<style>
    * {
        font-family: 'Century Gothic' !important;
    }


    *::-webkit-scrollbar {
        width: 15px;
        /* Tamaño del scroll en vertical */
        height: 15px;
        /* Tamaño del scroll en horizontal */
        border-radius: 4px;
    }

    *::-webkit-scrollbar-thumb {
        background: #DF982C !important;
        border-radius: 4px;
    }

    /* Cambiamos el fondo y agregamos una sombra cuando esté en hover */
    *::-webkit-scrollbar-thumb:hover {
        background: #b3b3b3;
        box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
    }

    /* Cambiamos el fondo cuando esté en active */
    *::-webkit-scrollbar-thumb:active {
        background-color: #999999;
    }


    .perfect-scrollbar-on::-webkit-scrollbar-track {
        background: #1D61A6;
        border-radius: 4px;
    }
</style>

<script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();

        md.initVectorMap();

    });
</script>
@stack('scripts')
</body>

</html>
