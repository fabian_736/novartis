<?php

namespace Database\Seeders;

use App\Models\Pharmacy;
use App\Models\PharmacyChain;
use Illuminate\Database\Seeder;

class PharmacyChainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment('local')) {
            PharmacyChain::insert(
                ['name' => 'Doctor +', 'country_id' => 1],
                ['name' => 'Boticas & Salud', 'country_id' => 1],
                ['name' => 'Rebaja', 'country_id' => 1],
                ['name' => 'Colsubsidio', 'country_id' => 1],
                ['name' => 'Cruz verde', 'country_id' => 1],
            );

            Pharmacy::insert(
                [
                    'name' => 'Colsubsidio',
                    'document_number' => '12345',
                    'address' => 'Cll falsa 1234',
                    'is_active' => 1,
                    'city_id' => 1,
                    'pharmacy_chain_id' => 1,
                    'document_type_id' => 1,
                ]
            );
        }
    }
}
