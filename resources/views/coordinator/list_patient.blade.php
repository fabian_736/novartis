@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h2">Consultar pacientes</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    @if (session('message'))
        <script>
            Swal.fire(
                'Paciente actualizado',
                'Los  datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif

    <div class="row-reverse mt-5 " style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-orange">
                <label for="" class="h3 m-0 text-white">Listado de pacientes</label>
            </div>
        </div>
        <div class="col d-flex justify-content-end mt-3">

            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                <span class="iconify mr-2" data-width="15" data-icon="ri:file-excel-2-line"></span> Importar
            </button>&nbsp;&nbsp;&nbsp;
        </div>

        <div class="col p-0">
            <table class="table table-sm table-bordered table-striped" id="pacientes">
                <thead class="thead text-white font-weight-bold bg-orange">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">N° Paciente</th>
                        <th scope="col">Identificación</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">País</th>
                        <th scope="col">Tipo de paciente</th>
                        <th scope="col">Tipo de beneficio</th>
                        <th scope="col">Estado</th>
                        <th class="text-center" scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($patients as $patient)
                        <tr class="text-center">
                            <td>{{ $patient->id }}</td>
                            <td>{{ $patient->pap }}</td>
                            <td>{{ $patient->document_number }}</td>
                            <td>{{ $patient->name }}</td>
                            <td>
                                @if ($patient->city->country->name == 'Perú')
                                    <img width="17px" height="17px" src="{{ asset('img/country/peru.png') }}" alt=""> Perú
                                @elseif ($patient->city->country->name == 'Ecuador')
                                    <img width="17px" height="17px" src="{{ asset('img/country/ecuador.png') }}" alt=""> Ecuador
                                @else
                                    <img class="mr-2" width="17px" height="17px" src="{{ asset('img/country/global.png') }}" alt="">{{ $patient->city->country->name }}
                                @endif
                            </td>
                            <td>{{ $patient->patient_type->label() }}</td>
                            <td>
                                @php
                                    $benefits = [];
                                    if (!empty($patient->lastBenefit?->benefit_type)) {
                                        $benefits[] = $patient->lastBenefit->benefit_type;
                                    }

                                    if ($patient->management_diagnostic_tests_exists) {
                                        $benefits[] = 'Prueba diagnóstica';
                                    }
                                @endphp
                                @if (empty($benefits))
                                    Sin beneficios
                                @else
                                    {{ implode(' y ', $benefits) }}
                                @endif
                            </td>
                            <td>{{ $patient->patient_status->value }}</td>
                            <td class="text-center">
                                <div class="dropdown">
                                    <button class="btn bg-orange dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="iconify" data-icon="fa:eye"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="{{ route('patients.personal_information', $patient) }}">Datos Personales</a></li>
                                        <li><a class="dropdown-item" href="{{ route('patients.treatment', $patient) }}">Tratamiento</a></li>
                                        @can('delete', $patient)
                                            <li>
                                                <button class="dropdown-item delete-patient" type="button" data-id="{{ $patient->id }}">Eliminar</button>
                                            </li>
                                        @endcan
                                        @can('restore', $patient)
                                            <li>
                                                <button class="dropdown-item restore-patient" type="button" data-id="{{ $patient->id }}">Restaurar</button>
                                            </li>
                                        @endcan
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">N° Paciente</th>
                        <th scope="col">Identificación</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">País</th>
                        <th scope="col">Tipo de paciente</th>
                        <th scope="col">Tipo de beneficio</th>
                        <th scope="col">Estado</th>
                        <th class="text-center" scope="col"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal2 fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Importar pacientes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <form id="import-form" action="#" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="d-flex justify-content-center mt-2">
                            <input accept=".xlsx,.csv" required type="file" name="file" id="file-1" class="inputfile input-excel inputfile-1" />
                            <label for="file-1">
                                <svg xmlns="http://www.w3.org/2000/svg" class="iborrainputfile" width="20" height="17" viewBox="0 0 20 17">
                                    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                                </svg>
                                <span class="iborrainputfile">Seleccionar archivo</span>
                            </label>
                        </div>
                        <div class="d-flex justify-content-center">
                            <a href="{{ route('patients.download_import_template') }}" download style="border-color:white;background:white;color:black" class="btn btn-primary"><span class="iconify mr-2" data-icon="fa:download"></span>Descargar plantilla</a>
                        </div>
                        <div class="d-flex justify-content-center">
                            <label id="archivo" for="image1"></label><br>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button data-target="#staticBackdrop" type="submit" class="btn btn-primary">Importar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
        /* PROGRESS AVANCE INACTIVO */

        .animated-progress-inactive {
            width: 100%;
            height: 30px;
            border-radius: 5px;
            border: 1px solid #E90016;
            overflow: hidden;
        }

        .animated-progress-inactive span {
            height: 100%;
            display: block;
            width: 0;
            color: #EF5630;
            line-height: 30px;
            text-align: center;
        }

        .progress-inactive span {
            background-color: #FFEFCA;
        }

        /* PROGRESS AVANCE ACTIVO */

        .animated-progress-active {
            width: 100%;
            height: 30px;
            border-radius: 5px;
            border: 1px solid #136200;
            overflow: hidden;
        }

        .animated-progress-active span {
            height: 100%;
            display: block;
            width: 0;
            color: #136200;
            line-height: 30px;
            text-align: center;
        }

        .progress-active span {
            background-color: rgba(37, 176, 3, 0.22);
        }
    </style>

    <script>
        $(".animated-progress-inactive span").each(function() {
            $(this).animate({
                    width: $(this).attr("data-progress") + "%",
                },
                1000
            );
            $(this).text($(this).attr("data-progress") + "%");
        });

        $(".animated-progress-active span").each(function() {
            $(this).animate({
                    width: $(this).attr("data-progress") + "%",
                },
                1000
            );
            $(this).text($(this).attr("data-progress") + "%");
        });
    </script>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };

        const form = document.getElementById("import-form");

        form.addEventListener("submit", e => {
            e.preventDefault();

            const data = new FormData(form);

            axios.post("patients/import", data)
                .then(response => {
                    Swal.fire(
                        'Carga de pacientes',
                        response.data.message,
                        'success'
                    );
                })
                .catch(error => {
                    const errorContainer = document.createElement("div");
                    if (Array.isArray(error.response.data.error)) {
                        const ul = document.createElement("ul");
                        error.response.data.error.forEach(error => {
                            error.forEach(errorMessage => {
                                const li = document.createElement("li");
                                li.textContent = errorMessage;

                                ul.appendChild(li);
                            });
                        });
                        errorContainer.appendChild(ul)
                    } else {
                        errorContainer.textContent = error.response.data.message;
                    }
                    Swal.fire({
                        title: 'Carga de pacientes',
                        type: 'error',
                        html: errorContainer,
                    });
                });
        })
    </script>

    <script>
        (function() {
            function deletePatient(e) {
                Swal.fire({
                        title: "¿Desea eliminar el paciente?",
                        text: "El paciente, su tratamiento, los beneficios y los seguimientos telefónicos serán eliminados.",
                        showDenyButton: true,
                        confirmButtonText: 'Eliminar',
                        denyButtonText: "Cancelar",
                    })
                    .then(response => {
                        if (response.isConfirmed) {
                            patientId = e.target.closest(".delete-patient").dataset.id;

                            axios.delete(`/patients/${patientId}`)
                                .then(response => {
                                    Swal.fire(
                                            'Eliminación de pacientes',
                                            "Paciente eliminado correctamente",
                                            'success'
                                        )
                                        .then(() => location.reload());
                                })
                                .catch(error => {
                                    Swal.fire(
                                        'Eliminación de pacientes',
                                        response.data.message,
                                        'error'
                                    );
                                });
                        }
                    });
            }

            function restorePatient(e) {
                Swal.fire({
                        title: "¿Desea restaurar el paciente?",
                        text: "El paciente, su tratamiento, los beneficios y los seguimientos telefónicos serán restaurados.",
                        showDenyButton: true,
                        confirmButtonText: 'Restaurar',
                        denyButtonText: "Cancelar",
                    })
                    .then(response => {
                        if (response.isConfirmed) {
                            patientId = e.target.closest(".restore-patient").dataset.id;

                            axios.patch(`/patients/${patientId}/restore`)
                                .then(response => {
                                    Swal.fire(
                                            'Restauración de pacientes',
                                            response.data.message,
                                            'success'
                                        )
                                        .then(() => location.reload());
                                })
                                .catch(error => {
                                    Swal.fire(
                                        'Restauración de pacientes',
                                        response.data.message,
                                        'error'
                                    );
                                });
                        }
                    });
            }

            Array.from(document.querySelectorAll(".delete-patient")).forEach(button => {
                button.addEventListener("click", deletePatient);
            });
            Array.from(document.querySelectorAll(".restore-patient")).forEach(button => {
                button.addEventListener("click", restorePatient);
            });
        })();
    </script>
@endsection
