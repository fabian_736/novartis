<?php

namespace App\Services\Permission;

use App\Models\Admin\Permission;

class PermissionService
{
    /**
     * Returns a list of permissions.
     *
     * @param  bool  $withTrashed
     * @return \Illuminate\Support\Collection<int, Permission>
     */
    public function list($withTrashed = false)
    {
        if ($withTrashed) {
            return Permission::withTrashed()->get();
        } else {
            return Permission::get();
        }
    }

    /**
     * Return to paginated permissions.
     *
     * @param  int  $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15)
    {
        return Permission::paginate($perPage);
    }

    /**
     * Create a Permission.
     *
     * @param  array  $input
     * @return Permission
     */
    public static function store(array $input): Permission
    {
        return Permission::create($input);
    }

    public static function update(Permission $permission, array $input): bool
    {
        return $permission->update($input);
    }

    public static function delete(Permission $permission): ?bool
    {
        return $permission->delete();
    }

    public function restore(Permission $permission): ?bool
    {
        return $permission->restore();
    }

    public function forceDelete(Permission $permission): ?bool
    {
        return $permission->forceDelete();
    }
}
