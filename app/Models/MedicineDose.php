<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Pivot;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperMedicineDose
 */
class MedicineDose extends Pivot implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $table = 'medicine_doses';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Pivot relationship between pathology, medicine and dose.
     *
     * @return BelongsToMany<Pathology>
     */
    public function pathologies(): BelongsToMany
    {
        return $this->belongsToMany(Pathology::class, 'pathology_has_medicine_doses', 'medicine_dose_id', 'pathology_id')
            ->using(PathologyHasMedicineDose::class)
            ->withPivot(['id'])
            ->withTimestamps();
    }

    /**
     * Retrieves the dose.
     *
     * @return BelongsTo<Dose, MedicineDose>
     */
    public function dose(): BelongsTo
    {
        return $this->belongsTo(Dose::class);
    }

    /**
     * Retrieves the medicine.
     *
     * @return BelongsTo<Medicine, MedicineDose>
     */
    public function medicine(): BelongsTo
    {
        return $this->belongsTo(Medicine::class);
    }

    /**
     * {@inheritDoc}
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Sets the model id when deleting.
         * This solves a Laravel bug when deleting pivots with IDs.
         */
        static::deleting(function ($model) {
            if (! $model->id) {
                $query = static::select(['id']);
                foreach ($model->getAttributes() as $key => $value) {
                    $query->where($key, $value);
                }
                $model->id = $query->first()?->id;
            }
        });
    }
}
