<div id="item_two" style="display: none">
    <div class="row mb-3">
        <div class="col mx-auto">
            <label for="" class="line w-100 lead font-weight-bold text-dark">Información del tratamiento</label>
        </div>
    </div>
    <div class="row mb-3 ">
        <div class="col-sm-6 col_form_input">
            <label for="" class="font-weight-bold h4">Tipo de paciente <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span></label>
            <div class="form-group has-default">
                <div class="input-group">
                    <div class="row">
                        <div class="col-1">
                            <input type="checkbox" name="patient_type" id="button1" value="1">
                            <label for="button1" class="fas"></label>
                        </div>
                        <div class="col ml-3">
                            <span class="h5 text_alert" id="confirm_text_alert_1">Asegurado</span>
                        </div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-1">
                            <input type="checkbox" name="patient_type" id="button2" value="0">
                            <label for="button2" class="fas"></label>
                        </div>
                        <div class="col ml-3">
                            <span class="h5 text_alert" id="confirm_text_alert_2">No asegurado</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="message_asegurado" class="text-danger font-weight-bold d-none">
                                <i class='fas fa-times mr-2 '></i>Por favor seleccione una opcion
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Validador con JS --}}
                <div id="message" class="text-danger font-weight-bold">
                    <i class='fas fa-times mr-2 '></i>Por favor seleccione una opcion
                </div>
                {{-- FIN Validador con JS --}}
            </div>
        </div>
        <div class="col-sm-6 p-0">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4">Beneficio <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span></label>
                <div class="form-group has-default mb-2">
                    <div class="input-group">
                        <div id="equipoA" style="display: none">
                            <div class="row">
                                <div class="col">
                                    <div class="row">
                                        <div class="col-3">
                                            <input type="checkbox" id="asegurado1" name="benefit_type" value="">
                                            <label for="asegurado1" class="fas"></label>
                                        </div>
                                        <div class="col">
                                            <span class="h5 text_alert_options" id="title_options1">CO-PAGO</span>
                                            <span class="h5" id="title1_No_asegurado">Descuento especial</span>
                                        </div>
                                        <input type="hidden" value="">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col-3">
                                            <input type="checkbox" id="asegurado3" name="diagnostic_test" value="1">
                                            <label for="asegurado3" class="fas"></label>
                                        </div>
                                        <div class="col">
                                            <span class="h5 text_alert_options" id="title_options2">Prueba diagnóstica</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    {{-- Validador con JS --}}
                                    <div id="message2" class="text-danger font-weight-bold">
                                        <i class='fas fa-times mr-2 '></i>Por favor seleccione una opcion
                                    </div>
                                    {{-- FIN Validador con JS --}}
                                </div>
                            </div>
                        </div>
                        @error('benefit_type')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div id="name_carrier" class="col-sm-6 p-0" style="display: none">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4">Nombre de la aseguradora</label><span style="color: #EF5630; font-size: 11px" class="font-weight-bold"> *Campo obligatorio</span>
                <div class="form-group has-default  mb-5">
                    <div class="input-group">
                        <select required name="insurance_carrier_id" id="insurance_carrier_id" class="form-control w-100 @error('insurance_carrier') is-invalid @enderror">
                            <option value="" selected disabled>Seleccione una opción...</option>
                            @foreach ($insuranceCarriers as $insuranceCarrier)
                                <option @selected(old('insurance_carrier_id') == $insuranceCarrier->name) value="{{ $insuranceCarrier->id }}">
                                    {{ $insuranceCarrier->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('insurance_carrier')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div id="content_hospital" class="col-sm-6 p-0" style="display: none">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4">Nombre de la institución u hospital</label><span style="color: #EF5630; font-size: 11px" class="font-weight-bold"> *Campo obligatorio</span>
                <div class="form-group has-default  mb-5">
                    <div class="input-group">
                        <select required name="hospital_id" id="hospital"
                            class="form-control w-100 @error('hospital') is-invalid @enderror">
                            <option value="" selected disabled>Seleccione una opción...</option>
                            @foreach ($hospitals as $hospital)
                                <option @selected(old('hospital_id') == $hospital->name) value="{{ $hospital->id }}">{{ $hospital->name }}</option>
                            @endforeach
                        </select>
                        @error('hospital_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3 ocultar" style='display:none'>
        <div class="col-sm-6 p-0">
            <div class="col col_form_input">
                <label for="" id="product_label" class="font-weight-bold h4">Producto <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span></label>
                <div class="form-group has-default  mb-5">
                    <div class="input-group">
                        <select required name="medicine_id" id="medicine" class="form-control w-100" autocomplete="off">
                            <option value="" @selected(empty(old('medicine'))) disabled>Seleccione una opción...</option>
                            @foreach ($medicines as $medicine)
                                <option @selected(old('medicine') == $medicine->name) value="{{ $medicine->id }}">{{ $medicine->name }}
                                </option>
                            @endforeach
                        </select>
                        {{-- Validador con JS --}}
                        <div class="invalid-feedback font-weight-bold" id="medicine_validate" style="margin-top: 7%; display: none">
                        </div>
                        {{-- FIN Validador con JS --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3 ocultar" style='display:none'>
        <div class="col-sm-6 p-0">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4" id="patology_label">Patología / Diagnóstico<span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span></label>
                <div class="form-group has-default  mb-2">
                    <div class="input-group">
                        <select required name="pathology_id" id="pathology" class="form-control w-100 ">
                            <option value="" selected disabled>Seleccione una opción...</option>
                        </select>
                        {{-- Validador con JS --}}
                        <div class="invalid-feedback font-weight-bold" id="pathology_validate" style="margin-top: 7%; display: none"></div>
                        {{-- FIN Validador con JS --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 p-0 p-0">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4" id="dose_label">Dosis <span style="color: #EF5630; font-size: 11px">*Campo obligatorio</span></label>
                <div class="form-group has-default  mb-5">
                    <div class="input-group">
                        <select required name="dose_id" id="dose" class="form-control w-100">
                            <option value="" selected disabled>Seleccione una opción...</option>
                        </select>
                        {{-- Validador con JS --}}
                        <div class="invalid-feedback font-weight-bold" id="dose_validate" style="margin-top: 7%; display: none"></div>
                        {{-- FIN Validador con JS --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row ocultar" style='display:none'>
        <div class="col-sm-6 p-0">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4">Frecuencia de aplicación</label>
                <div class="form-group has-default">
                    <div class="input-group">
                        <input required value="{{ old('frequency') }}" type="number" name="frequency" id="frequency" placeholder="Cantidad de dias..." class="form-control @error('frequency') is-invalid @enderror">
                        @error('frequency')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 p-0">
            <div class="col col_form_input">
                <label for="" class="font-weight-bold h4">¿Es la primera vez que usa biológicos?</label>
                <div class="form-group has-default  mb-5">
                    <div class="input-group">
                        <select required name="first_time_using_biological" id="first_time_using_biological" class="form-control w-100 @error('first_time_using_biological') is-invalid @enderror">
                            <option value="" selected disabled>Seleccione una opción...</option>
                            <option value="1">Si</option>
                            <option value="0">No</option>
                            <option value="2">Desconoce</option>
                        </select>
                        @error('use_biological')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="document" style="display: none">
        <div class="row d-flex justify-content-center mt-5 mb-5">
            <div class="row-reverse ml-4 mr-4">
                <div class="col p-0 m-0">
                    <label for="" class="font-weight-bold h4" id="text_alert_file">Orden médica <span style="color:red;">*</span></label>
                </div>
                <div class="col p-0 m-0">
                    <label for="diagnostic_test_order1" class="btn btn-white btn-sm" style="border: solid #000 2px; font-weight: 700; font-size: 14px; color: #000">
                        <i class="fa fa-cloud-upload mr-2"></i> SELECCIONAR ARCHIVO
                    </label>
                    <input accept="image/*,.pdf" class="input-file" size="1" name="diagnostic_test_order1" id="diagnostic_test_order1" type="file" style="display: none" />
                    {{-- Validador con JS --}}
                    <div class="text-danger font-weight-bold">
                        <i class='fas fa-warning mr-2 '></i>Campo obligatorio
                    </div>
                    <label class="d-flex justify-content-center" id="archivo1" for="diagnostic_test_order1"></label><br>
                    {{-- FIN Validador con JS --}}
                </div>
            </div>
            <div class="row-reverse ml-4 mr-4">
                <div class="col p-0 m-0">
                    <label for="" class="font-weight-bold h4" id="text_alert_file">Orden médica</label>
                </div>
                <div class="col p-0 m-0">
                    <label for="diagnostic_test_order2" class="btn btn-white btn-sm" style="border: solid #000 2px; font-weight: 700; font-size: 14px; color: #000">
                        <i class="fa fa-cloud-upload mr-2"></i> SELECCIONAR ARCHIVO
                    </label>
                    <input accept="image/*,.pdf" class="input-file" size="1" name="diagnostic_test_order2" id="diagnostic_test_order2" type="file" style="display: none" />
                    {{-- Validador con JS --}}
                    <div class="text-danger font-weight-bold">
                        <i class='fas fa-warning mr-2 '></i>Opcional
                    </div>
                    <label class="d-flex justify-content-center" id="archivo2" for="diagnostic_test_order2"></label><br>
                    {{-- FIN Validador con JS --}}
                </div>
            </div>
            <div class="row-reverse ml-4 mr-4">
                <div class="col p-0 m-0">
                    <label for="" class="font-weight-bold h4" id="text_alert_file">Orden médica</label>
                </div>
                <div class="col p-0 m-0">
                    <label for="diagnostic_test_order3" class="btn btn-white btn-sm" style="border: solid #000 2px; font-weight: 700; font-size: 14px; color: #000">
                        <i class="fa fa-cloud-upload mr-2"></i> SELECCIONAR ARCHIVO
                    </label>
                    <input accept=".pdf,image/*" class="input-file" size="1" name="diagnostic_test_order3" id="diagnostic_test_order3" type="file" style="display: none" />
                    {{-- Validador con JS --}}
                    <div class="text-danger font-weight-bold">
                        <i class='fas fa-warning mr-2 '></i>Opcional
                    </div>
                    <label class="d-flex justify-content-center" id="archivo2" for="diagnostic_test_order2"></label><br>
                    {{-- FIN Validador con JS --}}
                </div>
                <label class="d-flex justify-content-center" id="archivo3" for="diagnostic_test_order3"></label><br>
            </div>
        </div>
    </div>
    <div class="row my-3 mt-5">
        <div class="col col_form_input d-flex justify-content-center">
            <a href="javascript:;" id="bottom_two" class="btn btn-white" style="border: solid #000 2px; width: 210px; height: auto; font-weight: 700; font-size: 14px; color: #000">CONTINUAR</a>
        </div>
    </div>
</div>
