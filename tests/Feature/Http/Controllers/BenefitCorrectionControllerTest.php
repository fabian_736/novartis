<?php

namespace Tests\Feature\Http\Controllers;

use App\Enums\Benefit\AuthorizationLetterRejectionReasons;
use App\Enums\Benefit\AuthorizationLetterStatus;
use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Models\Admin\Permission;
use App\Models\Benefit;
use App\Models\Formulation;
use App\Models\Patient;
use App\Models\Pharmacy;
use App\Models\Treatment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class BenefitCorrectionControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_user_can_view_the_benefits_listing()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        Permission::create(['name' => 'restaurar beneficios']);
        Permission::create(['name' => 'eliminar beneficios']);

        Benefit::factory()->count(10)->create();

        $benefits = Benefit::with('patient')->latest()->get();

        $this->actingAs($user)
            ->get(route('corrections.benefits.index'))
            ->assertOk()
            ->assertViewIs('corrections.benefits.index')
            ->assertViewHas('benefits', $benefits);
    }

    public function test_user_can_view_the_correction_form()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        $benefit = Benefit::factory()->for($patient)->create();

        Pharmacy::factory()->count(2)->create();
        $pharmacies = Pharmacy::active()->get();

        $this->actingAs($user)
            ->get(route('corrections.benefits.edit', $benefit))
            ->assertOk()
            ->assertViewIs('corrections.benefits.edit')
            ->assertViewHasAll([
                'benefit' => $benefit,
                'pharmacies' => $pharmacies,
            ]);
    }

    public function test_user_can_update_a_benefit_as_pending_authorization_letter()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        $data = [
            'benefit_type' => BenefitTypes::Copago->value,
            'status' => BenefitStatus::CartaPendiente->value,
            'autorized' => AuthorizationLetterStatus::Pendiente->value,
            'is_accepted' => null,
            'causal_types' => null,
            'number_units' => null,
            'pharmacy_id' => null,
            'discount' => null,
            'transaction_date' => null,
            'invoice_number' => null,
            'factura' => null,
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::CartaPendiente->value, $benefit->status);
        $this->assertEquals(AuthorizationLetterStatus::Pendiente->value, $benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertNull($benefit->number_units);
        $this->assertNull($benefit->pharmacy_id);
        $this->assertNull($benefit->discount);
        $this->assertNull($benefit->transaction_date);
        $this->assertNull($benefit->invoice_number);
        $this->assertNull($benefit->closing_reason);
    }

    public function test_user_can_update_a_benefit_as_observed_authorization_letter()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        $data = [
            'benefit_type' => BenefitTypes::Copago->value,
            'status' => BenefitStatus::ProcesoObservado->value,
            'autorized' => AuthorizationLetterStatus::Observada->value,
            'is_accepted' => null,
            'causal_types' => null,
            'number_units' => null,
            'pharmacy_id' => null,
            'discount' => null,
            'transaction_date' => null,
            'invoice_number' => null,
            'factura' => null,
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::ProcesoObservado->value, $benefit->status);
        $this->assertEquals(AuthorizationLetterStatus::Observada->value, $benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertNull($benefit->number_units);
        $this->assertNull($benefit->pharmacy_id);
        $this->assertNull($benefit->discount);
        $this->assertNull($benefit->transaction_date);
        $this->assertNull($benefit->invoice_number);
        $this->assertNull($benefit->closing_reason);
    }

    public function test_user_can_update_a_copay_benefit_as_approved_authorization_letter()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        $data = [
            'benefit_type' => BenefitTypes::Copago->value,
            'status' => BenefitStatus::FacturaPendiente->value,
            'autorized' => AuthorizationLetterStatus::Aprobado->value,
            'is_accepted' => null,
            'causal_types' => null,
            'number_units' => null,
            'pharmacy_id' => null,
            'discount' => null,
            'transaction_date' => null,
            'invoice_number' => null,
            'factura' => null,
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::FacturaPendiente->value, $benefit->status);
        $this->assertEquals(AuthorizationLetterStatus::Aprobado->value, $benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertNull($benefit->number_units);
        $this->assertNull($benefit->pharmacy_id);
        $this->assertNull($benefit->discount);
        $this->assertNull($benefit->transaction_date);
        $this->assertNull($benefit->invoice_number);
        $this->assertNull($benefit->closing_reason);
    }

    public function test_user_can_update_a_copay_benefit_as_declined_special_discount()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        $data = [
            'benefit_type' => BenefitTypes::Copago->value,
            'status' => BenefitStatus::DescuentoEspecialRechazado->value,
            'autorized' => AuthorizationLetterStatus::Rechazado->value,
            'is_accepted' => 0,
            'causal_types' => AuthorizationLetterRejectionReasons::PolizaNocontemplaBiologicos->value,
            'number_units' => null,
            'pharmacy_id' => null,
            'discount' => null,
            'transaction_date' => null,
            'invoice_number' => null,
            'factura' => null,
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::DescuentoEspecialRechazado->value, $benefit->status);
        $this->assertEquals(AuthorizationLetterStatus::Rechazado->value, $benefit->autorized);
        $this->assertEquals(0, $benefit->is_accepted);
        $this->assertEquals(AuthorizationLetterRejectionReasons::PolizaNocontemplaBiologicos->value, $benefit->causal_types);
        $this->assertNull($benefit->number_units);
        $this->assertNull($benefit->pharmacy_id);
        $this->assertNull($benefit->discount);
        $this->assertNull($benefit->transaction_date);
        $this->assertNull($benefit->invoice_number);
        $this->assertNull($benefit->closing_reason);
    }

    public function test_user_can_update_a_copay_benefit_as_accepted_special_discount()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        $data = [
            'benefit_type' => BenefitTypes::Copago->value,
            'status' => BenefitStatus::FacturaPendiente->value,
            'autorized' => AuthorizationLetterStatus::Rechazado->value,
            'is_accepted' => 1,
            'causal_types' => AuthorizationLetterRejectionReasons::PolizaNocontemplaBiologicos->value,
            'number_units' => null,
            'pharmacy_id' => null,
            'discount' => null,
            'transaction_date' => null,
            'invoice_number' => null,
            'factura' => null,
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::OOP->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::FacturaPendiente->value, $benefit->status);
        $this->assertEquals(AuthorizationLetterStatus::Rechazado->value, $benefit->autorized);
        $this->assertEquals(1, $benefit->is_accepted);
        $this->assertEquals(AuthorizationLetterRejectionReasons::PolizaNocontemplaBiologicos->value, $benefit->causal_types);
        $this->assertNull($benefit->number_units);
        $this->assertNull($benefit->pharmacy_id);
        $this->assertNull($benefit->discount);
        $this->assertNull($benefit->transaction_date);
        $this->assertNull($benefit->invoice_number);
        $this->assertNull($benefit->closing_reason);
    }

    public function test_user_can_update_a_oop_benefit_as_pending_invoice()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->OOP()->closed()->for($patient)->create();

        $data = [
            'benefit_type' => BenefitTypes::OOP->value,
            'status' => BenefitStatus::FacturaPendiente->value,
            'autorized' => null,
            'is_accepted' => null,
            'causal_types' => null,
            'number_units' => null,
            'pharmacy_id' => null,
            'discount' => null,
            'transaction_date' => null,
            'invoice_number' => null,
            'factura' => null,
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::OOP->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::FacturaPendiente->value, $benefit->status);
        $this->assertNull($benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertNull($benefit->number_units);
        $this->assertNull($benefit->pharmacy_id);
        $this->assertNull($benefit->discount);
        $this->assertNull($benefit->transaction_date);
        $this->assertNull($benefit->invoice_number);
        $this->assertNull($benefit->factura);
        $this->assertNull($benefit->closing_reason);
    }

    public function test_user_can_update_a_copay_benefit_as_uploaded_invoice()
    {
        Carbon::setTestNow('2022-01-01');

        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        /** @var Pharmacy */
        $pharmacy = Pharmacy::factory()->create();

        $data = [
            'benefit_type' => BenefitTypes::Copago->value,
            'status' => BenefitStatus::FacturaCargada->value,
            'autorized' => AuthorizationLetterStatus::Aprobado->value,
            'is_accepted' => null,
            'causal_types' => null,
            'number_units' => 1,
            'pharmacy_id' => $pharmacy->id,
            'discount' => 100,
            'transaction_date' => '2022-01-01',
            'invoice_number' => 'ASDDFGDFG435345',
            'factura' => UploadedFile::fake()->image('invoice.png'),
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::FacturaCargada->value, $benefit->status);
        $this->assertEquals(AuthorizationLetterStatus::Aprobado->value, $benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertEquals(1, $benefit->number_units);
        $this->assertEquals($pharmacy->id, $benefit->pharmacy_id);
        $this->assertEquals(100, $benefit->discount);
        $this->assertEquals('2022-01-01', $benefit->transaction_date);
        $this->assertEquals('ASDDFGDFG435345', $benefit->invoice_number);
        $this->assertNull($benefit->closing_reason);

        $this->assertFileExists($benefit->getFirstMediaPath('invoice'));
    }

    public function test_user_can_update_a_oop_benefit_as_uploaded_invoice()
    {
        Carbon::setTestNow('2022-01-01');

        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        /** @var Pharmacy */
        $pharmacy = Pharmacy::factory()->create();

        $data = [
            'benefit_type' => BenefitTypes::OOP->value,
            'status' => BenefitStatus::FacturaCargada->value,
            'autorized' => null,
            'is_accepted' => null,
            'causal_types' => null,
            'number_units' => 1,
            'pharmacy_id' => $pharmacy->id,
            'discount' => 100,
            'transaction_date' => '2022-01-01',
            'invoice_number' => 'ASDDFGDFG435345',
            'factura' => UploadedFile::fake()->image('invoice.png'),
            'closing_reason' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::OOP->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::FacturaCargada->value, $benefit->status);
        $this->assertNull($benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertEquals(1, $benefit->number_units);
        $this->assertEquals($pharmacy->id, $benefit->pharmacy_id);
        $this->assertEquals(100, $benefit->discount);
        $this->assertEquals('2022-01-01', $benefit->transaction_date);
        $this->assertEquals('ASDDFGDFG435345', $benefit->invoice_number);
        $this->assertNull($benefit->closing_reason);

        $this->assertFileExists($benefit->getFirstMediaPath('invoice'));
    }

    public function test_user_can_update_a_oop_benefit_as_closed()
    {
        Carbon::setTestNow('2022-01-01');

        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'corregir beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        Formulation::factory()->for($treatment)->create();

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->closed()->for($patient)->create();

        /** @var Pharmacy */
        $pharmacy = Pharmacy::factory()->create();

        $data = [
            'benefit_type' => BenefitTypes::OOP->value,
            'status' => BenefitStatus::Cerrado->value,
            'autorized' => null,
            'is_accepted' => null,
            'causal_types' => null,
            'number_units' => 1,
            'pharmacy_id' => $pharmacy->id,
            'discount' => 100,
            'transaction_date' => '2022-01-01',
            'invoice_number' => 'ASDDFGDFG435345',
            'factura' => UploadedFile::fake()->image('invoice.png'),
            'closing_reason' => 'Lorem ipsum, dolor sit amet',
        ];

        $this->actingAs($user)
            ->postJson(route('corrections.benefits.update', $benefit), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(BenefitTypes::OOP->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::Cerrado->value, $benefit->status);
        $this->assertNull($benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertEquals(1, $benefit->number_units);
        $this->assertEquals($pharmacy->id, $benefit->pharmacy_id);
        $this->assertEquals(100, $benefit->discount);
        $this->assertEquals('2022-01-01', $benefit->transaction_date);
        $this->assertEquals('ASDDFGDFG435345', $benefit->invoice_number);
        $this->assertEquals('Lorem ipsum, dolor sit amet', $benefit->closing_reason);

        $this->assertFileExists($benefit->getFirstMediaPath('invoice'));
    }

    public function test_user_can_delete_a_benefit()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'eliminar beneficios']);
        $user->givePermissionTo($permission);

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->create();

        $this->actingAs($user)
            ->deleteJson(route('corrections.benefits.delete', $benefit), [], self::$HEADERS)
            ->assertNoContent();

        $this->assertSoftDeleted($benefit);
    }

    public function test_user_can_restore_a_benefit()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'restaurar beneficios']);
        $user->givePermissionTo($permission);

        /** @var Benefit */
        $benefit = Benefit::factory()->copay()->trashed()->create();

        $this->actingAs($user)
            ->patchJson(route('corrections.benefits.restore', $benefit), [], self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertNotSoftDeleted($benefit);
    }
}
