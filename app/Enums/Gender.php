<?php

namespace App\Enums;

enum Gender: string
{
    case male = 'Male';
    case Female = 'Female';
}
