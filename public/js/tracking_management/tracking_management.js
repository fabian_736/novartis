
function showClaimed_medication() {
    button1 = document.getElementById("button_claimed_medication1");
    button2 = document.getElementById("button_claimed_medication2");
    button1.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button1.checked == true) {
        $('#medicine1').show();
        $('#notClaime1').hide();
        $('#delivery1').show();
        $('#autorization1').show();
        $('#treatment1').show();
        $('#claimed_medication').addClass('col').removeClass('col-3');
        $('#checkbox_claimed_medication').addClass('col').removeClass('col-3');
        $('#answered_call').addClass('col-3').removeClass('col-6');
        $('#checkbox_answered_call').addClass('col-3').removeClass('col-6');
        $('#contactMed').addClass('col-3').removeClass('col');
        $('#contactMed1').addClass('col-3').removeClass('col');
        button2.checked = false;
    } else {
        $('#medicine1').hide();
        $('#notClaime1').hide();
        $('#delivery1').hide();
        $('#autorization1').hide();
        $('#treatment1').hide();
        $('#claimed_medication').addClass('col-3').removeClass('col');
        $('#checkbox_claimed_medication').addClass('col-3').removeClass('col');
    }
}

function showClaimed_medication1() {
    button1 = document.getElementById("button_claimed_medication1");
    button2 = document.getElementById("button_claimed_medication2");
    button2.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button2.checked == true) {
        $('#medicine1').hide();
        $('#notClaime1').show();
        $('#delivery1').hide();
        $('#autorization1').hide();
        $('#treatment1').hide();
        $('#claimed_medication').addClass('col-3').removeClass('col');
        $('#checkbox_claimed_medication').addClass('col-3').removeClass('col');
        $('#answered_call').addClass('col-6').removeClass('col');
        $('#checkbox_answered_call').addClass('col-6').removeClass('col');
        $('#contactMed').addClass('col').removeClass('col-3');
        $('#contactMed1').addClass('col').removeClass('col-3');
        button1.checked = false;
    }
}

function showAnswered_call() {
    button1 = document.getElementById("button_answered_call1");
    button2 = document.getElementById("button_answered_call2");
    button1.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button1.checked == true) {
        $('#call_made1').show();
        button2.checked = false;
    } else {
        $('#call_made1').hide();
    }
}

function showAnswered_call1() {
    button1 = document.getElementById("button_answered_call1");
    button2 = document.getElementById("button_answered_call2");
    button2.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button2.checked == true) {
        $('#call_made1').hide();
        button1.checked = false;
    }
}

function showDifficulty_access() {
    button1 = document.getElementById("button_difficulty_access1");
    button2 = document.getElementById("button_difficulty_access2");
    button1.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button1.checked == true) {
        $('#difficulty_type1').show();
        button2.checked = false;
    } else {
        $('#difficulty_type1').hide();
    }
}

function showDifficulty_access1() {
    button1 = document.getElementById("button_difficulty_access1");
    button2 = document.getElementById("button_difficulty_access2");
    button2.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button2.checked == true) {
        $('#difficulty_type1').hide();
        button1.checked = false;
    }
}

function showGenerate_request() {
    button1 = document.getElementById("button_generate_request1");
    button2 = document.getElementById("button_generate_request2");
    button1.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button1.checked == true) {
        button2.checked = false;
    } else {
    }
}

function showGenerate_request1() {
    button1 = document.getElementById("button_generate_request1");
    button2 = document.getElementById("button_generate_request2");
    button2.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button2.checked == true) {
        button1.checked = false;
    }
}

function showAdverse_event() {
    button1 = document.getElementById("button_adverse_event1");
    button2 = document.getElementById("button_adverse_event2");
    button1.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button1.checked == true) {
        $('#eventType1').show();
        $('#dateOfNextCall').addClass('col').removeClass('col-3');
        $('#dateOfNextCall1').addClass('col').removeClass('col-3');
        button2.checked = false;
    } else {
        $('#eventType1').hide();
    }
}

function showAdverse_event1() {
    button1 = document.getElementById("button_adverse_event1");
    button2 = document.getElementById("button_adverse_event2");
    button2.checked = true; // Pone/Quita permanentemente la selección de la casilla

    if (button2.checked == true) {
        $('#eventType1').hide();
        $('#dateOfNextCall').addClass('col-3').removeClass('col');
        $('#dateOfNextCall1').addClass('col-3').removeClass('col');
        button1.checked = false;
    }
}

// funcion para el boton de general
$(document).ready(function () {
    $('#btn_general').click(function () {
        $('#col_general').show('slow')
        $('#col_comunication').hide('slow')
        $('#a_general').addClass('bg-orange text-white');
        $('#a_Comunicaciónes').addClass('bg-gray text-gray');
        $('#a_Comunicaciónes').removeClass('bg-orange');
        $('#a2').addClass('text-gray');
        $('#a2').removeClass('text-orange');
        $('#a1').addClass('text-orange');
        $('#a1').removeClass('text-gray');
        $('#a3').removeClass('text-orange');
        $('#a3').addClass('text-gray');
        $('#a_paciente').removeClass('bg-orange');
        $('#a_paciente').addClass('text-gray');
        $('.modal-footer').show();
        $('#col_paciente').hide();
    })
    // funcion para el boton de comunication

    $('#btn_comunication').click(function () {
        $('#col_general').hide('slow')
        $('#col_comunication').show('slow');
        $('#a_general').addClass('bg-gray');
        $('#a_general').removeClass('bg-orange');
        $('#a_Comunicaciónes').addClass('bg-orange text-white');
        $('#a_Comunicaciónes').removeClass('text-gray');
        $('#a2').addClass('text-orange');
        $('#a2').removeClass('text-gray');
        $('#a1').removeClass('text-orange');
        $('#a1').addClass('text-gray');
        $('#a3').removeClass('text-orange');
        $('#a3').addClass('text-gray');
        $('#a_paciente').removeClass('bg-orange');
        $('#a_paciente').addClass('text-gray');
        $('.modal-footer').hide();
        $('#col_paciente').hide();
    })
    // funcion para el boton de paciente
    $('#btn_paciente').click(function () {
        $('#col_general').hide('slow')
        $('.modal-footer').hide();
        $('#col_comunication').hide('slow')
        $('#col_paciente').show('slow');
        $('#a_paciente').addClass('bg-orange text-white');
        $('#a_paciente').removeClass('text-gray');
        $('#a2').removeClass('text-orange');
        $('#a2').addClass('text-gray');
        $('#a3').addClass('text-orange');
        $('#a3').removeClass('text-gray');
        $('#a1').removeClass('text-orange');
        $('#a1').addClass('text-gray');
        $('#a_general').removeClass('bg-orange');
        $('#a_general').addClass('text-gray bg-gray');
        $('#a_Comunicaciónes').removeClass('bg-orange');
        $('#a_Comunicaciónes').addClass('text-gray');
    })
})


$(document).ready(function () {
    // funcion para limpiar los datos
    $('#button_claimed_medication1').click(function () {
        $('#cause_of_no_claim').val('');
    })
    // funcion para limpiar los datos
    $('#button_claimed_medication2').click(function () {
        $('#claim_date').val('');
        $('#medicine_up').val('');
        $('#delivery_point').val('');
        $('#delivery_city').val('');
        $('#authorization_number').val('');
        $('#authorization_date').val('');
    })
    // funcion para limpiar los datos
    $('#button_answered_call2').click(function () {
        $('#who_answered_call').val('');
        $('#call_reason').val('');
    })
    // funcion para limpiar los datos
    $('#button_adverse_event2').click(function () {
        $('#event_type').val('');
    })
    // funcion para limpiar los datos
    $('#button_difficulty_access2').click(function () {
        $('#difficulty_type').val('');
    })
})

$('#adjunto').click(function () {
    window.open('https://www.proturbiomarspa.com/files/_pdf-prueba.pdf', 'dsfsdfsf',
        "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
})
