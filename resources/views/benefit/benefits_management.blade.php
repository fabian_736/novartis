@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h2">{{ $benefit->patient->name }}</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    <x-errores />
    <div class="row-reverse mt-5 table" style="overflow-y: auto; max-height: 80vh">
        <div class="col">
            <form action="{{ route('benefits.status.benefits.management', ['benefit' => request()->benefit]) }}" method="POST">
                @csrf
                @method('PATCH')
                <ul class="li_form">
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Número de afiliación</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" placeholder="PAP" value="{{ $benefit->patient->pap }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold">Número de cédula</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" placeholder="+51 32324321" value="{{ $benefit->patient->document_number }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Nombre</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" placeholder="PAP" value="{{ $benefit->patient->name }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold">Número de
                                            contacto</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" placeholder="+51 32324321" value="{{ $benefit->patient->phone }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Correo electrónico</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="email" name="" id="" class="form-control" placeholder="example@example.com" value="{{ $benefit->patient->email }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Medicamento</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ $benefit->patient->treatment?->pathology->medicine->name }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Patología</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ $benefit->patient->treatment?->pathology->name }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Dosis</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ $benefit->patient->treatment->currentFormulation->pathologyHasMedicineDose->medicineDose->dose->presentation }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Frecuencia</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ $benefit->patient->treatment->currentFormulation->frequency }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold "> Tipo de beneficio</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ $benefit->benefit_type }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Estado del tipo de beneficio</label>
                                    </div>
                                    <div class="col">
                                        @if ($benefit->benefit_type_status == 0)
                                            <input type="text" class="form-control" value="Abierto" disabled>
                                        @elseif ($benefit->benefit_type_status == 1)
                                            <input type="text" class="form-control" value="Pagado" disabled>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Médico</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ isset($benefit->patient->treatment->doctor->name) ? $benefit->patient->treatment->doctor->name : 'Sin datos' }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Especialidad</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ isset($benefit->patient->treatment->doctor->specialty->name) ? $benefit->patient->treatment->doctor->specialty->name : 'Sin datos' }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Cantidad del producto</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" value="{{ isset($benefit->number_units) ? $benefit->number_units : 'Sin datos' }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Farmacia</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" value="{{ isset($benefit->pharmacy_id) ? $benefit->pharmacy->name : 'Sin datos' }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Fecha de transacción</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ isset($benefit->transaction_date) ? $benefit->transaction_date : 'Sin datos' }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Número de factura</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ isset($benefit->invoice_number) ? $benefit->invoice_number : 'Sin datos' }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Estado del beneficio</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="{{ $benefit->status_decode }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Fotografía de factura</label>
                                    </div>
                                    <div class="col">
                                        @if ($benefit->hasMedia('invoice') == null)
                                            <label for="" class="h5 font-weight-bold">No hay factura cargada</label>
                                        @else
                                            <a href="javascript:;" id="archive" class="btn bg-secondary w-100">Ver factura adjunta</a>
                                            <script>
                                                $('#archive').click(function() {
                                                    window.open("{{ $benefit->getFirstMediaUrl('invoice') }}",
                                                        'targetWindow',
                                                        `toolbar=no,
                                                    location=no,
                                                    status=no,
                                                    menubar=no,
                                                    scrollbars=yes,
                                                    resizable=yes,
                                                    width=SomeSize,
                                                    height=SomeSize`);
                                                    return false;
                                                })
                                            </script>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Motivo de cierre</label>
                                    </div>
                                    <div class="col-9">
                                        <textarea required autofocus class="form-control border border-secondary rounded" name="closing_reason" id="closing_reason" cols="50" rows="3" placeholder="Digite aquí el motivo de cierre del beneficio">{{ $benefit->closing_reason }}</textarea>
                                    </div>
                                </div>
                            </div>
                    </li>
                    {{-- <li style="list-style: none"> --}}
                    <div class="row mb-3">
                        <div class="col offset-6">
                            <div class="row">
                                <div class="col d-flex justify-content-end">
                                    <a href="{{ route('benefits.benefits.index') }}" class="btn bg-orange"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick-fill"></span> Salir</a>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <button type="submit" class="btn btn-dark w-100"><span class="iconify mr-2" data-icon="entypo:save"></span>Cerrar beneficio</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
            </form>
        </div>
    </div>
@endsection
