<?php

namespace App\Http\Controllers;

use App\Http\Requests\Hospital\StoreRequest;
use App\Http\Requests\Hospital\UpdateRequest;
use App\Http\Resources\Hospital\HospitalResource;
use App\Imports\HospitalsImport;
use App\Models\Country;
use App\Models\Hospital;
use App\Services\Hospital\HospitalService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class HospitalController extends Controller
{
    use ApiResponses;

    /** @var HospitalService */
    private $hospitalService;

    public function __construct(HospitalService $hospitalService)
    {
        $this->hospitalService = $hospitalService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Hospital::class);

        $countries = Country::get();

        $hospitals = $this->hospitalService->list(false);

        return view('supervisor.hospital.list', compact('hospitals', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Hospital::class);
            $hospital = $this->hospitalService->store($request->validated());

            return $this->resourceResponse(
                message: 'Hospital creado correctamente.',
                data: new HospitalResource($hospital),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Hospital  $hospital
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Hospital $hospital)
    {
        if ($request->ajax()) {
            $this->authorize('update', $hospital);

            return $this->resourceResponse(
                message: '',
                data: new HospitalResource($hospital)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Hospital  $hospital
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Hospital $hospital)
    {
        if ($request->ajax()) {
            $this->authorize('update', $hospital);
            $this->hospitalService->update($hospital, $request->validated());
            $hospital->refresh();

            return $this->resourceResponse(
                message: 'Hospital actualizado correctamente.',
                data: new HospitalResource($hospital)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Imports the fiven file
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ImportHospital(Request $request)
    {
        DB::transaction(function () use ($request) {
            /** @var \Illuminate\Http\UploadedFile */
            $file = $request->file('upload_hospital');

            Excel::import(new HospitalsImport, $file);
        });

        return redirect()->back()->with('message', 'message');
    }

    /**
     * Downloads the import template
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function dowload_template()
    {
        return Storage::download('import-templates/plantilla-hospitales.xlsx');
    }
}
