<?php

namespace App\Http\Controllers;

use App\Enums\Benefit\AuthorizationLetterStatus;
use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Http\Requests\Benefit\BenefitCorrectionRequest;
use App\Models\Benefit;
use App\Models\Pharmacy;
use App\Services\Benefit\BenefitService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class BenefitCorrectionController extends Controller
{
    use ApiResponses;

    /**
     * The benefit service instance
     *
     * @var BenefitService
     */
    private BenefitService $benefitService;

    /**
     * Creates a new controller instance
     *
     * @param  BenefitService  $benefitService
     */
    public function __construct(BenefitService $benefitService)
    {
        $this->benefitService = $benefitService;
    }

    /**
     * Display the listing of benefits
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $query = Benefit::with('patient')->latest();

        /** @var \App\Models\User */
        $user = auth()->user();

        if ($user->hasPermissionTo('restaurar beneficios') or $user->is_admin) {
            $query->withTrashed();
        }

        $benefits = $query->get();

        return view('corrections.benefits.index', compact('benefits'));
    }

    /**
     * Display the form to apply corrections to the given benefit
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Benefit $benefit)
    {
        $pharmacies = Pharmacy::active()->get();

        return view('corrections.benefits.edit', compact('benefit', 'pharmacies'));
    }

    /**
     * Updates the given benefit
     *
     * @param  BenefitCorrectionRequest  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BenefitCorrectionRequest $request, Benefit $benefit)
    {
        $attributes = $request->validated();

        $benefitType = BenefitTypes::from($attributes['benefit_type']);

        if ($benefitType == BenefitTypes::OOP) {
            $attributes['autorized'] = null;
            $attributes['is_accepted'] = null;
            $attributes['causal_types'] = null;
        } else {
            $authorizationLetterStatus = AuthorizationLetterStatus::from($attributes['autorized']);

            if ($authorizationLetterStatus == AuthorizationLetterStatus::Rechazado && $attributes['is_accepted'] == 1) {
                $attributes['benefit_type'] = BenefitTypes::OOP;
            } elseif ($authorizationLetterStatus != AuthorizationLetterStatus::Rechazado) {
                $attributes['is_accepted'] = null;
                $attributes['causal_types'] = null;
            }
        }

        $benefitStatus = BenefitStatus::from($attributes['status']);

        if (! in_array($benefitStatus, [BenefitStatus::FacturaCargada, BenefitStatus::Cerrado])) {
            $attributes['number_units'] = null;
            $attributes['pharmacy_id'] = null;
            $attributes['discount'] = null;
            $attributes['transaction_date'] = null;
            $attributes['invoice_number'] = null;
            $attributes['factura'] = null;
        }

        if ($benefitStatus != BenefitStatus::Cerrado) {
            $attributes['closing_reason'] = null;
            $attributes['closed_at'] = null;
        } else {
            $attributes['closed_at'] = now();
        }

        $this->benefitService->update($benefit, $attributes);

        if (! empty($attributes['factura'])) {
            $this->benefitService->uploadInvoice($benefit, $attributes['factura']);
        }

        return $this->jsonResponse('Beneficio actualizado correctamente');
    }

    /**
     * Deletes the given benefit.
     *
     * @param  Request  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, Benefit $benefit)
    {
        $this->authorize('delete', $benefit);

        DB::transaction(function () use ($benefit) {
            $benefit->delete();
        });

        return $this->jsonResponse(message: 'Beneficio eliminado correctamente', code: Response::HTTP_NO_CONTENT);
    }

    /**
     * Restores the given benefit.
     *
     * @param  Request  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(Request $request, Benefit $benefit)
    {
        $this->authorize('restore', $benefit);

        DB::transaction(function () use ($benefit) {
            $benefit->restore();
        });

        return $this->jsonResponse(message: 'Beneficio restaurado correctamente', code: Response::HTTP_OK);
    }
}
