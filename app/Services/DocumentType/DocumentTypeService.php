<?php

namespace App\Services\DocumentType;

use App\Models\DocumentType;

class DocumentTypeService
{
    /**
     * Returns a list of document types.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, DocumentType>
     */
    public function list($onlyActive = true)
    {
        $documentTypes = DocumentType::query();

        if ($onlyActive) {
            $documentTypes->active();
        }

        return $documentTypes->get();
    }

    /**
     * Create a DoocumentType.
     *
     * @param  array  $input
     * @return DocumentType
     */
    public function store(array $input): DocumentType
    {
        return DocumentType::create($input);
    }

    public function update(DocumentType $documentType, array $input): bool
    {
        return $documentType->update($input);
    }
}
