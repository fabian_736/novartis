<?php

namespace App\Enums;

enum RelationshipPatient: string
{
    case Padre = 'Padre';
    case Madre = 'Madre';
    case Hermanos = 'Hermano/a';
    case Enfermero = 'Enfermero/a';
}
