<?php

namespace App\Enums\Benefit;

enum AuthorizationLetterRejectionReasons: string
{
    case Preexistencia = 'Preexistencia';
    case PeriodoDeLatencia = 'Periodo de latencia';
    case PolizaNocontemplaBiologicos = 'Póliza no contempla biológicos';
}
