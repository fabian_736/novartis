<?php

namespace App\Http\Controllers;

use App\Enums\Benefit\AuthorizationLetterStatus;
use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Http\Requests\Benefit\ManagementBenefitUpdateRequest;
use App\Http\Requests\Benefit\NewBenefitStoreRequest;
use App\Http\Requests\Benefit\UpdateAuthorizationLetterStatusRequest;
use App\Http\Requests\Benefit\UpdateRequest;
use App\Http\Requests\Benefit\UploadInvoiceRequest;
use App\Http\Resources\Benefit\BenefitResource;
use App\Models\Benefit;
use App\Models\Doctor;
use App\Models\Patient;
use App\Models\Pharmacy;
use App\Services\Benefit\BenefitService;
use App\Services\Hospital\HospitalService;
use App\Services\InsuranceCarrier\InsuranceCarrierService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class BenefitController extends Controller
{
    use ApiResponses;

    /**
     * The benefit service instance
     *
     * @var BenefitService
     */
    private BenefitService $benefitService;

    /**
     * The insuranceCarrier service instance
     *
     * @var InsuranceCarrierService
     */
    private InsuranceCarrierService $insuranceCarrierService;

    /**
     * The hospital service instance
     *
     * @var HospitalService
     */
    private HospitalService $hospitalService;

    /**
     * Creates a new controller ionstance
     *
     * @param  BenefitService  $benefitService
     * @param  InsuranceCarrierService  $insuranceCarrierService
     * @param  HospitalService  $hospitalService
     */
    public function __construct(BenefitService $benefitService, InsuranceCarrierService $insuranceCarrierService, HospitalService $hospitalService)
    {
        $this->benefitService = $benefitService;
        $this->insuranceCarrierService = $insuranceCarrierService;
        $this->hospitalService = $hospitalService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $this->authorizeAny(['create', 'update'], Benefit::class);

        $benefits = null;
        $pap = $request->get('PAP');
        $document = $request->get('document');
        $names = $request->get('names');

        if (! empty($pap) || ! empty($document) || ! empty($names)) {
            $benefits = Benefit::whereHas('patient', function ($query) use ($pap, $document, $names) {
                if (! empty($pap)) {
                    $query->where('pap', '=', $pap);
                } elseif (! empty($document)) {
                    $query->where('document_number', '=', $document);
                } elseif (! empty($names)) {
                    $query->where('name', 'LIKE', "%$names%");
                }
            })->where('status', '!=', BenefitStatus::Cerrado->value)->get();
        }

        $patients = Patient::canGetDrugDiscountBenefit()->get();
        $insuranceCarriers = $this->insuranceCarrierService->list();
        $hospitals = $this->hospitalService->list();

        return view('benefit.list', compact('benefits', 'patients', 'insuranceCarriers', 'hospitals'));
    }

    /**
     * Display a view of the resource.
     *
     * @param  Benefit  $benefit
     * @return \Illuminate\Contracts\View\View
     */
    public function benefit_management(Benefit $benefit)
    {
        $benefit->load([
            'patient',
        ]);

        /** @var \App\Models\Treatment */
        $treatment = $benefit->patient->treatment;

        $doctors = Doctor::whereRelation('country', 'id', '=', $benefit->patient->city->country_id)
            ->orWhere('id', $treatment->doctor?->id)
            ->get();

        return view('benefit.benefits_management', compact('benefit', 'doctors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NewBenefitStoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newBenefitStore(NewBenefitStoreRequest $request)
    {
        if ($request->ajax()) {
            $patient = Patient::whereId($request->safe()->patient_id)->firstOrFail();

            $inputBenefit = $this->benefitService->createNewBenefit($patient);

            return $this->resourceResponse(
                message: 'Beneficio creado correctamente.',
                data: new BenefitResource($inputBenefit)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Benefit $benefit)
    {
        $benefit->update($request->safe()->toArray());

        return $request->wantsJson() ?
            response()->json([
                'message' => 'Beneficio modificado correctamente',
                'data' => $benefit,
            ]) :
            redirect()->back()->with('message', 'Beneficio modificado correctamente');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index_invoice()
    {
        $benefits = Benefit::with('patient')->get();

        return view('pharmacy.list_invoice', compact('benefits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ManagementBenefitUpdateRequest  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function status_update(ManagementBenefitUpdateRequest $request, Benefit $benefit)
    {
        $benefit->load([
            'patient.treatment.doctor',
        ]);

        $benefit->fill($request->validated());

        $benefit->status = 0;
        $benefit->closed_at = now();

        $benefit->save();

        return $request->wantsJson() ?
            response()->json([
                'message' => 'Beneficio modificado correctamente',
                'data' => $benefit,
            ]) :
            redirect()->route('benefits.benefits.index')->with('message', 'Beneficio modificado correctamente');
    }

    /**
     * Displays the authorization letter status
     *
     * @param  Request  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAuthorizationLetterStatus(Request $request, Benefit $benefit)
    {
        $benefit->load('patient');

        if ($request->ajax()) {
            $this->authorize('update', $benefit);

            return $this->resourceResponse(
                message: '',
                data: new BenefitResource($benefit)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Updates the authorization letter status
     *
     * @param  UpdateAuthorizationLetterStatusRequest  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAuthorizationLetterStatus(UpdateAuthorizationLetterStatusRequest $request, Benefit $benefit)
    {
        $this->authorize('update', $benefit);

        $attributes = $request->validated();

        $authorizationLetterStatus = AuthorizationLetterStatus::from($attributes['autorized']);

        switch ($authorizationLetterStatus) {
            case AuthorizationLetterStatus::Aprobado:
                $attributes['status'] = BenefitStatus::FacturaPendiente;
                $attributes['causal_types'] = null;
                $attributes['is_accepted'] = null;
                $attributes['benefit_type'] = BenefitTypes::Copago;
                break;
            case AuthorizationLetterStatus::Rechazado:
                if ($attributes['is_accepted'] == 1) {
                    $attributes['status'] = BenefitStatus::FacturaPendiente;
                    $attributes['benefit_type'] = BenefitTypes::OOP;
                } else {
                    $attributes['status'] = BenefitStatus::DescuentoEspecialRechazado;
                }
                break;
            case AuthorizationLetterStatus::Observada:
                $attributes['status'] = BenefitStatus::ProcesoObservado;
                break;
            default:
                $attributes['status'] = BenefitStatus::CartaPendiente;
                $attributes['causal_types'] = null;
                $attributes['is_accepted'] = null;
                $attributes['benefit_type'] = BenefitTypes::Copago;
        }

        $this->benefitService->update($benefit, $attributes);

        return $this->jsonResponse('Carta de autorización actualizada correctamente');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Contracts\View\View
     */
    public function upload_invoice(Request $request, Benefit $benefit)
    {
        $benefit->load('patient');

        $pharmacies = Pharmacy::active()
            ->orWhere('id', $benefit->pharmacy?->id)
            ->get();

        return view('pharmacy.upload_invoice', compact('benefit', 'pharmacies'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index_authorization()
    {
        $benefits = Benefit::with('patient')->get();

        return view('pharmacy.list_authorization', compact('benefits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  uploadInvoiceRequest  $request
     * @param  Benefit  $benefit
     * @return \Illuminate\Http\RedirectResponse
     */
    public function load_invoice(UploadInvoiceRequest $request, Benefit $benefit)
    {
        DB::transaction(function () use ($request, $benefit) {
            $benefit->load('patient.treatment');
            $benefit->patient->fill($request->all())->save();

            if ($request->hasFile('image_invoice')) {
                $benefit->addMediaFromRequest('image_invoice')->toMediaCollection('invoice');
            }

            if ($request->hasFile('image_invoice')) {
                $benefit->fill($request->all());
                $benefit->status = 4; //Actualiza el estado del beneficio a Factura Cargada
                $benefit->save();
            } else {
                $benefit->fill($request->all())->save();
            }
        });

        return redirect()->route('benefits.benefits.index')->with('message', 'La factura se ha cargado correctamente');
    }
}
