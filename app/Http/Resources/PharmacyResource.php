<?php

namespace App\Http\Resources;

use App\Http\Resources\City\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Pharmacy
 */
class PharmacyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>|\Illuminate\Contracts\Support\Arrayable<string, mixed>|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'document_number' => $this->document_number,
            'name' => $this->name,
            'address' => $this->address,
            'is_active' => $this->is_active,
            'city_id' => $this->city_id,
            'city' => new CityResource($this->whenLoaded('city')),
            'pharmacy_chain_id' => $this->pharmacy_chain_id,
            'document_type_id' => $this->document_type_id,
            'document_type' => new DocumentTypeResource($this->whenLoaded('documentType')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
