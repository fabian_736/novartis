<?php

namespace App\Models;

use App\Traits\SoftDeletesParent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperTreatment
 */
class Treatment extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory, SoftDeletes, SoftDeletesParent;

    protected $fillable = [
        'doctor_id',
        'patient_id',
        'pathology_id',
        'observations',
    ];

    /**
     * Obtiene la ultima Formulación asociada al Tratamiento.
     *
     * @return HasOne<Formulation>
     */
    public function currentFormulation(): HasOne
    {
        return $this->hasOne(Formulation::class)->ofMany([
            'id' => 'max',
        ], function ($query) {
            $query->where('starts_at', '<=', now('UTC'))
                ->where(function ($query) {
                    return $query->where('ends_at', '<=', now('UTC'))
                        ->orWhereNull('ends_at');
                });
        });
    }

    /**
     * Obtiene a las Formulaciones asociadas al Tratamiento.
     *
     * @return HasMany<Formulation>
     */
    public function formulations(): HasMany
    {
        return $this->hasMany(Formulation::class);
    }

    /**
     * Retrieves the patient.
     *
     * @return BelongsTo<Patient, Treatment>
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * Retrieves the pathology.
     *
     * @return BelongsTo<Pathology, Treatment>
     */
    public function pathology(): BelongsTo
    {
        return $this->belongsTo(Pathology::class);
    }

    /**
     * Retrieves the doctor.
     *
     * @return BelongsTo<Doctor, Treatment>
     */
    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class);
    }

    /**
     * Retrieves the dose.
     *
     * @return BelongsTo<Dose, Treatment>
     */
    public function dose(): BelongsTo
    {
        return $this->belongsTo(Dose::class);
    }
}
