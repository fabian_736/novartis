<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Authorize any of the given abilities.
     *
     * @param  iterable|string  $abilities
     * @param  array|mixed  $arguments
     * @return Response
     *
     * @throws AuthorizationException
     */
    public function authorizeAny($abilities, $arguments = [])
    {
        $authorized = app(Gate::class)
            ->forUser(auth()->user())
            ->any($abilities, $arguments);

        if ($authorized) {
            return Response::allow();
        } else {
            throw new AuthorizationException();
        }
    }
}
