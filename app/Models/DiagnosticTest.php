<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperDiagnosticTest
 */
class DiagnosticTest extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'name',
        'is_active',
    ];

    /**
     * Scope a query to only include active diagnostic test.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Obtiene la Gestión de Pruebas Diagnosticas asociadas a los examenes(Pruebas Diagnosticas).
     *
     * @return HasMany<ManagementDiagnosticTest>
     */
    public function managementDiagnosticTests(): HasMany
    {
        return $this->hasMany(ManagementDiagnosticTest::class);
    }
}
