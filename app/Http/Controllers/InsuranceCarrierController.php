<?php

namespace App\Http\Controllers;

use App\Http\Requests\InsuranceCarrier\StoreRequest;
use App\Http\Requests\InsuranceCarrier\UpdateRequest;
use App\Http\Resources\InsuranceCarrier\InsuranceCarrierResource;
use App\Models\Country;
use App\Models\InsuranceCarrier;
use App\Services\InsuranceCarrier\InsuranceCarrierService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InsuranceCarrierController extends Controller
{
    use ApiResponses;

    /** @var InsuranceCarrierService */
    private $insuranceCarrierService;

    public function __construct(InsuranceCarrierService $insuranceCarrierService)
    {
        $this->insuranceCarrierService = $insuranceCarrierService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], InsuranceCarrier::class);

        $countries = Country::get();

        $insuranceCarriers = $this->insuranceCarrierService->list(false);

        return view('supervisor.insurance_carrier.list', compact('insuranceCarriers', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', InsuranceCarrier::class);
            $insuranceCarrier = $this->insuranceCarrierService->store($request->validated());

            return $this->resourceResponse(
                message: 'Aseguradora creada correctamente.',
                data: new InsuranceCarrierResource($insuranceCarrier),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  InsuranceCarrier  $insuranceCarrier
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, InsuranceCarrier $insuranceCarrier)
    {
        if ($request->ajax()) {
            $this->authorize('update', $insuranceCarrier);

            return $this->resourceResponse(
                message: '',
                data: new InsuranceCarrierResource($insuranceCarrier)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  InsuranceCarrier  $insuranceCarrier
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, InsuranceCarrier $insuranceCarrier)
    {
        if ($request->ajax()) {
            $this->authorize('update', $insuranceCarrier);
            $this->insuranceCarrierService->update($insuranceCarrier, $request->validated());
            $insuranceCarrier->refresh();

            return $this->resourceResponse(
                message: 'Aseguradora actualizada correctamente.',
                data: new InsuranceCarrierResource($insuranceCarrier)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
