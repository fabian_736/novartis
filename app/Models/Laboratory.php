<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperLaboratory
 */
class Laboratory extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'document_number',
        'name',
        'is_active',
        'address',
        'city_id',
        'document_type_id',
    ];

    /**
     * Scope a query to only include active laboratories.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Retrieves the city.
     *
     * @return BelongsTo<City, Laboratory>
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Obtiene las Pruebas Diagnosticas asociadas al Laboratorio.
     *
     * @return HasMany<ManagementDiagnosticTest>
     */
    public function managementDiagnosticTests(): HasMany
    {
        return $this->hasMany(ManagementDiagnosticTest::class);
    }
}
