<?php

namespace App\Http\Controllers;

use App\Http\Requests\Doctor\StoreRequest;
use App\Http\Requests\Doctor\UpdateRequest;
use App\Http\Resources\Doctor\DoctorResource;
use App\Imports\DoctorsImport;
use App\Models\Country;
use App\Models\Doctor;
use App\Models\Specialty;
use App\Services\Doctor\DoctorService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class DoctorController extends Controller
{
    use ApiResponses;

    /** @var DoctorService */
    private $doctorService;

    public function __construct(DoctorService $doctorService)
    {
        $this->doctorService = $doctorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Doctor::class);

        $specialties = Specialty::get();
        $countries = Country::get();

        $doctors = $this->doctorService->list(false);

        return view('supervisor.doctor.list', compact('doctors', 'specialties', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Doctor::class);
            $doctor = $this->doctorService->store($request->validated());

            return $this->resourceResponse(
                message: 'Doctor creado correctamente.',
                data: new DoctorResource($doctor),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Doctor  $doctor
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Doctor $doctor)
    {
        if ($request->ajax()) {
            $this->authorize('update', $doctor);

            return $this->resourceResponse(
                message: '',
                data: new DoctorResource($doctor)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Doctor  $doctor
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Doctor $doctor)
    {
        if ($request->ajax()) {
            $this->authorize('update', $doctor);
            $this->doctorService->update($doctor, $request->validated());
            $doctor->refresh();

            return $this->resourceResponse(
                message: 'Doctor actualizado correctamente.',
                data: new DoctorResource($doctor)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Imports the doctors
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ImportDoctors(Request $request)
    {
        DB::transaction(function () use ($request) {
            /** @var \Illuminate\Http\UploadedFile */
            $file = $request->file('upload_doctor');

            Excel::import(new DoctorsImport, $file);
        });

        return redirect()->back()->with('message', 'message');
    }

    /**
     * Downloads the import template
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function dowload_template()
    {
        return Storage::download('import-templates/plantilla-medicos.xlsx');
    }
}
