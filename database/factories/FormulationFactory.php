<?php

namespace Database\Factories;

use App\Models\PathologyHasMedicineDose;
use App\Models\Treatment;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Formulation>
 */
class FormulationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'frequency' => $this->faker->randomDigitNotZero(),
            'starts_at' => now(),
            'ends_at' => null,
            'treatment_id' => Treatment::factory(),
            'pathology_has_medicine_dose_id' => PathologyHasMedicineDose::factory(),
        ];
    }
}
