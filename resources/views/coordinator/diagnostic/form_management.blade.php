@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-orange h2">Prueba diagnóstica</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    <x-errores />
    <div class="row-reverse ">
        <div class="col px-0 py-5">
            <form action="{{ route('management.update', $managementDiagnosticTest->id) }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">PAP </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->pap }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Número de documento</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->document_number }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Nombre </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->name }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Medicamento</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->treatment?->pathology->medicine->name ?? 'Sin medicamento' }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="laboratory_id" class="h5 font-weight-bold title_patient_cordi">Laboratorio </label>
                            </div>
                            <div class="col">
                                <select name="laboratory_id" id="laboratory_id" class="form-control">
                                    <option selected disabled>Seleccione un laboratorio...</option>
                                    @foreach ($laboratories as $laboratory)
                                        <option value="{{ $laboratory->id }}">{{ $laboratory->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="diagnostic_test_id" class="h5 font-weight-bold title_patient_cordi">Prueba a realizar</label>
                            </div>
                            <div class="col">
                                <select name="diagnostic_test_id" id="diagnostic_test_id" class="form-control">
                                    <option selected disabled>Seleccione una prueba...</option>
                                    @foreach ($diagnosticTests as $diagnosticTest)
                                        <option value="{{ $diagnosticTest->id }}">{{ $diagnosticTest->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="management_date" class="h5 font-weight-bold title_patient_cordi">Fecha de coordinación</label>
                            </div>
                            <div class="col">
                                <input type="date" name="management_date" id="management_date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Orden médica</label>
                            </div>
                            <div class="col">
                                <a href="javascript:;" class="btn bg-secondary w-100" id="test" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">Ver archivo</a>
                                <script>
                                    $('#test').click(function() {
                                        window.open("{{ $managementDiagnosticTest->getFirstMediaUrl('file-upload') }}",
                                            'targetWindow',
                                            `toolbar=no,
                                            location=no,
                                            status=no,
                                            menubar=no,
                                            scrollbars=yes,
                                            resizable=yes,
                                            width=SomeSize,
                                            height=SomeSize`);
                                        return false;
                                    })
                                </script>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-4 mx-auto">
                        <div class="row">
                            <div class="col">
                                <a href="{{ route('management-diagnostic-tests.index') }}"
                                    class="btn btn-orange w-100 d-flex align-items-center justify-content-center">
                                    <span class="iconify mr-2" data-icon="fa:close" data-width="15"></span>
                                    SALIR
                                </a>
                            </div>
                            <div class="col d-flex justify-content-end">
                                <button type="submit" class="btn btn-dark w-100 d-flex align-items-center justify-content-center">
                                    <span class="iconify mr-2" data-icon="entypo:save" data-width="15"></span>
                                    GUARDAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
