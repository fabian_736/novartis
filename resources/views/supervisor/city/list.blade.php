@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-danger h2">Ciudades</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse">
        <div class="col d-flex justify-content-end mt-3">
            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            @can('crear ciudades', \App\Models\City::class)
                <button type="button" class="btn bg-danger d-flex align-items-center" data-bs-toggle="modal"
                    data-bs-target="#crearCiudad">
                    <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span> Crear ciudad
                </button>
            @endcan
        </div>
        <div class="col">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-danger">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">País</th>
                        <th scope="col">Estado</th>
                        @can('actualizar ciudades')
                            <th scope="col">Acciones</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cities as $key => $city)
                        <tr>
                            <td align="center" style="width: 50px">{{ $key + 1 }}</td>
                            <td>{{ $city->name }}</td>
                            <td>{{ $city->country->name }}</td>
                            @if ($city->is_active == 0)
                                <td>Inactivo</td>
                            @elseif ($city->is_active == 1)
                                <td>Activo</td>
                            @endif
                            @can('actualizar ciudades', $city)
                                <td>
                                    <div class="col d-flex justify-content-center" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-secondary bg-danger edit-button" type="button" style="color: white" href="{{ route('admin.cities.update', $city) }}"
                                            data-id="{{ $city->id }}">
                                            <span class="iconify mr-2" data-icon="akar-icons:edit"></span>
                                            Editar
                                        </a>
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Modal crear ciudades --}}
    <div class="modal fade" id="crearCiudad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear ciudad</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('admin.cities.store') }}" method="POST" id="create-city">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name"
                                                    class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name" class="form-control"
                                                    placeholder="Nombre" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country_id"
                                                    class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country_id" id="country_id" class="form-control"
                                                    required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        @if ($country->is_active == 1)
                                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal editar ciudades --}}
    <div class="modal fade" id="updateCity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Editar ciudad</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="#" method="POST" id="update-city">
                                @csrf
                                @method('PUT')
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name2"
                                                    class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name2" id="name2" class="form-control"
                                                    placeholder="Nombre" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country_id2"
                                                    class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country_id2" id="country_id2" class="form-control"
                                                    required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country->id }}" data-is-active="{{ $country->is_active }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="is_active2"
                                                    class="lead text-danger font-weight-bold">Estado</label>
                                            </div>
                                            <div class="col">
                                                <select name="is_active2" id="is_active2" class="form-control"
                                                    required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    <option value="0">Inactivo</option>
                                                    <option value="1">Activo</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };

        //Funcion modal create
        const form = document.getElementById('create-city');

        const create = e => {
            e.preventDefault();
            const data = {
                name: document.getElementById('name').value,
                country_id: document.getElementById('country_id').value,
            }
            axios.post('/admin/management/cities', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }

        form.addEventListener('submit', create);
        //Fin función modal create

        // Función modal update
        const updateCityForm = document.getElementById('update-city');

        const editCity = e => {
            e.preventDefault();

            axios.get(`/admin/management/cities/${e.target.closest('.edit-button').dataset.id}`)
                .then((response) => {
                    const city = response.data.data;

                    updateCityForm.dataset.id = city.id;
                    updateCityForm.elements['name2'].value = city.name;
                    updateCityForm.elements['country_id2'].value = city.country_id;
                    updateCityForm.elements['is_active2'].value = city.is_active;

                    Array.from(document.getElementById("country_id2").options).forEach(option => {
                        if (option.dataset.isActive == "0" && option.value != city.country_id) {
                            option.setAttribute("hidden", "");
                            option.setAttribute("disabled", "");
                        } else {
                            option.removeAttribute("hidden");
                            option.removeAttribute("disabled");
                        }
                    })

                    Array.from(updateCityForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    $('#updateCity').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#221F1F',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editCity);
        });

        const updateCity = e => {
            e.preventDefault();

            const updateCityData = {
                name: document.getElementById('name2').value,
                country_id: document.getElementById('country_id2').value,
                is_active: document.getElementById('is_active2').value,
            }
            axios.put(
                    `/admin/management/cities/${updateCityForm.dataset.id}`,
                    updateCityData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updateCity').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(updateCityForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updateCityForm.elements[`${key}2`];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updateCityForm.addEventListener('submit', updateCity);
        // Fin función modal update
    </script>
@endsection
