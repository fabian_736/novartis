<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * This change should rename the deleted_at column to parent_deleted_at
         * due the previous behavior of this column. Due this error:
         * https://github.com/laravel/framework/issues/36838 its necesary create
         * the parent_deleted_at column and then, migrate the deleted_at column
         * values to parent_deleted_at in production.
         */
        Schema::table('benefits', function (Blueprint $table) {
            $table->softDeletesParent()->after('closed_at');
        });

        Schema::table('formulations', function (Blueprint $table) {
            $table->softDeletesParent()->after('updated_at');
        });

        Schema::table('management_diagnostic_tests', function (Blueprint $table) {
            $table->softDeletesParent()->after('updated_at');
        });

        Schema::table('trackings', function (Blueprint $table) {
            $table->softDeletesParent()->after('updated_at');
        });

        Schema::table('treatments', function (Blueprint $table) {
            $table->softDeletesParent()->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->dropSoftDeletesParent();
        });

        Schema::table('formulations', function (Blueprint $table) {
            $table->dropSoftDeletesParent();
        });

        Schema::table('management_diagnostic_tests', function (Blueprint $table) {
            $table->dropSoftDeletesParent();
        });

        Schema::table('trackings', function (Blueprint $table) {
            $table->dropSoftDeletesParent();
        });

        Schema::table('treatments', function (Blueprint $table) {
            $table->dropSoftDeletesParent();
        });
    }
};
