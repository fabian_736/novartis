<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperCity
 */
class City extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'name',
        'is_active',
        'country_id',
    ];

    /**
     * Scope a query to only include active cities.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Retrieves the country.
     *
     * @return BelongsTo<Country, City>
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Obtiene a los Pacientes asociados a la Ciudad.
     *
     * @return HasMany<Patient>
     */
    public function patients(): HasMany
    {
        return $this->hasMany(Patient::class);
    }

    /**
     * Obtiene a los Usuarios asociados a la Ciudad.
     *
     * @return HasMany<User>
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * Obtiene a las Farmacias asociados a la Ciudad.
     *
     * @return HasMany<Pharmacy>
     */
    public function pharmacies(): HasMany
    {
        return $this->hasMany(Pharmacy::class);
    }

    /**
     * Obtiene a los Laboratorios asociados a la Ciudad.
     *
     * @return HasMany<Laboratory>
     */
    public function laboratories(): HasMany
    {
        return $this->hasMany(Laboratory::class);
    }
}
