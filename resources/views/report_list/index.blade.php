@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-primary h2">Reportes</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    <br>
    @if (session('message'))
        <script>
            Swal.fire(
                'Buen trabajo',
                'Los datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif
    @foreach ($errors->all() as $error)
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: '{{ $error }}',
            })
        </script>
    @endforeach

    <div class="d-flex justify-content-center pt-3" style="width:100%; height:100vh">
        <iframe title="ModeloDemografico - Página 1" width="1920px" height="100%" src="https://app.powerbi.com/view?r=eyJrIjoiYThjMDBkZDctZGViOS00NjM2LTlkZDgtZmQ0NjI3NTRmZjZiIiwidCI6ImU5MDEyNjRhLTA5ZWQtNGUyMy1iMWJlLWY1Y2UyNDRjMDE2NyIsImMiOjR9&pageName=ReportSection" frameborder="0" allowFullScreen="true"></iframe>
    </div>
@endsection
