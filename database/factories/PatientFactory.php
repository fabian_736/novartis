<?php

namespace Database\Factories;

use App\Enums\Gender;
use App\Enums\Patient\PatientStatus;
use App\Enums\Patient\PatientTypes;
use App\Enums\RelationshipPatient;
use App\Models\City;
use App\Models\DocumentType;
use App\Models\Hospital;
use App\Models\InsuranceCarrier;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Patient>
 */
class PatientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'document_type_id' => DocumentType::factory(),
            'document_number' => $this->faker->regexify("[1-9]\d{7,15}"),
            'name' => $this->faker->name(),
            'birth_date' => $this->faker->dateTimeBetween('-100 years', '-18 years'),
            'gender' => $this->faker->randomElement(Gender::cases()),
            'city_id' => City::factory(),
            'email' => $this->faker->safeEmail(),
            'phone' => $this->faker->regexify("[1-9]\d{8}"),
            'patient_type' => $this->faker->randomElement(PatientTypes::cases()),
            'insurance_carrier_id' => InsuranceCarrier::factory(),
            'hospital_id' => Hospital::factory(),
            'first_time_using_biological' => $this->faker->boolean(),
            'has_carer' => $this->faker->boolean(),
            'relationship_patient' => $this->faker->randomElement(RelationshipPatient::cases()),
            'carer_document_type_id' => DocumentType::factory(),
            'carer_document_number' => $this->faker->regexify("[1-9]\d{7,15}"),
            'carer_name' => $this->faker->name(),
            'carer_birth_date' => $this->faker->dateTimeBetween('-100 years', '-18 years'),
            'carer_phone' => $this->faker->regexify("[1-9]\d{8}"),
            'carer_email' => $this->faker->safeEmail(),
            'patient_status' => PatientStatus::Active,
            'date_admission' => now(),
        ];
    }

    /**
     * Indicate that the model's should be insured.
     *
     * @return static
     */
    public function insured()
    {
        return $this->state(function (array $attributes) {
            return [
                'patient_type' => PatientTypes::Insured,
            ];
        });
    }

    /**
     * Indicate that the model's should be insured.
     *
     * @return static
     */
    public function notInsured()
    {
        return $this->state(function (array $attributes) {
            return [
                'patient_type' => PatientTypes::NotInsured,
            ];
        });
    }
}
