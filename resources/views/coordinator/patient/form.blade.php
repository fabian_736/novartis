@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-orange h2">Datos personales </label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>

        <div class="row">
            <div class="col ">
                <div class="row">
                    <div class="col-auto d-flex align-items-center ">
                        <div style="width: 50px; height: 50px; " class="border border-orange d-flex justify-content-center align-items-center">
                            <div style="width: 40px; height: 40px; " class="bg-orange  d-flex justify-content-center align-items-center">
                                <img src="{{ url('svg/Person.svg') }}" alt="" class="w-75">
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex align-items-center">
                        <label for="" class="h3 text-orange ">Paciente</label>
                    </div>
                </div>
            </div>
        </div>
        <x-errores />
    </div>

    @if (session('message'))
        <div class="alert alert-success" role="alert">
            <span>Paciente editado correctamente</span>
        </div>
    @endif
    <div class="row-reverse ">
        <div class="col px-0 py-5">
            <form action="{{ route('patients.update_patient', $patient) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="container-input rounded mb-4 col-sm-4">
                    <input name="file-upload" accept=".pdf,image/*" type="file" name="file-1" id="file-1" class="inputfile inputfile-1 consentimiento" />
                    <label for="file-1">
                        <svg xmlns="http://www.w3.org/2000/svg" class="iborrainputfile" width="20" height="17" viewBox="0 0 20 17">
                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                        </svg>
                        <span class="iborrainputfile font-weight-bold">Carga de consentimiento</span>
                    </label><br>
                    @if ($patient->hasMedia('consent'))
                        <a style="color:gray;font-size:13px" class="font-weight-bold ml-5" target="_blank" href="{{ $patient->getFirstMediaUrl('consent') }}"> <span class="iconify mr-2" data-icon="emojione-monotone:eye"></span>Ver consentimiento</a>
                    @endif
                </div>
                <ul class="li_form">
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Número de afiliación</label>
                                    </div>
                                    <div class="col">
                                        <input disabled type="text" name="pap" id="pap" class="form-control" value="{{ $patient->pap }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="date_admission" class="h5 font-weight-bold title_patient_cordi">Fecha de inscripción</label>
                                    </div>
                                    <div class="col">
                                        <input type="date" name="date_admission" id="date_admission" class="form-control" value="{{ old('date_admission', $patient->date_admission) }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Tipo de identificación</label>
                                    </div>
                                    <div class="col">
                                        <select name="document_type_id" id="document_type_id" class="form-control" required>
                                            @foreach ($documentTypes as $documentType)
                                                <option value="{{ $documentType->id }}" @selected(old('document_type_id', $patient->document_type_id) == $documentType->id)>{{ $documentType->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Número de identificación</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="document_number" class="form-control" id="document_number" value="{{ old('document_number', $patient->document_number) }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Nombre completo</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name', $patient->name) }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Sexo</label>
                                    </div>
                                    <div class="col">
                                        <select name="gender" id="gender" class="form-control" required>
                                            <option @selected(old('gender', $patient->gender) == 'Male') value="Male">Masculino</option>
                                            <option @selected(old('gender', $patient->gender) == 'Female') value="Female">Femenino</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Fecha de nacimiento</label>
                                    </div>
                                    <div class="col">
                                        <input required type="date" name="birth_date" id="birth_date" value="{{ old('birth_date', $patient->birth_date) }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Edad</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="age" class="form-control" id="age" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Pais</label>
                                    </div>
                                    <div class="col">
                                        <select required name="country_id" id="country" class="form-control">
                                            @foreach ($countries as $country)
                                                <option value="{{ $country->id }}" @selected(old('country_id', $patient->city->country_id) == $country->id)>{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Ciudad</label>
                                    </div>
                                    <div class="col">
                                        <select required name="city_id" id="city" class="form-control">
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}" @selected(old('city_id', $patient->city_id) == $city->id)>{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Correo electrónico</label>
                                    </div>
                                    <div class="col">
                                        <input type="email" name="email" id="email" class="form-control" value="{{ old('email', $patient->email) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Número de contacto</label>
                                    </div>
                                    <div class="col">
                                        <input required type="text" name="phone" id="phone" class="form-control" value="{{ old('phone', $patient->phone) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="patient_type" class="h5 font-weight-bold title_patient_cordi">Tipo de paciente</label>
                                    </div>
                                    <div class="col">
                                        <select required name="patient_type" id="patient_type" class="form-control" required>
                                            <option @selected(old('patient_type', $patient->patient_type->value) == 1) value="1">Asegurado</option>
                                            <option @selected(old('patient_type', $patient->patient_type->value) == 0) value="0">No asegurado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col insuranceCarrier" id="insuranceCarrier1">
                                <div class="row">
                                    <div class="col">
                                        <label for="insurance_carrier_id" class="h5 font-weight-bold title_patient_cordi">Aseguradora</label>
                                    </div>
                                    <div class="col-9">
                                        <select class="form-control" name="insurance_carrier_id" id="insurance_carrier_id">
                                            <option value="" @selected(empty(old('insurance_carrier_id', $patient->insurance_carrier_id))) disabled>Selecciona una opción...</option>
                                            @foreach ($insurance_carriers as $insurance_carrier)
                                                <option value="{{ $insurance_carrier->id }}" @selected(old('insurance_carrier_id', $patient->insurance_carrier_id) == $insurance_carrier->id)>{{ $insurance_carrier->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col hospital" id="hospital1">
                                <div class="row">
                                    <div class="col">
                                        <label for="hospital_id" class="h5 font-weight-bold title_patient_cordi">Hospital</label>
                                    </div>
                                    <div class="col-9">
                                        <select class="form-control" name="hospital_id" id="hospital_id">
                                            <option value="" @selected(empty(old('hospital_id', $patient->hospital_id))) disabled>Selecciona una opción...</option>
                                            @foreach ($hospitals as $hospital)
                                                <option value="{{ $hospital->id }}" @selected(old('hospital_id', $patient->hospital_id) == $hospital->id)>{{ $hospital->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col-3" id="title_posee">
                                        <label for="" class="font-weight-bold h4 title_patient_cordi">Posee responsable del paciente</label>
                                    </div>
                                    <div class="col-3" id="checkbox_posee">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="row d-inline-block">
                                                    <div class="col-1">
                                                        <input type="checkbox" name="has_carer" id="button1_patient_coordinator" class="square-selection-input" value="1" onchange="javascript:showContent_cordi()">
                                                        <label for="button1_patient_coordinator" aria-hidden="true"></label>
                                                    </div>
                                                    <div class="col ml-3">
                                                        <label for="button1_patient_coordinator" class="h5 text_alert">Si</label>
                                                    </div>
                                                </div>
                                                <div class="row mx-auto d-inline-block">
                                                    <div class="col-1">
                                                        <input type="checkbox" name="has_carer" id="button2_patient_coordinator" class="square-selection-input" value="0" onchange="javascript:showContent_cordi2()">
                                                        <label for="button2_patient_coordinator" aria-hidden="true"></label>
                                                    </div>
                                                    <div class="col ml-3">
                                                        <label for="button2_patient_coordinator" class="h5 text_alert">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cuidador" id="cuidador_cordi2" style="display: none">
                            <div class="row mb-3">
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Nombre completo</label>
                                        </div>
                                        <div class="col">
                                            <input type="text" name="carer_name" id="carer_name" placeholder="Digite el nombre del cuidador" value="{{ $patient->carer_name }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col cuidador" id="cuidador_cordi" style="display: none">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Vinculo con el paciente</label>
                                        </div>
                                        <div class="col">
                                            <select name="relationship_patient" id="relationship_patient" class="form-control">
                                                <option disabled @selected(empty(old('relationship_patient', $patient->relationship_patient))) value="">Seleccione una opción...</option>
                                                <option @selected(old('relationship_patient', $patient->relationship_patient) == 'Madre') value="Madre">Madre</option>
                                                <option @selected(old('relationship_patient', $patient->relationship_patient) == 'Padre') value="Padre">Padre</option>
                                                <option @selected(old('relationship_patient', $patient->relationship_patient) == 'Hermano/a') value="Hermano/a">Hermano/a</option>
                                                <option @selected(old('relationship_patient', $patient->relationship_patient) == 'Enfermero/a') value="Enfermero/a">Enfermero/a</option>
                                                <option @selected(old('relationship_patient', $patient->relationship_patient) == 'Otro/a') value="Otro/a">Otro/a</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Tipo de identificación</label>
                                        </div>
                                        <div class="col">
                                            <select name="carer_document_type_id" id="carer_document_type_id" class="form-control">
                                                <option disabled @selected(empty(old('carer_document_type_id', $patient->carer_document_type_id))) value="">Seleccione una opción...</option>
                                                @foreach ($documentTypes as $documentType)
                                                    <option value="{{ $documentType->id }}" @selected(old('carer_document_type_id', $patient->carer_document_type_id) == $documentType->id)>
                                                        {{ $documentType->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Número de identificación</label>
                                        </div>
                                        <div class="col">
                                            <input type="text" name="carer_document_number" id="carer_document_number" placeholder="Digite el numero de identificación" value="{{ old('carer_document_number', $patient->carer_document_number) }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Fecha de nacimiento</label>
                                        </div>
                                        <div class="col">
                                            <input type="date" name="carer_birth_date" id="carer_birth_date" value="{{ old('carer_birth_date', $patient->carer_birth_date) }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Edad</label>
                                        </div>
                                        <div class="col">
                                            <input type="text" name="carer_age" id="carer_age" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Correo electrónico</label>
                                        </div>
                                        <div class="col">
                                            <input type="email" placeholder="Digie el correo electronico" name="carer_email" id="carer_email" value="{{ old('carer_email', $patient->carer_email) }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="" class="h5 font-weight-bold title_patient_cordi">Número de contacto</label>
                                        </div>
                                        <div class="col">
                                            <input type="text" placeholder="Digite el numero de contacto" name="carer_phone" id="carer_phone" value="{{ old('carer_phone', $patient->carer_phone) }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col col-6">
                                <div class="row">
                                    <div class="col">
                                        <label for="patient_status" class="h5 font-weight-bold title_patient_cordi">Estado</label>
                                    </div>
                                    <div class="col">
                                        <select required name="patient_status" id="patient_status" class="form-control">
                                            <option value="En acceso" @selected(old('patient_status', $patient->patient_status->value) == 'En acceso')>En acceso</option>
                                            <option value="Activo" @selected(old('patient_status', $patient->patient_status->value) == 'Activo')>Activo</option>
                                            <option value="Inactivo" @selected(old('patient_status', $patient->patient_status->value) == 'Inactivo')>Inactivo</option>
                                            <option value="Suspendido" @selected(old('patient_status', $patient->patient_status->value) == 'Suspendido')>Suspendido</option>
                                            <option value="Fuera del programa" @selected(old('patient_status', $patient->patient_status->value) == 'Fuera del programa')>Fuera del programa</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="checkbox" name="is_reactivated" id="is_reactivated" class="square-selection-input" value="1" @checked(old('is_reactivated', $patient->reactivated_at))>
                                        <label for="is_reactivated" class="h5 mr-2" aria-hidden="true"></label>
                                        <label for="is_reactivated" class="h5 text_alert">Reactivado</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col-3">
                                        <label for="suspension_reason" class="h5 font-weight-bold title_patient_cordi">Motivo de suspensión</label>
                                    </div>
                                    <div class="col-3">
                                        <select required name="suspension_reason" id="suspension_reason" class="form-control">
                                            <option value="" @selected(empty(old('suspension_reason', $patient->suspension_reason))) disabled>Seleccione una opción</option>
                                            <option value="Paciente en embarazo" @selected(old('suspension_reason', $patient->suspension_reason) == 'Paciente en embarazo')>Paciente en embarazo</option>
                                            <option value="Paciente en viaje" @selected(old('suspension_reason', $patient->suspension_reason) == 'Paciente en viaje')>Paciente en viaje</option>
                                            <option value="Paciente con condiciones especiales" @selected(old('suspension_reason', $patient->suspension_reason) == 'Paciente con condiciones especiales')>Paciente con condiciones especiales</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="list-style: none">
                        <div class="row mb-3">
                            <div class="col-4 mx-auto">
                                <div class="row">
                                    <div class="col">
                                        <a href="{{ url('patients') }}" class="btn btn-orange w-100 d-flex align-items-center justify-content-center">
                                            <span class="iconify mr-2" data-icon="fa:close" data-width="15"></span>
                                            VOLVER
                                        </a>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-dark w-100 d-flex align-items-center justify-content-center">
                                            <span class="iconify mr-2" data-icon="entypo:save" data-width="15"></span>
                                            GUARDAR
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="sincuidador" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body p-5 text-center" style="font-size: 4em;">
                        <span class="iconify " style="color:tomato" data-icon="bi:exclamation-circle-fill"></span>
                        <h2>¿Estas seguro?</h2>
                        <h5>Si aceptas se borrarán los datos del cuidador</h5>
                        <button class="btn btn-cancel btn-primary">Cancelar</button>
                        <button class="btn btn-primary btn-aceptar">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

        <style>
            /* LISTA INACTIVA */

            .li_form .inactive {
                position: relative;
                list-style-type: none;
            }

            .li_form .inactive:before {
                content: "";
                position: absolute;
                top: 3px;
                left: -30px;
                width: 20px;
                height: 20px;
                background-image: url('/svg/li/square_inactive.svg');
            }


            /* LISTA ACTIVA */

            .li_form .active {
                position: relative;
                list-style-type: none;
            }

            .li_form .active:before {
                content: "";
                position: absolute;
                top: 3px;
                left: -30px;
                width: 20px;
                height: 20px;
                background-image: url('/svg/li/square_active.svg');
            }
        </style>

        @if ($patient->has_carer == 1)
            <script>
                bottom1 = document.getElementById("button1_patient_coordinator");
                $('.cuidador').show();
                bottom1.checked = true;
            </script>
        @else
            <script>
                bottom2 = document.getElementById("button2_patient_coordinator");
                $('.cuidador').hide();
                bottom2.checked = true;
                $('#relationship_patient').val('');
                $('#carer_document_type_id').val('');
                $('#carer_document_number').val('');
                $('#carer_name').val('');
                $('#carer_birth_date').val('');
                $('#carer_age').val('');
                $('#carer_phone').val('');
                $('#carer_email').val('');
            </script>
        @endif
        <script>
            function showContent_cordi() {
                bottom1 = document.getElementById("button1_patient_coordinator");
                bottom2 = document.getElementById("button2_patient_coordinator");
                bottom1.checked = true;

                if (bottom1.checked == true) {
                    $('#cuidador_cordi').show();
                    $('#cuidador_cordi2').show();
                    $('#title_posee').addClass('col').removeClass('col-3');
                    $('#checkbox_posee').addClass('col').removeClass('col-4');
                    bottom2.checked = false;
                } else {
                    $('#cuidador_cordi').hide();
                    $('#cuidador_cordi2').hide();
                    $('#title_posee').addClass('col-3').removeClass('col');
                    $('#checkbox_posee').addClass('col-4').removeClass('col');
                }
            }

            function showContent_cordi2() {
                bottom1 = document.getElementById("button1_patient_coordinator");
                bottom2 = document.getElementById("button2_patient_coordinator");

                if (bottom2.checked == true) {
                    $('#cuidador_cordi').hide();
                    $('#cuidador_cordi2').hide();
                    $('#title_posee').addClass('col-3').removeClass('col');
                    $('#checkbox_posee').addClass('col-4').removeClass('col');
                    bottom1.checked = false;
                    $('#sincuidador').modal('show');
                }
            }
            //boton de cancelar del modal
            $('.btn-cancel').click(function() {
                $('.modal').modal('hide');
                bottom1 = document.getElementById("button1_patient_coordinator");
                bottom2 = document.getElementById("button2_patient_coordinator");
                bottom1.checked = true;
                bottom2.checked = false;
                $('.cuidador').show();

            });
            //boton de Aceptar del modal
            $('.btn-aceptar').click(function() {
                $('#relationship_patient').val('');
                $('#carer_document_type_id').val('');
                $('#carer_document_number').val('');
                $('#carer_name').val('');
                $('#carer_birth_date').val('');
                $('#carer_age').val('');
                $('#carer_phone').val('');
                $('#carer_email').val('');
                $('.modal').modal('hide');
                Swal.fire({
                    icon: 'success',
                    title: 'Datos eliminados correctamente',
                    showConfirmButton: false,
                    timer: 1500
                })
            });

            function calcularEdad() {

                fecha = $('#birth_date').val();
                var hoy = new Date();
                var cumpleanos = new Date(fecha);
                var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                var m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }
                $('#age').val(edad);
            }

            function calcularEdadCuidador() {

                fecha = $('#carer_birth_date').val();
                var hoy = new Date();
                var cumpleanos = new Date(fecha);
                var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                var m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }
                if (isNaN(edad)) {
                    return '';
                }
                $('#carer_age').val(edad);
            }

            $(document).ready(function() {
                calcularEdad();
                calcularEdadCuidador();
            });
        </script>
    </div>
    <style>
        .inputfile-1+label {
            color: rgb(255, 255, 255);
            background-color: #EF5630;
        }

        .inputfile-1:focus+label,
        .inputfile-1.has-focus+label,
        .inputfile-1+label:hover {
            background-color: #FDEEEA;
            border: 2px solid #EF5630;
            color: #ff3806;
        }
    </style>
    <script>
        'use strict';

        ;
        (function(document, window, index) {
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function(input) {
                var label = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener('change', function(e) {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });
            });
        }(document, window, 0));
    </script>

    <script src="{{ asset('js/patient/personal_information.js') }}"></script>
@endsection
