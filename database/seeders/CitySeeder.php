<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert(
            [
                ['id' => 1, 'name' => 'Perú'],
                ['id' => 2, 'name' => 'Ecuador'],
            ]
        );

        City::insert(
            [
                ['name' => 'Lima', 'country_id' => 1],
                ['name' => 'Cusco', 'country_id' => 1],
                ['name' => 'Arequipa', 'country_id' => 1],
                ['name' => 'Huaraz', 'country_id' => 1],
                ['name' => 'Trujillo', 'country_id' => 1],
                ['name' => 'Chiclayo', 'country_id' => 1],
                ['name' => 'Quito', 'country_id' => 2],
                ['name' => 'Guayaquil', 'country_id' => 2],
                ['name' => 'Ambato', 'country_id' => 2],
                ['name' => 'Cuenca', 'country_id' => 2],
                ['name' => 'Manta', 'country_id' => 2],
                ['name' => 'Loja', 'country_id' => 2],
                ['name' => 'Tacna', 'country_id' => 1],
                ['name' => 'Ica', 'country_id' => 1],
                ['name' => 'Iquitos', 'country_id' => 1],
                ['name' => 'Piura', 'country_id' => 1],
                ['name' => 'Junin', 'country_id' => 1],
                ['name' => 'Tarapoto', 'country_id' => 1],
                ['name' => 'Ayacucho', 'country_id' => 1],
            ]
        );
    }
}
