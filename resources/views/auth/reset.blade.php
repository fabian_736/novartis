<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/favicon.png') }}">
    <link rel="icon" type="image/png" href="{{ url('img/favicon.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Novartis
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="{{ url('css/app.css') }}" rel="stylesheet" />
</head>

<body class="off-canvas-sidebar">
   

    <div class="row p-0 m-0">
        <div class="col d-flex justify-content-center" style="min-height: 100vh; padding: 5%">
            <div class="col-5" id="col_cardauth">
              <form  action="{{ route('password.update') }}" method="post">
                  @csrf
                  <input type="hidden" name="token" value="{{$request->route('token') }}">
                    <div class="card card-body shadow">
                        <div class="row-reverse ">
                            <div class="col d-flex justify-content-center p-5">
                                <img src="{{ url('img/2.png') }}" alt="">
                            </div>
                            <div class="col d-flex justify-content-center mb-3">
                                <label for="" class="h3 font-weight-bold">CAMBIO DE CONTRASEÑA</label>
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                  
                                    <div class="col">
                                        <div class="row-reverse">
                                            <div class="col"><label for=""
                                                    class="font-weight-bold h4"> <input type="hidden" name="email" value="{{ $request->email }}" class="form-control @error('email') is-invalid @enderror" placeholder="Correo Electronico"></label> </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-3">
                              <div class="row">
                                <div class="col-2 d-flex justify-content-end align-items-end">
                                    <span class="iconify" data-icon="ri:lock-password-fill"
                                        data-width="30"></span>
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col"><label for=""
                                                class="font-weight-bold h4">Contraseña</label></div>
                                        <div class="col">
                                          <input required type="password" name="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" placeholder="Nueva Contraseña">

                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-end">
                                        <span class="iconify" data-icon="ri:lock-password-fill"
                                            data-width="30"></span>
                                    </div>
                                    <div class="col">
                                        <div class="row-reverse">
                                            <div class="col"><label for=""
                                                    class="font-weight-bold h4">Repite Contraseña</label></div>
                                            <div class="col">
                                              <input required type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Repite Contraseña">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col my-5 d-flex justify-content-center">
                                <button type="submit" class="btn btn-dark w-50">CONFIRMAR</button>
                            </div>
                            <div class="col">
                              @error('email')
                              <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                              @error('password')
                              <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                              @error('password_confirmation')
                              <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--   Core JS Files   -->
    <!--   Core JS Files   -->
    <script src="{{ url('js/app.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>


    <style>
        ::placeholder {
            font-size: 1.2em !important;
        }

        input{
          padding-inline: 10px !important;
        }

        @media (max-width: 1366px) and (min-width: 1000px) {
            #col_cardauth {
                min-width: 50vw !important;
            }
        }

        @media (max-width: 1000px) {
            #col_cardauth {
                min-width: 80vw !important;
            }
        }

    </style>
</body>

</html>



