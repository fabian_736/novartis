<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperDose
 */
class Dose extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'presentation',
        'is_active',
    ];

    /**
     * Scope a query to only include active doses.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Relación MedicineDose - Dose.
     *
     * @return HasMany<Medicine>
     */
    public function medicines(): HasMany
    {
        return $this->hasMany(Medicine::class);
    }

    /**
     * Relación MedicineDose - Dose.
     *
     * @return HasMany<Pathology>
     */
    public function pathologies(): HasMany
    {
        return $this->hasMany(Pathology::class);
    }

    /**
     * Relación MedicineDose - Dose.
     *
     * @return HasMany<MedicineDose>
     */
    public function medicineDoses(): HasMany
    {
        return $this->hasMany(MedicineDose::class);
    }
}
