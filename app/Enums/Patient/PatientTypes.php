<?php

namespace App\Enums\Patient;

enum PatientTypes: int
{
    case Insured = 1;
    case NotInsured = 0;

    /**
     * Returns the equivalent string for each case
     *
     * @return string
     */
    public function label(): string
    {
        return match ($this) {
            PatientTypes::Insured => 'Asegurado',
            default => 'No asegurado',
        };
    }
}
