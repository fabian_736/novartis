(function () {
    function deleteBenefit(e) {
        Swal.fire({
            title: "¿Desea eliminar el beneficio?",
            text: "Eliminar beneficios puede causar inconsistencias en los reportes.",
            showDenyButton: true,
            confirmButtonText: 'Eliminar',
            denyButtonText: "Cancelar",
        })
            .then(response => {
                if (response.isConfirmed) {
                    benefitId = e.target.closest(".delete-benefit").dataset.id;

                    axios.delete(`/corrections/benefits/${benefitId}`)
                        .then(response => {
                            Swal.fire(
                                'Eliminación de beneficios',
                                "Beneficio eliminado correctamente",
                                'success'
                            )
                                .then(() => location.reload());
                        })
                        .catch(error => {
                            Swal.fire(
                                'Eliminación de beneficios',
                                response.data.message,
                                'error'
                            );
                        });
                }
            });
    }

    function restoreBenefit(e) {
        Swal.fire({
            title: "¿Desea restaurar el beneficio?",
            text: "Restaurar beneficios puede causar inconsistencias en los reportes y provocar comportamientos indeseados en este beneficio y el paciente.",
            showDenyButton: true,
            confirmButtonText: 'Restaurar',
            denyButtonText: "Cancelar",
        })
            .then(response => {
                if (response.isConfirmed) {
                    benefitId = e.target.closest(".restore-benefit").dataset.id;

                    axios.patch(`/corrections/benefits/${benefitId}/restore`)
                        .then(response => {
                            Swal.fire(
                                'Restauración de beneficios',
                                response.data.message,
                                'success'
                            )
                                .then(() => location.reload());
                        })
                        .catch(error => {
                            Swal.fire(
                                'Restauración de beneficios',
                                response.data.message,
                                'error'
                            );
                        });
                }
            });
    }

    Array.from(document.querySelectorAll(".delete-benefit")).forEach(button => {
        button.addEventListener("click", deleteBenefit);
    });
    Array.from(document.querySelectorAll(".restore-benefit")).forEach(button => {
        button.addEventListener("click", restoreBenefit);
    });
})();
