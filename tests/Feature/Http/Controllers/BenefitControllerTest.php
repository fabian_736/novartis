<?php

namespace Tests\Feature\Http\Controllers;

use App\Enums\Benefit\AuthorizationLetterRejectionReasons;
use App\Enums\Benefit\AuthorizationLetterStatus;
use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Enums\Patient\PatientStatus;
use App\Models\Admin\Permission;
use App\Models\Benefit;
use App\Models\Formulation;
use App\Models\Patient;
use App\Models\Treatment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BenefitControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_user_can_create_a_new_benefit_copay()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'crear beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->insured()->create();
        $treatment = Treatment::factory()->for($patient)->create();
        Formulation::factory()->for($treatment)->create();

        $this->actingAs($user)
            ->postJson(uri: route('benefits.new.benefits', [
                'patient_id' => $patient->id,
            ]), headers: self::$HEADERS)
            ->assertOk();

        $this->assertDatabaseHas('benefits', [
            'benefit_type' => BenefitTypes::Copago->value,
            'patient_id' => $patient->id,
        ]);
    }

    public function test_user_can_create_a_new_benefit_special_discount()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'crear beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->notInsured()->create();
        $treatment = Treatment::factory()->for($patient)->create();
        Formulation::factory()->for($treatment)->create();

        $this->actingAs($user)
            ->postJson(uri: route('benefits.new.benefits', [
                'patient_id' => $patient->id,
            ]), headers: self::$HEADERS)
            ->assertOk();

        $this->assertDatabaseHas('benefits', [
            'benefit_type' => BenefitTypes::OOP->value,
            'patient_id' => $patient->id,
        ]);
    }

    public function test_user_cannot_create_a_new_benefit_if_a_previous_benefit_is_open()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'crear beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->insured()->create();
        $treatment = Treatment::factory()->for($patient)->create();
        Formulation::factory()->for($treatment)->create();
        Benefit::factory()->for($patient)->copay()->create();

        $this->actingAs($user)
            ->postJson(uri: route('benefits.new.benefits', [
                'patient_id' => $patient->id,
            ]), headers: self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson(['message' => 'El paciente tiene un beneficio activo, aún no se puede crear un nuevo beneficio']);

        $patient->refresh();
        $this->assertCount(1, $patient->benefits);
    }

    public function test_user_cannot_create_a_new_benefit_if_patient_is_outside_of_program()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'crear beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->insured()->set('patient_status', PatientStatus::OutsideOfProgram)->create();
        $treatment = Treatment::factory()->for($patient)->create();
        Formulation::factory()->for($treatment)->create();

        $this->actingAs($user)
            ->postJson(uri: route('benefits.new.benefits', [
                'patient_id' => $patient->id,
            ]), headers: self::$HEADERS)
            ->assertNotFound();

        $patient->refresh();
        $this->assertCount(0, $patient->benefits);
    }

    public function test_user_cannot_create_a_new_benefit_if_patient_does_not_have_treatment()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'crear beneficios']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->insured()->create();

        $this->actingAs($user)
            ->postJson(uri: route('benefits.new.benefits', [
                'patient_id' => $patient->id,
            ]), headers: self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson(['message' => 'El beneficio no pudo ser creado porque el paciente no tiene registrado su tratamiento']);

        $patient->refresh();
        $this->assertCount(0, $patient->benefits);
    }

    public function test_user_can_decline_authorization_letter_and_special_discount()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'actualizar beneficios']);
        $user->givePermissionTo($permission);

        $patient = Patient::factory()->insured()->create();
        $benefit = Benefit::factory()->copay()->for($patient)->create();

        $data = [
            'autorized' => AuthorizationLetterStatus::Rechazado,
            'is_accepted' => 0,
            'causal_types' => AuthorizationLetterRejectionReasons::PeriodoDeLatencia,
        ];

        $this->actingAs($user)
            ->postJson(route('benefits.letter_authorization.update', ['benefit' => $benefit]), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(AuthorizationLetterStatus::Rechazado->value, $benefit->autorized);
        $this->assertEquals(0, $benefit->is_accepted);
        $this->assertEquals(AuthorizationLetterRejectionReasons::PeriodoDeLatencia->value, $benefit->causal_types);
        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::DescuentoEspecialRechazado->value, $benefit->status);
    }

    public function test_user_can_decline_authorization_letter_and_accept_special_discount()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'actualizar beneficios']);
        $user->givePermissionTo($permission);

        $patient = Patient::factory()->insured()->create();
        $benefit = Benefit::factory()->copay()->for($patient)->create();

        $data = [
            'autorized' => AuthorizationLetterStatus::Rechazado,
            'is_accepted' => 1,
            'causal_types' => AuthorizationLetterRejectionReasons::PeriodoDeLatencia,
        ];

        $this->actingAs($user)
            ->postJson(route('benefits.letter_authorization.update', ['benefit' => $benefit]), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(AuthorizationLetterStatus::Rechazado->value, $benefit->autorized);
        $this->assertEquals(1, $benefit->is_accepted);
        $this->assertEquals(AuthorizationLetterRejectionReasons::PeriodoDeLatencia->value, $benefit->causal_types);
        $this->assertEquals(BenefitTypes::OOP->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::FacturaPendiente->value, $benefit->status);
    }

    public function test_user_can_approve_authorization_letter()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'actualizar beneficios']);
        $user->givePermissionTo($permission);

        $patient = Patient::factory()->insured()->create();
        $benefit = Benefit::factory()->copay()->for($patient)->create();

        $data = [
            'autorized' => AuthorizationLetterStatus::Aprobado,
            'is_accepted' => null,
            'causal_types' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('benefits.letter_authorization.update', ['benefit' => $benefit]), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(AuthorizationLetterStatus::Aprobado->value, $benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::FacturaPendiente->value, $benefit->status);
    }

    public function test_user_can_mark_authorization_letter_as_observed()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'actualizar beneficios']);
        $user->givePermissionTo($permission);

        $patient = Patient::factory()->insured()->create();
        $benefit = Benefit::factory()->copay()->for($patient)->create();

        $data = [
            'autorized' => AuthorizationLetterStatus::Observada,
            'is_accepted' => null,
            'causal_types' => null,
        ];

        $this->actingAs($user)
            ->postJson(route('benefits.letter_authorization.update', ['benefit' => $benefit]), $data, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $benefit->refresh();

        $this->assertEquals(AuthorizationLetterStatus::Observada->value, $benefit->autorized);
        $this->assertNull($benefit->is_accepted);
        $this->assertNull($benefit->causal_types);
        $this->assertEquals(BenefitTypes::Copago->value, $benefit->benefit_type);
        $this->assertEquals(BenefitStatus::ProcesoObservado->value, $benefit->status);
    }
}
