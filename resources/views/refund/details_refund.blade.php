@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-warning h2">Reembolso</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse mt-3">
        <div class="col p-0">
            <div class="row ">
                <div class="col-10">
                    <div class="card-header bg-warning px-2 py-0 rounded">
                        <div class="row ">
                            <div class="col d-flex align-items-center">
                                <label for="" class="text-white lead font-weight-bold">Dr. +</label>
                            </div>
                            <div class="col ">
                                <form class="form-inline ">
                                    <input class="form-control mr-sm-2 w-75" type="search" placeholder="Search" id="search"
                                        aria-label="Search">
                                    <button class="btn btn-sm bg-warning" type="submit">
                                        <span class="iconify" data-width="25"
                                            data-icon="ant-design:search-outlined"></span>
                                    </button>
                                    <style>
                                        ::placeholder,
                                        #search {
                                            color: white !important;
                                            border-left: 0 !important;
                                            border-right: 0 !important;
                                            border-top: 0 !important;
                                        }

                                    </style>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <a href="javascript:;" class="btn btn-dark font-weight-bold">
                        <span class="iconify" data-icon="fa:cloud-download"></span> Descargar en reporte
                    </a>
                </div>
            </div>
        </div>
        <div class="col my-4 p-0 m-0">
            <form action="">
                <div class="row-reverse">
                    <div class="col mb-3">
                        <div class="row">
                            <div class="col ">
                                <div class="row">
                                    <div class="col-3 d-flex align-items-center">
                                        <p class="h5 p-0 m-0 font-weight-bold">Código de farmacia</p>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" value="DR-35681356498">

                                    </div>
                                </div>
                            </div>
                            <div class="col ">
                                <div class="row">
                                    <div class="col-3 d-flex align-items-center">
                                        <p class="h5 p-0 m-0 font-weight-bold">Fecha de creación</p>
                                    </div>
                                    <div class="col">
                                        <input type="date" name="" id="" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-3">
                        <div class="row">
                            <div class="col ">
                                <div class="row">
                                    <div class="col-3 d-flex align-items-center">
                                        <p class="h5 p-0 m-0 font-weight-bold">País</p>
                                    </div>
                                    <div class="col">
                                        <select name="" id="" class="form-control">
                                            <option value="" selected>Ecuador</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col ">
                                <div class="row">
                                    <div class="col-3 d-flex align-items-center">
                                        <p class="h5 p-0 m-0 font-weight-bold">Ciudad</p>
                                    </div>
                                    <div class="col">
                                        <select name="" id="" class="form-control">
                                            <option value="" selected>Guayaquil</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-3">
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-3 d-flex align-items-center">
                                        <p class="h5 p-0 m-0 font-weight-bold">Número de entregas</p>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row-reverse">
        <div class="col p-0 mt-5">
            <div class="card-header bg-warning">
                <label for="" class="h3 m-0 text-white">Listado de reembolsos</label>
            </div>
        </div>
        <div class="col p-0">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-warning">
                    <tr>
                        <th scope="col">Paciente</th>
                        <th scope="col">Producto</th>
                        <th scope="col">Fecha solicitud</th>
                        <th scope="col">Factura</th>
                        <th scope="col">Estado del beneficio</th>
                        <th scope="col">Responsable</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Maria H.</td>
                        <td>Cosentyx</td>
                        <td>10-05-2021</td>
                        <td>3qweq342341</td>
                        <td>Abierto</td>
                        <td>Asesor 1</td>
                        <td>
                            <div class="row">
                                <div class="col d-flex justify-content-center">
                                    <a href="{{ url('/refund/list_pharmacy/details') }}" class="btn btn-sm bg-warning"
                                        type="button">
                                        <span class="iconify" data-icon="akar-icons:eye" data-width="30"></span>
                                    </a>
                                </div>
                                <div class="col ">
                                    <div class="dropdown">
                                        <button class="btn bg-secondary dropdown-toggle d-flex justify-content-center align-items-center" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            <span class="iconify mr-2" data-icon="ant-design:edit-filled" data-width="25"></span> Modificar estado
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item" href="#">Abierto</a></li>
                                            <li><a class="dropdown-item" href="#">Pagado</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
