@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-danger h2">Usuarios</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    </div>

    <div class="row-reverse">
        <div class="col d-flex justify-content-end mt-3">
            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            @can('crear usuarios', \App\Models\User::class)
                <button type="button" class="btn bg-danger d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearUsuario">
                    <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>Crear usuario
                </button>
            @endcan
        </div>
        <div class="col">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-danger">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Identificación / Nit</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Ciudad</th>
                        <th scope="col">Estado</th>
                        @canany(['actualizar usuarios', 'asignar roles y permisos a usuarios'])
                            <th scope="col">Acción</th>
                        @endcanany
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $key => $user)
                        <tr>
                            <td align="center" style="width: 50px">{{ $key + 1 }}</td>
                            <td>{{ $user->document_number }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->city->name }}</td>
                            @if ($user->is_active == 0)
                                <td>Inactivo</td>
                            @elseif($user->is_active == 1)
                                <td>Activo</td>
                            @endif
                            @canany(['actualizar usuarios'], $user)
                                <td>
                                    <div class="col d-flex justify-content-center" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-secondary bg-danger edit-button" type="button" style="color: white" href="{{ route('admin.users.update', $user) }}" data-id="{{ $user->id }}">
                                            <span class="iconify" data-icon="bxs:edit"></span>&nbsp;
                                            Editar
                                        </a>
                                    </div>
                                </td>
                            @endcanany
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Modal crear usuarios --}}
    <div class="modal fade" id="crearUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear usuario</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('admin.users.store') }}" enctype="multipart/form-data" method="POST" id="create-user">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_type_id" class="lead text-danger font-weight-bold">Tipo de documento</label>
                                            </div>
                                            <div class="col">
                                                <select name="document_type_id" id="document_type_id" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($documentTypes as $documentType)
                                                        <option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_number" class="lead text-danger font-weight-bold">Número de documento</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="document_number" id="document_number" class="form-control" placeholder="Número de documento" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name" class="lead text-danger font-weight-bold">Nombres</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombres" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="phone" class="lead text-danger font-weight-bold">Teléfono</label>
                                            </div>
                                            <div class="col">
                                                <input type="number" name="phone" id="phone" class="form-control" placeholder="Número de teléfono" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country" class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country" id="country" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        @if ($country->is_active == 1)
                                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="city_id" class="lead text-danger font-weight-bold">Ciudad</label>
                                            </div>
                                            <div class="col">
                                                <select name="city_id" id="city" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="address" class="lead text-danger font-weight-bold">Dirección</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="address" id="address" class="form-control" placeholder="Dirección" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="email" class="lead text-danger font-weight-bold">Correo electrónico</label>
                                            </div>
                                            <div class="col">
                                                <input type="email" name="email" id="email" class="form-control" placeholder="Correo electrónico" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-2">
                                                <label for="role_id" class="lead text-danger font-weight-bold">Rol</label>
                                            </div>
                                            <div class="col-4">
                                                <select name="role_id" id="role_id" class="form-control" required autocomplete="off">
                                                    <option selected disabled>Seleccione un rol...</option>
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="col">
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label for="user_type" class="lead text-danger font-weight-bold">Tipo</label>
                                                    </div>
                                                    <div class="col">
                                                        <select name="user_type" id="user_type" class="form-control" required autocomplete="off">
                                                            <option value="" selected disabled>Seleccione un tipo...</option>
                                                            <option value="Admin">Administrador</option>
                                                            <option value="Farmacia">Farmacia</option>
                                                            <option value="Novartis">Novartis</option>
                                                        </select>
                                                        <div class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-2">
                                                <label for="pharmacy_id" class="lead text-danger font-weight-bold">Farmacia</label>
                                            </div>
                                            <div class="col-4">
                                                <select name="pharmacy_id" id="pharmacy_id" class="form-control" autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una farmacia...</option>
                                                    @foreach ($pharmacies as $pharmacy)
                                                        @if ($pharmacy->is_active == 1)
                                                            <option value="{{ $pharmacy->id }}">{{ $pharmacy->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-8">
                                        <div class="row">
                                            <div class="col-12">
                                                <label for="" class="lead text-danger font-weight-bold">Logotipo</label>&nbsp;&nbsp;&nbsp;
                                                <span style="font-size: 13px">Cargar si al usuario se le asocia una farmacia*</span> {{-- Temporal --}}
                                            </div>
                                            <div class="col">
                                                <input type="file" name="image_user" id="image_user" autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal editar usuarios --}}
    <div class="modal fade" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Editar usuario</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="#" method="POST" enctype="multipart/form-data" id="update-user">
                                @csrf
                                @method('PUT')
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_type_id2" class="lead text-danger font-weight-bold">Tipo de documento</label>
                                            </div>
                                            <div class="col">
                                                <select name="document_type_id2" id="document_type_id2" class="form-control" required>
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($documentTypes as $documentType)
                                                        <option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="document_number2" class="lead text-danger font-weight-bold">Número de documento</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="document_number2" id="document_number2" class="form-control" required>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name2" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name2" id="name2" class="form-control" placeholder="Nombre" required>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="phone2" class="lead text-danger font-weight-bold">Teléfono</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="phone2" id="phone2" class="form-control" placeholder="Número de teléfono" required>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country2" class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country2" id="country2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country->id }}" data-is-active="{{ $country->is_active }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="city_id2" class="lead text-danger font-weight-bold">Ciudad</label>
                                            </div>
                                            <div class="col">
                                                <select name="city_id2" id="city2" class="form-control" required autocomplete="off">
                                                    <option value="" selected>Seleccione una opcion</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="address2" class="lead text-danger font-weight-bold">Dirección</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="address2" id="address2" class="form-control" placeholder="Dirección" required>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="email2" class="lead text-danger font-weight-bold">Correo electrónico</label>
                                            </div>
                                            <div class="col">
                                                <input type="email" name="email2" id="email2" class="form-control" placeholder="Correo electrónico" required>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="is_active2" class="lead text-danger font-weight-bold">Estado</label>
                                            </div>
                                            <div class="col">
                                                <select name="is_active2" id="is_active2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    <option value="0">Inactivo</option>
                                                    <option value="1">Activo</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="role_id2" class="lead text-danger font-weight-bold">Rol</label>
                                            </div>
                                            <div class="col">
                                                <select name="role_id2" id="role_id2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione un rol...</option>
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="user_type2" class="lead text-danger font-weight-bold">Tipo</label>
                                            </div>
                                            <div class="col">
                                                <select name="user_type2" id="user_type2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione un tipo...</option>
                                                    <option value="Admin">Administrador</option>
                                                    <option value="Farmacia">Farmacia</option>
                                                    <option value="Novartis">Novartis</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="pharmacy_id" class="lead text-danger font-weight-bold">Farmacia</label>
                                            </div>
                                            <div class="col">
                                                <select name="pharmacy_id2" id="pharmacy_id2" class="form-control">
                                                    <option value="" selected disabled>Seleccione una farmacia...</option>
                                                    @foreach ($pharmacies as $pharmacy)
                                                        <option value="{{ $pharmacy->id }}" data-is-active="{{ $pharmacy->is_active }}">{{ $pharmacy->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="" class="lead text-danger font-weight-bold">Logotipo</label>
                                            </div>
                                            <div class="col">
                                                <input type="file" name="image_user2" id="image_user2">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ url('js/landing/select.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>


    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };


        //Funcion modal create
        const form = document.getElementById('create-user');

        const create = e => {
            e.preventDefault();
            const data = new FormData(form);
            axios.post('/admin/management/users', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }

        form.addEventListener('submit', create);
        //Fin función modal create


        // Función modal update
        const updateUserForm = document.getElementById('update-user');

        const editUser = e => {
            e.preventDefault();

            axios.get(`/admin/management/users/${e.target.closest('.edit-button').dataset.id}`)
                .then(async (response) => {
                    const user = response.data.data;

                    updateUserForm.dataset.id = user.id;
                    updateUserForm.elements['document_type_id2'].value = user.document_type_id;
                    updateUserForm.elements['document_number2'].value = user.document_number;
                    updateUserForm.elements['name2'].value = user.name;
                    updateUserForm.elements['phone2'].value = user.phone;
                    updateUserForm.elements['country2'].value = user.city.country_id;
                    await onSelectCountryChange(updateUserForm.elements['country2'], "#city2");
                    updateUserForm.elements['city2'].value = user.city_id;
                    updateUserForm.elements['email2'].value = user.email;
                    updateUserForm.elements['address2'].value = user.address;
                    updateUserForm.elements['user_type2'].value = user.user_type || "";
                    updateUserForm.elements['is_active2'].value = user.is_active;
                    updateUserForm.elements['role_id2'].value = user.roles[0].id;
                    updateUserForm.elements['pharmacy_id2'].value = user.pharmacy_id || "";

                    Array.from(document.getElementById("country2").options).forEach(option => {
                        if (option.dataset.isActive == "0" && option.value != user.city.country_id) {
                            option.setAttribute("hidden", "");
                            option.setAttribute("disabled", "");
                        } else {
                            option.removeAttribute("hidden");
                            option.removeAttribute("disabled");
                        }
                    })

                    Array.from(document.getElementById("pharmacy_id2").options).forEach(option => {
                        if (option.dataset.isActive == "0" && option.value != user.pharmacy_id) {
                            option.setAttribute("hidden", "");
                            option.setAttribute("disabled", "");
                        } else {
                            option.removeAttribute("hidden");
                            option.removeAttribute("disabled");
                        }
                    })

                    Array.from(updateUserForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    $('#updateUser').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#221F1F',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editUser);
        });

        const updateUser = e => {
            e.preventDefault();

            const updateUserData = new FormData(form);

            updateUserData.append('_method', "PUT");
            updateUserData.append('document_type_id', document.getElementById('document_type_id2').value);
            updateUserData.append('document_number', document.getElementById('document_number2').value);
            updateUserData.append('name', document.getElementById('name2').value);
            updateUserData.append('country', document.getElementById('country2').value);
            updateUserData.append('city_id', document.getElementById('city2').value);
            updateUserData.append('email', document.getElementById('email2').value);
            updateUserData.append('address', document.getElementById('address2').value);
            updateUserData.append('user_type', document.getElementById('user_type2').value);
            updateUserData.append('phone', document.getElementById('phone2').value);
            updateUserData.append('is_active', document.getElementById('is_active2').value);
            updateUserData.append('role_id', document.getElementById('role_id2').value);
            updateUserData.append('pharmacy_id', document.getElementById('pharmacy_id2').value);

            updateUserData.append('image_user', document.getElementById('image_user2').files[0]);
            axios.post(
                    `/admin/management/users/${updateUserForm.dataset.id}`,
                    updateUserData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updateUser').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message || error.response.data.error,
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(updateUserForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updateUserForm.elements[`${key}2`];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updateUserForm.addEventListener('submit', updateUser);
        // Fin función modal update

        $(function() {
            $('#country').on('change', e => {
                onSelectCountryChange(e.target, "#city");
            });
            $('#country2').on('change', e => {
                onSelectCountryChange(e.target, "#city2");
            });
        })
    </script>
@endsection
