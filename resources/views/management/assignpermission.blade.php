@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <div class="row mx-auto">
                    <div class="d-flex align-items-center" style="width: 100px">
                        <label for="" class="text-primary font-weight-bold h2">PSP |</label>
                    </div>
                    <div class="col d-flex align-items-center p-0 pt-2">
                        <label for="" class="h5" style="color: #0C68B0">Programa seguimiento <br> de
                            pacientes</label>
                    </div>
                </div>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>


    <br>
    <div class="container mt-3 col-sm-10 m-auto mr-4 text-danger">
        <h1 class="text-center">
            Asignar permisos
        </h1>
    </div>

    <div class="row mt-4 card p-4 ">
        <form class="container mt-3 col-sm-10 m-auto mr-4" method="POST" action=" {{ route('admin.management.assignpermission', ['role' => request()->role]) }}">
            @csrf
            <h3 class="text-center text-capitalize text-danger mb-4">{{ request()->role->name }}</h3>
            <hr>
            <div class="row">
                @foreach ($permissions->split(3) as $item)
                    <div class="col">
                        <ul class="list-group">
                            @foreach ($item as $permission)
                                <li class="list-group-item">
                                    <input class="switch" type="checkbox" name="permissions[]" id="" @checked($role->hasPermissionTo($permission)) value="{{ $permission->id }}">
                                    {{ $permission->name }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
            <div class="row d-flex justify-content-center mt-5 mb-3">
                <button class="btn btn-primary" type="submit">Guardar</button>
            </div>
        </form>
    </div>
    {{-- {{ $permissions->links() }} --}}

    <style>
        .switch {
            --inactive-bg: #cfd8dc;
            --active-bg: #00e676;
            --size: 20px;
            appearance: none;
            width: calc(var(--size) * 2.2);
            height: var(--size);
            display: inline-block;
            border-radius: calc(var(--size) / 2);
            cursor: pointer;
            background-color: var(--inactive-bg);
            background-image: radial-gradient(circle calc(var(--size) / 2.1),
                    #fff 100%,
                    #0000 0),
                radial-gradient(circle calc(var(--size) / 1.0), #0003 0%, #0000 100%);
            background-repeat: no-repeat;
            background-position: calc(var(--size) / -1.75) 0;
            transition: background 0.2s ease-out;
        }

        .switch:checked {
            background-color: var(--active-bg);
            background-position: calc(var(--size) / 1.75) 0;
        }

    </style>
@endsection
