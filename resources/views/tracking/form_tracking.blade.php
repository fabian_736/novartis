    <ul class="li_form">
        <li class="active">
            <div class="row mb-3 mt-5">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Fecha de llamada
                                inicial</label>
                        </div>
                        <div class="col">
                            <input type="date" name="date_initial_call" id="date_initial_call" class="form-control @error('date_initial_call') is-invalid @enderror" value="{{ isset($tracking->date_initial_call) ? $tracking->date_initial_call : old('date_initial_call') }}">
                            <div class="invalid-feedback">
                                @error('date_initial_call')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Visita inicial efectiva</label>
                        </div>
                        <div class="col">
                            <select name="effective_initial_visit" id="effective_initial_visit" class="form-control @error('effective_initial_visit') is-invalid @enderror"">
                                <option value="" selected disabled>Seleccione una opcion...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('effective_initial_visit')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col-6" id="generate_request">
                            <label for="" class="h5 font-weight-bold">Reclamó el medicamento</label>
                        </div>
                        <div class="col-6" id="checkbox_generate_request">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="row d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='claimed_medication' id="confirm_text_alert_1">Si</span>
                                            <input @checked($tracking->claimed_medication == 1) class="claimed_medication" type="checkbox" name="claimed_medication" id="button_claimed_medication1" value="1" onchange="javascript:showClaimed_medication()">
                                            <label for="button_claimed_medication1" class="fas"></label>
                                        </div>
                                    </div>
                                    <div class="row mx-auto d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='claimed_medication' id="confirm_text_alert_2">No</span>
                                            <input @checked($tracking->claimed_medication == 0) type="checkbox" name="claimed_medication" id="button_claimed_medication2" value="0" onchange="javascript:showClaimed_medication1()">
                                            <label for="button_claimed_medication2" class="fas"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-6" id="adverse_event">
                            <label for="" class="h5 font-weight-bold">Llamada contestada</label>
                        </div>
                        <div class="col-6" id="checkbox_adverse_event">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="row d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='answered_call' id="confirm_text_alert1">Si</span>
                                            <input @checked($tracking->answered_call == 1) type="checkbox" name="answered_call" id="button_answered_call1" value="1" onchange="javascript:showAnswered_call()">
                                            <label for="button_answered_call1" class="fas"></label>
                                        </div>
                                    </div>
                                    <div class="row mx-auto d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='answered_call' id="confirm_text_alert2">No</span>
                                            <input @checked($tracking->answered_call == 0) type="checkbox" name="answered_call" id="button_answered_call2" value="0" onchange="javascript:showAnswered_call1()">
                                            <label for="button_answered_call2" class="fas"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6"></div>
                    </div>
                </div>
            </div>
        </li>

        <div class="medicine" id="medicine1" style="display: none">
            <li class="active">
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Fecha de reclamo</label>
                            </div>
                            <div class="col">
                                <input value="{{ isset($tracking->claim_date) ? $tracking->claim_date : old('claim_date') }}" type="date" name="claim_date" id="claim_date" class="form-control @error('claim_date') is-invalid @enderror">
                                <div class="invalid-feedback">
                                    @error('claim_date')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Medicamento hasta</label>
                            </div>
                            <div class="col">
                                <input value="{{ isset($tracking->medicine_up) ? $tracking->medicine_up : old('medicine_up') }}" type="date" name="medicine_up" id="medicine_up" class="form-control @error('medicine_up') is-invalid @enderror">
                                <div class="invalid-feedback">
                                    @error('medicine_up')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </div>

        <div class="call_made" id="call_made1" style="display: none">
            <li class="active">
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Quién contestó la llamada</label>
                            </div>
                            <div class="col">
                                <input value="{{ isset($tracking->who_answered_call) ? $tracking->who_answered_call : old('who_answered_call') }}" type="text" name="who_answered_call" id="who_answered_call" class="form-control @error('who_answered_call')  @enderror" placeholder="Digité con quién se comunicó">
                                <div class="invalid-feedback">
                                    @error('who_answered_call')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Razón de la llamada</label>
                            </div>
                            <div class="col">
                                <select name="call_reason" id="call_reason" class="form-control @error('call_reason') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione una opción...</option>
                                    <option value="1">Opción 1</option>
                                    <option value="2">Opción 2</option>
                                </select>
                                <div class="invalid-feedback">
                                    @error('call_reason')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </div>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col-3" id="contactMed">
                            <label for="" class="h5 font-weight-bold ">Medio de contacto</label>
                        </div>
                        <div class="col-3" id="contactMed1">
                            <select name="contact_medium" id="contact_medium" class="form-control @error('contact_medium') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('contact_medium')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col notClaime" id="notClaime1" style="display: none">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Causa de no entrega</label>
                        </div>
                        <div class="col">
                            <select name="cause_of_no_claim" id="cause_of_no_claim" class="form-control @error('cause_of_no_claim') is-invalid @enderror"">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('cause_of_no_claim')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Tipo de llamada</label>
                        </div>
                        <div class="col">
                            <select name="type_call" id="type_call" class="form-control @error('type_call') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('type_call')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Número de intentos</label>
                        </div>
                        <div class="col">
                            <input value="{{ isset($tracking->number_of_attemps) ? $tracking->number_of_attemps : old('number_of_attemps') }}" type="text" name="number_of_attemps" id="number_of_attemps" class="form-control @error('number_of_attemps') is-invalid @enderror" placeholder="Digite el número de intentos de llamada">
                            <div class="invalid-feedback">
                                @error('number_of_attemps')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Aseguradora</label>
                        </div>
                        <div class="col">
                            <select name="insurance_carrier" id="insurance_carrier" class="form-control @error('insurance_carrier') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('insurance_carrier')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Hospital</label>
                        </div>
                        <div class="col">
                            <select name="hospital" id="hospital" class="form-control @error('hospital') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('hospital')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <div class="delivery" id="delivery1" style="display: none">
            <li class="active">
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Punto de entrega</label>
                            </div>
                            <div class="col">
                                <select name="delivery_point" id="delivery_point" class="form-control @error('delivery_point') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione una opción...</option>
                                    <option value="1">Opción 1</option>
                                    <option value="2">Opción 2</option>
                                </select>
                                <div class="invalid-feedback">
                                    @error('delivery_point')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Ciudad de entrega</label>
                            </div>
                            <div class="col">
                                <select name="delivery_city" id="delivery_city" class="form-control @error('delivery_city') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione una opción...</option>
                                    <option value="1">Opción 1</option>
                                    <option value="2">Opción 2</option>
                                </select>
                                <div class="invalid-feedback">
                                    @error('delivery_city')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </div>

        <div class="autorization" id="autorization1" style="display: none">
            <li class="active">
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Número de autorización</label>
                            </div>
                            <div class="col">
                                <input value="{{ isset($tracking->authorization_number) ? $tracking->authorization_number : old('authorization_number') }}" type="text" name="authorization_number" id="authorization_number" class="form-control @error('authorization_number') is-invalid @enderror" placeholder="Digite el número de autorización">
                                <div class="invalid-feedback">
                                    @error('authorization_number')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Fecha de autorización</label>
                            </div>
                            <div class="col">
                                <input value="{{ isset($tracking->authorization_date) ? $tracking->authorization_date : old('authorization_date') }}" type="date" name="authorization_date" id="authorization_date" class="form-control @error('authorization_date') is-invalid @enderror">
                                <div class="invalid-feedback">
                                    @error('authorization_date')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </div>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col-6" id="generate_request">
                            <label for="" class="h5 font-weight-bold">Genera solicitud</label>
                        </div>
                        <div class="col-6" id="checkbox_generate_request">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="row d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='generate_request' id="confirm_text_alert1">Si</span>
                                            <input @checked($tracking->generate_request == 1) type="checkbox" name="generate_request" id="button_generate_request1" value="1" onchange="javascript:showGenerate_request()">
                                            <label for="button_generate_request1" class="fas"></label>
                                        </div>
                                    </div>
                                    <div class="row mx-auto d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='generate_request' id="confirm_text_alert2">No</span>
                                            <input @checked($tracking->generate_request == 0) type="checkbox" name="generate_request" id="button_generate_request2" value="0" onchange="javascript:showGenerate_request1()">
                                            <label for="button_generate_request2" class="fas"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-6" id="adverse_event">
                            <label for="" class="h5 font-weight-bold">Evento adverso</label>
                        </div>
                        <div class="col-6" id="checkbox_adverse_event">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="row d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='adverse_event' id="confirm_text_alert1">Si</span>
                                            <input @checked($tracking->adverse_event == 1) value="1" type="checkbox" name="adverse_event" id="button_adverse_event1" onchange="javascript:showAdverse_event()">
                                            <label for="button_adverse_event1" class="fas"></label>
                                        </div>
                                    </div>
                                    <div class="row mx-auto d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='adverse_event' id="confirm_text_alert2">No</span>
                                            <input @checked($tracking->adverse_event == 0) type="checkbox" name="adverse_event" id="button_adverse_event2" value="0" onchange="javascript:showAdverse_event1()">
                                            <label for="button_adverse_event2" class="fas"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6"></div>
                    </div>
                </div>
            </div>
        </li>

        <li class="active">
            <div class="row mb-3">
                <div class="eventType col" id="eventType1" style="display: none">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Tipo de evento</label>
                        </div>
                        <div class="col">
                            <select name="event_type" id="event_type" class="form-control @error('event_type') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                                <option value="3">Opción 3</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('event_type')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-3" id="dateOfNextCall">
                            <label for="" class="h5 font-weight-bold ">Fecha de la próxima llamada</label>
                        </div>
                        <div class="col-3" id="dateOfNextCall1">
                            <input type="date" value="{{ isset($tracking->date_of_next_call) ? $tracking->date_of_next_call : old('date_of_next_call') }}" name="date_of_next_call" id="date_of_next_call" class="form-control @error('date_of_next_call') is-invalid @enderror">
                            <div class="invalid-feedback">
                                @error('date_of_next_call')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="active">
            <div class="row mb-3">
                <div class="col ">
                    <div class="row">
                        <div class="col-6">
                            <label for="" class="h5 font-weight-bold ">Razón de la próxima
                                llamada</label>
                        </div>
                        <div class="col-6">
                            <select name="next_call_reason" id="next_call_reason" class="form-control @error('next_call_reason') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                                <option value="3">Opción 3</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('next_call_reason')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-6" id="difficulty_access">
                            <label for="" class="h5 font-weight-bold">Dificultad en el acceso</label>
                        </div>
                        <div class="col-6" id="checkbox_difficulty_access">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="row d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='difficulty_access' id="confirm_text_alert1">Si</span>
                                            <input @checked($tracking->difficulty_access == 1) type="checkbox" name="difficulty_access" id="button_difficulty_access1" value="1" onchange="javascript:showDifficulty_access()">
                                            <label for="button_difficulty_access1" class="fas"></label>
                                        </div>
                                    </div>
                                    <div class="row mx-auto d-inline-block">
                                        <div class="col">
                                            <span class="h5 text_alert" name='difficulty_access' id="confirm_text_alert2">No</span>
                                            <input @checked($tracking->difficulty_access == 0) type="checkbox" name="difficulty_access" id="button_difficulty_access2" value="0" onchange="javascript:showDifficulty_access1()">
                                            <label for="button_difficulty_access2" class="fas"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>


        <div class="difficulty_type" id="difficulty_type1" style="display: none">
            <li class="active">
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Tipo de dificultad</label>
                            </div>
                            <div class="col-9">
                                <textarea name="difficulty_type" id="difficulty_type" cols="30" rows="2" class="form-control @error('difficulty_type') is-invalid @enderror" placeholder="Digite el tipo de dificultad...">{{ isset($tracking->difficulty_type) ? $tracking->difficulty_type : old('difficulty_type') }}</textarea>
                                <div class="invalid-feedback">
                                    @error('difficulty_type')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </div>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Observaciones de la próxima llamada</label>
                        </div>
                        <div class="col-9">
                            <textarea name="next_call_observations" id="next_call_observations" cols="30" rows="3" class="form-control @error('next_call_observations') is-invalid @enderror" placeholder="Digite las observaciones...">{{ isset($tracking->next_call_observations) ? $tracking->next_call_observations : old('next_call_observations') }}</textarea>
                            <div class="invalid-feedback">
                                @error('next_call_observations')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Consecutivo</label>
                        </div>
                        <div class="col">
                            <input type="text" value="{{ isset($tracking->consecutive) ? $tracking->consecutive : old('consecutive') }}" name="consecutive" id="consecutive" class="form-control @error('consecutive') is-invalid @enderror" placeholder="Digite el consecutivo">
                            <div class="invalid-feedback">
                                @error('consecutive')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Tratamiento previo</label>
                        </div>
                        <div class="col">
                            <select name="previous_treatment" id="previous_treatment" class="form-control @error('previous_treatment') is-invalid @enderror">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                <option value="1">Opción 1</option>
                                <option value="2">Opción 2</option>
                                <option value="3">Opción 3</option>
                            </select>
                            <div class="invalid-feedback">
                                @error('previous_treatment')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <div class="treatment" id="treatment1" style="display: none">
            <li class="active">
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Número de cajas/Unidades</label>
                            </div>
                            <div class="col-2">
                                <input value=">{{ isset($tracking->number_of_boxes) ? $tracking->number_of_boxes : old('number_of_boxes') }}" type="number"
                                    name="number_of_boxes" id="number_of_boxes" class="form-control @error('number_of_boxes')
is-invalid
@enderror" placeholder="N° de Cajas">
                            </div>
                            <div class="col-4">
                                <select name="units" id="units" class="form-control">
                                    <option value="" selected disabled>Seleccione una opción...</option>
                                    <option value="1">Opción 1</option>
                                    <option value="2">Opción 2</option>
                                    <option value="3">Opción 3</option>
                                </select>
                                <div class="invalid-feedback">
                                    @error('number_of_boxes')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold ">Medicamento</label>
                            </div>
                            <div class="col">
                                <select name="medicine" id="medicine" class="form-control @error('medicine') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione una opción...</option>
                                    <option value="1">Opción 1</option>
                                    <option value="2">Opción 2</option>
                                    <option value="3">Opción 3</option>
                                </select>
                                <div class="invalid-feedback">
                                    @error('medicine')
                                        <span>{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </div>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col-3">
                            <label for="" class="h5 font-weight-bold ">Dosis</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="treatment_dose" id="treatment_dose" class="form-control @error('treatment_dose') is-invalid @enderror" placeholder="Digite la dosis" value="{{ isset($tracking->treatment_dose) ? $tracking->treatment_dose : old('treatment_dose') }}">
                            <div class="invalid-feedback">
                                @error('treatment_dose')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <li class="active">
            <div class="row mb-3">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <label for="" class="h5 font-weight-bold ">Observaciones
                                generales</label>
                        </div>
                        <div class="col-9">
                            <textarea name="general_observations" id="general_observations" cols="30" rows="3" class="form-control @error('general_observations') is-invalid @enderror" placeholder="Digite observaciones generales...">{{ isset($tracking->general_observations) ? $tracking->general_observations : old('general_observations') }}</textarea>
                            <div class="invalid-feedback">
                                @error('general_observations')
                                    <span>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    </div>
