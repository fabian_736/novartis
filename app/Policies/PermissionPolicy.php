<?php

namespace App\Policies;

use App\Models\Admin\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear permisos', 'actualizar permisos', 'eliminar permisos', 'restaurar permisos', 'eliminar permisos permanentemente'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear permisos')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, Permission $permission)
    {
        if ($user->hasPermissionTo('actualizar permisos')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function delete(User $user, Permission $permission)
    {
        if ($user->hasPermissionTo('eliminar permisos')) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function restore(User $user, Permission $permission)
    {
        if (! $permission->trashed()) {
            return false;
        }

        if ($user->hasPermissionTo('restaurar permisos')) {
            return true;
        }
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Permission  $permission
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function forceDelete(User $user, Permission $permission)
    {
        if (! $permission->trashed()) {
            return false;
        }

        if ($user->hasPermissionTo('eliminar permisos permanentemente')) {
            return true;
        }
    }
}
