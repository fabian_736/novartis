<?php

namespace App\Enums\Patient;

enum PatientStatus: string
{
    case InAccess = 'En acceso';
    case Active = 'Activo';
    case Inactive = 'Inactivo';
    case OutsideOfProgram = 'Fuera del programa';
    case Suspended = 'Suspendido';
}
