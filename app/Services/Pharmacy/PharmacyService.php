<?php

namespace App\Services\Pharmacy;

use App\Models\Pharmacy;

class PharmacyService
{
    /**
     * Returns a list of pharmacies.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Pharmacy>
     */
    public function list($onlyActive = true)
    {
        $pharmacies = Pharmacy::query();

        if ($onlyActive) {
            $pharmacies->active();
        }

        return $pharmacies->get();
    }

    /**
     * Create a Pharmacy.
     *
     * @param  array  $input
     * @return Pharmacy
     */
    public function store(array $input): Pharmacy
    {
        return Pharmacy::create($input);
    }

    public function update(Pharmacy $pharmacy, array $input): bool
    {
        return $pharmacy->update($input);
    }
}
