<?php

namespace App\Http\Controllers;

use App\Http\Requests\PharmacyChain\StoreRequest;
use App\Http\Requests\PharmacyChain\UpdateRequest;
use App\Http\Resources\PharmacyChainResource;
use App\Models\Country;
use App\Models\PharmacyChain;
use App\Services\Pharmacy\PharmacyChainService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PharmacyChainController extends Controller
{
    use ApiResponses;

    /** @var PharmacyChainService */
    private $pharmacyChainService;

    public function __construct(PharmacyChainService $pharmacyChainService)
    {
        $this->pharmacyChainService = $pharmacyChainService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], PharmacyChain::class);

        $countries = Country::get();
        $pharmacyChains = $this->pharmacyChainService->list(false);

        return view('supervisor.pharmacy_chain.list', compact('countries', 'pharmacyChains'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', PharmacyChain::class);
            $pharmacyChain = $this->pharmacyChainService->store($request->validated());

            return $this->resourceResponse(
                message: 'Cadena creada correctamente.',
                data: new PharmacyChainResource($pharmacyChain),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  PharmacyChain  $pharmacyChain
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, PharmacyChain $pharmacyChain)
    {
        if ($request->ajax()) {
            $this->authorize('update', $pharmacyChain);

            return $this->resourceResponse(
                message: '',
                data: new PharmacyChainResource($pharmacyChain)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  PharmacyChain  $pharmacyChain
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, PharmacyChain $pharmacyChain)
    {
        if ($request->ajax()) {
            $this->authorize('update', $pharmacyChain);
            $this->pharmacyChainService->update($pharmacyChain, $request->validated());
            $pharmacyChain->refresh();

            return $this->resourceResponse(
                message: 'Cadena actualizada correctamente.',
                data: new PharmacyChainResource($pharmacyChain)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
