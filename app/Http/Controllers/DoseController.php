<?php

namespace App\Http\Controllers;

use App\Http\Requests\Dose\StoreRequest;
use App\Http\Requests\Dose\UpdateRequest;
use App\Http\Resources\Dose\DoseResource;
use App\Models\Dose;
use App\Services\Dose\DoseService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DoseController extends Controller
{
    use ApiResponses;

    /** @var DoseService */
    private $doseService;

    public function __construct(DoseService $doseService)
    {
        $this->doseService = $doseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Dose::class);

        $doses = $this->doseService->list(false);

        return view('supervisor.dose.list', compact('doses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Dose::class);
            $dose = $this->doseService->store($request->validated());

            return $this->resourceResponse(
                message: 'Dosis creada correctamente.',
                data: new DoseResource($dose),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Dose  $dose
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Dose $dose)
    {
        if ($request->ajax()) {
            $this->authorize('update', $dose);

            return $this->resourceResponse(
                message: '',
                data: new DoseResource($dose)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Dose  $dose
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Dose $dose)
    {
        if ($request->ajax()) {
            $this->authorize('update', $dose);
            $this->doseService->update($dose, $request->validated());
            $dose->refresh();

            return $this->resourceResponse(
                message: 'Dosis actualizada correctamente.',
                data: new DoseResource($dose)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
