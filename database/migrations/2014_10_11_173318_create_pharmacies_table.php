<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacies', function (Blueprint $table) {
            $table->id();
            $table->string('document_number');
            $table->string('name');
            $table->string('address');
            $table->boolean('is_active')->default(1)->comment('1.Activo | 0.Inactivo');

            //Llaves foraneas
            $table->foreignId('city_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->foreignId('pharmacy_chain_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->foreignId('document_type_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacies');
    }
};
