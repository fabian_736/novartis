<?php

namespace Database\Seeders;

use App\Models\Admin\Permission;
use App\Models\Admin\Role;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        $permissions = [
            ['guard_name' => 'web', 'name' => 'crear usuarios'],
            ['guard_name' => 'web', 'name' => 'actualizar usuarios'],
            ['guard_name' => 'web', 'name' => 'ver paciente'],
            ['guard_name' => 'web', 'name' => 'crear pacientes'],
            ['guard_name' => 'web', 'name' => 'actualizar pacientes'],
            ['guard_name' => 'web', 'name' => 'eliminar pacientes'],
            ['guard_name' => 'web', 'name' => 'restaurar pacientes'],
            ['guard_name' => 'web', 'name' => 'crear roles'],
            ['guard_name' => 'web', 'name' => 'actualizar roles'],
            ['guard_name' => 'web', 'name' => 'eliminar roles'],
            ['guard_name' => 'web', 'name' => 'restaurar roles'],
            ['guard_name' => 'web', 'name' => 'eliminar roles permanentemente'],
            ['guard_name' => 'web', 'name' => 'crear permisos'],
            ['guard_name' => 'web', 'name' => 'actualizar permisos'],
            ['guard_name' => 'web', 'name' => 'eliminar permisos'],
            ['guard_name' => 'web', 'name' => 'restaurar permisos'],
            ['guard_name' => 'web', 'name' => 'eliminar permisos permanentemente'],
            ['guard_name' => 'web', 'name' => 'asignar permisos a roles'],
            // ['guard_name' => 'web', 'name' => 'asignar roles y permisos a usuarios'],
            ['guard_name' => 'web', 'name' => 'crear farmacias'],
            ['guard_name' => 'web', 'name' => 'actualizar farmacias'],
            ['guard_name' => 'web', 'name' => 'crear cadenas'],
            ['guard_name' => 'web', 'name' => 'actualizar cadenas'],
            ['guard_name' => 'web', 'name' => 'crear paises'],
            ['guard_name' => 'web', 'name' => 'actualizar paises'],
            ['guard_name' => 'web', 'name' => 'crear ciudades'],
            ['guard_name' => 'web', 'name' => 'actualizar ciudades'],
            ['guard_name' => 'web', 'name' => 'crear especialidades'],
            ['guard_name' => 'web', 'name' => 'actualizar especialidades'],
            ['guard_name' => 'web', 'name' => 'crear doctores'],
            ['guard_name' => 'web', 'name' => 'actualizar doctores'],
            ['guard_name' => 'web', 'name' => 'crear patologias'],
            ['guard_name' => 'web', 'name' => 'actualizar patologias'],
            ['guard_name' => 'web', 'name' => 'crear medicinas'],
            ['guard_name' => 'web', 'name' => 'actualizar medicinas'],
            ['guard_name' => 'web', 'name' => 'crear hospitales'],
            ['guard_name' => 'web', 'name' => 'actualizar hospitales'],
            ['guard_name' => 'web', 'name' => 'crear aseguradoras'],
            ['guard_name' => 'web', 'name' => 'actualizar aseguradoras'],
            ['guard_name' => 'web', 'name' => 'crear seguimientos'],
            ['guard_name' => 'web', 'name' => 'actualizar seguimientos'],
            ['guard_name' => 'web', 'name' => 'crear reembolsos'],
            ['guard_name' => 'web', 'name' => 'actualizar reembolsos'],
            ['guard_name' => 'web', 'name' => 'crear compras'],
            ['guard_name' => 'web', 'name' => 'actualizar compras'],
            ['guard_name' => 'web', 'name' => 'crear cartas'],
            ['guard_name' => 'web', 'name' => 'actualizar cartas'],
            ['guard_name' => 'web', 'name' => 'crear beneficios'],
            ['guard_name' => 'web', 'name' => 'actualizar beneficios'],
            ['guard_name' => 'web', 'name' => 'cerrar beneficios'],
            ['guard_name' => 'web', 'name' => 'corregir beneficios'],
            ['guard_name' => 'web', 'name' => 'eliminar beneficios'],
            ['guard_name' => 'web', 'name' => 'restaurar beneficios'],
            ['guard_name' => 'web', 'name' => 'crear pruebas'],
            ['guard_name' => 'web', 'name' => 'actualizar pruebas'],
            ['guard_name' => 'web', 'name' => 'crear dosis'],
            ['guard_name' => 'web', 'name' => 'actualizar dosis'],
            ['guard_name' => 'web', 'name' => 'crear laboratorios'],
            ['guard_name' => 'web', 'name' => 'actualizar laboratorios'],
            ['guard_name' => 'web', 'name' => 'ver reportes'],
        ];

        Permission::insert($permissions);

        // Create roles and assign existing permissions
        $role1 = Role::create(['name' => 'Administrador']);
        $role1->givePermissionTo([
            'crear roles',
            'actualizar roles',
            'eliminar roles',
            'eliminar roles permanentemente',
            'crear permisos',
            'actualizar permisos',
            'eliminar permisos',
            'eliminar permisos permanentemente',
            'asignar permisos a roles',
            // 'asignar roles y permisos a usuarios',
            'crear usuarios',
            'actualizar usuarios',
            'ver paciente',
            'crear pacientes',
            'actualizar pacientes',
            'crear pruebas',
            'actualizar pruebas',
            'crear beneficios',
            'actualizar beneficios',
            'cerrar beneficios',
            'corregir beneficios',
            'crear cartas',
            'actualizar cartas',
            'crear compras',
            'actualizar compras',
            'ver reportes',
            'crear farmacias',
            'actualizar farmacias',
            'crear cadenas',
            'actualizar cadenas',
            'crear especialidades',
            'actualizar especialidades',
            'crear doctores',
            'actualizar doctores',
            'crear paises',
            'actualizar paises',
            'crear ciudades',
            'actualizar ciudades',
            'crear aseguradoras',
            'actualizar aseguradoras',
            'crear hospitales',
            'actualizar hospitales',
            'crear patologias',
            'actualizar patologias',
            'crear medicinas',
            'actualizar medicinas',
            'crear dosis',
            'actualizar dosis',
            'crear laboratorios',
            'actualizar laboratorios',
        ]);

        $role2 = Role::create(['name' => 'Farmacia']);
        $role2->givePermissionTo([
            'crear beneficios',
            'actualizar beneficios',
            'crear compras',
            'crear cartas',
            'actualizar cartas',
        ]);

        $role3 = Role::create(['name' => 'Novartis']);
        $role3->givePermissionTo([
            'ver reportes',
        ]);
    }
}
