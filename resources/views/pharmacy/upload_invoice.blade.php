@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h2">{{ $benefit->patient->name }}</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse mt-5 table" style="overflow-y: auto; max-height: 80vh">
        <div class="col">
            <form enctype="multipart/form-data" action="{{ route('benefits.patient.update', $benefit->id) }}" method="post">
                @csrf
                @method('PATCH')
                <ul class="li_form">
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Número de afiliación</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" placeholder="PAP" value="{{ $benefit->patient->pap }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold">Número de contacto</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control" placeholder="+51 32324321" value="{{ $benefit->patient->phone }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Correo electrónico</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="email" name="" id="" class="form-control" placeholder="example@example.com" value="{{ $benefit->patient->email }}" disabled>
                                        <input type="hidden" id="country" value="{{ $benefit->patient->country_id }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Producto</label>
                                    </div>
                                    <div class="col">
                                        <input disabled type="text" class="form-control" value="{{ $benefit->patient->treatment->pathology->medicine->name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Patología / Diagnóstico</label>
                                    </div>
                                    <div class="col">
                                        <input disabled type="text" value="{{ $benefit->patient->treatment->pathology->name }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Dosis</label>
                                    </div>
                                    <div class="col">
                                        <input disabled type="text" value="{{ $benefit->patient->treatment->currentFormulation->pathologyHasMedicineDose->medicineDose->dose->presentation }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Frecuencia</label>
                                    </div>
                                    <div class="col">
                                        <input disabled type="text" value="{{ $benefit->patient->treatment->currentFormulation->frequency }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Tipo de beneficio</label>
                                    </div>
                                    <div class="col">
                                        <input disabled type="text" value="{{ $benefit->benefit_type }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Estado del tipo beneficio</label>
                                    </div>
                                    <div class="col">
                                        @if ($benefit->benefit_type_status == 0)
                                            <input disabled type="text" value="Abierto" class="form-control">
                                        @elseif($benefit->benefit_type_status == 1)
                                            <input disabled type="text" value="Pagado" class="form-control">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Cantidad del producto</label>
                                    </div>
                                    <div class="col">
                                        <input placeholder="Digite cantidad" class="form-control" type="number" name="number_units" id="number_units" value="{{ isset($benefit->number_units) ? $benefit->number_units : '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="pharmacy_id" class="h5 font-weight-bold ">Farmacia</label>
                                    </div>
                                    <div class="col">
                                        <select name="pharmacy_id" id="pharmacy_id" class="form-control" required>
                                            <option value="" selected disabled>Seleccione una farmacia...</option>
                                            @foreach ($pharmacies as $pharmacy)
                                                <option value="{{ $pharmacy->id }}" @selected($benefit->pharmacy_id == $pharmacy->id)>
                                                    {{ $pharmacy->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Descuento otorgado en valores</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" id="discount" name="discount" value="{{ isset($benefit->discount) ? $benefit->discount : '' }}" placeholder="Digite descuento" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Fecha de transacción</label>
                                    </div>
                                    <div class="col">
                                        <input min="2022-01-01" max="{{ now()->toDateString() }}" type="date" value="{{ isset($benefit->transaction_date) ? $benefit->transaction_date : '' }}" name="transaction_date" id="transaction_date" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Orden de remisión</label>
                                    </div>
                                    <div class="col">
                                        <input value="{{ isset($benefit->invoice_number) ? $benefit->invoice_number : '' }}" type="text" id="invoice_number" name="invoice_number" placeholder="Codigo" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold ">Fotografía de factura / orden de remisión*</label>
                                    </div>
                                    <div class="col-9">
                                        <div class="custom-file border border-secondary rounded ">
                                            <input accept=".pdf,image/*" name="image_invoice" type="file" class="custom-file-input cursor-pointer" id="factura">
                                            <label class="custom-file-label" for="customFileLang">Seleccione un archivo
                                            </label>
                                        </div>
                                        <label id="archivo1" for="factura"></label><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="list-style: none">
                        <div class="row mb-3">
                            <div class="col offset-6">
                                <div class="row">
                                    <div class="col d-flex justify-content-end">
                                        <a href="{{ route('benefits.benefits.index') }}" class="btn btn-orange"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick-fill"></span>
                                            Salir</a>
                                    </div>
                                    <div class="col d-flex justify-content-end">
                                        <button type="submit" class="btn btn-dark w-100"><span class="iconify mr-2" data-icon="entypo:save"></span>GUARDAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
    </div>

    <script>
        $(document).on('change', 'input[type="file"]', function() {
            var fileName = this.files[0].name;
            var fileSize = this.files[0].size;

            if (fileSize > 70 * 1024 * 1024) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'El archivo no debe superar los 3MB',
                    showConfirmButton: false,
                    timer: 1500
                })
                this.value = '';
                this.files[0].name = '';
            } else {
                // recuperamos la extensión del archivo
                var ext = fileName.split('.').pop();
                ext = ext.toLowerCase();

                switch (ext) {
                    case 'png':
                    case 'jpg':
                    case 'jpeg':
                    case 'pdf':
                        break;
                    default:
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'El archivo no tiene la extensión adecuada',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        this.value = ''; // reset del valor
                        this.files[0].name = '';
                }
            }
            Swal.fire({
                icon: 'success',
                title: 'Archivo cargado con exito',
                showConfirmButton: true,
                timer: 1500
            })
            document.getElementById('archivo1').innerHTML = document.getElementById('factura').files[0].name;
        });
    </script>

    @foreach ($errors->all() as $error)
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: '{{ $error }}',
            })
        </script>
    @endforeach
@endsection
