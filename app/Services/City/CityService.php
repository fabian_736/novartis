<?php

namespace App\Services\City;

use App\Models\City;

class CityService
{
    /**
     * Returns a list of cities.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, City>
     */
    public function list($onlyActive = true)
    {
        $cities = City::query();

        if ($onlyActive) {
            $cities->active();
        }

        return $cities->get();
    }

    /**
     * Create a City.
     *
     * @param  array  $input
     * @return City
     */
    public function store(array $input): City
    {
        return City::create($input);
    }

    public function update(City $city, array $input): bool
    {
        return $city->update($input);
    }
}
