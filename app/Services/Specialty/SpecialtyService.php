<?php

namespace App\Services\Specialty;

use App\Models\Specialty;

class SpecialtyService
{
    /**
     * Returns a list of specialties.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Specialty>
     */
    public function list($onlyActive = true)
    {
        $specialties = Specialty::query();

        if ($onlyActive) {
            $specialties->active();
        }

        return $specialties->get();
    }

    /**
     * Create a Specialty.
     *
     * @param  array  $input
     * @return Specialty
     */
    public function store(array $input): Specialty
    {
        return Specialty::create($input);
    }

    public function update(Specialty $specialty, array $input): bool
    {
        return $specialty->update($input);
    }
}
