//al enviar el formulario valida los ultimos campos
$("#submit_end").click(function () {
    var btn = document.getElementById("button3");
    var btn2 = document.getElementById("button4");
    var relationship_patient = $("#relationship_patient").val();
    var carer_document_type_id = $("#carer_document_type_id").val();
    var carer_document_number = document.getElementById(
        "carer_document_number"
    ).value;
    var carer_name = document.getElementById("carer_name").value;
    var carer_phone = document.getElementById("carer_phone").value;
    var firm = $("#firm").val();
    var has_carer = $("#button3").val();

 
    if (btn.checked) {
        if (relationship_patient <= 0 || relationship_patient == "") {
            $("#relationship_patient_validate").show();
            $("#relationship_patient_validate").html(
                "<i class='fas fa-times mr-2 '></i>Por favor seleccione una opcion"
            );
            $("#relationship_patient")
                .addClass("is-invalid")
                .removeClass("is-valid");
        } else {
            $("#relationship_patient")
                .addClass("is-valid")
                .removeClass("is-invalid");
            $("#relationship_patient_validate").hide();
        }

        if (carer_document_type_id <= 0 || carer_document_type_id == "") {
            $("#carer_document_type_id_validate").show();
            $("#carer_document_type_id_validate").html(
                "<i class='fas fa-times mr-2 '></i>Por favor seleccione una opcion"
            );
            $("#carer_document_type_id")
                .addClass("is-invalid")
                .removeClass("is-valid");
        } else {
            $("#carer_document_type_id")
                .addClass("is-valid")
                .removeClass("is-invalid");
            $("#carer_document_type_id_validate").hide();
        }

        if (carer_document_number <= 0 || carer_document_number == "") {
            $("#carer_document_number")
                .addClass("is-invalid")
                .removeClass("is-valid");
        } else {
            $("#carer_document_number")
                .addClass("is-valid")
                .removeClass("is-invalid");
        }

        if (carer_name <= 0 || carer_name == "") {
            $("#carer_name_validate").show();
            $("#carer_name_validate").html(
                "<i class='fas fa-times mr-2 '></i>Por favor digite su nombre completo"
            );
            $("#carer_name").addClass("is-invalid").removeClass("is-valid");
        } else {
            $("#carer_name").addClass("is-valid").removeClass("is-invalid");

            $("#carer_name_validate").hide();
        }

        if (carer_phone <= 0 || carer_phone == "") {
            $("#carer_phone").addClass("is-invalid").removeClass("is-valid");
            $("#carer_phone_validate").show();
            $("#carer_phone_validate").html(
                "<i class='fas fa-times mr-2 '></i>Por favor digite el numero telefonico"
            );
        } else {
            $("#carer_phone").addClass("is-valid").removeClass("is-invalid");
            $("#carer_phone_validate").hide();
        }

        // if (carer_email <= 0 || carer_email == "") {
        //     $("#carer_email").addClass("is-invalid").removeClass("is-valid");
        //     $("#carer_email_validate").show();
        //     $("#carer_email_validate").html(
        //         "<i class='fas fa-times mr-2 '></i>Por favor digite un correo electronico"
        //     );
        // } else {
        //     $("#carer_email").addClass("is-valid").removeClass("is-invalid");
        // }

    }

    if (btn2.checked) {
        if (firm <= 0 || firm == "") {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "!La firma es obligatoria!",
                showConfirmButton: false,
                timer: 1500,
            });
        } else {
            $('#submit_end').click(function(){
                $('#f1').submit();
                $("#loading_spinner").modal("show");
            })
        }

    }

    if(btn.checked == false && btn2.checked == false && firm <= 0 || firm == ""){
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "¡Los campos son obligatorios!",
            showConfirmButton: false,
            timer: 1500,
        });
    }else {
        $('#submit_end').click(function(){
            $('#f1').submit();
           
        })
    }

    
});
