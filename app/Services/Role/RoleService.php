<?php

namespace App\Services\Role;

use App\Models\Admin\Role;

class RoleService
{
    /**
     * Returns a list of roles.
     *
     * @param  bool  $withTrashed
     * @return \Illuminate\Support\Collection<int, Role>
     */
    public function list($withTrashed = false)
    {
        if ($withTrashed) {
            return Role::withTrashed()->get();
        } else {
            return Role::get();
        }
    }

    /**
     * Return to paginated roles.
     *
     * @param  int  $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15)
    {
        return Role::paginate($perPage);
    }

    /**
     * Create a Role.
     *
     * @param  array  $input
     * @return Role
     */
    public function store(array $input): Role
    {
        return Role::create($input);
    }

    public function update(Role $role, array $input): bool
    {
        return $role->update($input);
    }

    public function delete(Role $role): ?bool
    {
        return $role->delete();
    }

    public function restore(Role $role): ?bool
    {
        return $role->restore();
    }

    public function forceDelete(Role $role): ?bool
    {
        return $role->forceDelete();
    }
}
