<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiagnosticTest\StoreRequest;
use App\Http\Requests\DiagnosticTest\UpdateRequest;
use App\Http\Resources\DiagnosticTest\DiagnosticTestResource;
use App\Models\DiagnosticTest;
use App\Services\DiagnosticTest\DiagnosticTestService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DiagnosticTestController extends Controller
{
    use ApiResponses;

    /** @var DiagnosticTestService */
    private $service;

    public function __construct(DiagnosticTestService $diagnosticTestService)
    {
        $this->service = $diagnosticTestService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], DiagnosticTest::class);

        $diagnosticTests = $this->service->list(false);

        return view('supervisor.diagnostic_test.list', compact('diagnosticTests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', DiagnosticTest::class);
            $diagnosticTest = $this->service->store($request->validated());

            return $this->resourceResponse(
                message: 'Prueba creada correctamente.',
                data: new DiagnosticTestResource($diagnosticTest),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  DiagnosticTest  $diagnosticTest
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, DiagnosticTest $diagnosticTest)
    {
        if ($request->ajax()) {
            $this->authorize('update', $diagnosticTest);

            return $this->resourceResponse(
                message: '',
                data: new DiagnosticTestResource($diagnosticTest)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  DiagnosticTest  $diagnosticTest
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, DiagnosticTest $diagnosticTest)
    {
        if ($request->ajax()) {
            $this->authorize('update', $diagnosticTest);
            $this->service->update($diagnosticTest, $request->validated());
            $diagnosticTest->refresh();

            return $this->resourceResponse(
                message: 'Prueba actualizada correctamente.',
                data: new DiagnosticTestResource($diagnosticTest)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
