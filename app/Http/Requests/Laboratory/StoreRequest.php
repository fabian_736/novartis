<?php

namespace App\Http\Requests\Laboratory;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'document_type_id' => 'required',
            'document_number' => ['required', 'unique:laboratories,document_number', 'digits_between:5,16'],
            'name' => ['required', 'string', 'between:4,100', 'regex:/^[a-záéíóúüñ ]+/i'],
            'address' => ['required', 'string', 'between:16,255'],
            'country' => ['required', 'exists:countries,id'],
            'city_id' => ['required', Rule::exists('cities', 'id')->where(function ($query) {
                return $query->where('country_id', $this->input('country'));
            })],
        ];
    }

    public function attributes()
    {
        return [
            'document_type_id' => 'Tipo de cocumento',
            'document_number' => 'Número de documento',
            'name' => 'Nombre',
            'address' => 'Dirección',
            'country' => 'País',
            'city_id' => 'Ciudad',
        ];
    }
}
