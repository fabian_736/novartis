<?php

namespace App\Models;

use App\Enums\Patient\PatientStatus;
use App\Enums\Patient\PatientTypes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperPatient
 */
class Patient extends Model implements AuditableContract, HasMedia
{
    use AuditableTrait, HasFactory, InteractsWithMedia, SoftDeletes;

    const CREATED_AT = 'registered_at';

    protected $fillable = [
        'document_type_id',
        'document_number',
        'name',
        'birth_date',
        'gender',
        'city_id',
        'email',
        'phone',
        'patient_type',
        'insurance_carrier_id',
        'hospital_id',
        'first_time_using_biological',
        'has_carer',
        'relationship_patient',
        'carer_document_type_id',
        'carer_document_number',
        'carer_name',
        'carer_birth_date',
        'carer_phone',
        'carer_email',
        'pap',
        'patient_status',
        'inactivated_at',
        'suspended_at',
        'suspension_reason',
        'reactivated_at',
        'outside_of_program_since',
        'date_admission',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'patient_type' => PatientTypes::class,
        'patient_status' => PatientStatus::class,
        'inactivated_at' => 'immutable_datetime',
        'suspended_at' => 'immutable_datetime',
        'reactivated_at' => 'immutable_datetime',
        'outside_of_program_since' => 'immutable_datetime',
    ];

    /**
     * Scope a query to only include active cities.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeCanGetDrugDiscountBenefit($query)
    {
        return $query->where('patient_status', '!=', PatientStatus::OutsideOfProgram);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('withinTheProgram', function (Builder $builder) {
            $builder->where('patient_status', '!=', PatientStatus::OutsideOfProgram);
        });

        static::created(function ($patient) {
            $patient->pap = 'PAP'.str_pad($patient->id, STR_PAD_LEFT);
            $patient->saveQuietly();
        });
    }

    /**
     * Retrieves the document type.
     *
     * @return BelongsTo<DocumentType, Patient>
     */
    public function documentType(): BelongsTo
    {
        return $this->belongsTo(DocumentType::class);
    }

    /**
     * Retrieves the city.
     *
     * @return BelongsTo<City, Patient>
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Retrieves the insurance carrier.
     *
     * @return BelongsTo<InsuranceCarrier, Patient>
     */
    public function insuranceCarrier(): BelongsTo
    {
        return $this->belongsTo(InsuranceCarrier::class);
    }

    /**
     * Retrieves the hospital.
     *
     * @return BelongsTo<Hospital, Patient>
     */
    public function hospital(): BelongsTo
    {
        return $this->belongsTo(Hospital::class);
    }

    /**
     * Obtiene un Tratamientos asociados al Paciente.
     *
     * @return HasOne<Treatment>
     */
    public function treatment(): HasOne
    {
        return $this->hasOne(Treatment::class);
    }

    /**
     * Obtiene a las Pruebas diagnosticas asociados al Paciente.
     *
     * @return HasMany<ManagementDiagnosticTest>
     */
    public function managementDiagnosticTests(): HasMany
    {
        return $this->hasMany(ManagementDiagnosticTest::class);
    }

    /**
     * Obtiene a los Seguimientos asociados al Paciente.
     *
     * @return HasMany<Tracking>
     */
    public function trakings(): HasMany
    {
        return $this->hasMany(Tracking::class);
    }

    /**
     * Obtiene el último beneficio asociada al paciente.
     *
     * @return HasOne<Benefit>
     */
    public function lastBenefit(): HasOne
    {
        return $this->hasOne(Benefit::class)->latestOfMany();
    }

    /**
     * Obtiene a los Beneficios asociados al Paciente.
     *
     * @return HasMany<Benefit>
     */
    public function benefits(): HasMany
    {
        return $this->hasMany(Benefit::class);
    }

    /**
     * Register the model's media collections.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('consent')
            ->singleFile();

        $this->addMediaCollection('file-upload')
            ->singleFile();
    }

    /**
     * Gets and sets the patient name.
     *
     * @return Attribute<string, string>
     */
    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ucwords(strtolower($value)),
            set: fn ($value) => ucwords(strtolower($value)),
        );
    }

    /**
     * Gets and sets the carer name.
     *
     * @return Attribute<string, string>
     */
    protected function carerName(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ucwords(strtolower($value)),
            set: fn ($value) => ucwords(strtolower($value)),
        );
    }
}
