<?php

namespace App\Models;

use App\Traits\SoftDeletesParent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperTracking
 */
class Tracking extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory, SoftDeletes, SoftDeletesParent;

    protected $fillable = [
        'date_initial_call',
        'effective_initial_visit',
        'claimed_medication',
        'claim_date',
        'medicine_up',
        'answered_call',
        'who_answered_call',
        'call_reason',
        'cause_of_no_claim',
        'contact_medium',
        'type_call',
        'number_of_attemps',
        'insurance_carrier',
        'hospital',
        'delivery_point',
        'delivery_city',
        'authorization_number',
        'authorization_date',
        'difficulty_access',
        'difficulty_type',
        'generate_request',
        'adverse_event',
        'event_type',
        'date_of_next_call',
        'next_call_reason',
        'next_call_observations',
        'consecutive',
        'number_of_boxes',
        'units',
        'previous_treatment',
        'medicine',
        'treatment_dose',
        'general_observations',
        'user_id',
        'patient_id',
    ];

    /**
     * Retrieves the patient.
     *
     * @return BelongsTo<Patient, Tracking>
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * Retrieves the user.
     *
     * @return BelongsTo<User, Tracking>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
