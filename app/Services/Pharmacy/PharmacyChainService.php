<?php

namespace App\Services\Pharmacy;

use App\Models\PharmacyChain;

class PharmacyChainService
{
    /**
     * Returns a list of pharmacy chains.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, PharmacyChain>
     */
    public function list($onlyActive = true)
    {
        $pharmacyChains = PharmacyChain::query();

        if ($onlyActive) {
            $pharmacyChains->active();
        }

        return $pharmacyChains->get();
    }

    /**
     * Create a pharmacy chain.
     *
     * @param  array  $input
     * @return PharmacyChain
     */
    public function store(array $input): PharmacyChain
    {
        return PharmacyChain::create($input);
    }

    public function update(PharmacyChain $pharmacyChain, array $input): bool
    {
        return $pharmacyChain->update($input);
    }
}
