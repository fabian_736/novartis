<?php

namespace App\Enums\Benefit;

enum BenefitTypes: string
{
    case Copago = 'Copago';
    case OOP = 'OOP';
}
