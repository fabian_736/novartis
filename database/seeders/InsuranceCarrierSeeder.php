<?php

namespace Database\Seeders;

use App\Models\InsuranceCarrier;
use Illuminate\Database\Seeder;

class InsuranceCarrierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InsuranceCarrier::insert([
            ['id' => 1, 'name' => 'RIMAC SEGUROS', 'country_id' => 1],
            ['id' => 2, 'name' => 'PACIFICO SEGUROS', 'country_id' => 1],
            ['id' => 3, 'name' => 'SIN SEGURO', 'country_id' => 1],
            ['id' => 4, 'name' => 'Petroleos del Peru', 'country_id' => 1],
            ['id' => 5, 'name' => 'MAPFRE PERU', 'country_id' => 1],
            ['id' => 6, 'name' => 'LA POSITIVA', 'country_id' => 1],
            ['id' => 7, 'name' => 'SANITAS', 'country_id' => 1],
            ['id' => 8, 'name' => 'institución pública', 'country_id' => 1],
            ['id' => 9, 'name' => 'INDEFINIDO PERU', 'country_id' => 1],
            ['id' => 10, 'name' => 'INDEFINIDO ECUADOR', 'country_id' => 2],
        ]);
    }
}
