<?php

namespace App\Http\Requests\Tracking;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->validator);
        $validations = [
            'date_initial_call' => ['required', 'date'],
            'effective_initial_visit' => 'required',
            'claimed_medication' => ['required', 'boolean'],
            'answered_call' => ['required', 'boolean'],
            'contact_medium' => 'required',
            'type_call' => 'required',
            'number_of_attemps' => ['required', 'numeric', 'min:1'],
            'insurance_carrier' => 'required',
            // 'hospital' => ['required', 'exists:hospitals,id'],
            'hospital' => ['required'],
            'difficulty_access' => ['required', 'boolean'],
            'generate_request' => ['required', 'boolean'],
            'adverse_event' => ['required', 'boolean'],
            'date_of_next_call' => ['required', 'date'],
            'next_call_reason' => 'required',
            'next_call_observations' => ['required', 'string'],
            'consecutive' => 'required',
            'previous_treatment' => 'required',
            'treatment_dose' => ['required', 'string'],
            'general_observations' => ['required', 'string'],
            // 'user_id' => ['required', 'exists:users,id'],
            'patient_id' => ['required', 'exists:patients,id'],
        ];

        if ($this->claimed_medication == 1) {
            $validations['claim_date'] = ['required', 'date'];
            $validations['medicine_up'] = ['required', 'date'];
            $validations['delivery_point'] = 'required';
            // $validations['delivery_city'] = ['required', 'exists:cities,id'];
            $validations['delivery_city'] = ['required'];
            $validations['authorization_number'] = ['required', 'alpha_num'];
            $validations['authorization_date'] = ['required', 'date'];
            $validations['number_of_boxes'] = ['required', 'numeric', 'min:1'];
            $validations['units'] = 'required';
            // $validations['medicine'] = ['required', 'exists:medicines,id'];
            $validations['medicine'] = ['required'];
            $validations['cause_of_no_claim'] = 'exclude';
        } else {
            $validations['claim_date'] = 'exclude';
            $validations['medicine_up'] = 'exclude';
            $validations['delivery_point'] = 'exclude';
            $validations['delivery_city'] = 'exclude';
            $validations['authorization_number'] = 'exclude';
            $validations['authorization_date'] = 'exclude';
            $validations['number_of_boxes'] = 'exclude';
            $validations['units'] = 'exclude';
            $validations['medicine'] = 'exclude';
            $validations['cause_of_no_claim'] = 'required';
        }

        if ($this->answered_call == 1) {
            $validations['who_answered_call'] = ['required', 'string'];
            $validations['call_reason'] = 'required';
        } else {
            $validations['who_answered_call'] = 'exclude';
            $validations['call_reason'] = 'exclude';
        }

        if ($this->adverse_event == 1) {
            $validations['event_type'] = 'required';
        } else {
            $validations['event_type'] = 'exclude';
        }

        if ($this->difficulty_access == 1) {
            $validations['difficulty_type'] = ['required', 'string'];
        } else {
            $validations['difficulty_type'] = 'exclude';
        }

        return $validations;
    }
}
