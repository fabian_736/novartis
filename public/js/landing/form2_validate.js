// //Valida los campos de la segunda seccion
$("#bottom_two").click(function () {
    var button1 = document.getElementById("button1");
    var button2 = document.getElementById("button2");
    var medicine = $("#medicine").val();
    var pathology = $("#pathology").val();
    var dose = $("#dose").val();
    const benefitType = document.getElementById("asegurado1");

    if (button1.checked == true && button2.checked == false) {
        $("#confirm_text_alert_1")
            .addClass("text-success")
            .removeClass("text-danger text-dark");
        $("#confirm_text_alert_2")
            .addClass("text-dark")
            .removeClass("text-danger text-success font-weight-bold");
        $("#message_asegurado").hide();
    }

    if (button1.checked == false && button2.checked == true) {
        $("#confirm_text_alert_1")
            .addClass("text-dark")
            .removeClass("text-danger text-success");
        $("#confirm_text_alert_2")
            .addClass("text-success font-weight-bold")
            .removeClass("text-danger text-dark");
        $("#message_asegurado").hide();
    }

    if (button1.checked == false && button2.checked == false) {
        $("#confirm_text_alert_1")
            .addClass("text-danger font-weight-bold")
            .removeClass("text-dark text-success");
        $("#confirm_text_alert_2")
            .addClass("text-danger font-weight-bold")
            .removeClass("text-dark text-success");
        $("#message_asegurado").removeClass();
    }

    if (medicine <= 0 || medicine == "") {
        $("#product_label")
            .addClass("text-danger font-weight-bold")
            .removeClass("text-danger text-success text-dark");
        $("#medicine").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#product_label")
            .addClass("text-success font-weight-bold")
            .removeClass("text-danger text-dark");
        $("#medicine").addClass("is-valid").removeClass("is-invalid");
    }

    if (pathology <= 0 || pathology == "") {
        $("#patology_label")
            .addClass("text-danger font-weight-bold")
            .removeClass("text-danger text-success text-dark");
        $("#pathology").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#patology_label")
            .addClass("text-success font-weight-bold")
            .removeClass("text-danger text-dark");
        $("#pathology").addClass("is-valid").removeClass("is-invalid");
    }

    if (dose <= 0 || dose == "") {
        $("#dose_label")
            .addClass("text-danger font-weight-bold")
            .removeClass("text-danger text-success text-dark");
        $("#dose").addClass("is-invalid").removeClass("is-valid");
    } else {
        $("#dose_label")
            .addClass("text-success font-weight-bold")
            .removeClass("text-danger text-dark");
        $("#dose").addClass("is-valid").removeClass("is-invalid");
    }

    if (
        benefitType.checked &&
        (medicine <= 0 || pathology <= 0 || dose <= 0)
    ) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "¡Completa todos los campos!",
            showConfirmButton: false,
            timer: 1500,
        });
    } else {
        $("#1").click(function () {
            $("#item_one").show("slow");
            $("#item_two").hide("slow");
            $("#item_three").hide("slow");

            $("#1").css("background-color", "#221F1F");
            $("#2").css("background-color", "#C4C4C4");
            $("#3").css("background-color", "#C4C4C4");
        });

        $("#2").click(function () {
            $("#item_one").hide("slow");
            $("#item_two").show("slow");
            $("#item_three").hide("slow");

            $("#1").css("background-color", "#221F1F");
            $("#2").css("background-color", "#221F1F");
            $("#3").css("background-color", "#C4C4C4");
        });

        $("#item_one").hide("slow");
        $("#item_two").hide("slow");
        $("#item_three").show("slow");

        $("#1").css("background-color", "#221F1F");
        $("#2").css("background-color", "#221F1F");
        $("#3").css("background-color", "#221F1F");
    }
});

$(document).on("change", 'input[type="file"]', function () {
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 100 * 1024 * 1024) {
        // alert('El archivo no debe superar los 100MB');
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "El archivo no debe superar los 100MB",
            showConfirmButton: false,
            timer: 1500,
        });
        this.value = "";
        this.files[0].name = "";
    } else {
        // recuperamos la extensión del archivo
        var ext = fileName.split(".").pop();
        ext = ext.toLowerCase();

        switch (ext) {
            case "png":
            case "jpg":
            case "jpeg":
            case "pdf":
                break;
            default: // reset del valor
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "El archivo no tiene la extensión adecuada",
                    showConfirmButton: false,
                    timer: 1500,
                });
                this.value = "";
                this.files[0].name = "";
        }
    }
    Swal.fire({
        icon: "success",
        title: "Archivo cargado con exito",
        showConfirmButton: false,
        timer: 1500,
    });
    document.getElementById("archivo1").innerHTML =
        document.getElementById("file-upload").files[0].name;
    document.getElementById("archivo2").innerHTML =
        document.getElementById("file-upload-2").files[0].name;
    document.getElementById("archivo3").innerHTML =
        document.getElementById("file-upload-3").files[0].name;
});

function clearData() {
    $("#medicine").val("");
    $("#pathology").val("");
    $("#dose").val("");
    $("#frequency").val("");
    $("#file-upload").parent().find(":input").val("");
    $("#file-upload-2").parent().find(":input").val("");
    $("#file-upload-3").parent().find(":input").val("");
}

$(document).ready(function () {
    $("#button1").change(function () {
        var btn1 = document.getElementById("button1");
        clearData();
        $("#title_options1").show();
        $("#title1_No_asegurado").hide();
        $("#equipoA").show();
        $("#asegurado1").val("Copago");
        $("#name_carrier").show();
        $("#content_hospital").hide();
        $("#insurance_carrier_id").val("");
        $("#first_time_using_biological").val("");
        $(".ocultar").hide("slow");
        $("#document").hide("slow");
        $("#button2").prop("checked", false);
        $("#asegurado1").prop("checked", false);
        $("#asegurado3").prop("checked", false);
        if (btn1.checked == false) {
            $("#message").show();
        } else {
            $("#message").hide();
        }
    });

    $("#button2").change(function () {
        var btn2 = document.getElementById("button2");
        clearData();
        $("#title_options1").hide();
        $("#title1_No_asegurado").show();
        $("#equipoA").show();
        $("#asegurado1").val("OOP");
        $("#hospital").val("");
        $(".ocultar").hide("slow");
        $("#document").hide("slow");
        $("#name_carrier").hide();
        $("#content_hospital").show();
        $("#button1").prop("checked", false);
        $("#asegurado1").prop("checked", false);
        $("#asegurado3").prop("checked", false);
        if (btn2.checked == false) {
            $("#message").show();
        } else {
            $("#message").hide();
        }
    });

    $("#asegurado3").click(function () {
        var a = document.getElementById("asegurado3");
        var b = document.getElementById("asegurado1");
        if (a.checked == true) {
            $("#document").show("slow");
        } else {
            $("#file-upload").parent().find(":input").val("");
            $("#file-upload-2").parent().find(":input").val("");
            $("#file-upload-3").parent().find(":input").val("");
            $("#document").hide("slow");
        }
        if (b.checked == true || a.checked == true) {
            $("#message2").hide();
        } else {
            $("#message2").show();
        }
    });

    $("#asegurado1").click(function () {
        var b = document.getElementById("asegurado1");
        var a = document.getElementById("asegurado3");
        if (b.checked == true) {
            $(".ocultar").show("slow");
        } else {
            $("#insurance_carrier_id").val("");
            $("#medicine").val("");
            $("#pathology").val("");
            $("#dose").val("");
            $("#frequency").val("");
            $("#first_time_using_biological").val("");
            $(".ocultar").hide("slow");
        }
        if (b.checked == true || a.checked == true) {
            $("#message2").hide();
        } else {
            $("#message2").show();
        }
    });
});
