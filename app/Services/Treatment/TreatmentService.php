<?php

namespace App\Services\Treatment;

use App\Models\Treatment;

class TreatmentService
{
    /**
     * Creates a new treatment.
     *
     * @param  array  $input
     * @return Treatment
     */
    public function store(array $input): Treatment
    {
        return Treatment::create($input);
    }

    public function update(Treatment $treatment, array $input): bool
    {
        return $treatment->update($input);
    }
}
