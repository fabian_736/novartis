<?php

namespace Database\Seeders;

use App\Models\Specialty;
use Illuminate\Database\Seeder;

class SpecialtySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Specialty::insert([
            ['id' => 1, 'name' => 'ONCOLOGIA'],
            ['id' => 2, 'name' => 'NEUMOLOGIA'],
            ['id' => 3, 'name' => 'REUMATOLOGIA'],
            ['id' => 4, 'name' => 'DERMATOLOGIA'],
            ['id' => 5, 'name' => 'CIRUGIA GENERAL'],
            ['id' => 6, 'name' => 'ALERGOLOGIA'],
            ['id' => 7, 'name' => 'NEFROLOGIA'],
            ['id' => 8, 'name' => 'PEDIATRIA'],
            ['id' => 9, 'name' => 'ENFERMERIA'],
            ['id' => 10, 'name' => 'MEDICINA INTERNA'],
            ['id' => 11, 'name' => 'MEDICINA GENERAL'],
            ['id' => 12, 'name' => 'FARMACOLOGIA'],
            ['id' => 13, 'name' => 'OTORRINOLARINGOLOGIA'],
            ['id' => 14, 'name' => 'GASTROENTEROLOGIA'],
            ['id' => 15, 'name' => 'ESPECIALIZACION POR DEFINIR'],
            ['id' => 16, 'name' => 'ALERGISTA'],
        ]);
    }
}
