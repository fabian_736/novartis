@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h2">Beneficios</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    <br>
    @if (session('message'))
        <script>
            Swal.fire(
                'Buen trabajo',
                'Los datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif
    <x-errores />
    <div class="col d-flex justify-content-end mt-3">
        <a href="{{ url('/portal') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
            <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
        </a>&nbsp;&nbsp;&nbsp;
        @can('crear beneficios', \App\Models\Benefit::class)
            <button type="button" class="btn bg-orange d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearBeneficio">
                <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>Crear beneficio
            </button>
        @endcan
    </div>
    <div class="row-reverse mt-5 table" style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-orange">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="h3 m-0 text-white">Listado de beneficios</label>
                    </div>
                    <div class="col-sm-6">
                        <form action="{{ route('benefits.benefits.index') }}" method="get">
                            <div class="input-group mb-3">
                                <input class="form-control bg-white mt-1 col-4" autocomplete="off" placeholder="Buscar por nombres" type="text" name="names" id="names">&nbsp;&nbsp;
                                <input class="form-control bg-white mt-1 col-4" autocomplete="off" placeholder="Buscar por documento" type="text" name="document" id="document">&nbsp;&nbsp;
                                <input class="form-control bg-white mt-1 col-3" autocomplete="off" placeholder="Buscar por # afiliación" type="text" name="PAP" id="PAP">&nbsp;&nbsp;
                                <button class="btn btn-success btn-sm " type="submit"><span class="iconify" data-icon="bx:search"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-0">
            <table class="table table-bordered table-striped" id="beneficios">
                <thead class="thead text-white font-weight-bold bg-orange">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Código de paciente</th>
                        <th scope="col">Paciente</th>
                        <th scope="col">Tipo de beneficio</th>
                        <th scope="col">Estado del beneficio</th>
                        <th scope="col">Motivo de cierre</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($benefits ?? [] as $benefit)
                        <tr>
                            <td>{{ $benefit->id }}</td>
                            <td>{{ $benefit->patient->pap }}</td>
                            <td>{{ $benefit->patient->name }}</td>
                            <td>{{ $benefit->benefit_type }}</td>
                            <td>{{ $benefit->status_decode }}</td>
                            @if ($benefit->closing_reason == null)
                                <td>El beneficio está abierto</td>
                            @else
                                <td>{{ $benefit->closing_reason }}</td>
                            @endif
                            @canany(['crear beneficios', 'actualizar beneficios', 'crear cartas', 'actualizar cartas', 'crear facturas', 'actualizar facturas', 'cerrar beneficios'], $benefits)
                                <td>
                                    <div class="dropdown col d-flex justify-content-center">
                                        <button class="btn btn-secondary dropdown-toggle bg-orange text-white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Acciones
                                        </button>
                                        @if ($benefit->benefit_type == 'OOP')
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                @can('cerrar beneficios', $benefit)
                                                    <a class="dropdown-item" href="{{ route('benefits.benefits.management', $benefit) }}" data-id="{{ $benefit->id }}">
                                                        <span class="iconify" data-icon="eos-icons:content-lifecycle-management"></span>&nbsp;
                                                        Gestionar beneficio
                                                    </a>
                                                @endcan
                                                @can('actualizar compras', $benefit)
                                                    <a class="dropdown-item" href="{{ route('benefits.upload_invoice', $benefit) }}" data-id="{{ $benefit->id }}">
                                                        <span class="iconify" data-icon="bi:card-checklist"></span>&nbsp;
                                                        Cargar factura
                                                    </a>
                                                @endcan
                                            </div>
                                        @else
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                @can('cerrar beneficios', $benefit)
                                                    <a class="dropdown-item" href="{{ route('benefits.benefits.management', $benefit) }}" data-id="{{ $benefit->id }}">
                                                        <span class="iconify" data-icon="eos-icons:content-lifecycle-management"></span>&nbsp;
                                                        Gestionar beneficio
                                                    </a>
                                                @endcan
                                                @can('actualizar cartas', $benefit)
                                                    <a class="dropdown-item edit-button" href="#" data-id="{{ $benefit->id }}">
                                                        <span class="iconify" data-icon="bi:card-list"></span>&nbsp;
                                                        Estado de carta de autorización
                                                    </a>
                                                @endcan
                                                @can('actualizar compras', $benefit)
                                                    <a class="dropdown-item" href="{{ route('benefits.upload_invoice', $benefit) }}" data-id="{{ $benefit->id }}">
                                                        <span class="iconify" data-icon="bi:card-checklist"></span>&nbsp;
                                                        Cargar factura
                                                    </a>
                                                @endcan
                                            </div>
                                        @endif
                                    </div>
                                </td>
                            @endcanany
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Código de paciente</th>
                        <th scope="col">Paciente</th>
                        <th scope="col">Tipo de beneficio</th>
                        <th scope="col">Estado del beneficio</th>
                        <th scope="col">Motivo de cierre</th>
                        <th scope="col"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


    {{-- Modal crear beneficios --}}
    <div class="modal fade" id="crearBeneficio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-orange p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear beneficio</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('benefits.new.benefits') }}" method="POST" id="create-benefit">
                                @csrf
                                <div class="col">
                                    <div class="row">
                                        <div class="col-4">
                                            <label for="patient_id" class="lead text-orange font-weight-bold">Paciente</label>
                                        </div>
                                        <div class="col">
                                            <select name="patient_id" id="patient_id" class="form-control" autocomplete="off" required>
                                                <option value="" selected disabled>Seleccione un paciente...</option>
                                                @foreach ($patients as $patient)
                                                    <option value="{{ $patient->id }}">{{ $patient->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Editar carta de autorización --}}
    <div class="modal fade" id="updateLetterAuth" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <form action="#" method="POST" id="update-letter-auth">
                        @csrf
                        <div class="row-reverse">
                            <div class="col mb-3">
                                <div class="card-header bg-orange">
                                    <div class="row">
                                        <div class="col">
                                            <label id="showName" for="" class="h4 m-0 text-white font-weight-bold"></label>
                                        </div>
                                        <div class="col d-flex justify-content-end align-items-center">
                                            <i class="fas fa-times text-white cursor-pointer " data-dismiss="modal"> </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col">
                                                <label for="" class="font-weight-bold text-orange">Autorización de carta co-pago</label>
                                                <select name="autorized" id="autorized" class="form-control autorized">
                                                    <option value="Pendiente">Pendiente</option>
                                                    <option value="Observada">Observada</option>
                                                    <option value="Aprobado">Aprobado</option>
                                                    <option value="Rechazado">Rechazado</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="descEspecial" style="display: none" class="col">
                                        <div class="row">
                                            <div class="col">
                                                <div class="row">
                                                    <div class="col">
                                                        <label for="" class="font-weight-bold text-orange">¿Acepta descuento especial?</label>
                                                        <select name="is_accepted" id="is_accepted" class="form-control">
                                                            <option value="" disabled selected>Seleccione una opción...</option>
                                                            <option value="0">No</option>
                                                            <option value="1">Sí</option>
                                                        </select>
                                                        <div class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="motiRechazo" style="display: none" class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="font-weight-bold text-orange">Motivo de rechazo</label>
                                        <select name="causal_types" id="causal_types" class="form-control">
                                            <option value="" selected disabled>Selecciona una opción...</option>
                                            <option title="Cuando tuvo la enfermedad antes de adquirir el seguro" value="Preexistencia">Preexistencia</option>
                                            <option title="Cuando no ha pasado el tiempo indicado en la póliza del paciente para adquirir medicamentos de alto costo" value="Periodo de latencia">Periodo de latencia</option>
                                            <option title="Cuando el plan contratado no incluye ningún medicamento biológico" value="Póliza no contempla biológicos">Póliza no contempla biológicos</option>
                                        </select>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-3 d-flex justify-content-center mt-5">
                                <button type="submit" class="btn btn-orange w-25 font-weight-bold"><i class="fas fa-save mr-2"></i>GUARDAR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };

        //Funcion modal crear beneficio
        const form = document.getElementById('create-benefit');

        const create = e => {
            e.preventDefault();
            const data = {
                patient_id: document.getElementById('patient_id').value,
            }
            axios.post('benefits-new', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message || error.response.data.error,
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        form.addEventListener('submit', create);
        //Fin función modal crear beneficio

        //Función modal editar estado de carta
        const updateLetterAuthForm = document.getElementById('update-letter-auth');

        const editLetterAuth = e => {
            e.preventDefault();

            axios.get(`/benefits/${e.target.closest('.edit-button').dataset.id}/letter-authorization`)
                .then((response) => {
                    const letter = response.data.data;

                    updateLetterAuthForm.dataset.id = letter.id;
                    updateLetterAuthForm.elements['autorized'].value = letter.autorized || "";
                    updateLetterAuthForm.elements['is_accepted'].value = letter.is_accepted || "";
                    updateLetterAuthForm.elements['causal_types'].value = letter.causal_types || "";
                    document.getElementById('showName').textContent = letter.patient.name || "";

                    Array.from(updateLetterAuthForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    showOptionsLetter();

                    $('#updateLetterAuth').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#221F1F',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editLetterAuth);
        });

        const updateLetterAuth = e => {
            e.preventDefault();

            const updateLetterAuthData = {
                autorized: document.getElementById('autorized').value,
                is_accepted: document.getElementById('is_accepted').value,
                causal_types: document.getElementById('causal_types').value,
                // _token: updateLetterAuthForm.elements["_token"].value,
            }
            axios.post(
                    `/benefits/${updateLetterAuthForm.dataset.id}/letter-authorization`,
                    updateLetterAuthData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updateLetterAuth').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(updateLetterAuthForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updateLetterAuthForm.elements[$key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updateLetterAuthForm.addEventListener('submit', updateLetterAuth);
        //Fin función modal editar estado de carta

        //Función borrar campos de busqueda
        $(document).ready(function() {
            $("#names").keyup(function() {
                $("#document").val("");
                $("#PAP").val("");
            })
            $("#document").keyup(function() {
                $("#names").val("");
                $("#PAP").val("");
            })
            $("#PAP").keyup(function() {
                $("#names").val("");
                $("#document").val("");
            })
        })

        // Muestra las opciones de las causales de rechazo
        $(document).ready(function() {
            $("#autorized").change(showOptionsLetter);
        })

        function showOptionsLetter() {
            var opt = $('#autorized').val();
            if (opt == "Rechazado") {
                $('#motiRechazo').show();
                $('#descEspecial').show();
            } else {
                $('#motiRechazo').hide();
                $('#descEspecial').hide();
            }
        }
    </script>
@endsection
