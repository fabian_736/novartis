<?php

namespace App\Http\Resources\User;

use App\Http\Resources\City\CityResource;
use App\Http\Resources\DocumentTypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\User
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>|\Illuminate\Contracts\Support\Arrayable<string, mixed>|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'document_type' => new DocumentTypeResource($this->whenLoaded('documentType')),
            'document_type_id' => $this->document_type_id,
            'document_number' => $this->document_number,
            'name' => $this->name,
            'phone' => $this->phone,
            'city' => new CityResource($this->whenLoaded('city')),
            'city_id' => $this->city_id,
            'address' => $this->address,
            'user_type' => $this->user_type,
            'email' => $this->email,
            'is_active' => $this->is_active,
            'roles' => $this->whenLoaded('roles'),
            'pharmacy_id' => $this->pharmacy_id,
            'image_user' => $this->getFirstMediaUrl('banner_user'),
            'is_admin' => $this->is_admin,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
