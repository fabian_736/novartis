<?php

namespace App\Traits;

use App\Scopes\SoftDeletesParentScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;

trait SoftDeletesParent
{
    /**
     * boots the SoftDeletesParent trait
     *
     * @return void
     */
    protected static function bootSoftDeletesParent()
    {
        static::addGlobalScope(new SoftDeletesParentScope());
    }

    /**
     * Register the model for the parent soft deletes
     *
     * @param  string  $parent
     * @return void
     */
    public static function softDeletesParent($parent)
    {
        Event::listen('eloquent.deleting: '.$parent, function (Model $model) {
            $models = static::query()->where([
                $model->getForeignKey() => $model->getKey(),
            ])->get();

            $models->each(function ($model) {
                $model->fireModelEvent('deleting', false);

                $query = $model->setKeysForSaveQuery($model->newModelQuery());

                $time = $model->freshTimestamp();

                $columns = ['parent_deleted_at' => $model->fromDateTime($time)];

                $model->parent_deleted_at = $time;

                if ($model->timestamps && ! is_null($model->getUpdatedAtColumn())) {
                    $model->{$model->getUpdatedAtColumn()} = $time;

                    $columns[$model->getUpdatedAtColumn()] = $model->fromDateTime($time);
                }

                $query->update($columns);

                $model->syncOriginalAttributes(array_keys($columns));

                $model->fireModelEvent('trashed', false);
            });
        });

        Event::listen('eloquent.restoring: '.$parent, function (Model $model) {
            $models = static::query()->withParentTrashed()->where([
                $model->getForeignKey() => $model->getKey(),
            ])->get();

            $models->each(function ($model) {
                $model->fireModelEvent('restoring', false);

                // If the restoring event does not return false, we will proceed with this
                // restore operation. Otherwise, we bail out so the developer will stop
                // the restore totally. We will clear the deleted timestamp and save.
                if ($model->fireModelEvent('restoring') === false) {
                    return false;
                }

                $model->parent_deleted_at = null;

                // Once we have saved the model, we will fire the "restored" event so this
                // developer will do anything they need to after a restore operation is
                // totally finished. Then we will return the result of the save call.
                $model->exists = true;

                $model->save();

                $model->fireModelEvent('restored', false);
            });
        });
    }

    /**
     * Scope the query to include the trashed parents models.
     *
     * @param  Builder<Model>  $query
     * @return Builder<Model>
     */
    public static function scopeWithParentTrashed(Builder $query)
    {
        return $query->withoutGlobalScope(new SoftDeletesParentScope());
    }

    /**
     * Scope the query to only include the trashed parents models.
     *
     * @param  Builder<Model>  $query
     * @return Builder<Model>
     */
    public static function scopeOnlyParentTrashed(Builder $query)
    {
        return $query->withoutGlobalScope(new SoftDeletesParentScope())
            ->whereNotNull('parent_deleted_at');
    }
}
