<?php

namespace App\Http\Controllers;

use App\Http\Requests\Permission\StoreRequest;
use App\Http\Requests\Permission\UpdateRequest;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\PermissionResourceCollection;
use App\Models\Admin\Permission;
use App\Models\Admin\Role;
use App\Services\Permission\PermissionService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionController extends Controller
{
    use ApiResponses;

    /** @var PermissionService */
    private $service;

    public function __construct(PermissionService $permissionService)
    {
        $this->service = $permissionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Permission::class);

        /** @var \App\Models\User */
        $user = auth()->user();

        $includeTrashed = $user->canAny(['eliminar permisos', 'eliminar permisos permanentemente']);

        $permissions = $this->service->list(withTrashed: $includeTrashed);

        // $permissions = $this->service->list();

        return view('supervisor.permissions.index', compact('permissions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function allPermissions(Role $role)
    {
        $role->load('permissions');

        $permissions = $this->service->list();

        return view('management.assignpermission', compact('permissions', 'role'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPermissions(Request $request)
    {
        if ($request->ajax()) {
            $permissions = $this->service->paginate();

            return $this->resourceResponse(
                message: '',
                data: new PermissionResourceCollection($permissions)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Permission::class);
            $permission = $this->service->store($request->validated());

            return $this->resourceResponse(
                message: 'Permiso creado correctamente.',
                data: new PermissionResource($permission),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Permission $permission)
    {
        if ($request->ajax()) {
            $this->authorize('update', $permission);

            return $this->resourceResponse(
                message: '',
                data: new PermissionResource($permission)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Permission $permission)
    {
        if ($request->ajax()) {
            $this->authorize('update', $permission);
            $this->service->update($permission, $request->validated());
            $permission->refresh();

            return $this->resourceResponse(
                message: 'Permiso actualizado correctamente.',
                data: new PermissionResource($permission)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, Permission $permission)
    {
        if ($request->ajax()) {
            $this->authorize('delete', $permission);
            $permission = $this->service::delete($permission);

            return $this->jsonResponse('Permiso eliminado correctamente.');
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  Request  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(Request $request, Permission $permission)
    {
        if ($request->ajax()) {
            $this->authorize('restore', $permission);
            $permission = $this->service->restore($permission);

            return $this->jsonResponse('Permiso restaurado correctamente.');
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Delete permanently the specified resource from storage.
     *
     * @param  Request  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelete(Request $request, Permission $permission)
    {
        if ($request->ajax()) {
            $this->authorize('forceDelete', $permission);
            $permission = $this->service->forceDelete($permission);

            return $this->jsonResponse('Permiso eliminado permanentemente.');
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
