<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\Specialty;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Doctor>
 */
class DoctorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->name(),
            'specialty_id' => Specialty::factory(),
            'country_id' => Country::factory(),
            'is_active' => true,
        ];
    }
}
