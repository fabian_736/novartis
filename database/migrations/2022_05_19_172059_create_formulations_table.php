<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulations', function (Blueprint $table) {
            $table->id();

            $table->string('frequency')->nullable();
            $table->dateTime('starts_at');
            $table->dateTime('ends_at')->nullable();

            $table->foreignId('treatment_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('pathology_has_medicine_dose_id')->constrained()->restrictOnDelete()->restrictOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulations');
    }
};
