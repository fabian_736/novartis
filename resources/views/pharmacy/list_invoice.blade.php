@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col ">
            <label for="" class="text-orange h2">Carta de autorización</label>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <img src="{{ url('img/2.png') }}" alt="">
        </div>
    </div>

    <div class="row-reverse mt-5 " style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-orange">
                <label for="" class="h3 m-0 text-white">Listado de pacientes</label>
            </div>
        </div>
        <div class="col p-0">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-orange">
                    <tr>
                        <th scope="col">N° Paciente</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">País</th>
                        <th scope="col">Tipo de paciente</th>
                        <th scope="col">Autorización carta co-pago</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($benefits as $benefit)
                        <tr>
                            <td>{{ $benefit->patient->pap }}</td>
                            <td>{{ $benefit->patient->name }}</td>
                            <td>{{ $benefit->patient->city->country->name }}</td>
                            <td>{{ $benefit->patient->patient_type->label() }}</td>
                            <td>{{ $benefit->autorized }}</td>
                            <td>
                                @include('pharmacy.edit_invoice')
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
