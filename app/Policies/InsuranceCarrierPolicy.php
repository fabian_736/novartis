<?php

namespace App\Policies;

use App\Models\InsuranceCarrier;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InsuranceCarrierPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear aseguradoras', 'actualizar aseguradoras'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\InsuranceCarrier  $insuranceCarrier
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function view(User $user, InsuranceCarrier $insuranceCarrier)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear aseguradoras')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\InsuranceCarrier  $insuranceCarrier
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, InsuranceCarrier $insuranceCarrier)
    {
        if ($user->hasPermissionTo('actualizar aseguradoras')) {
            return true;
        }
    }
}
