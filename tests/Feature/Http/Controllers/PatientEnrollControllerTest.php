<?php

namespace Tests\Feature\Http\Controllers;

use App\Enums\Gender;
use App\Enums\Patient\PatientTypes;
use App\Enums\RelationshipPatient;
use App\Mail\PatientRegistered;
use App\Models\City;
use App\Models\Country;
use App\Models\DocumentType;
use App\Models\Hospital;
use App\Models\InsuranceCarrier;
use App\Models\Medicine;
use App\Models\PathologyHasMedicineDose;
use App\Models\Patient;
use App\Models\Treatment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class PatientEnrollControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_patient_can_view_the_loading_page()
    {
        $this->get(route('landing.index'))
            ->assertOk()
            ->assertViewIs('landing.index');
    }

    public function test_patient_can_view_the_select_country_page()
    {
        $this->get(route('landing.show_select_country_view'))
            ->assertOk()
            ->assertViewIs('landing.select_country');
    }

    public function test_patient_can_view_the_policy_page()
    {
        $countries = [
            'peru' => Country::factory()->set('name', 'Perú')->create(),
            'ecuador' => Country::factory()->set('name', 'Ecuador')->create(),
        ];

        foreach ($countries as $countryName => $country) {
            $this->get(route('landing.show_consent_view', ['country' => $country]))
                ->assertOk()
                ->assertViewIs("landing.consents.$countryName");
        }
    }

    public function test_patient_can_view_the_registration_form()
    {
        DocumentType::factory()->create();
        Medicine::factory()->create();

        $countries = [
            Country::factory()->set('name', 'Perú')->create(),
            Country::factory()->set('name', 'Ecuador')->create(),
        ];

        foreach ($countries as $country) {
            City::factory()->count(3)->for($country)->create();
            Hospital::factory()->count(3)->for($country)->create();
            InsuranceCarrier::factory()->count(3)->for($country)->create();
        }

        foreach ($countries as $country) {
            $documentTypes = DocumentType::active()->get();
            $cities = City::active()->where('country_id', $country->id)->get();
            $medicines = Medicine::active()->get();
            $hospitals = Hospital::active()->where('country_id', $country->id)->get();
            $insuranceCarriers = InsuranceCarrier::active()->where('country_id', $country->id)->get();

            $this->get(route('landing.show_registration_form', ['country' => $country]))
                ->assertOk()
                ->assertViewIs('landing.form')
                ->assertViewHasAll([
                    'documentTypes' => $documentTypes,
                    'cities' => $cities,
                    'medicines' => $medicines,
                    'hospitals' => $hospitals,
                    'insuranceCarriers' => $insuranceCarriers,
                ]);
        }
    }

    public function test_patient_can_enroll_with_discount_benefit()
    {
        $documentType = DocumentType::factory()->create();
        $pathologyHasMedicineDose = PathologyHasMedicineDose::factory()->create();
        $pathology = $pathologyHasMedicineDose->pathology;
        $medicine = $pathologyHasMedicineDose->medicineDose->medicine;

        $countries = [
            Country::factory()->set('name', 'Perú')->create(),
            Country::factory()->set('name', 'Ecuador')->create(),
        ];

        foreach ($countries as $country) {
            City::factory()->count(3)->for($country)->create();
            Hospital::factory()->count(3)->for($country)->create();
            InsuranceCarrier::factory()->count(3)->for($country)->create();
        }

        Mail::fake();

        foreach ($countries as $country) {
            $city = City::where('country_id', $country->id)->first();
            $hospital = Hospital::where('country_id', $country->id)->first();
            $insuranceCarrier = InsuranceCarrier::where('country_id', $country->id)->first();

            $patientType = $this->faker->randomElement(PatientTypes::cases());
            $withCarer = $this->faker->numberBetween(0, 1);

            $patientData = [
                'document_type_id' => $documentType->id,
                'document_number' => $this->faker->regexify("[1-9]\d{7,15}"),
                'name' => $this->faker->name,
                'birth_date' => $this->faker->dateTimeBetween('-100 years', '-18 years')->format('Y-m-d'),
                'gender' => $this->faker->randomElement(Gender::cases())->value,
                'email' => "patient$country->name@test.com",
                'phone' => $this->faker->regexify("[1-9]\d{8}"),
                'patient_type' => $patientType->value,
                'city_id' => $city->id,
                'first_time_using_biological' => $this->faker->numberBetween(0, 2),
                'hospital_id' => $patientType->value ? null : $hospital->id,
                'insurance_carrier_id' => $patientType->value ? $insuranceCarrier->id : null,
                'has_carer' => $withCarer,
                'relationship_patient' => $withCarer ? $this->faker->randomElement(RelationshipPatient::cases())->value : null,
                'carer_document_type_id' => $withCarer ? $documentType->id : null,
                'carer_document_number' => $withCarer ? $this->faker->regexify("[1-9]\d{7,15}") : null,
                'carer_name' => $withCarer ? $this->faker->name() : null,
                'carer_birth_date' => $withCarer ? $this->faker->dateTimeBetween('-100 years', '-18 years')->format('Y-m-d') : null,
                'carer_phone' => $withCarer ? $this->faker->regexify("[1-9]\d{8}") : null,
                'carer_email' => $withCarer ? "carer$country->name@test.com" : null,
            ];

            $treatmentData = [
                'pathology_id' => $pathology->id,
            ];
            $formulationData = [
                'frequency' => $this->faker->randomDigitNotZero(),
                'dose_id' => $pathologyHasMedicineDose->id,
            ];
            $benefitData = [
                'benefit_type' => $patientType->value ? 'Copago' : 'OOP',
            ];

            $data = [
                'country_id' => $country->id,
                'diagnostic_test' => 0,
                'medicine_id' => $medicine->id,
                'firm' => base64_encode(UploadedFile::fake()->image('signature.png')),
            ];

            $formData = array_merge($patientData, $treatmentData, $formulationData, $benefitData, $data);

            $this->post(route('landing.store', ['country' => $country]), $formData)
                ->assertRedirect()
                ->assertSessionDoesntHaveErrors();

            $this->assertDatabaseHas('patients', $patientData);

            $patient = Patient::where('city_id', $city->id)->first();

            $this->assertFileExists($patient->getFirstMediaPath('consent'));

            $this->assertDatabaseHas('treatments', [
                'pathology_id' => $pathology->id,
                'patient_id' => $patient->id,
            ]);

            $treatment = Treatment::where('patient_id', $patient->id)->first();

            $this->assertDatabaseHas('formulations', [
                'frequency' => $formulationData['frequency'],
                'pathology_has_medicine_dose_id' => $pathologyHasMedicineDose->id,
                'treatment_id' => $treatment->id,
            ]);

            $this->assertDatabaseHas('benefits', [
                'benefit_type' => $patientType->value ? 'Copago' : 'OOP',
                'patient_id' => $patient->id,
            ]);

            $this->assertDatabaseCount('management_diagnostic_tests', 0);

            Mail::assertSent(PatientRegistered::class);
        }
    }

    public function test_patient_can_enroll_with_diagnostic_test_benefit()
    {
        $documentType = DocumentType::factory()->create();

        $countries = [
            Country::factory()->set('name', 'Perú')->create(),
            Country::factory()->set('name', 'Ecuador')->create(),
        ];

        foreach ($countries as $country) {
            City::factory()->count(3)->for($country)->create();
            Hospital::factory()->count(3)->for($country)->create();
            InsuranceCarrier::factory()->count(3)->for($country)->create();
        }

        Mail::fake();

        foreach ($countries as $country) {
            $city = City::where('country_id', $country->id)->first();
            $hospital = Hospital::where('country_id', $country->id)->first();
            $insuranceCarrier = InsuranceCarrier::where('country_id', $country->id)->first();

            $patientType = $this->faker->randomElement(PatientTypes::cases());
            $withCarer = $this->faker->numberBetween(0, 1);

            $patientData = [
                'document_type_id' => $documentType->id,
                'document_number' => $this->faker->regexify("[1-9]\d{7,15}"),
                'name' => $this->faker->name,
                'birth_date' => $this->faker->dateTimeBetween('-100 years', '-18 years')->format('Y-m-d'),
                'gender' => $this->faker->randomElement(Gender::cases())->value,
                'email' => "patient$country->name@test.com",
                'phone' => $this->faker->regexify("[1-9]\d{8}"),
                'patient_type' => $patientType->value,
                'city_id' => $city->id,
                'first_time_using_biological' => null,
                'hospital_id' => $patientType->value ? null : $hospital->id,
                'insurance_carrier_id' => $patientType->value ? $insuranceCarrier->id : null,
                'has_carer' => $withCarer,
                'relationship_patient' => $withCarer ? $this->faker->randomElement(RelationshipPatient::cases())->value : null,
                'carer_document_type_id' => $withCarer ? $documentType->id : null,
                'carer_document_number' => $withCarer ? $this->faker->regexify("[1-9]\d{7,15}") : null,
                'carer_name' => $withCarer ? $this->faker->name() : null,
                'carer_birth_date' => $withCarer ? $this->faker->dateTimeBetween('-100 years', '-18 years')->format('Y-m-d') : null,
                'carer_phone' => $withCarer ? $this->faker->regexify("[1-9]\d{8}") : null,
                'carer_email' => $withCarer ? "carer$country->name@test.com" : null,
            ];

            $data = [
                'country_id' => $country->id,
                'diagnostic_test' => 1,
                'firm' => base64_encode(UploadedFile::fake()->image('signature.png')),
            ];

            $totalDiagnosticTest = $this->faker->numberBetween(1, 3);

            for ($index = 1; $index <= $totalDiagnosticTest; $index++) {
                $data["diagnostic_test_order$index"] = UploadedFile::fake()->image("diagnostic_test_order$index.png");
            }

            $formData = array_merge($patientData, $data);

            $this->post(route('landing.store', ['country' => $country]), $formData)
                ->assertRedirect()
                ->assertSessionDoesntHaveErrors();

            $this->assertDatabaseHas('patients', $patientData);

            $patient = Patient::where('city_id', $city->id)->first();
            $this->assertFileExists($patient->getFirstMediaPath('consent'));

            $this->assertDatabaseCount('treatments', 0);
            $this->assertDatabaseCount('formulations', 0);
            $this->assertDatabaseCount('benefits', 0);
            $this->assertCount($totalDiagnosticTest, $patient->managementDiagnosticTests);

            Mail::assertSent(PatientRegistered::class);
        }
    }

    public function test_patient_can_register_with_both_benefits()
    {
        $documentType = DocumentType::factory()->create();
        $pathologyHasMedicineDose = PathologyHasMedicineDose::factory()->create();
        $pathology = $pathologyHasMedicineDose->pathology;
        $medicine = $pathologyHasMedicineDose->medicineDose->medicine;

        $countries = [
            Country::factory()->set('name', 'Perú')->create(),
            Country::factory()->set('name', 'Ecuador')->create(),
        ];

        foreach ($countries as $country) {
            City::factory()->count(3)->for($country)->create();
            Hospital::factory()->count(3)->for($country)->create();
            InsuranceCarrier::factory()->count(3)->for($country)->create();
        }

        Mail::fake();

        foreach ($countries as $country) {
            $city = City::where('country_id', $country->id)->first();
            $hospital = Hospital::where('country_id', $country->id)->first();
            $insuranceCarrier = InsuranceCarrier::where('country_id', $country->id)->first();

            $patientType = $this->faker->randomElement(PatientTypes::cases());
            $withCarer = $this->faker->numberBetween(0, 1);

            $patientData = [
                'document_type_id' => $documentType->id,
                'document_number' => $this->faker->regexify("[1-9]\d{7,15}"),
                'name' => $this->faker->name,
                'birth_date' => $this->faker->dateTimeBetween('-100 years', '-18 years')->format('Y-m-d'),
                'gender' => $this->faker->randomElement(Gender::cases())->value,
                'email' => "patient$country->name@test.com",
                'phone' => $this->faker->regexify("[1-9]\d{8}"),
                'patient_type' => $patientType->value,
                'city_id' => $city->id,
                'first_time_using_biological' => $this->faker->numberBetween(0, 2),
                'hospital_id' => $patientType->value ? null : $hospital->id,
                'insurance_carrier_id' => $patientType->value ? $insuranceCarrier->id : null,
                'has_carer' => $withCarer,
                'relationship_patient' => $withCarer ? $this->faker->randomElement(RelationshipPatient::cases())->value : null,
                'carer_document_type_id' => $withCarer ? $documentType->id : null,
                'carer_document_number' => $withCarer ? $this->faker->regexify("[1-9]\d{7,15}") : null,
                'carer_name' => $withCarer ? $this->faker->name() : null,
                'carer_birth_date' => $withCarer ? $this->faker->dateTimeBetween('-100 years', '-18 years')->format('Y-m-d') : null,
                'carer_phone' => $withCarer ? $this->faker->regexify("[1-9]\d{8}") : null,
                'carer_email' => $withCarer ? "carer$country->name@test.com" : null,
            ];

            $treatmentData = [
                'pathology_id' => $pathology->id,
            ];
            $formulationData = [
                'frequency' => $this->faker->randomDigitNotZero(),
                'dose_id' => $pathologyHasMedicineDose->id,
            ];
            $benefitData = [
                'benefit_type' => $patientType->value ? 'Copago' : 'OOP',
            ];

            $data = [
                'country_id' => $country->id,
                'diagnostic_test' => 1,
                'medicine_id' => $medicine->id,
                'firm' => base64_encode(UploadedFile::fake()->image('signature.png')),
            ];

            $totalDiagnosticTest = $this->faker->numberBetween(1, 3);

            for ($index = 1; $index <= $totalDiagnosticTest; $index++) {
                $data["diagnostic_test_order$index"] = UploadedFile::fake()->image("diagnostic_test_order$index.png");
            }

            $formData = array_merge($patientData, $treatmentData, $formulationData, $benefitData, $data);

            Mail::fake();

            $this->post(route('landing.store', ['country' => $country]), $formData)
                ->assertRedirect()
                ->assertSessionDoesntHaveErrors();

            $this->assertDatabaseHas('patients', $patientData);

            $patient = Patient::where('city_id', $city->id)->first();

            $this->assertFileExists($patient->getFirstMediaPath('consent'));

            $this->assertDatabaseHas('treatments', [
                'pathology_id' => $pathology->id,
                'patient_id' => $patient->id,
            ]);

            $treatment = Treatment::where('patient_id', $patient->id)->first();

            $this->assertDatabaseHas('formulations', [
                'frequency' => $formulationData['frequency'],
                'pathology_has_medicine_dose_id' => $pathologyHasMedicineDose->id,
                'treatment_id' => $treatment->id,
            ]);

            $this->assertDatabaseHas('benefits', [
                'benefit_type' => $patientType->value ? 'Copago' : 'OOP',
                'patient_id' => $patient->id,
            ]);

            $this->assertCount($totalDiagnosticTest, $patient->managementDiagnosticTests);

            Mail::assertSent(PatientRegistered::class);
        }
    }

    public function test_patient_can_view_the_end_page()
    {
        $countries = [
            Country::factory()->set('name', 'Perú')->create(),
            Country::factory()->set('name', 'Ecuador')->create(),
        ];

        foreach ($countries as $country) {
            $this->get(route('landing.end', ['country', $country]))
                ->assertOk()
                ->assertViewIs('landing.end');
        }
    }
}
