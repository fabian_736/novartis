<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\StoreRequest;
use App\Http\Requests\Role\UpdateRequest;
use App\Http\Resources\RoleResource;
use App\Http\Resources\RoleResourceCollection;
use App\Models\Admin\Permission;
use App\Models\Admin\Role;
use App\Models\User;
use App\Services\Role\RoleService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    use ApiResponses;

    /** @var RoleService */
    private $service;

    public function __construct(RoleService $roleService)
    {
        $this->service = $roleService;
    }

    /**
     * Display the view for the roles.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update', 'asignPermission'], Role::class);

        /** @var User */
        $user = auth()->user();

        $includeTrashed = $user->canAny(['eliminar roles', 'eliminar roles permanentemente']);

        $roles = $this->service->list(withTrashed: $includeTrashed);

        $roles = $this->service->list();

        return view('supervisor.roles.list', compact('roles'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  User  $user
     * @return \Illuminate\Contracts\View\View
     */
    public function allRoles(User $user)
    {
        $user->load('roles');
        $permissions = Permission::get();

        $roles = $this->service->list();

        return view('management.assignrol', compact('permissions', 'user', 'roles'));
    }

    /**
     * Return all roles.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoles(Request $request)
    {
        if ($request->ajax()) {
            $this->authorize('viewAny', Role::class);
            $roles = $this->service->paginate();

            return $this->resourceResponse(
                message: '',
                data: new RoleResourceCollection($roles)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Role::class);
            $role = $this->service->store($request->validated());

            return $this->resourceResponse(
                message: 'Rol creado correctamente.',
                data: new RoleResource($role),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function syncPermissions(Request $request, Role $role)
    {
        $this->authorize('asignPermission', Role::class);
        $role->syncPermissions($request->input('permissions'));

        return $request->wantsJson() ?
        response()->json([
            'message' => 'Permisos asignados correctamente',
            'data' => $role,
        ]) :
        redirect()->route('admin.roles.index')->with('message', 'Permisos asignados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Role $role)
    {
        if ($request->ajax()) {
            $this->authorize('update', $role);

            return $this->resourceResponse(
                message: '',
                data: new RoleResource($role)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Role  $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Role $role)
    {
        if ($request->ajax()) {
            $this->authorize('update', $role);
            $this->service->update($role, $request->validated());
            $role->refresh();

            return $this->resourceResponse(
                message: 'Rol actualizado correctamente.',
                data: new RoleResource($role)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, Role $role)
    {
        if ($request->ajax()) {
            $this->authorize('delete', $role);
            $role = $this->service->delete($role);

            return $this->jsonResponse('Rol eliminado correctamente.');
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(Request $request, Role $role)
    {
        if ($request->ajax()) {
            $this->authorize('restore', $role);
            $role = $this->service->restore($role);

            return $this->jsonResponse('Rol restaurado correctamente.');
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Delete permanently the specified resource from storage.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelete(Request $request, Role $role)
    {
        if ($request->ajax()) {
            $this->authorize('forceDelete', $role);
            $role = $this->service->forceDelete($role);

            return $this->jsonResponse('Rol eliminado permanentemente.');
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
