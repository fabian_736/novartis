<?php

namespace App\Http\Controllers;

use App\Http\Requests\Country\StoreRequest;
use App\Http\Requests\Country\UpdateRequest;
use App\Http\Resources\Country\CountryResource;
use App\Models\City;
use App\Models\Country;
use App\Services\Country\CountryService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CountryController extends Controller
{
    use ApiResponses;

    /** @var CountryService */
    private $service;

    public function __construct(CountryService $countryService)
    {
        $this->service = $countryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Country::class);

        $countries = $this->service->list(false);

        return view('supervisor.country.list', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Country::class);
            $country = $this->service->store($request->validated());

            return $this->resourceResponse(
                message: 'País creado correctamente.',
                data: new CountryResource($country),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Country  $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Country $country)
    {
        if ($request->ajax()) {
            $this->authorize('update', $country);

            return $this->resourceResponse(
                message: '',
                data: new CountryResource($country)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Country  $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Country $country)
    {
        if ($request->ajax()) {
            $this->authorize('update', $country);
            $this->service->update($country, $request->validated());
            $country->refresh();

            return $this->resourceResponse(
                message: 'País actualizado correctamente.',
                data: new CountryResource($country)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Returns an associated city.
     *
     * @param  int  $id
     * @return \Illuminate\Database\Eloquent\Collection<int, City>
     */
    public function bycity($id)
    {
        return City::where('country_id', $id)->get();
    }
}
