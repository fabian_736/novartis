<?php

namespace App\Http\Controllers;

use App\Http\Requests\Patient\PatientRegistrationRequest;
use App\Mail\PatientRegistered;
use App\Models\City;
use App\Models\Country;
use App\Models\DocumentType;
use App\Models\Dose;
use App\Models\Hospital;
use App\Models\InsuranceCarrier;
use App\Models\Medicine;
use App\Models\Pathology;
use App\Models\Patient;
use App\Services\Benefit\BenefitService;
use App\Services\Formulation\FormulationService;
use App\Services\ManagementDiagnosticTest\ManagementDiagnosticTestService;
use App\Services\Patient\PatientService;
use App\Services\Treatment\TreatmentService;
use App\Traits\ApiResponses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PatientEnrollController extends Controller
{
    use ApiResponses;

    /**
     * The benefit service instance
     *
     * @var BenefitService
     */
    private BenefitService $benefitService;

    /**
     * The formulation service instance
     *
     * @var FormulationService
     */
    private FormulationService $formulationService;

    /**
     * The formulation service instance
     *
     * @var ManagementDiagnosticTestService
     */
    private ManagementDiagnosticTestService $managementDiagnosticTestService;

    /**
     * The patient service instance
     *
     * @var PatientService
     */
    private PatientService $patientService;

    /**
     * The treatment service instance
     *
     * @var TreatmentService
     */
    private TreatmentService $treatmentService;

    /**
     * Creates a new controller instance
     *
     * @param  BenefitService  $benefitService
     * @param  FormulationService  $formulationService
     * @param  ManagementDiagnosticTestService  $managementDiagnosticTestService
     * @param  PatientService  $patientService
     * @param  TreatmentService  $treatmentService
     */
    public function __construct(BenefitService $benefitService, FormulationService $formulationService, ManagementDiagnosticTestService $managementDiagnosticTestService, PatientService $patientService, TreatmentService $treatmentService)
    {
        $this->benefitService = $benefitService;
        $this->formulationService = $formulationService;
        $this->managementDiagnosticTestService = $managementDiagnosticTestService;
        $this->patientService = $patientService;
        $this->treatmentService = $treatmentService;
    }

    /**
     * Displays the preload view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('landing.index');
    }

    /**
     * Displays the conutry selection view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function showSelectCountryView()
    {
        return view('landing.select_country');
    }

    /**
     * Displays the consent for the given country.
     *
     * @param  Country  $country
     * @return \Illuminate\Contracts\View\View
     */
    public function showConsentView(Country $country)
    {
        $view = match ($country->name) {
            'Perú' => view('landing.consents.peru'),
            'Ecuador' => view('landing.consents.ecuador'),
            default => abort(404)
        };

        return $view;
    }

    /**
     * Display the registration form
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function showRegistrationForm(Country $country)
    {
        $documentTypes = DocumentType::active()->get();
        $cities = City::active()->where('country_id', $country->id)->get();
        $medicines = Medicine::active()->get();
        $hospitals = Hospital::active()->where('country_id', $country->id)->get();
        $insuranceCarriers = InsuranceCarrier::active()->where('country_id', $country->id)->get();

        return view('landing.form', compact('insuranceCarriers', 'hospitals', 'documentTypes', 'cities', 'medicines'));
    }

    /**
     * registers a new patient
     *
     * @param  PatientRegistrationRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PatientRegistrationRequest $request, Country $country)
    {
        /** @var \Illuminate\Support\ValidatedInput */
        $safeRequest = $request->safe();

        /** @var Patient|null */
        $patient = null;

        DB::transaction(function () use ($safeRequest, &$patient) {
            /** @var \App\Models\Patient */
            $patient = $this->patientService->enroll($safeRequest);

            $this->patientService->generateConsent($patient, $safeRequest->firm);

            if (! empty($safeRequest->benefit_type)) {
                $inputTreatment = [
                    'pathology_id' => $safeRequest->pathology_id,
                    'patient_id' => $patient->id,
                ];
                $treatment = $this->treatmentService->store($inputTreatment);

                $inputFormulation = [
                    'frequency' => $safeRequest->frequency,
                    'starts_at' => now(),
                    'treatment_id' => $treatment->id,
                    'pathology_has_medicine_dose_id' => $safeRequest->dose_id,
                ];
                $this->formulationService->store($inputFormulation);

                $this->benefitService->createNewBenefit($patient);
            }

            // Se crea la prueba o pruebas diagnósticas
            if (isset($safeRequest->diagnostic_test) && $safeRequest->diagnostic_test == 1) {
                $this->managementDiagnosticTestService->store($patient, $safeRequest->diagnostic_test_order1);

                if (isset($safeRequest->diagnostic_test_order2)) {
                    $this->managementDiagnosticTestService->store($patient, $safeRequest->diagnostic_test_order2);
                }

                if (isset($safeRequest->diagnostic_test_order3)) {
                    $this->managementDiagnosticTestService->store($patient, $safeRequest->diagnostic_test_order3);
                }
            }

            $emails = array_filter([$patient->email, $patient->carer_email]);

            if (count($emails) > 0) {
                Mail::to($emails)->send(new PatientRegistered($patient));
            }
        });

        /** @var Patient $patient */
        $pap = $patient->pap;

        return redirect()->back()->with('status', $pap);
    }

    /**
     * Returns an associated pathology by medicine.
     *
     * @param  int  $id
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Pathology>
     */
    public function getPathologiesByMedicine($id)
    {
        return Pathology::where('medicine_id', $id)
            ->active()
            ->get();
    }

    /**
     * Returns an associated dose by medicine.
     *
     * @param  int  $id
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Dose>
     */
    public function bydose($id)
    {
        return Dose::where('medicine_id', $id)->get();
    }

    /**
     * Redirect to end_landing.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function end_landing()
    {
        return view('landing.end');
    }
}
