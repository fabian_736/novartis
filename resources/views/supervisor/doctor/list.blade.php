@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-danger h2">Doctores</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('message'))
        <div class="alert alert-success" role="alert">
            <span class="iconify mr-2" data-width="15" data-icon="bi:check-circle-fill"></span> Se han cargado los doctores de manera correcta
        </div>
    @endif
    @if (session('message_error'))
        <div class="alert alert-danger" role="alert">
            <span class="iconify mr-2" data-width="15" data-icon="bi:check-circle-fill"></span> No se cargo el archivo
        </div>
    @endif
    <div class="row-reverse">
        <div class="col d-flex justify-content-end mt-3">

            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                <span class="iconify mr-2" data-width="15" data-icon="ri:file-excel-2-line"></span> Importar
            </button>&nbsp;&nbsp;&nbsp;
            @can('crear doctores', \App\Models\Doctor::class)
                <button type="button" class="btn bg-danger d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearDoctor">
                    <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>Crear doctor
                </button>
            @endcan
        </div>
        <div class="col">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-danger">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombres</th>
                        <th scope="col">Especialidad</th>
                        <th scope="col">País</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Fecha de creación</th>
                        @can('actualizar doctores')
                            <th scope="col">Acciones</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach ($doctors as $key => $doctor)
                        <tr>
                            <td align="center" style="width: 50px">{{ $key + 1 }}</td>
                            <td>{{ $doctor->name }}</td>
                            <td>{{ $doctor->specialty->name }}</td>
                            <td>{{ $doctor->country->name }}</td>
                            @if ($doctor->is_active == 0)
                                <td>Inactivo</td>
                            @elseif ($doctor->is_active == 1)
                                <td>Activo</td>
                            @endif
                            <td>{{ $doctor->created_at->format('d-m-Y') }}</td>
                            @can('actualizar doctores', $doctor)
                                <td>
                                    <div class="col d-flex justify-content-center" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-secondary bg-danger edit-button" type="button" style="color: white" href="{{ route('admin.doctors.update', $doctor) }}" data-id="{{ $doctor->id }}">
                                            <span class="iconify mr-2" data-icon="akar-icons:edit"></span>
                                            Editar
                                        </a>
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th></th>
                        <th scope="col">Nombres</th>
                        <th scope="col">Especialidad</th>
                        <th scope="col">País</th>
                        <th scope="col">Estado</th>
                        <th scope="col"></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{-- Modal crear doctores --}}
    <div class="modal fade" id="crearDoctor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear doctor</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white" data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal" aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('admin.doctors.store') }}" method="POST" id="create-doctor">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name" class="lead text-danger font-weight-bold">Nombres</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombres" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="specialty_id" class="lead text-danger font-weight-bold">Especialidad</label>
                                            </div>
                                            <div class="col">
                                                <select name="specialty_id" id="specialty_id" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($specialties as $specialty)
                                                        @if ($specialty->is_active == 1)
                                                            <option value="{{ $specialty->id }}">{{ $specialty->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country_id" class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country_id" id="country_id" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        @if ($country->is_active == 1)
                                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal editar doctores --}}
    <div class="modal fade" id="updateDoctor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Editar doctor</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white" data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal" aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="#" method="POST" id="update-doctor">
                                @csrf
                                @method('PUT')
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name2" class="lead text-danger font-weight-bold">Nombres</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name2" id="name2" class="form-control" placeholder="Nombres" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="specialty_id2" class="lead text-danger font-weight-bold">Especialidad</label>
                                            </div>
                                            <div class="col">
                                                <select name="specialty_id2" id="specialty_id2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($specialties as $specialty)
                                                        <option value="{{ $specialty->id }}" data-is-active="{{ $specialty->is_active }}">{{ $specialty->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="country_id2" class="lead text-danger font-weight-bold">País</label>
                                            </div>
                                            <div class="col">
                                                <select name="country_id2" id="country_id2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country->id }}" data-is-active="{{ $country->is_active }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="is_active2" class="lead text-danger font-weight-bold">Estado</label>
                                            </div>
                                            <div class="col">
                                                <select name="is_active2" id="is_active2" class="form-control" required autocomplete="off">
                                                    <option value="" selected disabled>Seleccione una opcion</option>
                                                    <option value="0">Inactivo</option>
                                                    <option value="1">Activo</option>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal modal2 fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Importar Médicos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <form id="form" onsubmit="return checkSubmit();" action="{{ url('upload_doctors') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="d-flex justify-content-center mt-2">
                            <input accept=".xlsx,.csv" required type="file" name="upload_doctor" id="file-1" class="inputfile input-excel inputfile-1" />
                            <label for="file-1">
                                <svg xmlns="http://www.w3.org/2000/svg" class="iborrainputfile" width="20" height="17" viewBox="0 0 20 17">
                                    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                                </svg>
                                <span class="iborrainputfile">Seleccionar archivo</span>
                            </label>
                        </div>
                        <div class="d-flex justify-content-center">
                            <a href="{{ route('template_doctor') }}" style="border-color:white;background:white;color:black" class="btn btn-primary"><span class="iconify mr-2" data-icon="fa:download"></span>Descargar plantilla</a>
                        </div>
                        <div class="d-flex justify-content-center">
                            <label id="archivo" for="image1"></label><br>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button id="importar" data-target="#staticBackdrop" title="Importar archivo" type="submit" class="btn btn-primary">Importar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal modal1 fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <div class="d-flex justify-content-center">
                        <img src="{{ asset('img/2.png') }}" alt="">
                    </div>
                    <h3 class="text-center">Cargando...</h3>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        enviando = false; //Obligaremos a entrar el if en el primer submit
        function checkSubmit() {
            if (!enviando) {
                enviando = true;
                return true;
            } else {
                //Si llega hasta aca significa que pulsaron 2 veces el boton submit
                alert("El archivo ya se está procesando");
                return false;
            }
        }

        formulario = document.getElementById('form');
        formulario.addEventListener('submit', export_excel)

        function export_excel() {
            $('.modal2').modal('hide');
            $('.modal1').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('.modal1').modal('show');
        }
    </script>
    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };


        //Funcion modal create
        const form = document.getElementById('create-doctor');

        const create = e => {
            e.preventDefault();
            const data = {
                name: document.getElementById('name').value,
                specialty_id: document.getElementById('specialty_id').value,
                country_id: document.getElementById('country_id').value,
            }
            axios.post('/admin/management/doctors', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }

        form.addEventListener('submit', create);
        //Fin función modal create

        // Función modal update
        const updateDoctorForm = document.getElementById('update-doctor');

        const editDoctor = e => {
            e.preventDefault();

            axios.get(`/admin/management/doctors/${e.target.closest('.edit-button').dataset.id}`)
                .then((response) => {
                    const doctor = response.data.data;

                    updateDoctorForm.dataset.id = doctor.id;
                    updateDoctorForm.elements['name2'].value = doctor.name;
                    updateDoctorForm.elements['specialty_id2'].value = doctor.specialty_id;
                    updateDoctorForm.elements['country_id2'].value = doctor.country_id;
                    updateDoctorForm.elements['is_active2'].value = doctor.is_active;

                    Array.from(document.getElementById("specialty_id2").options).forEach(option => {
                        if (option.dataset.isActive == "0" && option.value != doctor.specialty_id) {
                            option.setAttribute("hidden", "");
                            option.setAttribute("disabled", "");
                        } else {
                            option.removeAttribute("hidden");
                            option.removeAttribute("disabled");
                        }
                    })

                    Array.from(document.getElementById("country_id2").options).forEach(option => {
                        if (option.dataset.isActive == "0" && option.value != doctor.country_id) {
                            option.setAttribute("hidden", "");
                            option.setAttribute("disabled", "");
                        } else {
                            option.removeAttribute("hidden");
                            option.removeAttribute("disabled");
                        }
                    })

                    Array.from(updateDoctorForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    $('#updateDoctor').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#221F1F',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editDoctor);
        });

        const updateDoctor = e => {
            e.preventDefault();

            const updateDoctorData = {
                name: document.getElementById('name2').value,
                specialty_id: document.getElementById('specialty_id2').value,
                country_id: document.getElementById('country_id2').value,
                is_active: document.getElementById('is_active2').value,
            }
            axios.put(
                    `/admin/management/doctors/${updateDoctorForm.dataset.id}`,
                    updateDoctorData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updateDoctor').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(updateDoctorForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updateDoctorForm.elements[`${key}2`];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updateDoctorForm.addEventListener('submit', updateDoctor);
        // Fin función modal update
    </script>
    <script></script>
@endsection
