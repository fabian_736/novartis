<?php

namespace Database\Seeders;

use App\Models\DiagnosticTest;
use Illuminate\Database\Seeder;

class DiagnosticTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DiagnosticTest::create([
            'name' => 'GE (Inmonoglobulina)',
        ]);

        DiagnosticTest::create([
            'name' => 'Prick test o panel de alergia',
        ]);

        DiagnosticTest::create([
            'name' => 'PPD (Prueba de tuberculina)',
        ]);

        DiagnosticTest::create([
            'name' => 'Quantiferon (TBC)',
        ]);

        DiagnosticTest::create([
            'name' => 'Espirometría No medicada',
        ]);

        DiagnosticTest::create([
            'name' => 'Espirometría medicada',
        ]);

        DiagnosticTest::create([
            'name' => 'Radiografía Simple de Torax AP y Lateral',
        ]);

        DiagnosticTest::create([
            'name' => 'RMN Simple',
        ]);
    }
}
