<?php

namespace App\Services\Medicine;

use App\Models\Medicine;
use Illuminate\Support\Facades\DB;

class MedicineService
{
    /**
     * Returns a list of medicines.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Medicine>
     */
    public function list($onlyActive = true)
    {
        $medicines = Medicine::query();

        if ($onlyActive) {
            $medicines->active();
        }

        return $medicines->get();
    }

    /**
     * Create a Medicine.
     *
     * @param  array  $input
     * @return Medicine
     */
    public function store(array $input): Medicine
    {
        /** @var Medicine|null */
        $medicine = null;

        DB::transaction(function () use (&$medicine, $input) {
            $medicine = Medicine::create($input);
            $medicine->doses()->attach($input['doses']);
        });

        /** @var Medicine $medicine */
        return $medicine;
    }

    public function update(Medicine $medicine, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use (&$updated, $medicine, $input) {
            $updated = $medicine->update($input);
            $medicine->doses()->sync($input['doses']);
        });

        return $updated;
    }
}
