<?php

use App\Http\Controllers\CountryController;
use App\Http\Controllers\MedicineController;
use App\Http\Controllers\PathologyController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PatientEnrollController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/medicines/{medicine}/doses', [MedicineController::class, 'getDoses']);

Route::get('/pathology/{pathology}/medicine/{medicine}/doses', [PathologyController::class, 'getPathologyDoses']);

Route::get('/countries/{id}/cities', [CountryController::class, 'bycity']);

Route::get('/country/{id}', [PatientEnrollController::class, 'bycountry']);

Route::get('/medicine/{id}/pathologies', [PatientEnrollController::class, 'getPathologiesByMedicine']);

Route::get('/doctor/{id}', [PatientController::class, 'bydoctor']);
