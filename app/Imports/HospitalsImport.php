<?php

namespace App\Imports;

use App\Models\Hospital;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class HospitalsImport implements ToModel, WithHeadingRow, WithBatchInserts, WithValidation
{
    /**
     * @param  array  $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Hospital([
            'name' => $row['name'],
            'is_active' => $row['is_active'],
            'country_id' => $row['country_id'],
            'created_at' => $row['created_at'],
            'updated_at' => $row['updated_at'],
        ]);
    }

    public function rules(): array
    {
        return [
            'name' => [
                'required',
            ],
            'country_id' => [
                'required', 'exists:countries,id',
            ],
        ];
    }

    /**
     * Show error messages.
     *
     * @return array<string, string>
     */
    public function customValidationMessages()
    {
        return [
            'name.required' => 'El nombre del hospital es requerido',
            'country_id.exists' => 'El país no se encuentra en nuestros registros',
        ];
    }

    /**
     * Rename form fields.
     *
     * @return array<string, string>
     */
    public function customValidationAttributes()
    {
        return [
            'name' => 'Nombre',
            'country_id' => 'País',
        ];
    }

    public function batchSize(): int
    {
        return 100;
    }
}
