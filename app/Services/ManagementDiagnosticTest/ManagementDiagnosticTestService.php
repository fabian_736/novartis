<?php

namespace App\Services\ManagementDiagnosticTest;

use App\Models\ManagementDiagnosticTest;
use App\Models\Patient;
use Illuminate\Http\UploadedFile;

class ManagementDiagnosticTestService
{
    /**
     * Returns a list of Management diagnostic test.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, ManagementDiagnosticTest>
     */
    public function list()
    {
        return ManagementDiagnosticTest::get();
    }

    /**
     * Create a management diagnostic test.
     *
     * @param  Patient  $patient
     * @param  UploadedFile  $file
     * @return ManagementDiagnosticTest
     */
    public function store(Patient $patient, UploadedFile $file): ManagementDiagnosticTest
    {
        $test = ManagementDiagnosticTest::create(['patient_id' => $patient->id]);

        $test->addMedia($file)->toMediaCollection('file-upload');

        return $test;
    }

    public function update(ManagementDiagnosticTest $managementDiagnosticTest, array $input): bool
    {
        return $managementDiagnosticTest->update($input);
    }
}
