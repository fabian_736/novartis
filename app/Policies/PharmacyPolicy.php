<?php

namespace App\Policies;

use App\Models\Pharmacy;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PharmacyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear farmacias', 'actualizar farmacias'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Pharmacy  $pharmacy
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function view(User $user, Pharmacy $pharmacy)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear farmacias')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Pharmacy  $pharmacy
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, Pharmacy $pharmacy)
    {
        if ($user->hasPermissionTo('actualizar farmacias')) {
            return true;
        }
    }
}
