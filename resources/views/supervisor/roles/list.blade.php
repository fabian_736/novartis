@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-danger h2">Roles</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    @if (session('message'))
        <script>
            Swal.fire(
                'Buen trabajo',
                'Permisos modificados correctamente ',
                'success'
            )
        </script>
    @endif

    <div class="row-reverse">
        <div class="col d-flex justify-content-end mt-3">
            <a href="{{ route('admin.management.index') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>&nbsp;&nbsp;&nbsp;
            @can('crear roles', \App\Models\Admin\Role::class)
                <button type="button" class="btn bg-danger d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearRol">
                    <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>Crear rol
                </button>
            @endcan
        </div>
        <div class="col">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-danger">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre del rol</th>
                        <th scope="col">Fecha de creacion</th>
                        @canany(['actualizar roles', 'eliminar roles', 'eliminar roles permanentemente', 'asignar permisos a roles'])
                            <th scope="col">Acciones</th>
                        @endcanany
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $key => $role)
                        <tr>
                            <td align="center" style="width: 50px">{{ $key + 1 }}</td>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->created_at->format('d-m-Y') }}</td>
                            @canany(['actualizar roles', 'eliminar roles', 'eliminar roles permanentemente', 'asignar permisos a roles'], $role)
                                <td>
                                    <div class="dropdown col d-flex justify-content-center">
                                        <button class="btn btn-secondary dropdown-toggle bg-danger text-white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Acciones
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            @can('actualizar roles', $role)
                                                <a class="dropdown-item edit-button" href="{{ route('admin.roles.update', $role) }}" data-id="{{ $role->id }}">
                                                    <span class="iconify" data-icon="bxs:edit"></span>&nbsp;
                                                    Editar
                                                </a>
                                            @endcan
                                            @can('eliminar roles', $role)
                                                <a class="dropdown-item delete-button" href="{{ route('admin.roles.destroy', $role) }}" data-id="{{ $role->id }}">
                                                    <span class="iconify" data-icon="clarity:trash-solid"></span>&nbsp;
                                                    Eliminar
                                                </a>
                                            @endcan
                                            @can('restaurar roles', $role)
                                                <a class="dropdown-item restore-button" href="{{ route('admin.roles.restore', $role) }}" data-id="{{ $role->id }}">
                                                    <span class="iconify" data-icon="fa-solid:trash-restore"></span>&nbsp;
                                                    Restaurar
                                                </a>
                                            @endcan
                                            @can('eliminar roles permanentemente', $role)
                                                <a class="dropdown-item forceDelete-button" href="{{ route('admin.roles.force_delete', $role) }}" data-id="{{ $role->id }}">
                                                    <span class="iconify" data-icon="clarity:trash-solid"></span>&nbsp;
                                                    Eliminar permanentemente
                                                </a>
                                            @endcan
                                            @can('asignar permisos a roles', $role)
                                                <a class="dropdown-item" href="{{ route('admin.management.assignpermission', $role) }}">
                                                    <i class="fas fa-plus"></i>&nbsp;
                                                    Asignar permisos
                                                </a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                            @endcanany
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Modal crear roles --}}
    <div class="modal fade" id="crearRol" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear rol</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('admin.roles.store') }}" method="POST" id="create-role">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del rol" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal editar roles --}}
    <div class="modal fade" id="updateRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-danger p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Editar rol</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="#" method="POST" id="update-role">
                                @csrf
                                @method('PUT')
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="name2" class="lead text-danger font-weight-bold">Nombre</label>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="name2" id="name2" class="form-control" placeholder="Nombre del rol" required autocomplete="off">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };


        //Funcion modal create
        const form = document.getElementById('create-role');

        const create = e => {
            e.preventDefault();
            const data = {
                name: document.getElementById('name').value
            }

            axios.post('/admin/management/roles', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        icon: 'error',
                        confirmButtonColor: '#3085d6',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }

        form.addEventListener('submit', create);
        //Fin función modal create


        // Función modal update
        const updateRoleForm = document.getElementById('update-role');

        const editRole = e => {
            e.preventDefault();

            axios.get(`/admin/management/roles/${e.target.closest('.edit-button').dataset.id}`)
                .then((response) => {
                    const role = response.data.data;

                    updateRoleForm.dataset.id = role.id;
                    updateRoleForm.elements['name2'].value = role.name;

                    Array.from(updateRoleForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    $('#updateRole').modal('show');
                })
                .catch((error) => {
                    Swal.fire({
                        title: error.response.data.message,
                        text: 'Consulte con el administrador del sistema.',
                        confirmButtonColor: '#3085d6',
                    });
                });
        };

        Array.prototype.forEach.call(document.querySelectorAll(".edit-button"), link => {
            link.addEventListener("click", editRole);
        });

        const updateRole = e => {
            e.preventDefault();

            const updateRoleData = {
                name: document.getElementById('name2').value
            }

            axios.put(
                    `/admin/management/roles/${updateRoleForm.dataset.id}`,
                    updateRoleData
                )
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        position: 'center',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: 'Cerrar'
                    }).then(() => {
                        $('#updateRole').modal('hide');
                        location.reload();
                    });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                        confirmButtonColor: '#3085d6',
                    });

                    Array.from(updateRoleForm.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = updateRoleForm.elements[`${key}2`];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        updateRoleForm.addEventListener('submit', updateRole);
        // Fin función modal update

        // Función delete
        const deleteRole = e => {
            e.preventDefault();
            Swal.fire({
                title: '¿Estas seguro que quieres eliminar este rol?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar'
            }).then((result) => {
                if (result.value) {
                    axios.delete(`/admin/management/roles/${e.target.closest('.delete-button').dataset.id}`, {
                            headers: {
                                'Content-Type': 'Application/json',
                                'Accepts': 'Application/json',
                                'X-Requested-With': 'XMLHttpRequest'
                            }
                        })
                        .then((response) => {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                            }).then(() => {
                                location.reload();
                            });

                        })
                }
            });
        }

        Array.prototype.forEach.call(document.querySelectorAll(".delete-button"), link => {
            link.addEventListener("click", deleteRole);
        });
        // Fin función delete

        // Función restore
        const restoreRole = e => {
            e.preventDefault();
            Swal.fire({
                title: '¿Estás seguro que quieres restaurar este rol?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Restaurar'
            }).then((result) => {
                if (result.value) {
                    axios.patch(`/admin/management/roles/${e.target.closest('.restore-button').dataset.id}/restore`, {
                            headers: {
                                'Content-Type': 'Application/json',
                                'Accepts': 'Application/json',
                                'X-Requested-With': 'XMLHttpRequest'
                            }
                        })
                        .then((response) => {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: true,
                                confirmButtonText: 'Cerrar'
                            }).then(() => {
                                location.reload();
                            });
                        })
                        .catch(error => {
                            Swal.fire({
                                title: 'No se puede restaurar este rol',
                                text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                                icon: 'error',
                                confirmButtonColor: '#3085d6',
                            });
                        });
                }
            });
        }

        Array.prototype.forEach.call(document.querySelectorAll('.restore-button'), link => {
            link.addEventListener('click', restoreRole);
        });
        // Fin función restore

        // Función forceDelete
        const forceDeleteRole = e => {
            e.preventDefault();
            Swal.fire({
                title: '¿Estas seguro que quieres eliminar este rol permanentemente?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar'
            }).then((result) => {
                if (result.value) {
                    axios.delete(`/admin/management/roles/${e.target.closest('.forceDelete-button').dataset.id}/force-delete`, {
                            headers: {
                                'Content-Type': 'Application/json',
                                'Accepts': 'Application/json',
                                'X-Requested-With': 'XMLHttpRequest'
                            }
                        })
                        .then((response) => {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                            }).then(() => {
                                location.reload();
                            });
                        })
                        .catch(error => {
                            Swal.fire({
                                title: 'No se puede eliminar este rol',
                                text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message,
                                icon: 'error',
                                confirmButtonColor: '#3085d6',
                            });
                        });
                }
            });
        }

        Array.prototype.forEach.call(document.querySelectorAll(".forceDelete-button"), link => {
            link.addEventListener("click", forceDeleteRole);
        });
        // Fin función forceDelete
    </script>
@endsection
