<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperPathologyHasMedicineDose
 */
class PathologyHasMedicineDose extends Pivot implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $table = 'pathology_has_medicine_doses';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Relación Formulation - PathologyHasMedicineDose.
     *
     * @return HasMany<Formulation>
     */
    public function formulations(): HasMany
    {
        return $this->hasMany(Formulation::class);
    }

    /**
     * Retrieves the medicine dose.
     *
     * @return BelongsTo<MedicineDose, PathologyHasMedicineDose>
     */
    public function medicineDose(): BelongsTo
    {
        return $this->belongsTo(MedicineDose::class);
    }

    /**
     * Retrieves the pathology.
     *
     * @return BelongsTo<Pathology, PathologyHasMedicineDose>
     */
    public function pathology(): BelongsTo
    {
        return $this->belongsTo(Pathology::class);
    }

    /**
     * {@inheritDoc}
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Sets the model id when deleting.
         * This solves a Laravel bug when deleting pivots with IDs.
         */
        static::deleting(function ($model) {
            if (! $model->id) {
                $query = static::select(['id']);
                foreach ($model->getAttributes() as $key => $value) {
                    $query->where($key, $value);
                }
                $model->id = $query->first()?->id;
            }
        });
    }
}
