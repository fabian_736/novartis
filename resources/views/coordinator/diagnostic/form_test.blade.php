@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-orange h2">Prueba diagnóstica</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    <x-errores />
    <div class="row-reverse ">
        <div class="col px-0 py-5">
            <form action="{{ route('test.update', $managementDiagnosticTest->id) }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">PAP </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->pap }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Número de documento</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->document_number }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Nombre </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->name }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Medicamento</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->treatment?->pathology->medicine->name ?? 'Sin medicamento' }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Laboratorio </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->laboratory->name }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Prueba a realizar</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->diagnosticTest->name }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="" class="h5 font-weight-bold title_patient_cordi">Fecha de coordinación</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->management_date }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="invoice_number" class="h5 font-weight-bold title_patient_cordi">Número de factura</label>
                            </div>
                            <div class="col">
                                <input type="text" name="invoice_number" id="invoice_number" class="form-control" placeholder="Digite el número de factura">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="test_date" class="h5 font-weight-bold title_patient_cordi">Fecha de la prueba</label>
                            </div>
                            <div class="col">
                                <input type="date" name="test_date" id="test_date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="test_price" class="h5 font-weight-bold title_patient_cordi">Valor de la prueba</label>
                            </div>
                            <div class="col">
                                <input type="number" name="test_price" id="test_price" class="form-control" placeholder="Digite el costo de la prueba">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="status" class="h5 font-weight-bold title_patient_cordi">Estado de la prueba</label>
                            </div>
                            <div class="col">
                                <select name="status" id="status" class="form-control" onchange="javascript:testNotes()">
                                    <option selected disabled value="">Seleccione una opción...</option>
                                    <option value="0">Ejecutada</option>
                                    <option value="3">No ejecutada</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="observation" id="observation1" style="display: none">
                    <div class="row mb-3">
                        <div class="col">
                            <div class="row">
                                <div class="col-3">
                                    <label for="observations" class="h5 font-weight-bold title_patient_cordi">Observaciones</label>
                                </div>
                                <div class="col-9">
                                    <textarea name="observations" id="observations" cols="30" rows="3" class="form-control" placeholder="Digite las observaciones..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-4 mx-auto">
                        <div class="row">
                            <div class="col">
                                <a href="{{ route('management-diagnostic-tests.index') }}"
                                    class="btn btn-orange w-100 d-flex align-items-center justify-content-center">
                                    <span class="iconify mr-2" data-icon="fa:close" data-width="15"></span>
                                    SALIR
                                </a>
                            </div>
                            <div class="col d-flex justify-content-end ">
                                <button type="submit" class="btn btn-dark w-100 d-flex align-items-center justify-content-center"><span class="iconify mr-2" data-icon="entypo:save" data-width="15"></span>
                                    GUARDAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <script src="{{ asset('js/management_diagnostic_test/management_test.js') }}"></script>

    </div>
@endsection
