<?php

namespace App\Imports;

use App\Models\Doctor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class DoctorsImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
     * @param  array  $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Doctor([
            'name' => $row['name'],
            'is_active' => $row['is_active'],
            'specialty_id' => $row['specialty_id'],
            'country_id' => $row['country_id'],
            'created_at' => $row['created_at'],
            'updated_at' => $row['updated_at'],
        ]);
    }

    public function rules(): array
    {
        return [
            'name' => [
                'required', 'string', 'between:3,100',
            ],
            'specialty_id' => [
                'required', 'exists:specialties,id',
            ],
            'country_id' => [
                'required', 'exists:countries,id',
            ],
        ];
    }

    /**
     * Show error messages.
     *
     * @return array<string, string>
     */
    public function customValidationMessages()
    {
        return [
            'name.required' => 'El nombre del médico es requerido',
            'country_id.exists' => 'El país no se encuentra en nuestros registros',
            'specialty_id.exists' => 'La especialidad no se encuentra en nuestros registros',
        ];
    }

    /**
     * Rename form fields.
     *
     * @return array<string, string>
     */
    public function customValidationAttributes()
    {
        return [
            'name' => 'Nombre',
            'country_id' => 'País',
            'specialty_id' => 'Especialidad',
        ];
    }

    public function batchSize(): int
    {
        return 100;
    }
}
