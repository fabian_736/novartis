@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <div class="row mx-auto">
                    <div class="d-flex align-items-center" style="width: 100px">
                        <label for="" class="text-primary font-weight-bold h2">PSP |</label>
                    </div>
                    <div class="col d-flex align-items-center p-0 pt-2">
                        <label for="" class="h5" style="color: #0C68B0">Programa seguimiento <br> de
                            pacientes</label>
                    </div>
                </div>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>


    <div class="row mt-5 " style="overflow-y: auto; max-height: 80vh">
        @canany(['crear usuarios', 'actualizar usuarios', 'crear permisos', 'actualizar permisos', 'eliminar permisos', 'eliminar permisos permanentemente', 'crear roles', 'asignar permisos a roles', 'actualizar roles', 'eliminar roles', 'eliminar roles permanentemente'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Administración general</label>
                        </div>
                    </div>
                    {{-- Usuarios --}}
                    @canany(['crear usuarios', 'actualizar usuarios'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.users.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar usuarios</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Roles --}}
                    @canany(['crear roles', 'actualizar roles', 'eliminar roles', 'eliminar roles permanentemente', 'asignar permisos a roles'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.roles.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar roles</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Permisos --}}
                    @canany(['crear permisos', 'actualizar permisos', 'eliminar permisos', 'eliminar permisos permanentemente'])
                        <div class="col">
                            <a href="{{ route('admin.permissions.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar permisos</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                </div>
            </div>
        @endcanany

        {{-- Cadenas y Farmacias --}}
        @canany(['crear cadenas', 'actualizar cadenas', 'crear farmacias', 'actualizar farmacias'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Farmacias</label>
                        </div>
                    </div>
                    {{-- Cadenas de farmacias --}}
                    @canany(['crear cadenas', 'actualizar cadenas'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.pharmacy-chains.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                cadenas</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Farmacias --}}
                    @canany(['crear farmacias', 'actualizar farmacias'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.pharmacies.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                farmacias</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    <div class="col">
                        <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                            <div class="row mx-auto ">
                                <div class="col-10 d-flex align-items-center">
                                    <div class="col p-0 d-flex justify-content-end">
                                        <div class="bg-white d-flex justify-content-center"
                                            style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col p-0 d-flex justify-content-end">
                                    <div class="bg-danger d-flex justify-content-center"
                                        style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                        <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endcanany

        {{-- Especialidades y Doctores --}}
        @canany(['crear especialidades', 'actualizar especialidades', 'crear doctores', 'editar doctores', 'actualizar doctores'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Personal
                                médico</label>
                        </div>
                    </div>
                    {{-- Especialidades --}}
                    @canany(['crear especialidades', 'actualizar especialidades'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.specialties.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                especialidades</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Doctores --}}
                    @canany(['crear doctores', 'actualizar doctores'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.doctors.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                doctores</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                </div>
            </div>
        @endcanany

        {{-- Paises y Ciudades --}}
        @canany(['crear paises', 'actualizar paises', 'crear ciudades', 'actualizar ciudades'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Ubicaciones</label>
                        </div>
                    </div>
                    {{-- Países --}}
                    @canany(['crear paises', 'actualizar paises'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.countries.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                países</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Ciudades --}}
                    @canany(['crear ciudades', 'actualizar ciudades'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.cities.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                ciudades</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                </div>
            </div>
        @endcanany

        {{-- Instituciones --}}
        @canany(['crear aseguradoras', 'actualizar aseguradoras', 'crear hospitales', 'actualizar hospitales'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Instituciones</label>
                        </div>
                    </div>
                    {{-- Aseguradoras --}}
                    @canany(['crear aseguradoras', 'actualizar aseguradoras'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.insurance-carriers.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                aseguradoras</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Hospitales --}}
                    @canany(['crear hospitales', 'actualizar hospitales'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.hospitals.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                hospitales</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    <div class="col">
                        <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                            <div class="row mx-auto ">
                                <div class="col-10 d-flex align-items-center">
                                    <div class="col p-0 d-flex justify-content-end">
                                        <div class="bg-white d-flex justify-content-center"
                                            style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col p-0 d-flex justify-content-end">
                                    <div class="bg-danger d-flex justify-content-center"
                                        style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                        <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endcanany

        {{-- Patologias || Medicinas || Dosis --}}
        @canany(['crear dosis', 'actualizar dosis', 'crear medicinas', 'actualizar medicinas', 'crear patologias', 'actualizar patologias'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Tratamiento</label>
                        </div>
                    </div>
                    {{-- Patologías --}}
                    @canany(['crear patologias', 'actualizar patologias'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.pathologies.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                patologías</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Medicinas --}}
                    @canany(['crear medicinas', 'actualizar medicinas'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.medicines.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                medicinas</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Dosis --}}
                    @canany(['crear dosis', 'actualizar dosis'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.doses.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">
                                                Administrar dosis
                                            </label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                </div>
            </div>
        @endcanany

        {{-- Laboratorios --}}
        @canany(['crear laboratorios', 'actualizar laboratorios', 'crear pruebas', 'actualizar pruebas'])
            <div class="col-6 p-0 mb-5">
                <div class="row-reverse">
                    <div class="col mb-4">
                        <div class="card-header bg-danger py-0">
                            <label for="" class="text-white font-weight-bold lead text-uppercase">Laboratorios</label>
                        </div>
                    </div>
                    @canany(['crear laboratorios', 'actualizar laboratorios'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.laboratories.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for="" class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">
                                                Administrar laboratorios
                                            </label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                    {{-- Pruebas --}}
                    @canany(['crear pruebas', 'actualizar pruebas'])
                        <div class="col mb-1">
                            <a href="{{ route('admin.diagnostic-tests.index') }}">
                                <div class="card-body bg-white shadow p-0" style="border-bottom: solid #901E1D 1px;">
                                    <div class="row mx-auto ">
                                        <div class="col-10 d-flex align-items-center">
                                            <label for=""
                                                class="title font-weight-normal m-0 mb-2 link_card_danger text-capitalize">Administrar
                                                pruebas</label>
                                        </div>
                                        <div class="col p-0 d-flex justify-content-end">
                                            <div class="bg-danger d-flex justify-content-center"
                                                style="max-height: 50px; max-width: 50px; min-height: 50px; min-width: 50px; ">
                                                <img src="{{ url('svg/6.svg') }}" alt="" class="w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcanany
                </div>
            </div>
        @endcanany

    </div>


    <style>
        #tracking_pharmacy {
            cursor: pointer;

        }

        #tracking_pharmacy:hover {
            border: 2px solid #EF5630;
        }

    </style>

    <style>
        .title {
            font-size: 1.4em;
        }

        .cardhome {
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }

    </style>


    <style>
        #tracking_pharmacy {
            cursor: pointer;

        }

        #tracking_pharmacy:hover {
            border: 2px solid #EF5630;
        }

    </style>

    <style>
        .title {
            font-size: 1.4em;
        }

        .cardhome {
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }

    </style>


    <style>
        .cardhome {
            cursor: pointer;
        }

        .cardhome:hover {
            border-color: #DF982C;
            border-width: 1px;
            border-style: solid;
        }


        .cardhome2 {
            cursor: pointer;
        }

        .cardhome2:hover {
            border-color: #CB5032;
            border-width: 1px;
            border-style: solid;
        }


        .cardhome3 {
            cursor: pointer;
        }

        .cardhome3:hover {
            border-color: #1D61A6;
            border-width: 1px;
            border-style: solid;
        }

        .cardhome4 {
            cursor: pointer;
        }

        .cardhome4:hover {
            border-color: #901E1D;
            border-width: 1px;
            border-style: solid;
        }

    </style>
@endsection
