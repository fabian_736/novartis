<?php

namespace App\Http\Requests\Medicine;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'between:4,100', 'regex:/^[a-záéíóúüñ ]+/i', Rule::unique('medicines', 'name')->ignore($this->medicine)],
            'doses' => ['required', 'array', 'min:1'],
            'doses.*' => ['required', 'exists:doses,id'],
            'is_active' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'doses' => 'Presentaciones',
            'is_active' => 'Estado',
        ];
    }
}
