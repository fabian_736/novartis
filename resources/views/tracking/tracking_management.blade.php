@extends('layouts.app')
@section('content')
    <script src="{{ asset('js/tracking_management/tracking_management.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/tracking_management/tracking_management.css') }}">
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h3">Seguimiento</label><br>
                <label for="" class="text-orange h4">Editar</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <ul>
        @foreach ($errors->all() as $error)
            <script>
                Swal.fire({
                    icon: 'error',
                    title: '{{ $error }}',
                })
            </script>
        @endforeach
    </ul>

    @if (session('messagee'))
        <div class="alert alert-success" role="alert">
            <span>Seguimiento creado correctamente</span>
        </div>
    @endif
    @if (session('edit'))
        <script>
            Swal.fire(
                'Seguimiento actualizado',
                'Los  datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif
    <div class="card-header mt-4">
        <nav class="nav nav-pills nav-justified">
            <div class="row">
                <div id="btn_general" class="col-auto">
                    <a id="a1" href="#" class="text-decoration-none color-orange" style="cursor: pointer">
                        <div class="row">
                            <div class="col-auto">
                                <div id="a_general" style="width: 50px; height: 50px;" class="rounded-circle bg-orange p-2 d-flex justify-content-center align-items-center">
                                    <span class="iconify text-white" data-icon="gg:search-loading" data-width="35"></span>
                                </div>
                            </div>
                            <div class="col d-flex align-items-center">
                                <label for="" class="h4 font-weight-bold">General</label>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="btn_comunication" class="col-auto">
                    <a id="a2" href="#" class="text-decoration-none text-gray" style="cursor: pointer;">
                        <div class="row">
                            <div class="col-auto">
                                <div id="a_Comunicaciónes" style="width: 50px; height: 50px;" class="bg-gray text-gray rounded-circle p-2 d-flex justify-content-center align-items-center">
                                    <span class="iconify" data-icon="gg:search-loading" data-width="35"></span>
                                </div>
                            </div>
                            <div class="col d-flex align-items-center">
                                <label for="" class="h4 font-weight-bold">Comunicaciónes</label>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="btn_paciente" class="col-auto">
                    <a id="a3" href="#" class="text-decoration-none text-gray" style="cursor: pointer;">
                        <div class="row">
                            <div class="col-auto">
                                <div id="a_paciente" style="width: 50px; height: 50px;" class="bg-gray text-gray rounded-circle p-2 d-flex justify-content-center align-items-center">
                                    <span class="iconify" data-icon="carbon:user-avatar" data-width="35"></span>
                                </div>
                            </div>
                            <div class="col d-flex align-items-center">
                                <label for="" class="h4 font-weight-bold">Datos: {{$tracking->patient->name}}</label>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="card-body">
        <div class="row-reverse" style="overflow-y: none; max-height: 60vh">
            <div class="col">
                <form action="{{ route('tracking.management_update', isset($tracking->patient) ? $tracking->patient : $patient) }}" method="PATCH">
                    @csrf
                    @method('PATCH')
                    <div id="col_general">
                        @include('tracking.form_tracking')
                        <div class="modal-footer">
                            <button class="btn btn-secondary"><b>Salir</b></button>
                            <button class="btn btn-primary">Guardar cambios</button>
                        </div>
                    </div>
                    <div id="col_comunication" style="display: none">
                        <div class="row">
                            <div class="col">
                                @include('tracking.table_tracking')
                            </div>
                        </div>
                    </div>
                    <div class="text-center" id="col_paciente" style="display:none">
                        <table class="table table-striped">
                            <thead>
                                <th class="font-weight-bold">PAP</th>
                                <th class="font-weight-bold">Nombre</th>
                                <th class="font-weight-bold">Documento</th>
                                <th class="font-weight-bold">Cuidador</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$tracking->patient->name}}</td>
                                    <td>{{$tracking->patient->pap}}</td>
                                    <td>{{$tracking->patient->document_number}}</td>
                                    <td>{{$tracking->patient->carer_name}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <style>
        .color-orange {
            color: #EF5630;
        }
    </style>
@endsection
