<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('carer_age');
            $table->string('patient_status')->default(null)->change();
            $table->after('patient_status', function (Blueprint $table) {
                $table->dateTime('inactivated_at')->nullable();
                $table->dateTime('suspended_at')->nullable();
                $table->string('suspension_reason')->nullable();
                $table->dateTime('reactivated_at')->nullable();
                $table->dateTime('outside_of_program_since')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn([
                'inactivated_at',
                'suspended_at',
                'suspension_reason',
                'reactivated_at',
                'outside_of_program_since',
            ]);

            $table->enum('patient_status', ['Activo', 'Inscrito', 'Abandono', 'Fuera del programa', 'Reactivado', 'Rechazado'])->default('Inscrito')->change();
            $table->integer('carer_age')->nullable()->after('carer_birth_date');
        });
    }
};
