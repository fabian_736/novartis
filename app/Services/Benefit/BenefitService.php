<?php

namespace App\Services\Benefit;

use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Enums\Patient\PatientTypes;
use App\Exceptions\UserCannotCreatedBenefitException;
use App\Models\Benefit;
use App\Models\Patient;
use Illuminate\Http\UploadedFile;

class BenefitService
{
    public function createNewBenefit(Patient $patient): Benefit
    {
        if (! $patient->treatment()->exists()) {
            throw new UserCannotCreatedBenefitException('El beneficio no pudo ser creado porque el paciente no tiene registrado su tratamiento', 422);
        }

        if ($patient->lastBenefit()->exists() && $patient->lastBenefit?->status != BenefitStatus::Cerrado->value) {
            throw new UserCannotCreatedBenefitException('El paciente tiene un beneficio activo, aún no se puede crear un nuevo beneficio', 422);
        }

        $attributes = [
            'patient_id' => $patient->id,
        ];

        if ($patient->patient_type == PatientTypes::Insured) {
            $attributes['benefit_type'] = BenefitTypes::Copago;
            $attributes['status'] = BenefitStatus::CartaPendiente;
        } else {
            $attributes['benefit_type'] = BenefitTypes::OOP;
            $attributes['status'] = BenefitStatus::FacturaPendiente;
        }

        return Benefit::create($attributes);
    }

    /**
     * Creates a new benefit.
     *
     * @param  array<string, mixed>  $input
     * @return Benefit
     */
    public function store(array $input): Benefit
    {
        return Benefit::create($input);
    }

    /**
     * Updates the given benefit
     *
     * @param  Benefit  $benefit
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(Benefit $benefit, array $input): bool
    {
        return $benefit->update($input);
    }

    /**
     * Uploads the invoice to the given benefit
     *
     * @param  Benefit  $benefit
     * @param  UploadedFile  $file
     * @return void
     */
    public function uploadInvoice(Benefit $benefit, UploadedFile $file)
    {
        $benefit->addmedia($file)
            ->usingName('invoice')
            ->toMediaCollection('invoice');
    }
}
