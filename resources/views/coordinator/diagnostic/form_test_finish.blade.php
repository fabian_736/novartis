@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-orange h2">Prueba diagnóstica</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    <x-errores />
    <div class="row-reverse ">
        <div class="col px-0 py-5">
            <form action="{{ route('test.update', $managementDiagnosticTest->id) }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="laboratory_id" class="h5 font-weight-bold title_patient_cordi">PAP </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->pap }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="medicine_id" class="h5 font-weight-bold title_patient_cordi">Número de documento</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->document_number }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="laboratory_id" class="h5 font-weight-bold title_patient_cordi">Nombre </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->name }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="medicine_id" class="h5 font-weight-bold title_patient_cordi">Medicamento</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->patient->treatment?->pathology->medicine->name ?? 'Sin medicamento' }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="laboratory_id" class="h5 font-weight-bold title_patient_cordi">Laboratorio </label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->laboratory->name }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="diagnostic_test_id" class="h5 font-weight-bold title_patient_cordi">Prueba a realizar</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->diagnosticTest->name }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="management_date" class="h5 font-weight-bold title_patient_cordi">Fecha de coordinación</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->management_date }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="invoice_number" class="h5 font-weight-bold title_patient_cordi">Número de factura</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ isset($managementDiagnosticTest->invoice_number) ? $managementDiagnosticTest->invoice_number : 'Sin datos' }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="test_date" class="h5 font-weight-bold title_patient_cordi">Fecha de la prueba</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ isset($managementDiagnosticTest->test_date) ? $managementDiagnosticTest->test_date : 'Sin datos' }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="test_price" class="h5 font-weight-bold title_patient_cordi">Valor de la prueba</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ isset($managementDiagnosticTest->test_price) ? $managementDiagnosticTest->test_price : 'Sin datos' }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-6">
                        <div class="row">
                            <div class="col">
                                <label for="status" class="h5 font-weight-bold title_patient_cordi">Estado de la prueba</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $managementDiagnosticTest->status_decode }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-4 mx-auto ">
                        <div class="row d-flex align-items-center justify-content-center">
                            <div class="col col-6">
                                <a href="{{ route('management-diagnostic-tests.index') }}"
                                    class="btn btn-orange w-100 d-flex align-items-center justify-content-center"><span
                                        class="iconify mr-2" data-icon="fa:close" data-width="15"></span>SALIR</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
