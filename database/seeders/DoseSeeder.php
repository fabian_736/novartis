<?php

namespace Database\Seeders;

use App\Models\Dose;
use App\Models\Medicine;
use App\Models\MedicineDose;
use App\Models\Pathology;
use App\Models\PathologyHasMedicineDose;
use Illuminate\Database\Seeder;

class DoseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();

        Medicine::insert([
            [
                'id' => 1,
                'name' => 'Cosentyx',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 2,
                'name' => 'Xolair',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);

        Dose::insert([
            [
                'id' => 1,
                'presentation' => '75mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 2,
                'presentation' => '150mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 3,
                'presentation' => '225mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 4,
                'presentation' => '300mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 5,
                'presentation' => '375mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 6,
                'presentation' => '450mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 7,
                'presentation' => '525mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 8,
                'presentation' => '600mg',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);

        MedicineDose::insert([

            // COSENTYX
            [
                'id' => 1,
                'medicine_id' => 1,
                'dose_id' => 2,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 2,
                'medicine_id' => 1,
                'dose_id' => 4,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            // XOLAIR
            [
                'id' => 3,
                'medicine_id' => 2,
                'dose_id' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 4,
                'medicine_id' => 2,
                'dose_id' => 2,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 5,
                'medicine_id' => 2,
                'dose_id' => 3,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 6,
                'medicine_id' => 2,
                'dose_id' => 4,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 7,
                'medicine_id' => 2,
                'dose_id' => 5,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 8,
                'medicine_id' => 2,
                'dose_id' => 6,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 9,
                'medicine_id' => 2,
                'dose_id' => 7,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 10,
                'medicine_id' => 2,
                'dose_id' => 8,
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);

        Pathology::insert([
            [
                'id' => 1,
                'medicine_id' => 1,
                'name' => 'Artritis Psoriásica',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 2,
                'medicine_id' => 1,
                'name' => 'Psoriasis',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 3,
                'medicine_id' => 1,
                'name' => 'Espondilitis Anquilosante',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 4,
                'medicine_id' => 2,
                'name' => 'Asma alérgica severa',
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 5,
                'medicine_id' => 2,
                'name' => 'Urticaria crónica espontánea',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);

        PathologyHasMedicineDose::insert([
            [
                'id' => 1,
                'pathology_id' => 1,
                'medicine_dose_id' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 2,
                'pathology_id' => 1,
                'medicine_dose_id' => 2,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 3,
                'pathology_id' => 2,
                'medicine_dose_id' => 2,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 4,
                'pathology_id' => 3,
                'medicine_dose_id' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 5,
                'pathology_id' => 4,
                'medicine_dose_id' => 3,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 6,
                'pathology_id' => 4,
                'medicine_dose_id' => 4,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 7,
                'pathology_id' => 4,
                'medicine_dose_id' => 5,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 8,
                'pathology_id' => 4,
                'medicine_dose_id' => 6,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 9,
                'pathology_id' => 4,
                'medicine_dose_id' => 7,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 10,
                'pathology_id' => 4,
                'medicine_dose_id' => 8,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 11,
                'pathology_id' => 4,
                'medicine_dose_id' => 9,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 12,
                'pathology_id' => 4,
                'medicine_dose_id' => 10,
                'created_at' => $now,
                'updated_at' => $now,
            ],

            [
                'id' => 13,
                'pathology_id' => 5,
                'medicine_dose_id' => 6,
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}
