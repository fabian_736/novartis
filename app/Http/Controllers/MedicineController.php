<?php

namespace App\Http\Controllers;

use App\Http\Requests\Medicine\StoreRequest;
use App\Http\Requests\Medicine\UpdateRequest;
use App\Http\Resources\Medicine\MedicineResource;
use App\Models\Medicine;
use App\Services\Dose\DoseService;
use App\Services\Medicine\MedicineService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MedicineController extends Controller
{
    use ApiResponses;

    /** @var MedicineService */
    private $service;

    /** @var DoseService */
    private $doseService;

    public function __construct(MedicineService $medicineService, DoseService $doseService)
    {
        $this->service = $medicineService;
        $this->doseService = $doseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], Medicine::class);

        $doses = $this->doseService->list();
        $medicines = $this->service->list(false);

        return view('supervisor.medicine.list', compact('medicines', 'doses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', Medicine::class);
            $medicine = $this->service->store($request->validated());

            return $this->resourceResponse(
                message: 'Medicina creada correctamente.',
                data: new MedicineResource($medicine),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  Medicine  $medicine
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Medicine $medicine)
    {
        if ($request->ajax()) {
            $this->authorize('update', $medicine);

            $medicine->load('doses');

            return $this->resourceResponse(
                message: '',
                data: new MedicineResource($medicine)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Medicine  $medicine
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Medicine $medicine)
    {
        if ($request->ajax()) {
            $this->authorize('update', $medicine);
            $this->service->update($medicine, $request->validated());
            $medicine->refresh();

            return $this->resourceResponse(
                message: 'Medicina actualizada correctamente.',
                data: new MedicineResource($medicine)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Returns the doses associated with a medicine.
     *
     * @param  Medicine  $medicine
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Dose>
     */
    public function getDoses(Medicine $medicine)
    {
        return $medicine->doses;
    }
}
