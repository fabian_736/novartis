<?php

namespace App\Models;

use App\Traits\SoftDeletesParent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperFormulation
 */
class Formulation extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory, SoftDeletes, SoftDeletesParent;

    protected $fillable = [
        'frequency',
        'starts_at',
        'ends_at',
        'treatment_id',
        'pathology_has_medicine_dose_id',
    ];

    /**
     * Scope a query to only include active formulations.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Retrieves the treatment.
     *
     * @return BelongsTo<Treatment, Formulation>
     */
    public function treatment(): BelongsTo
    {
        return $this->belongsTo(Treatment::class);
    }

    /**
     * Retrieves the pathology has medicine dose.
     *
     * @return BelongsTo<PathologyHasMedicineDose, Formulation>
     */
    public function pathologyHasMedicineDose(): BelongsTo
    {
        return $this->belongsTo(PathologyHasMedicineDose::class);
    }
}
