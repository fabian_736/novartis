<?php

namespace Database\Seeders;

use App\Models\DocumentType;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentType::insert(
            [
                ['id' => 1, 'name' => 'DNI'],
                ['id' => 2, 'name' => 'Cédula'],
                ['id' => 3, 'name' => 'Pasaporte'],
            ]
        );
    }
}
