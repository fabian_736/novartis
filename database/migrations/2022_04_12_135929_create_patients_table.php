<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string('document_number')->nullable()->comment('Numero de documento');
            $table->foreignId('document_type_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->string('name')->comment('Nombre del paciente');
            $table->date('birth_date')->nullable()->comment('Fecha de nacimiento');
            $table->enum('gender', ['Male', 'Female'])->nullable()->comment('Genero del paciente');
            $table->string('email')->nullable()->comment('Correo del paciente');
            $table->string('phone')->nullable()->comment('Celular del paciente');
            $table->boolean('patient_type')->nullable()->comment('Tipo de paciente');
            $table->boolean('first_time_using_biological')->nullable()->comment('¿usa biologicos?');
            $table->boolean('has_carer')->nullable()->comment('Posee un cuidador 1. Si 0. No');
            $table->enum('relationship_patient', ['Padre', 'Madre', 'Hermano/a', 'Enfermero/a', 'Otro/a'])->nullable()->comment('Cuidador');

            $table->foreignId('city_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->foreignId('hospital_id')->nullable()->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->foreignId('insurance_carrier_id')->nullable()->constrained()->restrictOnUpdate()->restrictOnDelete();

            //CUIDADOR
            $table->string('carer_document_number')->nullable()->comment('N.documento cuidador');
            $table->string('carer_name')->nullable()->comment('Nombre del cuidador');
            $table->date('carer_birth_date')->nullable()->comment('Fecha de nacimiento cuidador');
            $table->integer('carer_age')->nullable()->comment('Edad del cuidador');
            $table->string('carer_phone')->nullable()->comment('Telefono del cuidador');
            $table->string('carer_email')->nullable()->comment('Correo del cuidador');
            $table->string('pap')->unique()->comment('')->nullable();
            $table->enum('patient_status', ['Activo', 'Inscrito', 'Abandono', 'Fuera del programa', 'Reactivado', 'Rechazado'])->default('Inscrito');

            $table->foreignId('carer_document_type_id')->nullable()->constrained('document_types')->restrictOnUpdate()->restrictOnDelete();

            $table->timestamp('registered_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
};
