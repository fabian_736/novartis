<?php

namespace App\Http\Requests\PathologyHasMedicineDose;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'pathology_id' => 'required',
            'medicine_dose_id' => 'required',
        ];
    }
}
