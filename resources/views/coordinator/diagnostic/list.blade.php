@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-success h2">Consultar pruebas diagnósticas de pacientes</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>
    @if (session('message'))
        <script>
            Swal.fire(
                'Buen trabajo',
                'Los datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif

    <div class="col d-flex justify-content-end mt-3">
        <a href="{{ url('/portal') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
            <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
        </a>&nbsp;&nbsp;&nbsp;
        @can('crear pruebas', \App\Models\ManagementDiagnosticTest::class)
            <button type="button" class="btn bg-success d-flex align-items-center" data-bs-toggle="modal" data-bs-target="#crearPrueba">
                <span class="iconify mr-2" data-icon="carbon:add-alt" data-width="20"></span>
                Crear prueba
            </button>
        @endcan
    </div>

    <div class="row-reverse mt-5 table" style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-success">
                <div class="row">
                    <div class="col-sm-8">
                        <label for="" class="h3 m-0 text-white">Listado de pruebas</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-0">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-success">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">N° Paciente</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">País</th>
                        <th scope="col">Tipo de paciente</th>
                        <th scope="col">Estado del paciente</th>
                        <th scope="col">Estado</th>
                        @can('actualizar pruebas')
                            <th class="text-center" scope="col">Acción</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach ($managementDiagnosticTests as $key => $managementDiagnosticTest)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $managementDiagnosticTest->patient->pap }}</td>
                            <td>{{ $managementDiagnosticTest->patient->name }}</td>
                            <td>{{ $managementDiagnosticTest->patient->city->country->name }}</td>
                            <td>{{ $managementDiagnosticTest->patient->patient_type->label() }}</td>
                            <td>{{ $managementDiagnosticTest->patient->patient_status->value }}</td>
                            <td>{{ $managementDiagnosticTest->status_decode }}</td>
                            @can('actualizar pruebas', $managementDiagnosticTest)
                                <td class="text-center">
                                    <div>
                                        <a type="button" class="btn bg-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Prueba diagnostica" href="{{ route('management-diagnostic-test', $managementDiagnosticTest) }}">
                                            @if ($managementDiagnosticTest->status == 0)
                                                <span class="iconify" data-icon="clarity:eye-solid"data-width="12"></span>
                                            @else
                                                <span class="iconify" data-icon="bxs:edit"></span>
                                            @endif
                                        </a>
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <style>
        .btnColor:hover {
            background-color: #4CAF50 !important;
        }
    </style>

    {{-- Modal crear prueba diagnostica --}}
    <div class="modal fade" id="crearPrueba" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col mb-3">
                            <div class="row bg-success p-2 mx-auto">
                                <div class="col ">
                                    <label for="" class="text-white font-weight-bold h5 m-0">Crear prueba diagnóstica</label>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <span class="iconify cursor-pointer text-white"
                                        data-icon="ant-design:close-circle-filled" data-width="30" data-bs-dismiss="modal"
                                        aria-label="Close"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="{{ route('new.management-diagnostic-test') }}" method="POST" id="create-test" enctype="multipart/form-data">
                                @csrf
                                <br>
                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="patient_id" class="lead text-success font-weight-bold">Paciente</label>
                                            </div>
                                            <div class="col">
                                                <select name="patient_id" id="patient_id" class="form-control" required autocomplete="off" required>
                                                    <option value="" selected disabled>Seleccione un paciente...</option>
                                                    @foreach ($patients as $patient)
                                                        <option value="{{ $patient->id }}">{{ $patient->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="file" class="lead text-success font-weight-bold">Orden médica</label>
                                            </div>
                                            <div class="col">
                                                <label for="file" class="btn btn-white btn-sm" style="border: solid #000 2px; font-weight: 700; font-size: 14px; color: #000">
                                                    <span class="iconify" data-icon="entypo:upload-to-cloud"></span>
                                                    SELECCIONAR ARCHIVO
                                                </label>
                                                <input accept="image/*,.pdf" class="input-file" size="1" name="file" id="file" type="file" style="display: none" />
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                        <label class="d-flex justify-content-center" id="archivo1" for="file"></label><br>
                                    </div>
                                </div>
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-dark w-25" type="submit">
                                            <img src="{{ url('svg/15.svg') }}" alt="" class="mr-2">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/landing/form2_validate.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common = {
            "Content-Type": "Application/json",
            "Accepts": "Application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": document.querySelector("meta[name=\"csrf-token\"]").getAttribute("content")
        };


        //Funcion modal create
        const form = document.getElementById('create-test');

        const create = e => {
            e.preventDefault();
            const data = new FormData(form);
            axios.post('management-diagnostic-tests-new', data)
                .then((response) => {
                    Swal.fire({
                            icon: 'success',
                            type: 'success',
                            position: 'center',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: 'Cerrar'
                        })
                        .then((data) => {
                            location.reload();
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: 'Ocurrió un error al validar sus datos',
                        text: Object.hasOwnProperty.call(error.response.data, 'errors') ? '' : error.response.data.message || error.response.data.error,
                        confirmButtonColor: '#221F1F',
                    });

                    Array.from(form.elements).forEach(input => {
                        if (input.classList.contains('form-control')) {
                            input.classList.remove('is-invalid');
                            input.nextElementSibling.textContent = '';
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, 'errors')) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = form.elements[key];
                                input.classList.add('is-invalid');
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }
        form.addEventListener('submit', create);
        //Fin función modal create
    </script>
@endsection
