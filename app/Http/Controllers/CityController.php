<?php

namespace App\Http\Controllers;

use App\Http\Requests\City\StoreRequest;
use App\Http\Requests\City\UpdateRequest;
use App\Http\Resources\City\CityResource;
use App\Models\City;
use App\Models\Country;
use App\Services\City\CityService;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CityController extends Controller
{
    use ApiResponses;

    /** @var CityService */
    private $cityService;

    public function __construct(CityService $cityService)
    {
        $this->cityService = $cityService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorizeAny(['create', 'update'], City::class);

        $countries = Country::active()
            ->get();

        $cities = $this->cityService->list(false);

        return view('supervisor.city.list', compact('countries', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if ($request->ajax()) {
            $this->authorize('create', City::class);
            $city = $this->cityService->store($request->validated());

            return $this->resourceResponse(
                message: 'Ciudad creada correctamente.',
                data: new CityResource($city),
                code: Response::HTTP_CREATED
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  City  $city
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, City $city)
    {
        if ($request->ajax()) {
            $this->authorize('update', $city);

            return $this->resourceResponse(
                message: '',
                data: new CityResource($city)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  City  $city
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, City $city)
    {
        if ($request->ajax()) {
            $this->authorize('update', $city);
            $this->cityService->update($city, $request->validated());
            $city->refresh();

            return $this->resourceResponse(
                message: 'Ciudad actualizada correctamente.',
                data: new CityResource($city)
            );
        } else {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Returns an associated country.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, Country>
     */
    public function getCountries()
    {
        return Country::active()->get();
    }
}
