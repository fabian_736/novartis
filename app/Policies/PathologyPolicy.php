<?php

namespace App\Policies;

use App\Models\Pathology;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PathologyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear patologias', 'actualizar patologias'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Pathology  $pathology
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function view(User $user, Pathology $pathology)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear patologias')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Pathology  $pathology
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, Pathology $pathology)
    {
        if ($user->hasPermissionTo('actualizar patologias')) {
            return true;
        }
    }
}
