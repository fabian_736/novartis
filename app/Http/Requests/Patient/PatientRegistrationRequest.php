<?php

namespace App\Http\Requests\Patient;

use App\Enums\Gender;
use App\Enums\Patient\PatientTypes;
use App\Enums\RelationshipPatient;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatientRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'benefit_option' => array_filter([
                $this->benefit_type,
                $this->diagnostic_test,
            ]),
        ]);
    }

    /**
     * Returns the validations for the object.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $minDate = now()->subCentury()->toDateString();
        $fecha = now()->subYears(18)->toDateString();

        $validations = [
            'document_type_id' => 'required|exists:document_types,id',
            'document_number' => 'required|alpha_num|unique:patients,document_number',
            'name' => ['required', 'regex:/^[a-záéíóúüñ ]+/i'],
            'birth_date' => "required|date|after_or_equal:$minDate|before_or_equal:today",
            'gender' => ['required', new Enum(Gender::class)],
            'email' => 'nullable|email:rfc,dns|unique:patients,email',
            'phone' => 'required|numeric|digits_between:7,10',
            'patient_type' => ['required', new Enum(PatientTypes::class)],
            'city_id' => ['required', Rule::exists('cities', 'id')->where('country_id', $this->country->id)],
            'has_carer' => 'required|boolean',
            'diagnostic_test' => 'sometimes|boolean',
            'frequency' => 'nullable|numeric|integer|min:1',
            'benefit_option' => 'required|array|min:1',
            'firm' => 'required|string',
        ];

        if ($this->patient_type == PatientTypes::Insured->value) {
            $validations['benefit_type'] = 'sometimes|in:Copago';
            $validations['hospital_id'] = 'exclude';
            $validations['insurance_carrier_id'] = ['required', Rule::exists('insurance_carriers', 'id')->where('country_id', $this->country->id)];
        } else {
            $validations['benefit_type'] = 'sometimes|in:OOP';
            $validations['hospital_id'] = ['required', Rule::exists('hospitals', 'id')->where('country_id', $this->country->id)];
            $validations['insurance_carrier_id'] = 'exclude';
        }

        if ($this->benefit_type == 'OOP' || $this->benefit_type == 'Copago') {
            $validations['medicine_id'] = 'required|exists:medicines,id';
            $validations['pathology_id'] = 'required|exists:pathologies,id';
            $validations['dose_id'] = 'required|exists:pathology_has_medicine_doses,id';
            $validations['first_time_using_biological'] = 'required|in:1,0,2';
        }

        if ($this->diagnostic_test == 1) {
            $validations['diagnostic_test_order1'] = 'required|file|mimes:png,jpg,jpeg,pdf|max:800000';
            $validations['diagnostic_test_order2'] = 'nullable|file|mimes:png,jpg,jpeg,pdf|max:800000';
            $validations['diagnostic_test_order3'] = 'nullable|file|mimes:png,jpg,jpeg,pdf|max:800000';
        } else {
            $validations['diagnostic_test_order1'] = 'exclude';
            $validations['diagnostic_test_order2'] = 'exclude';
            $validations['diagnostic_test_order3'] = 'exclude';
        }

        if ($this->has_carer == 1) {
            $validations['carer_document_type_id'] = 'required|exists:document_types,id';
            $validations['carer_document_number'] = 'required|alpha_num';
            $validations['carer_name'] = ['required', 'regex:/^[a-záéíóúüñ ]+/i'];
            $validations['carer_birth_date'] = "required|date|after_or_equal:$minDate|before_or_equal:$fecha";
            $validations['relationship_patient'] = ['required', new Enum(RelationshipPatient::class)];
            $validations['carer_email'] = 'nullable|email:rfc,dns';
            $validations['carer_phone'] = 'required|numeric|digits_between:7,9';
        } else {
            $validations['relationship_patient'] = 'exclude';
            $validations['carer_document_type_id'] = 'exclude';
            $validations['carer_document_number'] = 'exclude';
            $validations['carer_name'] = 'exclude';
            $validations['carer_birth_date'] = 'exclude';
            $validations['carer_email'] = 'exclude';
            $validations['carer_phone'] = 'exclude';
        }

        return $validations;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return string[]
     */
    public function attributes()
    {
        return [
            'benefit_option' => 'Beneficio',
            'document_type_id' => 'Tipo de documento',
            'document_number' => 'Número de identificación',
            'name' => 'Nombre',
            'benefit_type' => 'Beneficio',
            'birth_date' => 'Fecha de nacimiento',
            'gender' => 'Genero',
            'email' => 'Correo electrónico',
            'phone' => 'Número de contacto',
            'patient_type' => 'Tipo de paciente',
            'first_time_using_biological' => '¿Usa biológicos?',
            'city_id' => 'Ciudad',
            'has_carer' => 'Posee cuidador',
            'insurance_carrier_id' => 'Nombre de la aseguradora',
            'hospital_id' => 'Hospital',
            'dose_id' => 'Dosis',
            'frequency' => 'Frecuencia ',
            'pathology_id' => 'Patología',
            'medicine_id' => 'Medicina',

            'diagnostic_test_order1' => 'Orden médica',
            'diagnostic_test_order2' => 'Orden médica',
            'diagnostic_test_order3' => 'Orden médica',

            'carer_document_type_id' => 'Tipo de identificación del paciente',
            'carer_document_number' => 'Número de identificación del paciente',
            'carer_name' => 'Nombre del paciente',
            'carer_birth_date' => 'Fecha de nacimiento del paciente',
            'carer_email' => 'Correo electrónico del paciente',
            'carer_phone' => 'Número de contacto del paciente',
        ];
    }

    public function messages()
    {
        return [
            'diagnostic_test_order1.required' => 'Por favor adjunta una orden medica',
            'carer_birth_date.before_or_equal' => 'El cuidador debe ser mayor de edad',
            'carer_birth_date.after_or_equal' => 'El cuidador debe ser menor de 100 años',
            'birth_date.after_or_equal' => 'El usuario debe ser mayor de edad',
            'benefit_option.required' => 'El Beneficio es requerido',
            'benefit_option.min' => 'Debe tener minimo un Beneficio',
        ];
    }
}
