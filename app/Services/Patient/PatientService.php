<?php

namespace App\Services\Patient;

use App\Enums\Benefit\BenefitTypes;
use App\Enums\Patient\PatientStatus;
use App\Enums\Patient\PatientTypes;
use App\Models\Patient;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ValidatedInput;

class PatientService
{
    /**
     * Returns a list of patients.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, Patient>
     */
    public static function list()
    {
        return Patient::get();
    }

    /**
     * Creates a new patient
     *
     * @param  ValidatedInput  $input
     * @return Patient
     */
    public function enroll(ValidatedInput $input): Patient
    {
        $input = $input->merge([
            'date_admission' => now(),
        ]);

        if ($input->patient_type == PatientTypes::NotInsured && $input->benefit_type == BenefitTypes::OOP->value) {
            $input = $input->merge([
                'patient_status' => PatientStatus::Active,
            ]);
        } else {
            $input = $input->merge([
                'patient_status' => PatientStatus::InAccess,
            ]);
        }

        $attributes = $input->only([
            'document_type_id',
            'document_number',
            'name',
            'birth_date',
            'gender',
            'email',
            'phone',
            'patient_type',
            'first_time_using_biological',
            'has_carer',
            'city_id',
            'insurance_carrier_id',
            'hospital_id',
            'relationship_patient',
            'carer_document_type_id',
            'carer_document_number',
            'carer_name',
            'carer_birth_date',
            'carer_email',
            'carer_phone',
            'date_admission',
            'patient_status',
        ]);

        return Patient::create($attributes);
    }

    /**
     * Generate the consent for the given patient
     *
     * @param  Patient  $patient
     * @param  string  $signature
     * @return \Spatie\MediaLibrary\MediaCollections\Models\Media
     */
    public function generateConsent(Patient $patient, string $signature)
    {
        $view = match ($patient->city->country->name) {
            'Ecuador' => 'landing.pdf.ecuador',
            default => 'landing.pdf.peru'
        };
        $pdf = Pdf::loadView($view, compact('patient', 'signature'))->output();

        return $patient->addMediaFromStream($pdf)
            ->usingFileName('consent.pdf')
            ->usingName('consent')
            ->toMediaCollection('consent');
    }

    /**
     * Undocumented function.
     *
     * @param  mixed  $patient
     * @param  array  $input
     * @return bool
     */
    public static function update($patient, array $input)
    {
        return $patient->update($input);
    }

    /**
     * store the given file in storage
     *
     * @param  Patient  $patient
     * @param  UploadedFile|null  $file
     * @return \Spatie\MediaLibrary\MediaCollections\Models\Media|null
     */
    public function uploadConsent(Patient $patient, ?UploadedFile $file)
    {
        if ($file === null) {
            return null;
        }

        return $patient->addMedia($file)
            ->usingFileName('consent.pdf')
            ->usingName('consent')
            ->toMediaCollection('consent');
    }

    /**
     * Deletes the given patient
     *
     * @param  Patient  $patient
     * @return bool|null
     */
    public function delete(Patient $patient): ?bool
    {
        $deleted = false;

        DB::transaction(function () use ($patient, &$deleted) {
            $this->update($patient, [
                'patient_status' => PatientStatus::Inactive,
                'inactivated_at' => now(),
            ]);
            $deleted = $patient->delete();
        });

        return $deleted;
    }

    /**
     * Restores the given patient
     *
     * @param  Patient  $patient
     * @return bool|null
     */
    public function restore(Patient $patient): ?bool
    {
        return $patient->restore();
    }
}
