<?php

namespace App\Rules\Corrections\Benefits;

use App\Enums\Benefit\BenefitStatus;
use App\Enums\Benefit\BenefitTypes;
use App\Models\Benefit;
use Illuminate\Contracts\Validation\Rule;

class NewStatusIsValid implements Rule
{
    /**
     * The benefit instance
     *
     * @var Benefit
     */
    private Benefit $benefit;

    /**
     * The benefit type
     *
     * @var string
     */
    private string $benefitType;

    /**
     * Create a new rule instance.
     *
     * @param  Benefit  $benefit
     * @param  string  $benefitType
     * @return void
     */
    public function __construct(Benefit $benefit, $benefitType)
    {
        $this->benefit = $benefit;
        $this->benefitType = $benefitType;
    }

    /**
     * Determine if the benefit status can change to the given value.
     * Validation will pass if the given value is a previous status of the current status.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $newBenefitStatus = BenefitStatus::from(intval($value, 10));
        $currentBenefitStatus = BenefitStatus::from($this->benefit->status);

        // If the benefit should be reopened, it will validate if not exists another open benefit
        if (
            $newBenefitStatus != BenefitStatus::Cerrado &&
            $currentBenefitStatus == BenefitStatus::Cerrado &&
            $this->benefit->patient->lastBenefit?->isNot($this->benefit) &&
            $this->benefit->patient->lastBenefit->status != BenefitStatus::Cerrado->value
        ) {
            return false;
        }

        // Disallows that OOP benefit has a Copago benefit status
        if (
            $this->benefitType == BenefitTypes::OOP->value &&
            in_array($newBenefitStatus, [BenefitStatus::CartaPendiente, BenefitStatus::ProcesoObservado, BenefitStatus::DescuentoEspecialRechazado])
        ) {
            return false;
        }

        // This allows change from Copago benefit in CartaPendiente, ProcesoObservado or DescuentoEspecialRechazado to OOP benefit
        if (
            $this->benefitType == BenefitTypes::OOP->value &&
            in_array($currentBenefitStatus, [BenefitStatus::CartaPendiente, BenefitStatus::ProcesoObservado, BenefitStatus::DescuentoEspecialRechazado]) &&
            $newBenefitStatus == BenefitStatus::FacturaPendiente
        ) {
            return true;
        }

        $validPreviousStatus = match ($newBenefitStatus) {
            BenefitStatus::CartaPendiente => BenefitStatus::cases(),
            BenefitStatus::ProcesoObservado => [BenefitStatus::ProcesoObservado, BenefitStatus::DescuentoEspecialRechazado, BenefitStatus::FacturaPendiente, BenefitStatus::FacturaCargada, BenefitStatus::Cerrado],
            BenefitStatus::DescuentoEspecialRechazado, BenefitStatus::FacturaPendiente => [BenefitStatus::DescuentoEspecialRechazado, BenefitStatus::FacturaPendiente, BenefitStatus::FacturaCargada, BenefitStatus::Cerrado],
            BenefitStatus::FacturaCargada => [BenefitStatus::FacturaCargada, BenefitStatus::Cerrado],
            BenefitStatus::Cerrado => [BenefitStatus::Cerrado],
        };

        return in_array($currentBenefitStatus, $validPreviousStatus);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El beneficio no cumple con los requisitos para cambiar al :attribute seleccionado.';
    }
}
