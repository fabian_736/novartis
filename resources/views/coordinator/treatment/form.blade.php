@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-orange h2">Tratamiento</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>

        <div class="row">
            <div class="col ">
                <div class="row">
                    <div class="col-auto d-flex align-items-center ">
                        <div style="width: 50px; height: 50px; "
                            class="border border-orange d-flex justify-content-center align-items-center">
                            <div style="width: 40px; height: 40px; "
                                class="bg-orange  d-flex justify-content-center align-items-center">
                                <img src="{{ url('svg/Person.svg') }}" alt="" class="w-75">
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex align-items-center">
                        <label for="" class="h3 text-orange ">Tratamiento</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul>
        @foreach ($errors->all() as $error)
            <script>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '{{ $error }}',
                })
            </script>
        @endforeach
    </ul>

    @if (session('message'))
        <div class="alert alert-success" role="alert">
            Tratamiento editado con exito
        </div>
    @endif
    <div class="row-reverse ">
        <div class="col px-0 py-5">
            <form action="{{ route('patients.treatment', $patient) }}" method="post">
                @csrf
                @method('PATCH')
                <ul class="li_form">
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Producto</label>
                                    </div>
                                    <div class="col">
                                        <select name="medicine_id" id="medicine" class="form-control">
                                            <option selected disabled value="">Selecciona una opción...</option>
                                            @foreach ($medicines as $medicine)
                                                <option @selected($patient->treatment?->currentFormulation->pathologyHasMedicineDose->medicineDose->medicine_id == $medicine->id) value="{{ $medicine->id }}">{{ $medicine->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Patología / Diagnóstico</label>
                                    </div>
                                    <div class="col">
                                        <select name="pathology_id" id="pathology" class="form-control">
                                            @foreach ($pathologies as $pathology)
                                                <option @selected($patient->treatment?->currentFormulation->pathologyHasMedicineDose->pathology_id == $pathology->id) value="{{ $pathology->id }}">{{ $pathology->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Dosis</label>
                                    </div>
                                    <div class="col">
                                        <select name="pathology_has_medicine_dose_id" id="dose" class="form-control">
                                            @forelse ($doses??[] as $dose)
                                                <option @selected($patient->treatment?->currentFormulation->pathologyHasMedicineDose->medicine_dose_id == $dose->id) value="{{ $dose->id }}">{{ $dose->dose->presentation }}</option>
                                            @empty
                                                <option value="" disabled selected>Seleccione una patología y medicamento</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Frecuencia</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="frequency" id="frequency" value="{{ $patient->treatment?->currentFormulation->frequency }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Primera vez usando
                                            biológicos</label>
                                    </div>
                                    <div class="col-9">
                                        <select name="first_time_using_biological" id="first_time_using_biological" class="form-control">
                                            <option @selected($patient->first_time_using_biological == '1') value="1">Si</option>
                                            <option @selected($patient->first_time_using_biological == '0') value="0">No</option>
                                            <option @selected($patient->first_time_using_biological == '2') value="2">Desconoce</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Médico</label>
                                    </div>
                                    <div class="col">
                                        <select required name="doctor_id" id="doctor" class="js-states form-control">
                                            <option value="" selected disabled>Seleccione una opción...</option>
                                            @foreach ($doctors as $doctor)
                                                <option @selected($patient->treatment?->doctor_id == $doctor->id) value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Especialidad</label>
                                    </div>
                                    <div class="col">
                                        <select required name="" id="especialidad" class="form-control">
                                            <option value="" disabled selected>Seleccione una especialidad...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="" class="h5 font-weight-bold title_patient_cordi">Observaciones</label>
                                    </div>
                                    <div class="col-9">
                                        <textarea name="observations" id="observations" cols="30" rows="5" class="form-control">{{ $patient->treatment?->observations }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="list-style: none">
                        <div class="row mb-3">
                            <div class="col-4 mx-auto">
                                <div class="row">
                                    <div class="col">
                                        <a href="{{ url('patients') }}"
                                            class="btn btn-orange w-100 d-flex align-items-center justify-content-center"><span
                                                class="iconify mr-2" data-icon="fa:close" data-width="15"></span>
                                            SALIR
                                        </a>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-dark w-100 d-flex align-items-center justify-content-center" type="submit"><span
                                                class="iconify mr-2" data-icon="entypo:save" data-width="15"></span>
                                            GUARDAR
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
        <style>
            /* LISTA INACTIVA */

            .li_form .inactive {
                position: relative;
                list-style-type: none;
            }

            .li_form .inactive:before {
                content: "";
                position: absolute;
                top: 3px;
                left: -30px;
                width: 20px;
                height: 20px;
                background-image: url('/svg/li/square_inactive.svg');
            }


            /* LISTA ACTIVA */

            .li_form .active {
                position: relative;
                list-style-type: none;
            }

            .li_form .active:before {
                content: "";
                position: absolute;
                top: 3px;
                left: -30px;
                width: 20px;
                height: 20px;
                background-image: url('/svg/li/square_active.svg');
            }


            #button1_patient_coordinator {
                display: none;
            }

            #button1_patient_coordinator+label:before {
                content: "\f0c8";
                color: #fff;
                background-color: #fff;
                border: 1px solid #EF5630;
                box-sizing: border-box;
                padding-inline: 5px;
                font-size: 25px;
                cursor: pointer;
            }

            #button1_patient_coordinator:checked+label:before {
                content: "\f0c8";
                color: #EF5630;
                background: #EF5630;
                border: 1px solid #EF5630;
                box-sizing: border-box;
                padding-inline: 5px;
                font-size: 25px;
                cursor: pointer;


            }

            #button2_patient_coordinator {
                display: none;
            }

            #button2_patient_coordinator+label:before {
                content: "\f0c8";
                color: #fff;
                background-color: #fff;
                border: 1px solid #EF5630;
                box-sizing: border-box;
                padding-inline: 5px;
                font-size: 25px;
                cursor: pointer;
            }

            #button2_patient_coordinator:checked+label:before {
                content: "\f0c8";
                color: #EF5630;
                background: #EF5630;
                border: 1px solid #EF5630;
                box-sizing: border-box;
                padding-inline: 5px;
                font-size: 25px;
                cursor: pointer;
            }

            .title_patient_cordi {
                color: #EF5630;
            }
        </style>

        <script>
            function showContent_cordi() {
                bottom1 = document.getElementById("button1_patient_coordinator");
                bottom2 = document.getElementById("button2_patient_coordinator");

                if (bottom1.checked == true) {
                    $('#cuidador_cordi').show();
                    $('#cuidador_cordi2').show();
                    $('#title_posee').addClass('col').removeClass('col-3');
                    $('#checkbox_posee').addClass('col').removeClass('col-4');
                    bottom2.checked = false;

                } else {
                    $('#cuidador_cordi').hide();
                    $('#cuidador_cordi2').hide();
                    $('#title_posee').addClass('col-3').removeClass('col');
                    $('#checkbox_posee').addClass('col-4').removeClass('col');

                }
            }

            function showContent_cordi2() {
                bottom1 = document.getElementById("button1_patient_coordinator");
                bottom2 = document.getElementById("button2_patient_coordinator");

                if (bottom2.checked == true) {
                    $('#cuidador_cordi').hide();
                    $('#cuidador_cordi2').hide();
                    $('#title_posee').addClass('col-3').removeClass('col');
                    $('#checkbox_posee').addClass('col-4').removeClass('col');
                    bottom1.checked = false;

                }
            }

            $(function() {
                $('#doctor').on('change', onselectmedico);
                $('#medicine').on('change', onSelectmedicineChange);
                onselectmedico();
            })

            function onselectmedico() {
                const medico = $('#doctor').val();
                $.get(`/api/doctor/${medico}`, function(data) {
                    if (data.id != null) {
                        var option = '<option value="' + data.id + '">' + data.name + '</option>';
                        $('#especialidad').html(option)
                    } else {
                        var option = '<option value="">Seleccione un médico</option>';
                        $('#especialidad').html(option)
                    }
                })
            }

            //cargar patologias y dosis
            function onSelectmedicineChange() {
                const medicine = $(this).val();
                const dose = medicine;
                $.get(`/api/medicine/${medicine}/pathologies`, function(data) {
                    pathologies = data;
                    let option = '<option selected disabled>Seleccione una patologia...</option>';
                    pathologies.forEach(pathology => {
                        option += `<option value="${pathology.id}">${pathology.name}</option>`;
                    });

                    $('#pathology').html(option);
                    $('#dose').html('<option selected disabled>Seleccione una dosis...</option>');
                })
            }

            $('#pathology').on('change', onSelectpathologyChange);

            function onSelectpathologyChange() {
                const pathology = document.getElementById("pathology").value;
                const medicine = document.getElementById("medicine").value;
                $.get(`/api/pathology/${pathology}/medicine/${medicine}/doses`, function(medicineDoses) {
                    let option = '<option selected disabled>Seleccione una dosis...</option>';
                    medicineDoses.forEach(medicineDose => {
                        option += `<option value="${medicineDose.pivot.id}">${medicineDose.dose.presentation}</option>`;
                    });
                    $('#dose').html(option)
                })
            }
        </script>
    </div>
@endsection
