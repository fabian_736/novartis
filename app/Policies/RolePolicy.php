<?php

namespace App\Policies;

use App\Models\Admin\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function viewAny(User $user)
    {
        if ($user->hasAnyPermission(['crear roles', 'actualizar roles', 'eliminar roles', 'restaurar roles', 'eliminar roles permanentemente', 'asignar permisos a roles'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function create(User $user)
    {
        if ($user->hasPermissionTo('crear roles')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Role  $role
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function update(User $user, Role $role)
    {
        if ($user->hasPermissionTo('actualizar roles')) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function asignPermission(User $user)
    {
        if ($user->hasPermissionTo('asignar permisos a roles')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Role  $role
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function delete(User $user, Role $role)
    {
        if ($user->hasPermissionTo('eliminar roles')) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Role  $role
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function restore(User $user, Role $role)
    {
        if (! $role->trashed()) {
            return false;
        }

        if ($user->hasPermissionTo('restaurar roles')) {
            return true;
        }
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Admin\Role  $role
     * @return \Illuminate\Auth\Access\Response|bool|void
     */
    public function forceDelete(User $user, Role $role)
    {
        if (! $role->trashed()) {
            return false;
        }

        if ($user->hasPermissionTo('eliminar roles permanentemente')) {
            return true;
        }
    }
}
