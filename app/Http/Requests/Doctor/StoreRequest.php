<?php

namespace App\Http\Requests\Doctor;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'between:3,100', 'regex:/^[a-záéíóúüñ ]+/i'],
            'specialty_id' => ['required', 'exists:specialties,id'],
            'country_id' => ['required', 'exists:countries,id'],
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'specialty_id' => 'Especialidad',
            'country_id' => 'País',
        ];
    }
}
