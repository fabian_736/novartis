<?php

namespace Database\Factories;

use App\Models\MedicineDose;
use App\Models\Pathology;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PathologyHasMedicineDose>
 */
class PathologyHasMedicineDoseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'pathology_id' => Pathology::factory(),
            'medicine_dose_id' => MedicineDose::factory(),
        ];
    }
}
