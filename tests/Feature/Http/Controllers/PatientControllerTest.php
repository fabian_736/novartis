<?php

namespace Tests\Feature\Http\Controllers;

use App\Enums\Gender;
use App\Enums\Patient\PatientStatus;
use App\Enums\Patient\PatientTypes;
use App\Models\Admin\Permission;
use App\Models\Benefit;
use App\Models\City;
use App\Models\Country;
use App\Models\DocumentType;
use App\Models\Formulation;
use App\Models\InsuranceCarrier;
use App\Models\ManagementDiagnosticTest;
use App\Models\Patient;
use App\Models\Tracking;
use App\Models\Treatment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PatientControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_user_can_view_the_patient_list()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'actualizar pacientes']);
        $user->givePermissionTo($permission);

        Permission::create(['name' => 'eliminar pacientes']);
        Permission::create(['name' => 'restaurar pacientes']);

        /** @var Patient */
        $patients = Patient::factory()->count(10)->create();

        $this->actingAs($user)
            ->get(route('patients.list_patients'))
            ->assertOk()
            ->assertViewIs('coordinator.list_patient')
            ->assertViewHas('patients', $patients);
    }

    public function test_user_can_update_a_patient()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'actualizar pacientes']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        $country = Country::factory()->create();
        $documentType = DocumentType::factory()->create();
        $city = City::factory()->for($country)->create();
        $insuranceCarrier = InsuranceCarrier::factory()->create();

        $patientDataUpdate = [
            'document_type_id' => $documentType->id,
            'document_number' => '3654161',
            'name' => 'Prueba',
            'city_id' => $city->id,
            'country_id' => $country->id,
            'birth_date' => '1989-10-10',
            'gender' => Gender::male->value,
            'email' => 'prueba@test.com',
            'phone' => '123456789',
            'patient_type' => PatientTypes::Insured->value,
            'has_carer' => 0,
            'patient_status' => PatientStatus::Active->value,
            'date_admission' => '2022-07-15',
            'insurance_carrier_id' => $insuranceCarrier->id,
        ];

        $this->actingAs($user)
            ->patch(route('patients.update_patient', $patient), $patientDataUpdate)
            ->assertRedirect()
            ->assertSessionDoesntHaveErrors();

        $patient->refresh();

        $this->assertEquals($patientDataUpdate['document_type_id'], $patient->document_type_id);
        $this->assertEquals($patientDataUpdate['document_number'], $patient->document_number);
        $this->assertEquals($patientDataUpdate['name'], $patient->name);
        $this->assertEquals($patientDataUpdate['city_id'], $patient->city_id);
        $this->assertEquals($patientDataUpdate['birth_date'], $patient->birth_date);
        $this->assertEquals($patientDataUpdate['gender'], $patient->gender);
        $this->assertEquals($patientDataUpdate['email'], $patient->email);
        $this->assertEquals($patientDataUpdate['phone'], $patient->phone);
        $this->assertEquals($patientDataUpdate['patient_type'], $patient->patient_type->value);
        $this->assertEquals($patientDataUpdate['has_carer'], $patient->has_carer);
        $this->assertEquals($patientDataUpdate['patient_status'], $patient->patient_status->value);
        $this->assertEquals($patientDataUpdate['date_admission'], $patient->date_admission);
        $this->assertEquals($patientDataUpdate['insurance_carrier_id'], $patient->insurance_carrier_id);
    }

    public function test_user_cannot_update_a_patient()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'actualizar pacientes']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->create();

        $patientDataUpdate = [
            'email' => 'prueba@test.com',
        ];

        $this->actingAs($user)
            ->patch(route('patients.update_patient', $patient), $patientDataUpdate)
            ->assertInvalid();

        $patient->refresh();

        $this->assertNotEquals($patientDataUpdate['email'], $patient->email);
    }

    public function test_user_can_delete_a_patient()
    {
        Storage::fake('local');

        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'eliminar pacientes']);
        $user->givePermissionTo($permission);

        $files = [];

        /** @var Patient */
        $patient = Patient::factory()->create();
        $files[] = $patient->addMedia(UploadedFile::fake()->create('consent.pdf', 10))->toMediaCollection('consent')->getPath();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->create();

        /** @var Formulation */
        $formulations = Formulation::factory()->count(2)->for($treatment)->create();

        /** @var Benefit */
        $benefits = Benefit::factory()->count(2)->for($patient)->create();
        $benefits->each(function ($benefit) use (&$files) {
            $files[] = $benefit->addMedia(UploadedFile::fake()->image('invoice.png'))->toMediaCollection('invoice')->getPath();
        });

        /** @var ManagementDiagnosticTest */
        $diagnosticTests = ManagementDiagnosticTest::factory()->count(2)->for($patient)->create();
        $diagnosticTests->each(function ($diagnosticTest) use (&$files) {
            $files[] = $diagnosticTest->addMedia(UploadedFile::fake()->image('test.jpg'))->toMediaCollection('file-upload')->getPath();
        });

        /** @var Tracking */
        $trackings = Tracking::factory()->count(2)->for($patient)->create();

        $this->actingAs($user)
            ->deleteJson(uri: route('patients.delete', ['patient' => $patient]), headers: self::$HEADERS)
            ->assertNoContent();

        $patient->refresh();
        $this->assertSoftDeleted($patient);
        $this->assertEquals(PatientStatus::Inactive, $patient->patient_status);
        $this->assertNotNull($patient->inactivated_at);

        $this->assertNotNull($treatment->fresh()->parent_deleted_at);

        $formulations->each(function ($formulation) {
            $this->assertNotNull($formulation->fresh()->parent_deleted_at);
        });

        $benefits->each(function ($benefit) {
            $this->assertNotNull($benefit->fresh()->parent_deleted_at);
        });

        $diagnosticTests->each(function ($diagnosticTest) {
            $this->assertNotNull($diagnosticTest->fresh()->parent_deleted_at);
        });

        $trackings->each(function ($tracking) {
            $this->assertNotNull($tracking->fresh()->parent_deleted_at);
        });

        foreach ($files as $path) {
            $this->assertFileExists($path);
        }
    }

    public function test_user_can_restore_a_patient()
    {
        /** @var User */
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'restaurar pacientes']);
        $user->givePermissionTo($permission);

        /** @var Patient */
        $patient = Patient::factory()->trashed()->create();

        /** @var Treatment */
        $treatment = Treatment::factory()->for($patient)->trashed()->create();

        /** @var Formulation */
        $formulations = Formulation::factory()->count(2)->for($treatment)->trashed()->create();

        /** @var Benefit */
        $benefits = Benefit::factory()->count(2)->for($patient)->trashed()->create();

        /** @var ManagementDiagnosticTest */
        $diagnosticTests = ManagementDiagnosticTest::factory()->count(2)->for($patient)->trashed()->create();

        /** @var Tracking */
        $trackings = Tracking::factory()->count(2)->for($patient)->trashed()->create();

        $this->actingAs($user)
            ->patchJson(uri: route('patients.restore', ['patient' => $patient]), headers: self::$HEADERS)
            ->assertOk();

        $this->assertNotSoftDeleted($patient);
        $this->assertNull($treatment->fresh()->parent_deleted_at);
        $formulations->each(function ($formulation) {
            $this->assertNull($formulation->fresh()->parent_deleted_at);
        });
        $benefits->each(function ($benefit) {
            $this->assertNull($benefit->fresh()->parent_deleted_at);
        });
        $diagnosticTests->each(function ($diagnosticTest) {
            $this->assertNull($diagnosticTest->fresh()->parent_deleted_at);
        });
        $trackings->each(function ($tracking) {
            $this->assertNull($tracking->fresh()->parent_deleted_at);
        });
    }
}
