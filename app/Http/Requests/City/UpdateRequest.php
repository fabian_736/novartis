<?php

namespace App\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'between:4,100', 'regex:/^[a-záéíóúüñ ]+/i'],
            'country_id' => 'required',
            'is_active' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'country_id' => 'País',
            'is_active' => 'Estado',
        ];
    }
}
