@extends('layouts.landing.app')
@section('content')
    <div class="row-reverse" style="min-height: 100vh">
        <div class="col d-flex justify-content-center align-items-end" style="min-height: 50vh">
            <img id="formulario" src="{{ url('img/logo_novartis.png') }}" alt="">
        </div>
        <div class="col d-flex justify-content-center align-items-center" style="min-height: 30vh">
            <progress id="progreso">Progreso</progress>
        </div>
    </div>

    <style>
        progress {
            width: 80%;
            border: 5px solid #DDDDDD;
            border-radius: 40px;
        }


        progress::-moz-progress-bar {
            background: #0C68B0;
            width: 0;
            transition: width 3.5s linear;
            border-radius: 40px;
        }

        progress::-webkit-progress-bar {
            background: #0C68B0;
            width: 0;
            transition: width 3.5s linear;
            border-radius: 40px;
        }

        .llenandose::-webkit-progress-bar {
            width: 100%;
        }

        .llenandose::-moz-progress-bar {
            width: 100%;
        }

        #formulario {
            min-width: 60%;
            max-height: 60%;
        }
    </style>



    <script>
        animacion = function() {
            $("#formulario").fadeTo(500, .1)
                .fadeTo(500, 1)
        }
        setInterval(animacion, 800);
        setTimeout(function() {
            window.location = "{{ route('landing.show_select_country_view') }}";
        }, 3000);

        window.onload = function() {
            document.querySelector('#progreso').className = 'llenandose';
        }
    </script>
@endsection
