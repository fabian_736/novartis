<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('document_number');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->boolean('is_active')->default(1)->comment('1. Activo | 0 .Inactivo');
            $table->boolean('is_admin')->default(0)->comment('1. Superadmin | 0. User');

            //Llaves foraneas
            $table->foreignId('city_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->foreignId('document_type_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->foreignId('pharmacy_id')->nullable()->constrained()->restrictOnUpdate()->restrictOnDelete();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
