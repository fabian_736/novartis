@auth
    <div class="sidebar" style="background-color: #1D61A6">
        <div class="logo">
            <div class="row mx-auto">
                <div class="col p-0">
                    <a title="Volver al home" href="{{ url('/portal') }}" class="simple-text logo-mini">
                        <span class="iconify text-white" data-width='23' data-icon="ant-design:home-filled"></span>
                    </a>
                </div>
                <div class="col d-flex justify-content-end">
                    <a href="javascript:;" class="simple-text logo-normal ">
                        <img src="{{ url('svg/2.svg') }}" alt="">
                    </a>
                </div>
            </div>
        </div>

        <div class="sidebar-wrapper ">
            <div class="user px-4">
                <div class="container">
                    <div class="d-flex justify-content-center h-100">
                        <div class="image_outer_container">
                            <div class="green_icon"></div>
                            <div class="image_inner_container">
                                <img src="{{ Auth::user()->getFirstMediaUrl('banner_user') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-reverse  text-center">
                    <div class="col pl-0">
                        <label class="h5 text-white font-weight-bold text-capitalize">{{ Auth::user()->name }}</label><br>
                        @if (!empty(Auth::user()->pharmacy->pharmacyChain))
                            <label class="h6 text-white font-weight-bold text-capitalize">{{ Auth::user()->roles[0]->name ?? '' }} / {{ Auth::user()->pharmacy->pharmacyChain->name }}</label>
                            <br>
                        @else
                            <label class="h6 text-white font-weight-bold text-capitalize">{{ Auth::user()->roles[0]->name ?? '' }} </label>
                        @endif
                        @if (Auth::user()->city->country->name == 'Ecuador')
                            <img style="border-radius: 100%" width="18px" height="18px" src="{{ asset('img/country/ecuador.png') }}" alt="Logo del país">
                        @elseif (Auth::user()->city->country->name == 'Perú')
                            <img width="18px" height="18px" src="{{ asset('img/country/peru.png') }}" alt="Logo del país">
                        @else
                            <img width="20px" height="20px" src="{{ asset('img/country/global2.png') }}" alt="Logo del país">
                        @endif
                    </div>
                </div>
            </div>
            <div class="user px-4">

                <div class="row">
                    <div class="col d-flex justify-content-center mb-5">
                        <img src="{{ url('svg/3.svg') }}" alt="" dat="modal" data-target="#exampleModalCenter" style="cursor: pointer">
                        <a href="javascript:;" dat="modal" data-target="#exampleModalCenter" class="text-white">Mis notificaciones</a>
                    </div>
                    <div class="{{ request()->is('portal') ? 'col d-flex justify-content-center mt-5' : 'd-none' }} ">
                        <form class="mt-5" method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a title="Cerrar sesión" class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                                <h5><b class="text-white"><span class="iconify mr-2" data-icon="ri:logout-circle-line"></span>Cerrar sesión</b></h5>
                            </a>
                            <a target="_blanck" href="https://peoplemarketing.com/inicio/">
                                <b class="text-white"> People By Overall</b>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
            <div style="margin:auto" class="row w-100 {{ request()->is('portal') ? 'bg-dark d-none' : 'bg-white' }}">
                <div class="col mt-auto ">
                    <ul class="nav  w-100 ">
                        <li class="nav-item ">
                            <div class="mb-5">
                                @canany(['asignar permisos a roles', 'crear pacientes', 'actualizar pacientes', 'crear usuarios', 'actualizar usuarios', 'crear permisos', 'actualizar permisos', 'eliminar permisos', 'eliminar permisos permanentemente', 'crear roles', 'actualizar roles', 'eliminar roles', 'eliminar roles permanentemente', 'crear cadenas', 'actualizar cadenas', 'crear farmacias', 'actualizar farmacias', 'crear especialidades', 'actualizar especialidades', 'crear doctores', 'actualizar doctores', 'crear paises', 'actualizar paises', 'crear ciudades', 'actualizar ciudades', 'crear aseguradoras', 'actualizar aseguradoras', 'crear hospitales', 'actualizar hospitales', 'crear medicinas', 'actualizar medicinas', 'crear patologias', 'actualizar patologias'])
                                    <div class="d-flex justify-content-center">
                                        <a href="{{ route('admin.management.index') }}" style="background: rgba(239, 86, 48, 0.1);" class="btn font-weight-bold ml-3 text-orange w-100">
                                            <b><span class="iconify mr-2" data-icon="icon-park-outline:setting-two"></span>Administración</b>
                                        </a>
                                    </div>
                                @endcanany
                                @canany(['ver paciente', 'crear pacientes', 'actualizar pacientes'])
                                    <div class="d-flex justify-content-center">
                                        <a href="{{ route('patients.list_patients') }}" style="background: rgba(239, 86, 48, 0.1);" class="btn font-weight-bold ml-3 text-orange w-100">
                                            <b><span class="iconify mr-2" data-icon="carbon:user-multiple"></span>Paciente</b>
                                        </a>
                                    </div>
                                @endcanany
                                @canany(['crear beneficios', 'actualizar beneficios', 'cerrar beneficios', 'crear compras', 'actualizar compras', 'crear cartas', 'actualizar cartas'])
                                    <div class=" d-flex justify-content-center">
                                        <a href="{{ route('benefits.benefits.index') }}" style="background: rgba(12, 104, 176, 0.1);" class="w-100 text-primary ml-3 btn">
                                            <b><span class="iconify mr-2" data-icon="dashicons:index-card"></span>Beneficio</b>
                                        </a>
                                    </div>
                                @endcanany
                                @canany(['crear pruebas', 'actualizar pruebas'])
                                    <div class=" d-flex justify-content-center">
                                        <a href="{{ route('management-diagnostic-tests.index') }}" style="background: rgba(12, 104, 176, 0.1);" class="w-100 text-success ml-3 btn">
                                            <b><span class="iconify mr-2" data-icon="ion:documents"></span>Pruebas diagnosticas</b>
                                        </a>
                                    </div>
                                @endcanany
                                @canany(['crear reembolsos', 'actualizar reembolsos'])
                                    <div class=" d-flex justify-content-center">
                                        <a href="#" style="background: rgba(238, 154, 49, 0.1);" class="w-100 text-orange ml-3 font-weight-bold  btn ">
                                            <b style="color:#EE9A31;"><span class="iconify mr-2" data-icon="fa6-solid:dollar-sign"></span>Reembolso</b>
                                        </a>
                                    </div>
                                @endcanany
                                @can(['ver reportes'])
                                    <div class=" d-flex justify-content-center mb-5 text-center">
                                        <a href="{{ route('reports.index') }}" style="background: rgba(12, 104, 176, 0.1);" class="w-100 text-primary ml-3 font-weight-bold  btn ">
                                            <b><span class="iconify mr-2" data-icon="wpf:statistics"></span>Reportes</b>
                                        </a>
                                    </div>
                                @endcan
                            </div>
                            <hr>
                            <div class="d-flex justify-content-center">
                                <form class="" method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <a title="Cerrar sesión" class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                                        <h5><b><span class="iconify mr-2" data-icon="ri:logout-circle-line"></span>Cerrar sesión</b></h5>
                                    </a>
                                    <a class="ml-4" target="_blanck" href="https://peoplemarketing.com/inicio/">
                                        <b> People By Overall</b>
                                    </a>
                                </form>
                            </div>

                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end align-items-center">
                    <a href="javascript:;" class="btn-close text-dark" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="fa-2x">&times;</span>
                    </a>
                </div>
                <div class="modal-body pt-0">
                    <div class="row-reverse ">
                        <div class="col">
                            <div class="card shadow-xl p-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-bell fa-2x"></i>
                                    </div>
                                    <div class="col">
                                        <label>Titulo de la notificacion</label>
                                        <p>Descripcion de la notificacion</p>
                                    </div>
                                    <div class="col-1">
                                        <i class="fas fa-times-circle fa-2x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="card shadow-xl p-5">
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fas fa-bell fa-2x"></i>
                                    </div>
                                    <div class="col">
                                        <label>Titulo de la notificacion</label>
                                        <p>Descripcion de la notificacion</p>
                                    </div>
                                    <div class="col-1">
                                        <i class="fas fa-times-circle fa-2x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="card shadow-xl p-5">
                                <div class="row">
                                    <div class="col-1 d-flex justify-content-center align-items-center">
                                        <i class="fas fa-bell fa-2x"></i>
                                    </div>
                                    <div class="col">
                                        <label>Titulo de la notificacion</label>
                                        <p>Descripcion de la notificacion</p>
                                    </div>
                                    <div class="col-1 d-flex justify-content-center align-items-center">
                                        <i class="fas fa-times-circle fa-2x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .item_box {
            border-radius: 40px !important;
        }

        .item_nav {
            cursor: pointer !important;
        }

        .item_nav:hover {
            color: #1D61A6 !important;
            font-weight: bold !important;
            text-decoration-line: underline !important;
            font-size: 18px;
        }

        .overflow-hidden {
            overflow: hidden !important;
        }

        .div_list {
            border-radius: 10px;
            background: rgba(239, 86, 48, 0.1);
            cursor: pointer;
        }

        html,
        body {
            height: 100%;
        }

        .container {
            height: 100%;
            align-content: center;
        }

        .image_outer_container {
            margin-top: auto;
            margin-bottom: auto;
            border-radius: 50%;
            position: relative;
        }

        .image_inner_container {
            border-radius: 50%;
            padding: 5px;
            background: orange;
            /* background: -webkit-linear-gradient(to bottom, #fcb045, #fd1d1d, #833ab4);
                                                            background: linear-gradient(to bottom, #fcb045, #fd1d1d, #833ab4); */
        }

        .image_inner_container img {
            height: 120px;
            width: 120px;
            border-radius: 50%;
            border: 5px solid white;
        }

        .image_outer_container .green_icon {
            background-color: #4cd137;
            position: absolute;
            right: 15px;
            bottom: 10px;
            height: 20px;
            width: 20px;
            border: 5px solid white;
            border-radius: 50%;
        }
    </style>
@endauth
