@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col ">
            <div class="row mx-auto">
                <div class="d-flex align-items-center" style="width: 100px">
                    <label for="" class="text-primary font-weight-bold h2">PSP |</label>
                </div>
                <div class="col d-flex align-items-center p-0 pt-2">
                    <label for="" class="h5" style="color: #0C68B0">Programa seguimiento <br> de
                        pacientes</label>
                </div>
            </div>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <img src="{{ url('img/2.png') }}" alt="">
        </div>
    </div>

    <br>
    <div class="container mt-3 col-sm-10 m-auto mr-4 text-danger">
        <h1 class="text-center">
            Asignar Rol
        </h1>
    </div>

    <h3 class="text-center text-capitalize text-danger mb-4">{{ request()->user->name }}</h3>

    <form action="" method="post">
        @csrf
        @foreach ($roles as $role)
            <div class="row mt-5 card p-5 ">
                <div class="accordion" id="accordionExample">
                    <div>
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4><b>{{ $role->name }}</b></h4>
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <table id="example" class="table table-striped" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><input class="mr-3" id="selectall" type="checkbox" name="" id=""></th>
                                            <th>Descripción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($permissions as $permission)
                                            <tr>
                                                <td class="text-center"><input class="mr-3 option" type="checkbox" name="roles[]" id="" @checked($role->hasPermissionTo($permission)) value="{{ $permission->id }}"></td>
                                                <td>{{ $permission->name }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row d-flex justify-content-center mt-5 mb-3">
            <button class="btn btn-primary" type="submit">Guardar</button>
        </div>
    </form>

    <script>
        $("#selectall").on("click", function() {
            $(".option").prop("checked", this.checked);
        });

        $(".option").on("click", function() {
            if ($(".option").length == $(".option:checked").length) {
                $("#selectall").prop("checked", true);
            } else {
                $("#selectall").prop("checked", false);
            }
        });
    </script>
    <style>
        * {
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }

    </style>
@endsection
