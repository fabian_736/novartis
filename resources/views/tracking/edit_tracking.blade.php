@extends('layouts.app')
@section('content')
    <script src="{{ asset('js/tracking_management/tracking_management.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/tracking_management/tracking_management.css') }}">
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h3">Seguimiento</label><br>
                <label for="" class="text-orange h4">Editar</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    @if (session('message'))
        <script>
            Swal.fire(
                'Paciente actualizado',
                'Los  datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif
    @if (session('edit'))
        <script>
            Swal.fire(
                'Seguimiento actualizado',
                'Los  datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif
    <ul>
        @foreach ($errors->all() as $error)
            <script>
                Swal.fire({
                    icon: 'error',
                    title: '{{ $error }}',
                })
            </script>
        @endforeach
    </ul>
    <div class="row-reverse mt-5 " style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-orange">
                <label for="" class="h3 m-0 text-white">Seguimiento</label>
            </div>
        </div>
        <div class="col d-flex justify-content-end mt-3">

            <a href="{{ url('/portal') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>
        </div>
    </div>
    <form action="{{ route('tracking.management_update', $tracking->id) }}" method="PATCH">
        @csrf
        @method('PATCH')
        @include('tracking.form_tracking')
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-dark"><span class="iconify mr-2" data-icon="entypo:save"></span>Guardar cambios</button>
        </div>
    </form>
@endsection
