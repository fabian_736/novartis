<?php

namespace App\Services\Laboratory;

use App\Models\Laboratory;

class LaboratoryService
{
    /**
     * Returns a list of laboratories.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Laboratory>
     */
    public function list($onlyActive = true)
    {
        $laboratories = Laboratory::query();

        if ($onlyActive) {
            $laboratories->active();
        }

        return $laboratories->get();
    }

    /**
     * Create a Pharmacy.
     *
     * @param  array  $input
     * @return Laboratory
     */
    public function store(array $input): Laboratory
    {
        return Laboratory::create($input);
    }

    public function update(Laboratory $laboratory, array $input): bool
    {
        return $laboratory->update($input);
    }
}
