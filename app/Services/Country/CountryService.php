<?php

namespace App\Services\Country;

use App\Models\Country;

class CountryService
{
    /**
     * Returns a list of countries.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, Country>
     */
    public function list($onlyActive = true)
    {
        $countries = Country::query();

        if ($onlyActive) {
            $countries->active();
        }

        return $countries->get();
    }

    /**
     * Create a Country.
     *
     * @param  array  $input
     * @return Country
     */
    public function store(array $input): Country
    {
        return Country::create($input);
    }

    public function update(Country $country, array $input): bool
    {
        return $country->update($input);
    }
}
