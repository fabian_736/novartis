<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('benefits', function (Blueprint $table) {
        //     $table->enum('autorized', ['Aprobado','Observada','Pendiente', 'Rechazado'])->change()->default('Pendiente')->comment('Estado de la carta de autorizacion');
        // });
        DB::statement("ALTER TABLE `benefits` CHANGE `autorized` `autorized` ENUM('Aprobado','Observada','Pendiente', 'Rechazado') DEFAULT 'Pendiente'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('benefits', function (Blueprint $table) {
        //     $table->dropColumn('autorized');
        // });
        DB::statement("ALTER TABLE `benefits` CHANGE `autorized` `autorized` ENUM('Aprobado','Observada','Pendiente', 'Rechazado') DEFAULT 'Pendiente'");
    }
};
