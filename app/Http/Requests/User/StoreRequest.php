<?php

namespace App\Http\Requests\User;

use App\Enums\UserTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'document_type_id' => 'required',
            'document_number' => ['required', 'unique:users,document_number', 'digits_between:5,16'],
            'name' => ['required', 'string', 'between:4,100', 'regex:/^[a-záéíóúüñ ]+/i'],
            'email' => 'required|email:rfc,dns|unique:users,email',
            'address' => ['required', 'string', 'between:16,255'],
            'user_type' => ['required', 'string', new Enum(UserTypes::class)],
            'phone' => ['required', 'digits_between:8,16'],
            'country' => ['required', 'exists:countries,id'],
            'city_id' => ['required', Rule::exists('cities', 'id')->where(function ($query) {
                return $query->where('country_id', $this->input('country'));
            })],
            'role_id' => ['required', 'exists:roles,id'],
        ];

        if ($this->user_type == 'Farmacia') {
            $validations['pharmacy_id'] = ['required', 'exists:pharmacies,id'];
            $validations['image_user'] = ['required', 'file:png,jpg,jpeg'];
        } else {
            $validations['pharmacy_id'] = 'exclude';
            $validations['image_user'] = 'exclude';
        }

        return $validations;
    }

    public function attributes()
    {
        return [
            'document_type_id' => 'Tip de documento',
            'document_number' => 'Número de documento',
            'name' => 'Nombre',
            'email' => 'Correo electrónico',
            'address' => 'Dirección',
            'user_type' => 'Tipo',
            'phone' => 'Teléfono',
            'country' => 'País',
            'city_id' => 'Ciudad',
            'role_id' => 'Rol',
            'pharmacy_id' => 'Farmacia',
            'image_user' => 'Logotipo',
        ];
    }
}
