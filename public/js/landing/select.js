function showContent_cuidador1() {
    element1 = document.getElementById("button3");
    element2 = document.getElementById("button4");
    form = document.getElementById("cuidadoroculto");

    if (element1.checked) {
        $('#cuidadoroculto').show('slow');
        $('#cuidadoroculto2').show('slow');
        element2.checked = false;
    } else {
        form.style.display = "none";
        $('#relationship_patient').val('');
        $('#carer_document_type_id').val('');
        $('#carer_document_number').val('');
        $('#carer_name').val('');
        $('#carer_birth_date').val('');
        $('#carer_age').val('');
        $('#carer_phone').val('');
        $('#carer_email').val('');

        $('#cuidadoroculto').hide('slow');
        $('#cuidadoroculto2').hide('slow');
    }
}

function showContent_cuidador2() {
    element1 = document.getElementById("button3");
    element2 = document.getElementById("button4");
    form = document.getElementById("cuidadoroculto");

    if (element2.checked) {
        $('#cuidadoroculto').hide('slow');
        $('#cuidadoroculto2').hide('slow');
        $('#relationship_patient').val('');
        $('#carer_document_type_id').val('');
        $('#carer_document_number').val('');
        $('#carer_name').val('');
        $('#carer_birth_date').val('');
        $('#carer_age').val('');
        $('#carer_phone').val('');
        $('#carer_email').val('');
        element1.checked = false;
    } else {
        form.style.display = "none";
        $('#cuidadoroculto').hide('slow');
        $('#cuidadoroculto2').hide('slow');
    }
}

$(document).ready(function () {
    $(".opcion1").click(function () {
        $(".cuidadoroculto").show("slow");
    });

    $(".opcion2").click(function () {
        $(".cuidadoroculto").hide("slow");
    });

    $("#sig-submitBtn").click(function () {
        $("#image_print").show("slow");
        $("#col_canvas").hide("slow");
        $("#img_print").show("slow");
        $(".form_initial").show("slow");
        $("#after_sign").hide("slow");
        $(".col_back_firma").show("slow");
        $(".card-title").hide();
        $("#item_three").show("slow");
        $("body").css("position", "relative");
        $("#dialog_firma").show();
        $('#before_sign').hide();
        $('.back_firma').show();
    });

    $("#sig-clearBtn").click(function () {
        $("#image_print").hide("slow");
        $("#col_canvas").show("slow");
        $("#img_print").hide();
        $("body").css("position", "fixed");
        $("#dialog_firma").hide();
        $('#before_sign').show();
        $("body").css("position", "relative");
    });

    $(".back_firma").click(function () {
        $("#image_print").show("slow");
        $("#after_sign").show("slow");
        $("#exampleModalCenter1").modal("show");
        $("#col_canvas").show("slow");
        $("body").css({
            position: "fixed",
            width: "100%",
        });
        $("#dialog_firma").hide();
    });

    $("#before_sign").click(function () {
        $("#after_sign").show("slow");

        $("body").css({
            position: "fixed",
            width: "100%",
        });
        $("#dialog_firma").hide();
    });
});

$(document).ready(function () {
    (function () {
        window.requestAnimFrame = (function (callback) {
            return (
                window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimaitonFrame ||
                function (callback) {
                    window.setTimeout(callback, 1000 / 60);
                }
            );
        })();

        var canvas = document.getElementById("sig-canvas");
        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#222222";
        ctx.lineWidth = 4;

        var drawing = false;
        var mousePos = {
            x: 0,
            y: 0,
        };
        var lastPos = mousePos;

        canvas.addEventListener(
            "mousedown",
            function (e) {
                drawing = true;
                lastPos = getMousePos(canvas, e);
            },
            false
        );

        canvas.addEventListener(
            "mouseup",
            function (e) {
                drawing = false;
            },
            false
        );

        canvas.addEventListener(
            "mousemove",
            function (e) {
                mousePos = getMousePos(canvas, e);
            },
            false
        );

        // Add touch event support for mobile
        canvas.addEventListener("touchstart", function (e) { }, false);

        canvas.addEventListener(
            "touchmove",
            function (e) {
                var touch = e.touches[0];
                var me = new MouseEvent("mousemove", {
                    clientX: touch.clientX,
                    clientY: touch.clientY,
                });
                canvas.dispatchEvent(me);
            },
            false
        );

        canvas.addEventListener(
            "touchstart",
            function (e) {
                mousePos = getTouchPos(canvas, e);
                var touch = e.touches[0];
                var me = new MouseEvent("mousedown", {
                    clientX: touch.clientX,
                    clientY: touch.clientY,
                });
                canvas.dispatchEvent(me);
            },
            false
        );

        canvas.addEventListener(
            "touchend",
            function (e) {
                var me = new MouseEvent("mouseup", {});
                canvas.dispatchEvent(me);
            },
            false
        );

        function getMousePos(canvasDom, mouseEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: mouseEvent.clientX - rect.left,
                y: mouseEvent.clientY - rect.top,
            };
        }

        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top,
            };
        }

        function renderCanvas() {
            if (drawing) {
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                lastPos = mousePos;
            }
        }

        // Prevent scrolling when touching the canvas
        document.body.addEventListener(
            "touchstart",
            function (e) {
                if (e.target == canvas) {
                    e.preventDefault();
                }
            },
            false
        );
        document.body.addEventListener(
            "touchend",
            function (e) {
                if (e.target == canvas) {
                    e.preventDefault();
                }
            },
            false
        );
        document.body.addEventListener(
            "touchmove",
            function (e) {
                if (e.target == canvas) {
                    e.preventDefault();
                }
            },
            false
        );

        (function drawLoop() {
            requestAnimFrame(drawLoop);
            renderCanvas();
        })();

        function clearCanvas() {
            canvas.width = canvas.width;
        }

        // Set up the UI
        var sigText = document.getElementById("sig-dataUrl");
        var sigImage = document.getElementById("sig-image");
        var clearBtn = document.getElementById("sig-clearBtn");
        var submitBtn = document.getElementById("sig-submitBtn");
        clearBtn.addEventListener(
            "click",
            function (e) {
                clearCanvas();
                sigText.innerHTML = "Data URL for your signature will go here!";
                sigImage.setAttribute("src", "");
            },
            false
        );
        submitBtn.addEventListener(
            "click",
            function (e) {
                var dataUrl = canvas.toDataURL();
                sigText.innerHTML = dataUrl;
                sigImage.setAttribute("src", dataUrl);

                let firm = document.getElementById("firm");
                firm.value = dataUrl;
            },
            false
        );
    })();
});

function showContent_diagn() {
    check1 = document.getElementById("asegurado1");
    check2 = document.getElementById("asegurado2");
    check3 = document.getElementById("asegurado3");
    element1 = document.getElementById("document");

    if (check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    } else {
        element1.style.display = "none";
    }

    if (check1.checked && check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    }
}

function showContent_copago() {
    check1 = document.getElementById("asegurado1");
    check2 = document.getElementById("asegurado2");
    check3 = document.getElementById("asegurado3");
    element1 = document.getElementById("document");

    if (check1.checked) {
        element1.style.display = "none";
        check1.checked = true;
        check2.checked = false;
    } else {
        check1.checked = false;
        check2.checked = false;
    }

    if (check1.checked && check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    }
}

function showContent_descuento() {
    check1 = document.getElementById("asegurado1");
    check2 = document.getElementById("asegurado2");
    check3 = document.getElementById("asegurado3");
    element1 = document.getElementById("document");

    if (check2.checked) {
        element1.style.display = "none";
        check1.checked = false;
        check2.checked = true;
        check3.checked = false;
    } else {
        element1.style.display = "none";
        check1.checked = false;
        check2.checked = false;
        check3.checked = false;
    }

    if (check1.checked && check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    }
}

function showContent_Nodiagn() {
    check1 = document.getElementById("Noasegurado1");
    check2 = document.getElementById("Noasegurado2");
    check3 = document.getElementById("Noasegurado3");
    element1 = document.getElementById("document");

    if (check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    } else {
        element1.style.display = "none";
    }

    if (check1.checked && check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    }
}

function showContent_oop() {
    check1 = document.getElementById("Noasegurado1");
    check2 = document.getElementById("Noasegurado2");
    check3 = document.getElementById("Noasegurado3");
    element1 = document.getElementById("document");

    if (check1.checked) {
        element1.style.display = "none";
        check1.checked = true;
        check2.checked = false;
    } else {
        check1.checked = false;
        check2.checked = false;
    }

    if (check1.checked && check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    }
}

function showContent_Nodescuento() {
    check1 = document.getElementById("Noasegurado1");
    check2 = document.getElementById("Noasegurado2");
    check3 = document.getElementById("Noasegurado3");
    element1 = document.getElementById("document");

    if (check2.checked) {
        element1.style.display = "none";
        check1.checked = false;
        check2.checked = true;
        check3.checked = false;
    } else {
        element1.style.display = "none";
        check1.checked = false;
        check2.checked = false;
        check3.checked = false;
    }

    if (check1.checked && check3.checked) {
        element1.style.display = "block";
        check2.checked = false;
    }
}

$('document').ready(function () {
    $("#desmarcar").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
});

$(function () {
    $('#medicine').on('change', onSelectmedicineChange);
    $('#birth_date').on('change', calcularEdad);
    $('#carer_birth_date').on('change', calcularEdadCuidador);
});

//cargar ciudades
async function onSelectCountryChange(countryElement, cityElementId) {
    const country_id = countryElement.value;
    await $.get(`/api/country/${country_id}`, function (data) {
        var option = '<option value="" disabled selected>Seleccione una ciudad...</option>';
        for (var i = 0; i < data.length; ++i)
            option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        $(cityElementId).html(option);
    });
}

$(document).ready(function () {
    calcularEdad();
    calcularEdadCuidador();
});

//cargar patologias y dosis
function onSelectmedicineChange() {
    const medicine = $(this).val();
    const dose = medicine;
    $.get(`/api/medicine/${medicine}/pathologies`, function (data) {
        pathologies = data;
        let option = '<option selected disabled>Seleccione una patologia...</option>';
        pathologies.forEach(pathology => {
            option += `<option value="${pathology.id}">${pathology.name}</option>`;
        });

        $('#pathology').html(option);
        $('#dose').html('<option selected disabled>Seleccione una dosis...</option>');
    });
}

$('#pathology').on('change', onSelectpathologyChange);
function onSelectpathologyChange() {
    const pathology = document.getElementById("pathology").value;
    const medicine = document.getElementById("medicine").value;
    $.get(`/api/pathology/${pathology}/medicine/${medicine}/doses`, function (medicineDoses) {
        let option = '<option selected disabled>Seleccione una dosis...</option>';
        medicineDoses.forEach(medicineDose => {
            option += `<option value="${medicineDose.pivot.id}">${medicineDose.dose.presentation}</option>`;
        });
        $('#dose').html(option);
    });
}

function calcularEdad() {
    fecha = $(this).val();
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    $('#age').val(edad);
}

function calcularEdadCuidador() {
    fechac = $(this).val();
    var hoyc = new Date();
    var cumpleanosc = new Date(fechac);
    var edadc = hoyc.getFullYear() - cumpleanosc.getFullYear();
    var mc = hoyc.getMonth() - cumpleanosc.getMonth();

    if (mc < 0 || (mc === 0 && hoyc.getDate() < cumpleanosc.getDate())) {
        edadc--;
    }

    $('#carer_age').val(edadc);
}
