    <table class="w-100 table text-center table-bordered table-striped" id="example">
        <thead class="thead text-white font-weight-bold bg-orange">
            <tr>
                <th scope="col">Fecha de gestión</th>
                <th scope="col">Observaciones generales</th>
                <th scope="col">Paceinte</th>
                <th scope="col">Reclamo medicamento</th>
                <th scope="col">Fecha ultima recolección</th>
                <th scope="col">Punto de entrega</th>
                <th scope="col">Ciudad de entrega</th>
                <th scope="col">Observaciones de la proxima llamada</th>
                <th scope="col">Autor</th>
            </tr>
        </thead>
        <tbody>
                @foreach ($trackings as $tracking)
                    <tr>
                        <td>{{ isset($tracking->date_initial_call) ? $tracking->date_initial_call : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->general_observations) ? $tracking->general_observations : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->patient->name) ? $tracking->patient->name : '' }}</td>
                        <td>{{ isset($tracking->claimed_medication) ? $tracking->claimed_medication : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->claim_date) ? $tracking->claim_date : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->delivery_point) ? $tracking->delivery_point : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->delivery_city) ? $tracking->delivery_city : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->next_call_observations) ? $tracking->next_call_observations : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->user->name) ? $tracking->user->name : 'Sin datos' }}</td>
                    </tr>
                @endforeach
        </tbody>
    </table>
</div>
