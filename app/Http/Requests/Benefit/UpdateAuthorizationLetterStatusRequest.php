<?php

namespace App\Http\Requests\Benefit;

use App\Enums\Benefit\AuthorizationLetterRejectionReasons;
use App\Enums\Benefit\AuthorizationLetterStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateAuthorizationLetterStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validations = [
            'autorized' => ['required', new Enum(AuthorizationLetterStatus::class)],
            'causal_types' => [
                Rule::when(
                    $this->autorized == 'Rechazado',
                    ['required', new Enum(AuthorizationLetterRejectionReasons::class)],
                    'exclude'
                ),
            ],
            'is_accepted' => [
                Rule::when(
                    $this->autorized == 'Rechazado',
                    'required|in:0,1',
                    'exclude'
                ),
            ],
        ];

        return $validations;
    }

    public function attributes()
    {
        return [
            'autorized' => 'autorización de carta co-pago',
            'causal_types' => 'motivos de rechazo',
            'is_accepted' => 'acepta descuento especial',
        ];
    }
}
