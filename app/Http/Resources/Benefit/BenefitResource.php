<?php

namespace App\Http\Resources\Benefit;

use App\Http\Resources\Patient\PatientResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Benefit
 */
class BenefitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>|\Illuminate\Contracts\Support\Arrayable<string, mixed>|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'autorized' => $this->autorized,
            'number_units' => $this->number_units,
            'transaction_date' => $this->transaction_date,
            'invoice_number' => $this->invoice_number,
            'is_accepted' => $this->is_accepted,
            'causal_types' => $this->causal_types,
            'status' => $this->status,
            'discount' => $this->discount,
            'closing_reason' => $this->closing_reason,
            'benefit_type' => $this->benefit_type,
            'benefit_type_status' => $this->benefit_type_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'closed_at' => $this->closed_at,
            'patient' => new PatientResource($this->whenLoaded('patient')),
            'patient_id' => $this->patient_id,
        ];
    }
}
