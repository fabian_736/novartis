<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperPharmacy
 */
class Pharmacy extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'pharmacy_chain_id',
        'document_type_id',
        'document_number',
        'name',
        'city_id',
        'address',
        'is_active',
    ];

    /**
     * Scope a query to only include active pharmacies.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Retrieves the pharmacy chain.
     *
     * @return BelongsTo<PharmacyChain, Pharmacy>
     */
    public function pharmacyChain(): BelongsTo
    {
        return $this->belongsTo(PharmacyChain::class);
    }

    /**
     * Retrieves the city.
     *
     * @return BelongsTo<City, Pharmacy>
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Obtiene a los Beneficios asociados a la Farmacia.
     *
     * @return HasMany<Benefit>
     */
    public function benefits(): HasMany
    {
        return $this->hasMany(Benefit::class);
    }
}
