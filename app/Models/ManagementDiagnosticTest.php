<?php

namespace App\Models;

use App\Traits\SoftDeletesParent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperManagementDiagnosticTest
 */
class ManagementDiagnosticTest extends Model implements AuditableContract, HasMedia
{
    use AuditableTrait, HasFactory, InteractsWithMedia, SoftDeletes, SoftDeletesParent;

    protected $fillable = [
        'management_date',
        'test_date',
        'invoice_number',
        'test_price',
        'status',
        'diagnostic_test_id',
        'laboratory_id',
        'patient_id',
    ];

    /**
     * Retrieves the patient.
     *
     * @return BelongsTo<Patient, ManagementDiagnosticTest>
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * Retrieves the laboratory.
     *
     * @return BelongsTo<Laboratory, ManagementDiagnosticTest>
     */
    public function laboratory(): BelongsTo
    {
        return $this->belongsTo(Laboratory::class, 'laboratory_id');
    }

    /**
     * Retrieves the diagnostic test.
     *
     * @return BelongsTo<DiagnosticTest, ManagementDiagnosticTest>
     */
    public function diagnosticTest(): BelongsTo
    {
        return $this->belongsTo(DiagnosticTest::class, 'diagnostic_test_id');
    }

    /**
     * Register the model's media collections.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('file-upload')->singleFile();
    }

    /**
     * ManagementDiagnosticTest status is coded.
     *
     * @return string
     */
    public function getStatusDecodeAttribute()
    {
        switch ($this->status) {
            case 0:
                return 'Ejecutada';
            case 1:
                return 'Por gestionar';
            case 2:
                return 'Pendiente por realizar prueba';
            case 3:
                return 'No ejecutada';
            default:
                return 'Indefinido';
        }
    }
}
