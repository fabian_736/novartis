<?php

namespace App\Http\Requests\Benefit;

use Illuminate\Foundation\Http\FormRequest;

class ManagementBenefitUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validations = [
            'closing_reason' => 'required|string|between:10,300',
        ];

        return $validations;
    }

    public function attributes()
    {
        return [
            'closing_reason' => 'Motivo de cierre',
        ];
    }
}
