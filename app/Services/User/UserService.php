<?php

namespace App\Services\User;

use App\Models\User;

class UserService
{
    /**
     * Returns a list of users.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, User>
     */
    public function list($onlyActive = true)
    {
        $users = User::query();

        if ($onlyActive) {
            $users->active();
        }

        return $users->get();
    }

    /**
     * Create a user.
     *
     * @param  array  $input
     * @return User
     */
    public function store(array $input): User
    {
        return User::create($input);
    }

    public function update(User $user, array $input): bool
    {
        return $user->update($input);
    }
}
