@extends('layouts.landing.app')
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js"></script>
    <div class="row">
        <div class="col-md-12 ml-auto mr-auto">
            <div class="card shadow-lg">
                <div class="row-reverse mt-4">
                    <div class="col d-flex justify-content-center ">
                        <img src="{{ url('img/2.png') }}" alt="" class="w-25">
                    </div>
                </div>
                <div class="row-reverse mt-4">
                    <div class="col">
                        <h2 class="text-center font-weight-bold" style="color: #0C68B0">Formulario Inscripción</h2>
                    </div>
                </div>
                <div class="row row-reverse mt-4">
                    <div class="col"></div>
                    <div class="col-md-auto">
                        <span class="h5 p-0">Para mayor información comuníquese a la línea {{ request()->route()->country->id == 1 ? '01-6409653' : '2-342-9377' }}</span>
                    </div>
                    <div class="col"></div>
                </div>
                <ul>
                    <div class="row mx-auto">
                        <div class="col">
                            @if ($errors->any())
                                <div class="card border-warning mb-3 col-12">
                                    <div class="card-body">
                                        <h5 class="card-title"><i class="fa fa-exclamation-triangle mr-2"></i> Completa
                                            los siguientes errores</h5>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </ul>

                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-md-11 mx-auto ">
                            <form action="{{ route('landing.store',request()->route()->parameters()) }}" method="post" id="f1" enctype="multipart/form-data">
                                @csrf
                                @include('landing.parts.form1')
                                @include('landing.parts.form2')
                                @include('landing.parts.form3')
                            </form>
                        </div>

                    </div>
                </div>

                <script src="{{ url('js/landing/select.js') }}"></script>
                <script src="{{ url('js/landing/form1_validate.js') }}"></script>
                <script src="{{ url('js/landing/form2_validate.js') }}"></script>
                <script src="{{ url('js/landing/form3_validate.js') }}"></script>
            </div>

        </div>
    </div>
    <div class="row mx-auto mb-3">
        <div class="col d-flex justify-content-center">
            <div class="row mx-auto">
                <div class="col ">
                    <div class="item-carrousel" id="1" style="background: #221F1F">
                    </div>
                </div>
                <div class="col">
                    <div class="item-carrousel" id="2" style="background: #C4C4C4">
                    </div>
                </div>
                <div class="col">
                    <div class="item-carrousel" id="3" style="background: #C4C4C4">
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'No se creo Su Registro ',
            })
        </script>
    @endif
    @if (session()->has('status'))
        <script>
            $(document).ready(function() {
                $("#exampleModalCenter").modal("show");
                $('a.yourlink').click(function(e) {
                    e.preventDefault();
                    window.location.href = "{{ route('landing.end',request()->route()->parameters()) }}";
                });
            });
        </script>
    @endif

    <div class="modal modall fade" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row-reverse mx-auto">
                        <div class="col d-flex justify-content-center">
                            <img src="{{ url('svg/check_landing.svg') }}" alt="">
                        </div>
                        <div class="col d-flex justify-content-center">
                            <label for="" class="h3 font-weight-bold">INSCRIPCIÓN COMPLETADA</label>
                        </div>
                        <div class="col d-flex justify-content-center">
                            <p>Recuerda que tu número de paciente es:</p>
                        </div>
                        <div class="col d-flex justify-content-center">
                            <label for="" class="h2 font-weight-bold">{{ session('status') }}</label>
                        </div>
                        <div class="col d-flex justify-content-center" style="padding-inline: 15%">
                            <p class="text-center ">A través de correo electrónico o mensaje de texto recibirá la información relacionada al programa.</p>
                        </div>
                        <div class="col d-flex justify-content-center">
                            <a data-bs-dismiss="modal" href="{{ route('landing.end',request()->route()->parameters()) }}"
                                class="yourlink btn btn-dark">FINALIZAR</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalCenter1" data-backdrop="static" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col col_form_input">
                            <div class="row" style="display: none" id="after_sign">
                                <div class="col col_form_input">
                                    <div class="row mx-auto my-3">
                                        <div class="col">
                                            <label for="" class="text-dark font-weight-bold">Escribir Firma:
                                                (Obligatorio)</label>
                                        </div>
                                        <div class="col col_form_input" id="col_canvas">
                                            <canvas id="sig-canvas" width="620" height="160">
                                                Get a better browser, bro.
                                            </canvas>

                                        </div>
                                        <div class="col col_form_input d-none" id="url_sign">
                                            <textarea id="sig-dataUrl" class="form-control" rows="5"></textarea>
                                        </div>

                                    </div>

                                    <div class="row mx-auto">
                                        <div class="col-md-12 col_form_input">
                                            <button class="btn btn-primary" id="sig-submitBtn" data-dismiss="modal"
                                                aria-label="Close">Confirmar firma</button>
                                            <button class="btn btn-default" id="sig-clearBtn">Borrar firma</button>
                                        </div>
                                    </div>

                                    <style>
                                        #sig-canvas {
                                            border: 2px dotted #CCCCCC;
                                            border-radius: 15px;
                                            cursor: crosshair;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
