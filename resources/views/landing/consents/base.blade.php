@extends('layouts.landing.app')
@section('content')
    <div class="row pt-5">
        <div class="col">
            <div class="card card-body ">

                <div class="row-reverse">
                    <div class="col d-flex justify-content-center my-5">
                        <img src="{{ url('img/logo_novartis.png') }}" alt="" class="w-25">
                    </div>
                    <div class="col d-flex justify-content-center pt-4">
                        <label for="" class="lead font-weight-bold text-dark">CONSENTIMIENTO INFORMADO</label>
                    </div>
                    <div class="row mx-auto">
                        @yield('consent')
                    </div>
                    <div class="col d-flex justify-content-center">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter1" class="btn btn-lg" style="background-color: #221F1F">ACEPTAR Y CONTINUAR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="exampleModalCenter1" ata-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" aria-labelledby=" exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row-reverse p-3">
                        <div class="col d-flex justify-content-center align-items-center mb-3">
                            <img src="{{ url('svg/11.svg') }}" alt="">
                        </div>
                        <div class="col d-flex justify-content-center ">
                            <label for="" class="h3 text-center text-dark" style="font-weight: bold">Confirma que ha <br>
                                leído y aceptado el consentimiento informado</label>
                        </div>
                        <div class="col my-4">
                            <p class="h5 text-center">A continuación complete el formulario de <br>
                                su inscripción al plan de beneficios de Novartis.</p>
                        </div>
                        <div class="col d-flex justify-content-center">
                            <a href="{{ route('landing.show_registration_form',request()->route()->parameters()) }}" class="btn text-white w-25" style="background: #221F1F">ACEPTO</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
