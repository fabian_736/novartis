<?php

namespace App\Http\Requests\Benefit;

use Illuminate\Foundation\Http\FormRequest;

class UploadInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        if ($user->user_type == 'Admin') {
            $validations['pharmacy_id'] = ['required', 'exists:pharmacies,id'];
            $validations['transaction_date'] = ['date', 'after_or_equal:2022-01-01', 'before_or_equal:today', 'sometimes'];
            $validations['number_units'] = ['numeric', 'min:1', 'sometimes'];
            $validations['discount'] = ['string', 'sometimes'];
            $validations['invoice_number'] = ['alpha_num', 'sometimes'];
            $validations['image_invoice'] = ['file', 'mimes:png,jpg,pdf,jpeg', 'max:560000', 'sometimes'];
        } else {
            $validations['pharmacy_id'] = 'exclude';
            $validations['transaction_date'] = ['required', 'date', 'after_or_equal:2022-01-01', 'before_or_equal:today'];
            $validations['number_units'] = ['required', 'numeric', 'min:1'];
            $validations['discount'] = ['required', 'string'];
            $validations['invoice_number'] = ['required', 'alpha_num'];
            $validations['image_invoice'] = ['file', 'mimes:png,jpg,pdf,jpeg', 'max:560000'];
        }

        return $validations;
    }

    public function attributes()
    {
        return [
            'number_units' => 'Cantidad del producto',
            'pharmacy_id' => 'Farmacia',
            'discount' => 'Descuento',
            'transaction_date' => 'Fecha de la transacción',
            'invoice_number' => 'Orden de remisión',
            'image_invoice' => 'Imagen de la factura',
        ];
    }
}
