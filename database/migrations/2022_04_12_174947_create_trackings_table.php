<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->id();
            $table->boolean('autorization')->nullable()->comment('1. passed 0. refused');
            $table->enum('causal_type', ['Preexistence', 'Latency', 'Policy does not include biological'])->nullable();
            $table->boolean('communication')->comment('1. yes 0. no');
            $table->integer('number_attemps');
            $table->boolean('delivery')->comment('1. yes 0. no');
            $table->string('delivery_address');
            $table->string('delivery_city');
            $table->boolean('claim_medicine')->comment('1. yes 0. no');
            $table->dateTime('transaction_date')->nullable();
            $table->string('sale_number')->nullable();
            $table->text('observation');

            $table->foreignId('patient_id')->constrained()->restrictOnUpdate()->restrictOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackings');
    }
};
