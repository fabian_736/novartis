<?php

namespace App\Services\Tracking;

use App\Models\Tracking;

class TrackingService
{
    /**
     * Returns a list of trackings.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, Tracking>
     */
    public static function list()
    {
        return Tracking::get();
    }

    /**
     * Create a Tracking.
     *
     * @param  array  $input
     * @return Tracking
     */
    public static function store(array $input): Tracking
    {
        return Tracking::create($input);
    }

    public static function update(Tracking $tracking, array $input): bool
    {
        return $tracking->update($input);
    }
}
