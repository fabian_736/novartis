<?php

namespace App\Http\Requests\PharmacyChain;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'between:3,100', 'regex:/^[a-záéíóúüñ ]+/i', Rule::unique('pharmacy_chains', 'name')->ignore($this->pharmacy_chain)],
            'country_id' => 'required',
            'is_active' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Empresa',
            'country_id' => 'País',
            'is_active' => 'Estado',
        ];
    }
}
