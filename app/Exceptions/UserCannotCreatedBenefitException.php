<?php

namespace App\Exceptions;

use App\Traits\ApiResponses;
use Exception;

class UserCannotCreatedBenefitException extends Exception
{
    use ApiResponses;

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return $this->jsonErrorResponse(message: $this->getMessage(), code: $this->getCode());
    }
}
