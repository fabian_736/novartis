<?php

namespace App\Services\MedicineDose;

use App\Models\MedicineDose;

class MedicineDoseService
{
    /**
     * Returns a list of medicine dose.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, MedicineDose>
     */
    public static function list()
    {
        return MedicineDose::get();
    }

    /**
     * Create a MedicineDose pivot.
     *
     * @param  array  $input
     * @return MedicineDose
     */
    public static function store(array $input): MedicineDose
    {
        return MedicineDose::create($input);
    }

    public static function update(MedicineDose $medicineDose, array $input): bool
    {
        return $medicineDose->update($input);
    }
}
