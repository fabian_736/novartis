@extends('layouts.app')
@section('content')
    <script src="{{ asset('js/tracking_management/tracking_management.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/tracking_management/tracking_management.css') }}">
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h2">Lista de Seguimientos</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    @if (session('message'))
        <script>
            Swal.fire(
                'Paciente actualizado',
                'Los  datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif
    @if (session('messagee'))
        <script>
            Swal.fire(
                'Seguimiento actualizado',
                'Los  datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif
    <ul>
        @foreach ($errors->all() as $error)
            <script>
                Swal.fire({
                    icon: 'error',
                    title: '{{ $error }}',
                })
            </script>
        @endforeach
    </ul>
    <div class="row-reverse mt-5 " style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-orange">
                <label for="" class="h3 m-0 text-white">Seguimiento</label>
            </div>
        </div>
        <div class="col d-flex justify-content-end mt-3">

            <a href="{{ url('/portal') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="w-100 table text-center table-bordered table-striped" id="example">
            <thead class="thead text-white font-weight-bold bg-orange">
                <tr>
                    <th scope="col">Fecha de gestión</th>
                    <th scope="col">Observaciones generales</th>
                    <th scope="col">Paceinte</th>
                    <th scope="col">Reclamo medicamento</th>
                    <th scope="col">Fecha ultima recolección</th>
                    <th scope="col">Punto de entrega</th>
                    <th scope="col">Ciudad de entrega</th>
                    <th scope="col">Observaciones de la proxima llamada</th>
                    <th scope="col">Autor</th>
                    <th scope="col">Editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tracking as $tracking)
                    <tr>
                        <td>{{ isset($tracking->date_initial_call) ? $tracking->date_initial_call : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->general_observations) ? $tracking->general_observations : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->patient->name) ? $tracking->patient->name : '' }}</td>
                        <td>{{ isset($tracking->claimed_medication) ? $tracking->claimed_medication : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->claim_date) ? $tracking->claim_date : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->delivery_point) ? $tracking->delivery_point : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->delivery_city) ? $tracking->delivery_city : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->next_call_observations) ? $tracking->next_call_observations : 'Sin datos' }}</td>
                        <td>{{ isset($tracking->user->name) ? $tracking->user->name : 'Sin datos' }}</td>
                        <td class="{{ request()->is('trackings') ? '' : 'd-none' }} ">
                            <a class="btn btn-sm bg-orange" href="{{ route('tracking.management_edit', isset($tracking) ? $tracking : '') }}">
                                <span class="iconify" data-icon="akar-icons:edit"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>

    </div>
@endsection
