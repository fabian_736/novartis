<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consentimiento de información Personal</title>
    <style>
        .container {
            padding-left: 80px;
            padding-right: 35px;
            text-align: justify;
        }

        p {
            font-size: 14PX;
        }

        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }

        body {
            margin: 3cm 2cm 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            color: white;
            text-align: center;
            line-height: 30px;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        .texto {
            font-size: 14px;
        }
    </style>
</head>

<body>
    <header>
        <img src="{{ public_path('img/Header Consentimiento.png') }}" alt="">
    </header>
    <main>
        <div class="container">
            <h3><strong>Aviso de privacidad para pacientes</strong></h3>
            <p>
                Novartis Ecuador S.A., con domicilio en Av. Amazonas N37-29, Quito, Ecuador, correo electrónico datospersonales.ecuador@novartis.com y teléfono 0223990100 (en adelante “Novartis”) ha generado un programa de apoyo que otorga los siguientes beneficios a los pacientes que hayan sido prescritos por su médico tratante con los medicamentos Cosentyx® o Xolair®.
            </p>
            <p>A través de este programa, el paciente podrá recibir los siguientes beneficios:</p>
            <ol type="1">
                <li>Descuento para pacientes que realicen la compra del tratamiento a través de puntos de venta autorizados y no cuenten con seguro privado</li>
                <li>Cobertura del co-pago del tratamiento según póliza de aseguradora</li>
            </ol>
            <p>Para lo anterior, si usted continúa con su registro en esta página, entenderemos que otorga su consentimiento para el tratamiento de datos personales registrados con base en el Aviso de Privacidad; así como la aceptación de términos y condiciones del programa</p>
            <ul class="texto">
                <li>Nombre completo del paciente: {{ $patient->name }}</li>
                <li>Fecha de nacimiento: {{ $patient->birth_date }}</li>
                <li>Correo electrónico: {{ $patient->email }}</li>
                <li>Teléfono: {{ $patient->phone }}</li>
                @if ($patient->treatment()->exists())
                    @php
                        $pathologyDose = $patient->treatment->currentFormulation->pathologyHasMedicineDose;
                        $pathology = $pathologyDose->pathology->name;
                        $medicine = $pathologyDose->medicineDose->medicine->name;
                        $dose = $pathologyDose->medicineDose->dose->presentation;
                    @endphp
                    <li>Producto prescrito: {{ $medicine }}</li>
                    <li>Patología y dosis: {{ $pathology }}-{{ $dose }}</li>
                @endif
            </ul>
            <p>Con base en lo anterior, los datos personales que Novartis recabe podrán ser tratados únicamente para las siguientes finalidades:</p>
            <p>
                Finalidades primarias, estas son aquellas necesarias para poder concretar la relación con usted y que cumplen el objetivo principal de la recolección, en caso de no estar de acuerdo con estas no podrán realizarse las acciones descritas: (i) Para que usted pueda participar en programas de pacientes y/o de investigación clínica; (ii) Para que usted pueda participar y/o colaborar en diversos eventos o foros relacionados con la salud, ya sea presenciales o digitales (remotos); (iii) Para integrar bases de datos acorde con las otras finalidades aquí descritas; (iv) Para cumplir la legislación aplicable; (v) Para mantener la relación jurídica que pueda generarse; (vi) En algunos casos, Novartis podrán realizar las acciones que considere necesarias, a efecto de comprobar, directamente o a través de terceros, la veracidad de los datos proporcionados y; (vii) Permitirle el acceso a cualquiera de nuestras instalaciones.
            </p>
            <p>
                Finalidades secundarias o complementarias, no necesarias para cumplir con el objetivo principal de la recolección: adicionalmente, se podrán utilizar sus datos personales: (i) Para invitarlo a participar en diferentes programas, eventos, foros y conferencias relacionados con la salud ya sea en forma presencial o remota; (ii) Para ofrecerle y/o enviarle información relacionada con la salud y, en algunos casos, muestras médicas; (iii) Para, previa codificación y/o anonimización de los datos, agregarlos a otros y realizar análisis estadísticos, de generación de modelos de información y/o perfiles de comportamiento actual y predictivo y/o investigaciones clínicas o científicas; (iv) Participar en encuestas; (v) Para cumplir con nuestros procesos internos y; (vi) Para dar seguimiento a los eventos adversos que hayan tenido que ser reportados conforme a la legislación aplicable. Adicionalmente, y únicamente para las finalidades descritas más arriba, Novartis podrá
                compartir sus datos con otras filiales de Novartis o su tercero designado.
            </p>
            <p>Los datos personales se almacenarán en un banco de datos personales titularidad de Novartis o su tercero designado.</p>
            <p>
                Se deja constancia de que la entrega de los datos personales es facultativa.<em>[Sin embargo, en caso de que el paciente no desee brindar sus datos, no será posible brindarle los beneficios del Programa]</em>.
            </p>
            <p>
                Asimismo, se le informa que los datos personales que se recabarán podrían ser remitidos por Novartis a servidores, filiales y/o proveedores que se encuentren fuera de Ecuador, pero siempre con el único propósito de cumplir con las finalidades descritas más arriba y respetando en todo momento la seguridad y confidencialidad de los datos.
            </p>
            <p>
                Con base en lo anterior, Novartis da a conocer a usted el uso de la información para el tratamiento de sus datos personales y personales sensibles con base en las finalidades aquí descritas y cumple con la legislación aplicable sobre datos personales y personales sensibles.
            </p>
            <p>
                Al dar click en el botón “Aceptar y continuar”, yo {{ $patient->name }} otorgo mi consentimiento y autorizo a Novartis para el procesamiento, dentro y/o fuera del país, de mis datos personales y personales sensibles que recabe con base el presente documento.
            </p>
            <p>Manifiesto que mi consentimiento es voluntario y que no es requerimiento para poder adquirir mi medicamento.</p>
            <p>
                Finalmente reconozco que: (i) El presente consentimiento tendrá la misma vigencia que el Programa o la de mi participación en este, (ii) Tengo el derecho de solicitar acceso, la corrección, o incluso, revocar mi consentimiento en cualquier momento respecto a mis datos personales y (iii) Cualquier solicitud respecto a mi información personal, debe realizarse a través del siguiente correo: datospersonales.ecuador@novartis.com.
            </p>
            <p>
                Novartis garantiza que la información personal estará debidamente protegida a través de la implementación de medidas administrativas, técnicas y físicas tendientes a prevenir la pérdida, el uso indebido, el acceso no autorizado, la divulgación o la alteración de sus datos personales. La información podrá ser bloqueada y almacenada por un plazo de 10 años, contados a partir del cumplimiento del objetivo para el que se recabó la misma.
            </p>
            <p>Para más información sobre nuestro aviso de privacidad puede ingresar al siguiente enlace Política de privacidad | CANDEAN.Novartis.com</p>
            <p>
                <strong>*NOTA</strong>: Si quien firma no es directamente el paciente y es un cuidador, familiar o representante legal por favor completar la información relacionada al cuidador y colocar sus datos:
            </p>
            <p>Nombre y apellido del cuidador, familiar o represente legal (si aplica):</p>
            {{ $patient->name }}
            <br><br>
            <img src="{{ $signature }}" alt="Firma">

        </div>
    </main>
    <footer>
        <img src="{{ public_path('img/Footer Consentimiento.png') }}" alt="">
    </footer>
</body>

</html>
