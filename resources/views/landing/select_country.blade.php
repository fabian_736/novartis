<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <title>Novartis</title>
</head>

<body>
    <main class="main-content mt-0 ps">
        <div class="page-header align-items-start min-vh-100" style="background-image: url('https://upload.wikimedia.org/wikipedia/commons/2/24/Industria_Novartis.jpg');">
            <span class="mask bg-gradient-dark opacity-6"></span>
            <div class="container my-auto" style="z-index: 100000">
                <div class="row">
                    <a href="{{ route('landing.show_consent_view', ['country' => 1]) }}" target="_self" class="col text-decoration-none">
                        <div class="row-reverse animate__animated animate__backInLeft">
                            <div class="col d-flex justify-content-center zoom">
                                <img src="https://cdn.countryflags.com/thumbs/peru/flag-round-250.png" alt="">
                            </div>
                            <div class="col d-flex justify-content-center py-4">
                                <h3 class="text-white font-weight-bold">Perú</h3>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('landing.show_consent_view', ['country' => 2]) }}" target="_self" class="col text-decoration-none">
                        <div class="row-reverse animate__animated animate__backInLeft">
                            <div class="col d-flex justify-content-center zoom">
                                <img src="https://cdn.countryflags.com/thumbs/ecuador/flag-round-250.png" alt="">
                            </div>
                            <div class="col d-flex justify-content-center py-4">
                                <h3 class="text-white font-weight-bold">Ecuador</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>


<style>
    .page-header {
        padding: 0;
        position: relative;
        overflow: hidden;
        display: flex;
        align-items: center;
        background-size: cover;
        background-position: 50%;
    }

    .bg-gradient-dark {
        background-image: linear-gradient(195deg, #42424a, #191919);
    }

    .mask {
        position: absolute;
        background-position: 50%;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: .8;
    }

    .zoom {
        transition: transform .2s;
    }

    .zoom:hover {
        transform: scale(1.1);
    }
</style>
