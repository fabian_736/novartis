<?php

namespace App\Services\InsuranceCarrier;

use App\Models\InsuranceCarrier;

class InsuranceCarrierService
{
    /**
     * Returns a list of insurance carrier.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, InsuranceCarrier>
     */
    public function list($onlyActive = true)
    {
        $insuranceCarriers = InsuranceCarrier::query();

        if ($onlyActive) {
            $insuranceCarriers->active();
        }

        return $insuranceCarriers->get();
    }

    /**
     * Create una Insurance carrier.
     *
     * @param  array  $input
     * @return InsuranceCarrier
     */
    public function store(array $input): InsuranceCarrier
    {
        return InsuranceCarrier::create($input);
    }

    public function update(InsuranceCarrier $insuranceCarrier, array $input): bool
    {
        return $insuranceCarrier->update($input);
    }
}
