<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trackings', function (Blueprint $table) {
            $table->dropColumn([
                'autorization',
                'causal_type',
                'communication',
                'number_attemps',
                'delivery',
                'delivery_address',
                'delivery_city',
                'claim_medicine',
                'transaction_date',
                'sale_number',
                'observation',
            ]);
        });

        Schema::table('trackings', function (Blueprint $table) {
            $table->date('date_initial_call')->nullable()->comment('Fecha de llamada inicial')->after('id');
            $table->string('effective_initial_visit')->nullable()->comment('Visita inicial efectiva')->after('date_initial_call');
            $table->boolean('claimed_medication')->nullable()->comment('Reclamó el medicamento | 1. Si 0. No')->after('effective_initial_visit');
            $table->date('claim_date')->nullable()->comment('Fecha de reclamo del medicamento')->after('claimed_medication');
            $table->date('medicine_up')->nullable()->comment('Medicamento hasta')->after('claim_date');
            $table->boolean('answered_call')->nullable()->nullable()->comment('1. Si 0. No')->after('medicine_up');
            $table->string('who_answered_call')->nullable()->comment('Quién contestó la llamada')->after('answered_call');
            $table->string('call_reason')->nullable()->comment('Razón de la llamada')->after('who_answered_call');
            $table->string('cause_of_no_claim')->nullable()->comment('Causa de no entrega')->after('call_reason');
            $table->string('contact_medium')->nullable()->nullable()->comment('Medio de contacto')->after('cause_of_no_claim');
            $table->string('type_call')->nullable()->nullable()->comment('Tipo de llamada')->after('contact_medium');
            $table->integer('number_of_attemps')->nullable()->comment('Número de intentos')->after('type_call');
            $table->string('insurance_carrier')->nullable()->comment('Aseguradora')->after('number_of_attemps');
            $table->string('hospital')->nullable()->comment('Hospital')->after('insurance_carrier');
            $table->string('delivery_point')->nullable()->comment('Punto de entrega')->after('hospital');
            $table->string('delivery_city')->nullable()->comment('Ciudad de entrega')->after('delivery_point');
            $table->string('authorization_number')->nullable()->comment('Número de autorización')->after('delivery_city');
            $table->date('authorization_date')->nullable()->comment('Fecha de autorización')->after('authorization_number');
            $table->boolean('difficulty_access')->nullable()->comment('Dificultad de acceso')->after('authorization_date');
            $table->text('difficulty_type')->nullable()->comment('Tipo de dificultad')->after('difficulty_access');
            $table->boolean('generate_request')->nullable()->comment('Genera solicitud')->after('difficulty_type');
            $table->boolean('adverse_event')->nullable()->comment('Evento adverso')->after('generate_request');
            $table->string('event_type')->nullable()->comment('Tipo de evento')->after('adverse_event');
            $table->date('date_of_next_call')->nullable()->comment('Fecha de la proxima llamada')->after('event_type');
            $table->string('next_call_reason')->nullable()->comment('Razón de la próxima llamada')->after('date_of_next_call');
            $table->text('next_call_observations')->nullable()->comment('Observaciones de la próxima llamada')->after('next_call_reason');
            $table->string('consecutive')->nullable()->comment('Consecutivo')->after('next_call_observations');
            $table->integer('number_of_boxes')->nullable()->comment('Número de cajas')->after('consecutive');
            $table->string('units')->nullable()->comment('Unidades')->after('number_of_boxes');
            $table->string('previous_treatment')->nullable()->comment('Tratamiento previo')->after('units');
            $table->string('medicine')->nullable()->comment('Medicamento')->after('previous_treatment');
            $table->string('treatment_dose')->nullable()->comment('Dosis')->after('medicine');
            $table->text('general_observations')->nullable()->comment('Observaciones generales')->after('treatment_dose');

            $table->foreignId('user_id')->constrained()->restrictOnUpdate()->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trackings', function (Blueprint $table) {
            $table->dropColumn([
                'date_initial_call',
                'effective_initial_visit',
                'claimed_medication',
                'claim_date',
                'medicine_up',
                'answered_call',
                'who_answered_call',
                'call_reason',
                'cause_of_no_claim',
                'contact_medium',
                'type_call',
                'number_of_attemps',
                'insurance_carrier',
                'hospital',
                'delivery_point',
                'delivery_city',
                'authorization_number',
                'authorization_date',
                'difficulty_access',
                'difficulty_type',
                'generate_request',
                'adverse_event',
                'event_type',
                'date_of_next_call',
                'next_call_reason',
                'next_call_observations',
                'consecutive',
                'number_of_boxes',
                'units',
                'previous_treatment',
                'medicine',
                'treatment_dose',
                'general_observations',
                'user_id',
            ]);
        });

        Schema::table('trackings', function (Blueprint $table) {
            $table->boolean('autorization')->nullable()->comment('1. passed 0. refused');
            $table->enum('causal_type', ['Preexistence', 'Latency', 'Policy does not include biological'])->nullable();
            $table->boolean('communication')->comment('1. yes 0. no');
            $table->integer('number_attemps');
            $table->boolean('delivery')->comment('1. yes 0. no');
            $table->string('delivery_address');
            $table->string('delivery_city');
            $table->boolean('claim_medicine')->comment('1. yes 0. no');
            $table->dateTime('transaction_date')->nullable();
            $table->string('sale_number')->nullable();
            $table->text('observation');
        });
    }
};
