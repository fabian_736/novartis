<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits', function (Blueprint $table) {
            $table->id();
            $table->integer('number_units')->nullable()->comment('cantidad del producto');
            $table->date('transaction_date')->nullable()->comment('Fecha de entrega');
            $table->string('invoice_number')->nullable();
            $table->enum('autorized', ['Aprobado', 'Pendiente', 'Rechazado'])->default('Pendiente')->comment('Estado de la carta de autorizacion');
            $table->string('is_accepted')->nullable()->comment('Acepta el beneficio | 1. Si | 0. No');
            $table->enum('causal_types', ['Preexistencia', 'Periodo de latencia', 'Póliza no contempla biológicos'])->nullable();
            $table->integer('status')->default(1)->comment('Estado del beneficio | 0. Cerrado | 1. Carta pendiente | 2. Carta aprobada | 3. Carta rechazada | 4. Descuento especial rechazado | 5. Factura pendiente | 6. Factura cargada');
            $table->string('discount')->nullable()->comment('Descuento del beneficio');
            $table->string('closing_reason')->nullable()->comment('Motivo de cierre del beneficio');
            $table->enum('benefit_type', ['Copago', 'OOP'])->comment('Copago y OOP');
            $table->boolean('benefit_type_status')->comment('Estado del beneficio 1. Pagado 0. Abierto ')->default(0);

            $table->foreignId('patient_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('user_id')->comment('farmacia')->nullable()->constrained()->restrictOnDelete()->restrictOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefits');
    }
};
