@extends('layouts.app')
@section('content')
    <div class="sticky-top ">
        <div class="row ">
            <div class="col ">
                <label for="" class="text-warning h2">Reembolso</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse">
        <div class="col p-0 mt-5">
            <div class="card-header bg-warning">
                <label for="" class="h3 m-0 text-white">Listado de farmacias / laboratorios</label>
            </div>
        </div>
        <div class="col p-0">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-warning">
                    <tr>
                        <th scope="col">Tipo</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Tarjeta</th>
                        <th scope="col">País</th>
                        <th scope="col">Estado del beneficio</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Farmacia</td>
                        <td>Dr. +</td>
                        <td>No aparece en figma</td>
                        <td>Perú</td>
                        <td>Activo</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{url('/refund/list_pharmacy/details')}}" class="btn btn-sm bg-warning" type="button" >
                                <span class="iconify" data-icon="akar-icons:eye" data-width="30"></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
