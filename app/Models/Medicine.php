<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperMedicine
 */
class Medicine extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'name',
        'presentation',
        'is_active',
    ];

    /**
     * Scope a query to only include active medicines.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Obtiene las Patología asociadas a la Medicinas.
     *
     * @return HasMany<Pathology>
     */
    public function pathologies(): HasMany
    {
        return $this->hasMany(Pathology::class);
    }

    /**
     * Retrieves the dose.
     *
     * @return BelongsToMany<Dose>
     */
    public function doses(): BelongsToMany
    {
        return $this->belongsToMany(Dose::class, 'medicine_doses')
            ->using(MedicineDose::class)
            ->withPivot(['id'])
            ->withTimestamps();
    }
}
