<?php

namespace App\Providers;

use App\Models\Benefit;
use App\Models\Formulation;
use App\Models\ManagementDiagnosticTest;
use App\Models\Patient;
use App\Models\Tracking;
use App\Models\Treatment;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Blueprint::macro('softDeletesParent', function () {
            /** @var Blueprint $this */
            return $this->timestamp('parent_deleted_at')->nullable();
        });
        Blueprint::macro('dropSoftDeletesParent', function ($column = 'parent_deleted_at') {
            /** @var Blueprint $this */
            return $this->dropColumn($column);
        });

        Benefit::softDeletesParent(Patient::class);
        Formulation::softDeletesParent(Treatment::class);
        ManagementDiagnosticTest::softDeletesParent(Patient::class);
        Tracking::softDeletesParent(Patient::class);
        Treatment::softDeletesParent(Patient::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
