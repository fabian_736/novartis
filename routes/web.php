<?php

use App\Http\Controllers\Auth\WelcomeController;
use App\Http\Controllers\BenefitController;
use App\Http\Controllers\BenefitCorrectionController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\DiagnosticTestController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\DoseController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\InsuranceCarrierController;
use App\Http\Controllers\LaboratoryController;
use App\Http\Controllers\ManagementDiagnosticTestController;
use App\Http\Controllers\MedicineController;
use App\Http\Controllers\PathologyController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PatientEnrollController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PharmacyChainController;
use App\Http\Controllers\PharmacyController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SpecialtyController;
use App\Http\Controllers\TrackingController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Spatie\WelcomeNotification\WelcomesNewUsers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'landing');

Route::view('/refund/list_pharmacy', 'refund.list_pharmacy');
Route::view('/refund/list_pharmacy/details', 'refund.details_refund');

// RUTAS NUEVAS PARA EL FLUJO DE FARMACIA
//benefit
Route::controller(BenefitController::class)->middleware('auth')->prefix('benefits')->as('benefits.')->group(function () {
    Route::get('/invoice', 'index_invoice')->name('invoice');
    Route::get('{benefit}/letter-authorization', 'showAuthorizationLetterStatus')->name('letter_authorization.show');
    Route::post('{benefit}/letter-authorization', 'updateAuthorizationLetterStatus')->name('letter_authorization.update');
    Route::get('/upload_invoice/{benefit}', 'upload_invoice')->name('upload_invoice');
    Route::get('/authorization', 'index_authorization')->name('authorization');
    Route::patch('/patient/{benefit}', 'load_invoice')->name('patient.update');

    Route::get('benefits/{benefit}/management', 'benefit_management')->name('benefits.management');
    Route::patch('benefits/{benefit}/management', 'status_update')->name('status.benefits.management');
    Route::post('benefits-new', 'newBenefitStore')->name('new.benefits');
    Route::apiResource('benefits', BenefitController::class);
});

Route::group(['middleware' => ['web', WelcomesNewUsers::class]], function () {
    Route::get('welcome/{user}', [WelcomeController::class, 'showWelcomeForm'])->name('welcome');
    Route::post('welcome/{user}', [WelcomeController::class, 'savePassword']);
});

Route::controller(PatientController::class)->middleware('auth')->prefix('patients')->as('patients.')->group(function () {
    Route::get('/', 'list_patients')->name('list_patients');
    Route::get('/diagnostic-test', 'patient_diagnostic')->name('diagnostic_test');
    Route::get('import/template', 'downloadImportTemplate')->name('download_import_template');
    Route::post('import', 'import')->name('import');
    Route::get('/patient/{patient}/personal_information', 'personal_information')->name('personal_information');
    Route::patch('patient/{patient}/update', 'update_patient')->name('update_patient');
    Route::get('patient/{patient}/treatment', 'treatment')->name('treatment');
    Route::patch('patient/{patient}/treatment', 'update_treatment');
    Route::delete('{patient}', 'destroy')->name('delete');
    Route::patch('{patient}/restore', 'restore')->name('restore')->withTrashed();
});

//Pruebas diagnosticas gestión
Route::controller(ManagementDiagnosticTestController::class)->middleware('auth')->group(function () {
    Route::get('management-diagnostic-tests/{management_diagnostic_test}/management', 'management_diagnostic_test')->name('management-diagnostic-test');
    Route::patch('management-diagnostic-tests/{management_diagnostic_test}/management', 'updateManagement')->name('management.update');
    Route::patch('management-diagnostic-tests/{management_diagnostic_test}/test', 'updateTest')->name('test.update');
    Route::post('management-diagnostic-tests-new', 'newManagementDiagnosticTestStore')->name('new.management-diagnostic-test');
    Route::apiResource('management-diagnostic-tests', ManagementDiagnosticTestController::class);
});

// Segumiento
Route::controller(TrackingController::class)->middleware('auth')->group(function () {
    Route::get('trackings', 'show')->name('tracking.management_show');
    Route::get('trackings/edit/{tracking}', 'edit')->name('tracking.management_edit');
    Route::get('trackings/update/{tracking}', 'update')->name('tracking.management_update');
    Route::get('trackings/patients', 'patients')->name('tracking.patients');
    Route::get('trackings/patient/{patient}', 'store')->name('trackings.store');
    Route::get('trackings/{tracking}', 'managementTracking')->name('tracking.management');
});

Route::middleware('auth')->prefix('corrections')->name('corrections.')->group(function () {
    Route::controller(BenefitCorrectionController::class)->prefix('benefits')->name('benefits.')->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('{benefit}', 'edit')->name('edit');
        Route::post('{benefit}', 'update')->name('update');
        Route::delete('{benefit}', 'destroy')->name('delete');
        Route::patch('{benefit}/restore', 'restore')->name('restore')->withTrashed();
    });
});

//Reports
Route::view('reports/', 'report_list.index')->name('reports.index')->middleware('auth');

Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {
    //Management
    Route::view('management/', 'management.index')->name('management.index')->middleware('auth');

    //Roles
    Route::controller(RoleController::class)->group(function () {
        Route::get('management/roles/all', 'getRoles')->name('roles_all');
        Route::post('management/roles/{role}/assign-permission', 'syncPermissions');
        Route::apiResource('management/roles', RoleController::class);
        Route::delete('management/roles/{role}/force-delete', 'forceDelete')->name('roles.force_delete')->withTrashed();
        Route::patch('management/roles/{role}/restore', 'restore')->name('roles.restore')->withTrashed();

        //Users
        Route::get('management/users/{user}/assign-rol', 'allRoles')->name('management.assignrol');
    });

    //Permisos
    // Route::get('management/permissions/all', [PermissionController::class, 'getPermissions'])->name('permissions_all');
    Route::controller(PermissionController::class)->group(function () {
        Route::get('management/roles/{role}/assign-permission', 'allPermissions')->name('management.assignpermission');
        Route::apiResource('management/permissions', PermissionController::class);
        Route::delete('management/permissions/{permission}/force-delete', 'forceDelete')->name('permissions.force_delete')->withTrashed();
        Route::patch('management/permissions/{permission}/restore', 'restore')->name('permissions.restore')->withTrashed();
    });

    Route::controller(UserController::class)->group(function () {
        //Users
        Route::post('management/users/{user}/assign-rol', 'syncRoles');
        Route::apiResource('management/users', UserController::class);
    });

    Route::resources([
        //Farmacias
        'management/pharmacies' => PharmacyController::class,

        //Cadenas
        'management/pharmacy-chains' => PharmacyChainController::class,

        //Especialidades
        'management/specialties' => SpecialtyController::class,

        //Doctores
        'management/doctors' => DoctorController::class,

        //Países
        'management/countries' => CountryController::class,

        //Ciudades
        'management/cities' => CityController::class,

        //Aseguradoras
        'management/insurance-carriers' => InsuranceCarrierController::class,

        //Hospitales
        'management/hospitals' => HospitalController::class,

        //Patologías
        'management/pathologies' => PathologyController::class,

        //Medicinas
        'management/medicines' => MedicineController::class,

        //Dosis
        'management/doses' => DoseController::class,

        //Laboratorios
        'management/laboratories' => LaboratoryController::class,

        //Pruebas Diagnosticas
        'management/diagnostic-tests' => DiagnosticTestController::class,
    ]);
});

Route::controller(PatientEnrollController::class)->prefix('landing')->name('landing.')->group(function () {
    Route::get('', 'index')->name('index');
    Route::get('select-country', 'showSelectCountryView')->name('show_select_country_view');

    Route::prefix('{country}')->group(function () {
        Route::get('consent', 'showConsentView')->name('show_consent_view');
        Route::get('register', 'showRegistrationForm')->name('show_registration_form');
        Route::POST('register', 'store')->name('store');
        Route::get('end', 'end_landing')->name('end');
    });
});

Route::view('/portal', 'portal.index')->middleware('auth');

// RUTAS SUPERVISOR/SUPERADMIN
Route::view('/supervisor/user/create', 'supervisor.user.create');
Route::view('/supervisor/user/list', 'supervisor.user.list');

Route::post('upload_doctors', [DoctorController::class, 'ImportDoctors'])->name('ImportDoctors');
Route::post('upload_hospitals', [HospitalController::class, 'ImportHospital'])->name('ImportHospital');
Route::get('download-médicos', [DoctorController::class, 'dowload_template'])->name('template_doctor');
Route::get('download-hospitales', [HospitalController::class, 'dowload_template'])->name('template_hospitales');
