<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment('local', 'testing')) {
            $user = User::factory()
                ->set('email', 'superadmin@test.com')
                ->set('name', 'Super admin')
                ->set('is_admin', 1)
                ->set('user_type', 'Admin')
                ->create();

            $user = User::factory()
                ->set('email', 'admin@test.com')
                ->set('name', 'Admin')
                ->set('is_admin', 0)
                ->set('user_type', 'Admin')
                ->create();

            $user->assignRole(['role_id' => 1]);

            $user = User::factory()
                ->set('email', 'pharmacy@test.com')
                ->set('name', 'Pharmacy')
                ->set('is_admin', 0)
                ->set('user_type', 'Farmacia')
                ->set('pharmacy_id', 1)
                ->create()
                ->assignRole(['role_id' => 2]);

            $user = User::factory()
                ->set('email', 'novartis@test.com')
                ->set('name', 'Novartis')
                ->set('is_admin', 0)
                ->create()
                ->assignRole(['role_id' => 3]);
        }

        if (app()->environment(['production'])) {
            User::create([
                'document_number' => '1234',
                'name' => 'superadmin',
                'email' => 'superadmin@test.com',
                'password' => Hash::make('superadmin1234'),
                'phone' => '3212345623',
                'address' => 'Cll falsa 1234',
                'is_active' => 1,
                'city_id' => 1,
                'document_type_id' => 1,
                'is_admin' => 1,
            ]);

            User::create([
                'document_number' => '12345',
                'name' => 'Admin',
                'email' => 'admin@test.com',
                'password' => Hash::make('admin1234'),
                'phone' => '3212345623',
                'address' => 'Cll falsa 1234',
                'is_active' => 1,
                'city_id' => 1,
                'document_type_id' => 1,
            ])->assignRole(['role_id' => 1]);
        }
    }
}
