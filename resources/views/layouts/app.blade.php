@include('layouts.components.head')

<div class="wrapper ">

    @include('layouts.components.sidebar')

    <div class="main-panel">
        <div class="content">
            <div class="content">
                <div class="container-fluid">

                    @yield('content')

                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.components.end')
