@extends('layouts.landing.app')
@section('content')
    <div class="row-reverse" style="min-height: 100vh">
        <div class="col d-flex justify-content-center align-items-end" style="min-height: 50vh">
            <img id="formulario" src="{{ url('img/logo_novartis.png') }}" alt="" class="w-75">
        </div>
        <div class="col mt-5 d-flex justify-content-center">
            <label for="" class="h3 text-center">¡Gracias por participar en el programa de Novartis!</label>
        </div>
    </div>
@endsection
