<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\DocumentType;
use App\Models\PharmacyChain;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pharmacy>
 */
class PharmacyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'pharmacy_chain_id' => PharmacyChain::factory(),
            'document_type_id' => DocumentType::factory(),
            'document_number' => $this->faker->regexify("[1-9]\d{7,14}"),
            'name' => $this->faker->unique->company(),
            'city_id' => City::factory(),
            'address' => $this->faker->address(),
            'is_active' => true,
        ];
    }
}
