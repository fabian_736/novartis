<?php

namespace App\Services\DiagnosticTest;

use App\Models\DiagnosticTest;

class DiagnosticTestService
{
    /**
     * Returns a list of diagnostic tests.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, DiagnosticTest>
     */
    public function list($onlyActive = true)
    {
        $diagnosticTest = DiagnosticTest::query();

        if ($onlyActive) {
            $diagnosticTest->active();
        }

        return $diagnosticTest->get();
    }

    /**
     * Create a Specialty.
     *
     * @param  array  $input
     * @return DiagnosticTest
     */
    public function store(array $input): DiagnosticTest
    {
        return DiagnosticTest::create($input);
    }

    public function update(DiagnosticTest $diagnosticTest, array $input): bool
    {
        return $diagnosticTest->update($input);
    }
}
