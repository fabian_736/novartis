<?php

namespace App\Http\Resources\Patient;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Patient
 */
class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>|\Illuminate\Contracts\Support\Arrayable<string, mixed>|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'document_type_id' => $this->document_type_id,
            'document_number' => $this->document_number,
            'name' => $this->name,
            'birth_date' => $this->birth_date,
            'gender' => $this->gender,
            'city_id' => $this->city_id,
            'email' => $this->email,
            'phone' => $this->phone,
            'patient_type' => $this->patient_type?->label(),
            'insurance_carrier_id' => $this->insurance_carrier_id,
            'hospital_id' => $this->hospital_id,
            'first_time_using_biological' => $this->first_time_using_biological,
            'has_carer' => $this->has_carer,
            'relationship_patient' => $this->relationship_patient,
            'carer_document_type_id' => $this->carer_document_type_id,
            'carer_document_number' => $this->carer_document_number,
            'carer_name' => $this->carer_name,
            'carer_birth_date' => $this->carer_birth_date,
            'carer_phone' => $this->carer_phone,
            'carer_email' => $this->carer_email,
            'pap' => $this->pap,
            'patient_status' => $this->patient_status->value,
        ];
    }
}
