@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <label for="" class="text-orange h2">Consultar pacientes</label>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    @if (session('message'))
        <script>
            Swal.fire(
                'Paciente actualizado',
                'Los  datos han sido modificados correctamente ',
                'success'
            )
        </script>
    @endif

    <div class="row-reverse mt-5 " style="overflow-y: auto; max-height: 80vh">
        <div class="col p-0">
            <div class="card-header bg-orange">
                <label for="" class="h3 m-0 text-white">Seguimiento</label>
            </div>
        </div>
        <div class="col d-flex justify-content-end mt-3">

            <a href="{{ url('/portal') }}" type="button" class="btn btn-dark d-flex align-items-center" style="color: white">
                <span class="iconify mr-2" data-icon="carbon:close-outline" data-width="20"></span>Regresar
            </a>
            {{-- &nbsp;&nbsp;&nbsp;
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                <span class="iconify mr-2" data-width="15" data-icon="ri:file-excel-2-line"></span> Importar
            </button>&nbsp;&nbsp;&nbsp; --}}
        </div>

        <div class="col p-0">
            <table class="table table-bordered table-striped" id="example">
                <thead class="thead text-white font-weight-bold bg-orange">
                    <tr>
                        <th scope="col">N° Paciente</th>
                        <th scope="col">Identificación</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">País</th>
                        <th scope="col">Tipo de paciente</th>
                        <th scope="col">Tipo de beneficio</th>
                        <th scope="col">Estado</th>
                        <th class="text-center" scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($patients as $patient)
                        <tr>
                            <td>{{ $patient->pap }}</td>
                            <td>{{ $patient->document_number }}</td>
                            <td>{{ $patient->name }}</td>

                            <td>
                                @if ($patient->city->country->name == 'Perú')
                                    <img width="17px" height="17px" src="{{ asset('img/country/peru.png') }}" alt=""> Perú
                                @elseif ($patient->city->country->name == 'Ecuador')
                                    <img width="17px" height="17px" src="{{ asset('img/country/ecuador.png') }}" alt=""> Ecuador
                                @else
                                    <img class="mr-2" width="17px" height="17px" src="{{ asset('img/country/global.png') }}" alt="">{{ $patient->city->country->name }}
                                @endif
                            </td>
                            <td>{{ $benefit->patient->patient_type->label() }}</td>
                            <td>
                                @php
                                    $benefits = [];
                                    if (!empty($patient->lastBenefit?->benefit_type)) {
                                        $benefits[] = $patient->lastBenefit->benefit_type;
                                    }

                                    if ($patient->management_diagnostic_tests_exists) {
                                        $benefits[] = 'Prueba diagnóstica';
                                    }
                                @endphp
                                @if (empty($benefits))
                                    Sin beneficios
                                @else
                                    {{ implode(' y ', $benefits) }}
                                @endif
                            </td>
                            <td>{{ $patient->patient_status->value }}</td>
                            <td class="text-center">
                                <div class="dropdown">
                                    <button class="btn bg-orange dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="iconify" data-icon="fa:eye"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="{{ route('trackings.store', $patient) }}">Seguimiento</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <style>
        /* PROGRESS AVANCE INACTIVO */

        .animated-progress-inactive {
            width: 100%;
            height: 30px;
            border-radius: 5px;
            border: 1px solid #E90016;
            overflow: hidden;
        }

        .animated-progress-inactive span {
            height: 100%;
            display: block;
            width: 0;
            color: #EF5630;
            line-height: 30px;
            text-align: center;
        }

        .progress-inactive span {
            background-color: #FFEFCA;
        }

        /* PROGRESS AVANCE ACTIVO */

        .animated-progress-active {
            width: 100%;
            height: 30px;
            border-radius: 5px;
            border: 1px solid #136200;
            overflow: hidden;
        }

        .animated-progress-active span {
            height: 100%;
            display: block;
            width: 0;
            color: #136200;
            line-height: 30px;
            text-align: center;
        }

        .progress-active span {
            background-color: rgba(37, 176, 3, 0.22);
        }
    </style>
@endsection
