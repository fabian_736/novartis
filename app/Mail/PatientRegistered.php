<?php

namespace App\Mail;

use App\Models\Patient;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PatientRegistered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The patient instance.
     *
     * @var Patient
     */
    public $patient;

    /**
     * Create a new message instance.
     *
     * @param  Patient  $patient
     * @return void
     */
    public function __construct(Patient $patient)
    {
        $this->afterCommit();
        $this->patient = $patient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = match ($this->patient->city->country->name) {
            'Ecuador' => 'mail.registration.patient-registered.ecuador',
            default => 'mail.registration.patient-registered.peru'
        };

        return $this->view($view)
            ->subject('Bienvenido/a al programa de apoyo a pacientes')
            ->attach($this->patient->getFirstMediaPath('consent'), [
                'as' => 'concentimiento informado.pdf',
                'mime' => 'application/pdf',
            ]);
    }
}
