<?php

namespace App\Http\Requests\ManagementDiagnosticTest;

use Illuminate\Foundation\Http\FormRequest;

class NewManagementDiagnosticTestStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'patient_id' => ['required', 'exists:patients,id'],
            'file' => ['required', 'file', 'mimes:pdf,jpg,jpeg'],
        ];
    }

    public function attributes()
    {
        return [
            'patient_id' => 'Paciente',
            'file' => 'Orden médica',
        ];
    }
}
