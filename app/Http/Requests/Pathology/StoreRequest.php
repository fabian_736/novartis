<?php

namespace App\Http\Requests\Pathology;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'between:4,100', 'regex:/^[a-záéíóúüñ ]+/i', 'unique:pathologies,name,'],
            'medicine_id' => ['required', 'exists:medicines,id'],
            'doses' => ['required', 'array', 'min:1'],
            'doses.*' => ['required', 'exists:medicine_doses,id'],
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'medicine_id' => 'Medicina',
            'doses' => 'Presentaciones',
        ];
    }
}
