<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * @mixin IdeHelperCountry
 */
class Country extends Model implements AuditableContract
{
    use AuditableTrait, HasFactory;

    protected $fillable = [
        'name',
        'is_active',
    ];

    /**
     * Scope a query to only include active countries.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Obtiene a las Ciudades asociadas al País.
     *
     * @return HasMany<City>
     */
    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }

    /**
     * Obtiene a las Cadenas asociadas al País.
     *
     * @return HasMany<PharmacyChain>
     */
    public function pharmacyChains(): HasMany
    {
        return $this->hasMany(PharmacyChain::class);
    }

    /**
     * Obtiene a los Doctores asociadas al País.
     *
     * @return HasMany<Doctor>
     */
    public function doctors(): HasMany
    {
        return $this->hasMany(Doctor::class);
    }

    /**
     * Obtiene a los Aseguradoras asociadas al País.
     *
     * @return HasMany<InsuranceCarrier>
     */
    public function insuranceCarriers(): HasMany
    {
        return $this->hasMany(InsuranceCarrier::class);
    }

    /**
     * Obtiene a los Hosplitales asociadas al País.
     *
     * @return HasMany<Hospital>
     */
    public function hospitals(): HasMany
    {
        return $this->hasMany(Hospital::class);
    }
}
