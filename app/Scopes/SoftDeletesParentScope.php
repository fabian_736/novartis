<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class SoftDeletesParentScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder<Model>  $builder
     * @param  Model  $model
     * @return Builder<Model>
     */
    public function apply(Builder $builder, Model $model)
    {
        return $builder->whereNull('parent_deleted_at');
    }
}
