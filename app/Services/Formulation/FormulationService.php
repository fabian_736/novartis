<?php

namespace App\Services\Formulation;

use App\Models\Formulation;

class FormulationService
{
    /**
     * Create a Formulation.
     *
     * @param  array<string, mixed>  $input
     * @return Formulation
     */
    public function store(array $input): Formulation
    {
        return Formulation::create($input);
    }

    /**
     * Updates the given formulation
     *
     * @param  Formulation  $formulation
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(Formulation $formulation, array $input): bool
    {
        return $formulation->update($input);
    }
}
