@extends('layouts.app')
@section('content')
    <div class="sticky-top">
        <div class="row">
            <div class="col ">
                <span class="text-orange h2">Corrección del beneficio de {{ $benefit->patient->name }}</span>
            </div>
            <div class="col d-flex justify-content-end align-items-center">
                <img src="{{ url('img/2.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row-reverse mt-5 table" style="overflow-y: auto; max-height: 80vh">
        <div class="col">
            <form id="formulario" action="#" method="post">
                <input type="hidden" id="insured-patient" class="form-control" value="{{ $benefit->patient->patient_type->value }}" />
                <ul class="li_form">
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="patient-pap" class="h5 font-weight-bold ">Número de afiliación</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="patient-pap" class="form-control" value="{{ $benefit->patient->pap }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="patient-phone" class="h5 font-weight-bold">Número de contacto</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="patient-phone" class="form-control" value="{{ $benefit->patient->phone }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="patient-email" class="h5 font-weight-bold ">Correo electrónico</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="email" id="patient-email" class="form-control" placeholder="example@example.com" value="{{ $benefit->patient->email }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="medicine" class="h5 font-weight-bold ">Producto</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="medicine" class="form-control" value="{{ $benefit->patient->treatment->pathology->medicine->name }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="pathology" class="h5 font-weight-bold ">Patología / Diagnóstico</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="pathology" value="{{ $benefit->patient->treatment->pathology->name }}" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="inactive">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="dose" class="h5 font-weight-bold ">Dosis</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="dose" value="{{ $benefit->patient->treatment->currentFormulation->pathologyHasMedicineDose->medicineDose->dose->presentation }}" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="frequency" class="h5 font-weight-bold ">Frecuencia</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="frequency" value="{{ $benefit->patient->treatment->currentFormulation->frequency }}" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="benefit_type" class="h5 font-weight-bold ">Tipo de beneficio</label>
                                    </div>
                                    <div class="col">
                                        <select name="benefit_type" id="benefit_type" class="form-control" required autocomplete="off">
                                            <option value="Copago" @selected($benefit->benefit_type == 'Copago')>Copago</option>
                                            <option value="OOP" @selected($benefit->benefit_type == 'OOP')>OOP</option>
                                        </select>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="benefit-type-status" class="h5 font-weight-bold ">Estado del tipo beneficio</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="benefit-type-status" value="{{ $benefit->benefit_type_status ? 'Pagado' : 'Abierto' }}" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <hr>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col col-6">
                                <div class="row">
                                    <div class="col">
                                        <label for="status" class="h5 font-weight-bold ">Estado del beneficio</label>
                                    </div>
                                    <div class="col">
                                        <select name="status" id="status" class="form-control" required autocomplete="off">
                                            <option value="1" @selected($benefit->status == 1)>Carta pendiente</option>
                                            <option value="5" @selected($benefit->status == 5)>Proceso observado</option>
                                            <option value="2" @selected($benefit->status == 2)>Descuento especial rechazado</option>
                                            <option value="3" @selected($benefit->status == 3)>Factura pendiente</option>
                                            <option value="4" @selected($benefit->status == 4)>Factura cargada</option>
                                            <option value="0" @selected($benefit->status == 0)>Cerrado</option>
                                        </select>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <hr>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="autorized" class="h5 font-weight-bold ">Autorización de carta co-pago</label>
                                    </div>
                                    <div class="col">
                                        <select name="autorized" id="autorized" class="form-control" required autocomplete="off">
                                            <option value="" @selected(empty($benefit->autorized)) disabled>Seleccione una opción</option>
                                            <option value="Pendiente" @selected($benefit->autorized == 'Pendiente')>Pendiente</option>
                                            <option value="Observada" @selected($benefit->autorized == 'Observada')>Observada</option>
                                            <option value="Aprobado" @selected($benefit->autorized == 'Aprobado')>Aprobado</option>
                                            <option value="Rechazado" @selected($benefit->autorized == 'Rechazado')>Rechazado</option>
                                        </select>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="is_accepted" class="h5 font-weight-bold ">¿El paciente acepta el descuento especial?</label>
                                    </div>
                                    <div class="col">
                                        <select name="is_accepted" id="is_accepted" class="form-control" required autocomplete="off">
                                            <option value="" @selected(empty($benefit->is_accepted)) disabled>Seleccione una opción</option>
                                            <option value="0" @selected($benefit->is_accepted == 0)>No</option>
                                            <option value="1" @selected($benefit->is_accepted == 1)>Sí</option>
                                        </select>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col col-6">
                                <div class="row">
                                    <div class="col">
                                        <label for="causal_types" class="h5 font-weight-bold ">Motivo de rechazo</label>
                                    </div>
                                    <div class="col">
                                        <select name="causal_types" id="causal_types" class="form-control" required autocomplete="off">
                                            <option value="" @selected(empty($benefit->causal_types)) disabled>Seleccione una opción</option>
                                            <option value="Preexistencia" @selected($benefit->causal_types == 'Preexistencia')>Preexistencia</option>
                                            <option value="Periodo de latencia" @selected($benefit->causal_types == 'Periodo de latencia')>Periodo de latencia</option>
                                            <option value="Póliza no contempla biológicos" @selected($benefit->causal_types == 'Póliza no contempla biológicos')>Póliza no contempla biológicos</option>
                                        </select>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <hr>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="number_units" class="h5 font-weight-bold ">Cantidad del producto</label>
                                    </div>
                                    <div class="col">
                                        <input type="number" id="number_units" name="number_units" class="form-control" placeholder="Digite cantidad" value="{{ isset($benefit->number_units) ? $benefit->number_units : '' }}" autocomplete="off">
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="pharmacy_id" class="h5 font-weight-bold ">Farmacia</label>
                                    </div>
                                    <div class="col">
                                        <select name="pharmacy_id" id="pharmacy_id" class="form-control" required autocomplete="off">
                                            <option value="" @selected(empty($benefit->pharmacy_id)) disabled>Seleccione una farmacia...</option>
                                            @foreach ($pharmacies as $pharmacy)
                                                <option value="{{ $pharmacy->id }}" @selected($benefit->pharmacy_id == $pharmacy->id)>
                                                    {{ $pharmacy->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="discount" class="h5 font-weight-bold ">Descuento otorgado en valores</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" id="discount" name="discount" value="{{ isset($benefit->discount) ? $benefit->discount : '' }}" placeholder="Digite descuento" class="form-control" autocomplete="off">
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="transaction_date" class="h5 font-weight-bold ">Fecha de transacción</label>
                                    </div>
                                    <div class="col">
                                        <input type="date" id="transaction_date" class="form-control" name="transaction_date" min="2022-01-01" max="{{ now()->toDateString() }}" value="{{ $benefit->transaction_date ?? '' }}" autocomplete="off">
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="invoice_number" class="h5 font-weight-bold ">Orden de remisión</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="invoice_number" name="invoice_number" class="form-control" value="{{ isset($benefit->invoice_number) ? $benefit->invoice_number : '' }}" placeholder="Codigo" autocomplete="off">
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="factura" class="h5 font-weight-bold ">Fotografía de factura / orden de remisión*</label>
                                    </div>
                                    <div class="col-9">
                                        <div class="custom-file border border-secondary rounded ">
                                            <label class="custom-file-label" for="factura">Seleccione un archivo</label>
                                            <input type="file" id="factura" name="factura" class="custom-file-input cursor-pointer" accept=".pdf,image/*" autocomplete="off">
                                            <span class="invalid-feedback"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="active">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <label for="closing_reason" class="h5 font-weight-bold ">Motivo de cierre</label>
                                    </div>
                                    <div class="col-9">
                                        <textarea id="closing_reason" name="closing_reason" cols="50" rows="3" class="form-control border border-secondary rounded" required autofocus placeholder="Digite aquí el motivo de cierre del beneficio" autocomplete="off">{{ $benefit->closing_reason }}</textarea>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="list-style: none">
                        <div class="row mb-3">
                            <div class="col offset-6">
                                <div class="row">
                                    <div class="col d-flex justify-content-end">
                                        <a href="{{ route('benefits.benefits.index') }}" class="btn btn-orange">
                                            <span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick-fill"></span>Salir
                                        </a>
                                    </div>
                                    <div class="col d-flex justify-content-end">
                                        <button type="submit" class="btn btn-dark w-100">
                                            <span class="iconify mr-2" data-icon="entypo:save"></span>GUARDAR
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="{{ asset('js/corrections/benefits.js') }}" defer></script>
@endsection
