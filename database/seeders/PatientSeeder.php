<?php

namespace Database\Seeders;

use App\Enums\Benefit\BenefitTypes;
use App\Enums\Patient\PatientStatus;
use App\Models\Benefit;
use App\Models\Formulation;
use App\Models\ManagementDiagnosticTest;
use App\Models\Patient;
use App\Models\Treatment;
use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment('local')) {
            $patient1 = Patient::factory()->insured()->create();
            $treatment = Treatment::factory()->for($patient1, 'patient')->create();
            Formulation::factory()->for($treatment)->create();
            Benefit::factory()->for($patient1, 'patient')->set('benefit_type', BenefitTypes::Copago)->create();

            $patient2 = Patient::factory()->notInsured()->set('patient_status', PatientStatus::InAccess)->create();

            ManagementDiagnosticTest::factory()->for($patient2, 'patient')->create();
        }
    }
}
