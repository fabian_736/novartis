<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathology_has_medicine_doses', function (Blueprint $table) {
            $table->id();

            $table->foreignId('pathology_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('medicine_dose_id')->constrained()->restrictOnDelete()->restrictOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathology_has_medicine_doses');
    }
};
