<button type="button" class="btn bg-orange" data-bs-toggle="modal"
    data-bs-target="#Modal_list_invoice{{ $benefit->patient->id }}">
    <i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="Modal_list_invoice{{ $benefit->patient->id }}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col mb-3">
                        <div class="card-header bg-orange">
                            <div class="row">
                                <div class="col">
                                    <label for=""
                                        class="h4 m-0 text-white font-weight-bold">{{ $benefit->patient->name }}</label>

                                </div>
                                <div class="col d-flex justify-content-end align-items-center">
                                    <i class="fas fa-times text-white cursor-pointer " data-dismiss="modal"> </i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('benefits.letter_authorization.update', $benefit->id) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="col mb-3">
                            <div class="row p-2">
                                <div class="col-12 mb-3">
                                    <label for="" class="font-weight-bold text-orange">Autorización
                                        de carta co-pago</label>
                                    <select name="autorized" id="autorized" class="form-control">
                                        <option value="{{ $benefit->autorized }}" selected disabled>
                                            {{ $benefit->autorized }}</option>
                                        <option value="Pendiente">Pendiente</option>
                                        <option value="Aprobado">Aprobado</option>
                                        <option value="Rechazado">Rechazado</option>
                                    </select>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="" class="font-weight-bold text-orange">Motivos de
                                        rechazo </label>
                                    <select name="causal_types" id="causal_types" class="form-control text-wrap">
                                        <option value="{{ $benefit->causal_types }}" selected disabled>
                                            {{ isset($benefit->causal_types) ? $benefit->causal_types : 'Selecciona una opción...' }}
                                        </option>
                                        <option value="Cuando tuvo la enfermedad antes de adquirir el seguro.">Cuando
                                            tuvo la enfermedad antes de adquirir el seguro.</option>
                                        <option
                                            value="Cuando no ha pasado el tiempo indicado en la póliza del paciente para adquirir medicamentos de alto costo.">
                                            Cuando no ha pasado el tiempo indicado en la póliza del paciente para
                                            adquirir medicamentos de alto costo.</option>
                                        <option
                                            value="Cuando el plan contratado no incluye ningún medicamento biológico.">
                                            Cuando el plan contratado no incluye ningún medicamento biológico.</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <label for="" class="font-weight-bold text-orange">¿Acepta descuento especial?</label>
                                    <select name="is_accepted" id="is_accepted" class="form-control">
                                        <option @selected($benefit->is_accepted == '0') value="0">No</option>
                                        <option @selected($benefit->is_accepted == '1') value="1">Sí</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col mb-3 d-flex justify-content-center">
                            <button class="btn btn-orange font-weight-bold" type="submit"><i class="fas fa-save mr-2"></i>GUARDAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
