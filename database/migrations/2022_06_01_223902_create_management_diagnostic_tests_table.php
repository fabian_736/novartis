<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_diagnostic_tests', function (Blueprint $table) {
            $table->id();

            $table->date('management_date')->nullable()->comment('Fecha de coordinación');
            $table->date('test_date')->nullable()->comment('Fecha de la prueba');
            $table->string('invoice_number')->nullable()->comment('Número de la factura');
            $table->double('test_price')->nullable()->comment('Valor de la prueba');
            $table->integer('status')->default(1)->comment('Estado');

            $table->foreignId('diagnostic_test_id')->nullable()->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('laboratory_id')->nullable()->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('patient_id')->constrained()->restrictOnDelete()->restrictOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_diagnostic_tests');
    }
};
